<?php
class Model_armada_kso extends CI_Model
{
  public function getAllarmada($show=null, $start=null, $cari=null, $id_bu)
  {

    $this->db->select("a.*, b.nm_bu");
    $this->db->from("ref_armada a");
    $this->db->join("ref_bu b", "a.id_bu = b.id_bu","left");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    if($id_bu<>0){ $this->db->where('a.id_bu', $id_bu); }
    $this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
    $this->db->where("a.active IN (0, 1) ");
    $this->db->where("a.status_armada = 'KSO'");
    if ($show == null && $start == null) {} else {
      $this->db->limit($show, $start);
    }

    return $this->db->get();
  }

  public function get_count_armada($search = null, $id_bu)
  {
   $count = array();
   $session = $this->session->userdata('login');

   $this->db->select(" COUNT(id_armada) as recordsFiltered ");
   $this->db->from("ref_armada");
   $this->db->where('id_perusahaan', $session['id_perusahaan']);
   $this->db->where("id_bu in (3, 7, 8, 17)");
   if($id_bu<>0){ $this->db->where('id_bu', $id_bu); }
    $this->db->where("active IN (0, 1) ");
   $this->db->where("status_armada = 'KSO'");
   $this->db->like("kd_armada ", $search);
   $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

   $this->db->select(" COUNT(id_armada) as recordsTotal ");
   $this->db->from("ref_armada");
   $this->db->where('id_perusahaan', $session['id_perusahaan']);
   $this->db->where("id_bu in (3, 7, 8, 17)");
   if($id_bu<>0){ $this->db->where('id_bu', $id_bu);}
   $this->db->where("active IN (0, 1) ");
   $this->db->where("status_armada = 'KSO'");
   $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

   return $count;
 }

}
