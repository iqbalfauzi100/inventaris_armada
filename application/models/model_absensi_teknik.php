<?php
class Model_absensi_teknik extends CI_Model
{


	public function getAllabsensi_teknik($show = null, $start = null, $cari = null, $id_bu, $tanggal, $id_segment)
	{
		$this->db->select("a.*,b.status,b.tgl_absensi,b.keterangan,b.cnm_user,b.cdate as date_create");
		$this->db->from("ref_armada a");
		$this->db->join("tr_absensi_armada_teknik b", "a.id_bu = b.id_bu AND a.kd_armada = b.kd_armada AND b.tgl_absensi = '$tanggal'", "left");
		$session = $this->session->userdata('login');
		$this->db->where('a.id_perusahaan', $session['id_perusahaan']);
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where("a.id_bu !=''");
		$this->db->where("a.active IN (0, 1) ");
		$this->db->where("(a.kd_armada  LIKE '%" . $cari . "%' OR  b.status  LIKE '%" . $cari . "%' OR  b.tgl_absensi  LIKE '%" . $cari . "%') ");
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}

		$this->db->order_by('a.id_armada', 'ASC');
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_absensi_teknik($cari = null, $id_bu, $tanggal, $id_segment)
	{
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("tr_absensi_armada_teknik b", "a.id_bu = b.id_bu AND a.kd_armada = b.kd_armada AND b.tgl_absensi = '$tanggal'", "left");
		$this->db->where('id_perusahaan', $session['id_perusahaan']);
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where("a.active IN (0, 1) ");
		$this->db->where("(a.kd_armada  LIKE '%" . $cari . "%' OR  b.status  LIKE '%" . $cari . "%' OR  b.tgl_absensi  LIKE '%" . $cari . "%') ");
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("tr_absensi_armada_teknik b", "a.id_bu = b.id_bu AND a.kd_armada = b.kd_armada AND b.tgl_absensi = '$tanggal'", "left");
		$this->db->where('id_perusahaan', $session['id_perusahaan']);
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where("a.active IN (0, 1) ");
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}
		// $this->db->group_by('a.kd_armada');
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;
	}

	public function getAllabsensi_teknik_new($show = null, $start = null, $cari = null, $id_bu, $tanggal, $id_segment)
	{
		$this->db->select("a.*");
		$this->db->from("tr_absensi_armada_teknik a");
		$session = $this->session->userdata('login');
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where('a.tgl_absensi', $tanggal);
		$this->db->where("a.id_bu !=''");
		$this->db->where("(a.kd_armada  LIKE '%" . $cari . "%' OR  a.status  LIKE '%" . $cari . "%' OR  a.tgl_absensi  LIKE '%" . $cari . "%') ");
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}

		$this->db->order_by('a.kd_armada', 'ASC');
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_absensi_teknik_new($cari = null, $id_bu, $tanggal, $id_segment)
	{
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_absensi_armada) as recordsFiltered ");
		$this->db->from("tr_absensi_armada_teknik a");
		$this->db->where('a.id_bu', $id_bu);
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		$this->db->select(" COUNT(a.id_absensi_armada) as recordsTotal ");
		$this->db->from("tr_absensi_armada_teknik a");
		$this->db->where('a.id_bu', $id_bu);
		if ($id_segment != 0) {
			$this->db->where('a.id_segment', $id_segment);
		}
		// $this->db->group_by('a.kd_armada');
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;
	}

	public function getAllkm_armada_simateknik($id_bu)
    {
        $data = array(
            'id_bu'    => $id_bu,
        );
        $url = 'https://teknik.damri.co.id/index.php/api/api_km_service_cabang';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $responseJson = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($responseJson);
        }
	}
	
	public function getAllkm_armada_simateknik_2($cari, $id_bu)
    {
        $this->db->select("a.kd_armada, a.id_bu, ifnull(sum(b.km_tempuh),0) as km_tempuh");
		$this->db->from("ref_armada a");
		$this->db->join("tr_absensi_armada_teknik b","a.kd_armada = b.kd_armada","left");
		$session = $this->session->userdata('login');
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where("(a.kd_armada  LIKE '%" . $cari . "%') ");
		$this->db->group_by("a.kd_armada");
		$this->db->order_by("a.kd_armada");
		return $this->db->get();
	}

	// public function get_count_km_armada_simateknik_2($cari = null, $id_bu)
	// {
	// 	$count = array();
	// 	$session = $this->session->userdata('login');

	// 	$this->db->select(" COUNT(DISTINCT(a.kd_armada)) as recordsFiltered ");
	// 	$this->db->from("tr_absensi_armada_teknik a");
	// 	$this->db->where('a.id_bu', $id_bu);
	// 	$this->db->group_by('a.kd_armada');
	// 	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	// 	$this->db->select(" COUNT(DISTINCT(a.kd_armada)) as recordsTotal ");
	// 	$this->db->from("tr_absensi_armada_teknik a");
	// 	$this->db->where('a.id_bu', $id_bu);
	// 	$this->db->group_by('a.kd_armada');
	// 	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	// 	return $count;
	// }
	
	// public function getAllkm_armada_simateknik_3($cari = null, $id_bu)
    // {
	// 	$this->db->select("a.kd_armada, a.id_bu, b.status, ifnull(sum(b.km_tempuh),0) as km_tempuh, ifnull(c.total_km,0) as total, ifnull(sum(b.km_tempuh)+c.total_km,0) as km_total,
	// 	(case
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>12000) then 'MELEWATI WAKTU SERVICE'
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>11000 && ifnull(sum(b.km_tempuh)+c.total_km,0)<=12000) then 'WAKTU SERVICE'
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>=10000 && ifnull(sum(b.km_tempuh)+c.total_km,0)<=11000) then 'PERHATIKAN WAKTU SERVICE'
	// 		else 'SERVICE' end) as keterangan,
	// 	(case
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>12000) then '1'
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>11000 && ifnull(sum(b.km_tempuh)+c.total_km,0)<=12000) then '2'
	// 		when (ifnull(sum(b.km_tempuh)+c.total_km,0)>=10000 && ifnull(sum(b.km_tempuh)+c.total_km,0)<=11000) then '3'
	// 		else '4' end) as urutan");
	// 	$this->db->from("ref_armadax a");
	// 	$this->db->join("tr_absensi_armada_teknik b","a.kd_armada = b.kd_armada","left");
	// 	$this->db->join("temp_km_service_simateknik c","a.kd_armada = c.armada","left");
	// 	$session = $this->session->userdata('login');
	// 	$this->db->where('a.id_bu', $id_bu);
	// 	$this->db->where("a.kd_armada != ''");
	// 	$this->db->where("(a.kd_armada  LIKE '%" . $cari . "%') ");
	// 	$this->db->group_by("a.kd_armada");
	// 	$this->db->order_by("a.kd_armada");
	// 	return $this->db->get();
	// }

	public function getAllkm_armada_simateknik_3($cari = null, $id_bu)
    {
		$this->db->select("n.kd_armada, n.id_bu, n.status, n.km_tempuh, n.total, n.km_total, n.keterangan, n.urutan");
		$this->db->from("(select a.kd_armada, a.id_bu, b.status, ifnull(sum(b.km_tempuh), 0) as km_tempuh, ifnull(c.total_km, 0) as total, ifnull(sum(b.km_tempuh)+c.total_km, 0) as km_total,
			(case
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>12000) then 'MELEWATI WAKTU SERVICE'
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>11000 && ifnull(sum(b.km_tempuh)+c.total_km, 0)<=12000) then 'WAKTU SERVICE' 
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>=10000 && ifnull(sum(b.km_tempuh)+c.total_km, 0)<=11000) then 'PERHATIKAN WAKTU SERVICE'
				else 'SERVICE' end) as keterangan,
			(case 
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>12000) then '1' 
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>11000 && ifnull(sum(b.km_tempuh)+c.total_km, 0)<=12000) then '2' 
				when (ifnull(sum(b.km_tempuh)+c.total_km, 0)>=10000 && ifnull(sum(b.km_tempuh)+c.total_km, 0)<=11000) then '3' 
				else '4' end) as urutan 
			FROM ref_armada a 
			LEFT JOIN tr_absensi_armada_teknik b ON a.kd_armada = b.kd_armada 
			LEFT JOIN temp_km_service_simateknik c ON a.kd_armada = c.armada 
			WHERE a.id_bu = '$id_bu' AND a.kd_armada != ''
			GROUP BY a.kd_armada
		) n");
		$this->db->where("n.km_total > 10000");
		$this->db->where("(n.kd_armada  LIKE '%" . $cari . "%') ");
		$this->db->order_by("n.km_total desc");
		return $this->db->get();
	}
	
	// public function get_count_km_armada_simateknik_3($cari = null, $id_bu)
	// {
	// 	$count = array();
	// 	$session = $this->session->userdata('login');

	// 	$this->db->select(" COUNT(DISTINCT(a.kd_armada)) as recordsFiltered ");
	// 	$this->db->from("tr_absensi_armada_teknik a");
	// 	$this->db->join("temp_km_service_simateknik b","a.kd_armada = b.armada","left");
	// 	$this->db->where('a.id_bu', $id_bu);
	// 	$this->db->group_by('a.kd_armada');
	// 	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	// 	$this->db->select(" COUNT(DISTINCT(a.kd_armada)) as recordsTotal ");
	// 	$this->db->from("tr_absensi_armada_teknik a");
	// 	$this->db->join("temp_km_service_simateknik b","a.kd_armada = b.armada","left");
	// 	$this->db->where('a.id_bu', $id_bu);
	// 	$this->db->group_by('a.kd_armada');
	// 	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	// 	return $count;
	// }

	public function insert_absensi_teknik($data)
	{
		$this->db->from('tr_absensi_armada_teknik');
		$this->db->where('tgl_absensi', $data['tgl_absensi']);
		$this->db->where('kd_armada', $data['kd_armada']);
		$this->db->where('id_bu', $data['id_bu']);
		$cek = $this->db->get();
		if ($cek->num_rows() > 0) {
			$this->db->where('tgl_absensi', $data['tgl_absensi']);
			$this->db->where('kd_armada', $data['kd_armada']);
			$this->db->where('id_bu', $data['id_bu']);
			$this->db->delete('tr_absensi_armada_teknik');
			if ($this->db->affected_rows()) {
				$this->db->insert('tr_absensi_armada_teknik', $data);
				return $this->db->insert_id();
			}
		} else {
			$this->db->insert('tr_absensi_armada_teknik', $data);
			return $this->db->insert_id();
		}
	}

	public function insert_absensi_teknik_new($data)
	{
		// $this->db->insert('tr_absensi_armada_teknik', $data);
		// return $this->db->insert_id();
		$this->db->from('tr_absensi_armada_teknik');
		$this->db->where('tgl_absensi', $data['tgl_absensi']);
		$this->db->where('kd_armada', $data['kd_armada']);
		$this->db->where('id_bu', $data['id_bu']);
		$cek = $this->db->get();
		if ($cek->num_rows() > 0) {
			$this->db->where('tgl_absensi', $data['tgl_absensi']);
			$this->db->where('kd_armada', $data['kd_armada']);
			$this->db->where('id_bu', $data['id_bu']);
			$this->db->update('tr_absensi_armada_teknik', $data);
			// if ($this->db->affected_rows()) {
			// 	$this->db->insert('tr_absensi_armada_teknik', $data);
			// 	return $this->db->insert_id();
			// }
		} else {
			$this->db->insert('tr_absensi_armada_teknik', $data);
			return $this->db->insert_id();
		}
	}

	public function update_absensi_teknik_new($data)
        {
            $session = $this->session->userdata('login');
            $this->db->where('id_perusahaan', $session['id_perusahaan']);
            $this->db->where('id_event', $data['id_event']);
            $this->db->update('tr_event', array('active' => '2'));
			return $data['id_event'];
		}

	public function delete_absensi_armada($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_perusahaan', $session['id_perusahaan']);
		$this->db->where('id_pegawai', $data['id_pegawai']);
		$this->db->delete('tr_absensi_armada_teknik');
		return $data['id_pegawai'];
	}

	public function delete_absensi_teknik_new($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_absensi_armada', $data['id_absensi_armada']);
		$this->db->delete('tr_absensi_armada_teknik');
		return $data['id_absensi_armada'];
	}

	public function get_absensi_armada_by_id($id_pegawai, $id_bu)
	{
		if (empty($id_pegawai)) {
			return array();
		} else {
			$session = $this->session->userdata('login');
			$query = $this->db->query("select id_pegawai, nm_pegawai, tanggal, status, petugas, cdate, id_bu
				from (
				select id_pegawai, nm_pegawai, '' tanggal, '' status, '' petugas, '' cdate, id_bu 
				from hris.ref_pegawai 
				where id_posisi = 10 and id_bu <> 0
				union all
				select a.id_pegawai, nm_pegawai, tgl_absensi tanggal, status, a.cuser petugas, a.cdate, a.id_bu
				from tr_absensi_armada_teknik a
				join hris.ref_pegawai on a.id_pegawai = hris.ref_pegawai.id_pegawai
				where a.id_bu <> 0
				) z
				where id_bu = '$id_bu' and id_pegawai = '$id_pegawai' ");
			return $query->row_array();
		}
	}

	public function get_odometer_auto($kd_armada, $tgl_absensi)
	{
		if (empty($kd_armada)) {
			return array();
		} else {
			$session = $this->session->userdata('login');
			$query = $this->db->query("select odometer_akhir from tr_absensi_armada_teknik where kd_armada = '$kd_armada' and tgl_absensi = date_sub('$tgl_absensi', interval 1 day) order by tgl_absensi desc limit 1");
			return $query->row_array();
		}
	}

	public function get_segment_auto($kd_armada)
	{
		if (empty($kd_armada)) {
			return array();
		} else {
			$session = $this->session->userdata('login');
			$query = $this->db->query("select a.id_segment, b.nm_segment from ref_armada a left join ref_segment b on a.id_segment = b.id_segment where a.kd_armada = '$kd_armada' limit 1");
			return $query->row_array();
		}
	}

	public function get_absensi_armada_teknik_by_id($id_absensi_armada)
	{
		if (empty($id_absensi_armada)) {
            return array();
        } else {
            $session = $this->session->userdata('login');
            $this->db->select("a.*, b.nm_bu");
            $this->db->from("tr_absensi_armada_teknik a");
            $this->db->join("ref_bu b","a.id_bu = b.id_bu","left");
            $this->db->where('a.id_absensi_armada', $id_absensi_armada);
            return $this->db->get()->row_array();
        }
	}

	public function combobox_bu()
	{
		$session = $this->session->userdata('login');
		$this->db->from("ref_bu_access b");
		$this->db->join("ref_bu a", "b.id_bu = a.id_bu", "left");
		$this->db->where('b.id_perusahaan', $session['id_perusahaan']);
		$this->db->where('b.id_user', $session['id_user']);
		$this->db->where('b.active', 1);

		return $this->db->get();
	}

	public function combobox_segment()
	{
		$session = $this->session->userdata('login');
		$this->db->from("ref_segment b");
		$this->db->where('b.id_perusahaan', $session['id_perusahaan']);
		$this->db->where('b.active', 1);

		return $this->db->get();
	}

	public function combobox_armada()
	{
		$session = $this->session->userdata('login');
		$this->db->from("ref_armada b");
		$this->db->where('b.id_perusahaan', $session['id_perusahaan']);
		$this->db->where('b.active', 0);

		return $this->db->get();
	}

	public function combobox_trayek()
	{
		$session = $this->session->userdata('login');
		$this->db->from("ref_trayek b");
		$this->db->where('b.id_perusahaan', $session['id_perusahaan']);
		$this->db->where('b.active', 1);

		return $this->db->get();
	}

	public function list_trayek($id_cabang)
	{
		$this->db->from("ref_trayek a");
		$this->db->where('a.id_bu', $id_cabang);
		$this->db->where('a.active', 1);
		$this->db->order_by('a.id_trayek');
		return $this->db->get();
	}

	public function list_armada($id_cabang)
	{
		$this->db->from("ref_armada a");
		$this->db->where('a.id_bu', $id_cabang);
		$this->db->where('a.active', 0);
		$this->db->order_by('a.id_armada');
		return $this->db->get();
	}

	public function get_cabang($id_bu)
	{
		$this->db->from("ref_bu a");
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where('a.active', 1);
		$query = $this->db->get();

		return $query->row();
	}

	public function general_manager($id_bu)
	{
		$this->db->select("a.nm_pegawai");
		$this->db->from("hris.ref_pegawai a");
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where('a.id_posisi', 1);
		$query = $this->db->get();

		return $query->row()->nm_pegawai;
	}

	public function asmen_pelayan_jasa($id_bu)
	{
		$this->db->select("a.nm_pegawai");
		$this->db->from("hris.ref_pegawai a");
		$this->db->where('a.id_bu', $id_bu);
		$this->db->where('a.id_posisi', 5);
		$query = $this->db->get();

		return $query->row()->nm_pegawai;
	}

	public function copyAbsensiArmada($id_cabang, $id_segment, $tanggal_from, $tanggal_to)
	{
		if ($id_segment == 0) {
			$sql = "CALL p_copy_absensi_armada_teknik('$id_cabang','$tanggal_from','$tanggal_to')";
		} else {
			$sql = "CALL p_copy_absensi_armada_teknik_by_segment('$id_cabang','$id_segment','$tanggal_from','$tanggal_to')";
		}
		$this->db->query($sql);
		return true;
	}
}
