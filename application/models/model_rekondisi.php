<?php
class Model_rekondisi extends CI_Model
{
	public function getAllrekondisi($show=null, $start=null, $cari=null,$id_bu,$tahun,$active)
	{
		$this->db->select("a.*");
		$this->db->from("ref_armada_rekondisi a");
		$session = $this->session->userdata('login');
		$this->db->where("(a.nm_bu  LIKE '%".$cari."%' OR a.kd_armada  LIKE '%".$cari."%' OR a.tgl_rekondisi  LIKE '%".$cari."%') ");
		$this->db->where("a.active !=2 ");
		// $this->db->where("a.id_bu in (3, 7, 8, 17)");

		$this->db->where("a.tahun",$tahun);
		if($id_bu<>0){$this->db->where("a.id_bu",$id_bu);}
		if($active<>"all"){$this->db->where("a.active",$active);}
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_rekondisi($cari = null,$id_bu,$tahun,$active)
	{
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(id_rekondisi) as recordsFiltered ");
		$this->db->from("ref_armada_rekondisi");
		$this->db->where("active != '2' ");
		$this->db->where("tahun",$tahun);
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){$this->db->where("id_bu",$id_bu);}
		if($active<>"all"){$this->db->where("active",$active);}
		$this->db->where("(nm_bu  LIKE '%".$cari."%' OR kd_armada  LIKE '%".$cari."%' OR tgl_rekondisi  LIKE '%".$cari."%') ");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		$this->db->select(" COUNT(id_rekondisi) as recordsTotal ");
		$this->db->from("ref_armada_rekondisi");
		$this->db->where("active != '2' ");
		$this->db->where("tahun",$tahun);
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where("id_bu",$id_bu);
			}
		if($active<>"all"){
			$this->db->where("active",$active);
			}
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;
	}

	public function getAllrekondisi_history($show=null, $start=null, $cari=null,$id_rekondisi)
	{
		$this->db->select("a.*,b.nm_user");
		$this->db->from("ref_armada_rekondisi_history a");
		$this->db->join("ref_user b", "a.cuser = b.id_user","left");
		$session = $this->session->userdata('login');
		$this->db->where("(a.tgl_rekondisi  LIKE '%".$cari."%' OR a.biaya  LIKE '%".$cari."%' OR a.no_surat  LIKE '%".$cari."%') ");
		$this->db->where("a.id_rekondisi",$id_rekondisi);
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_rekondisi_history($cari = null,$id_rekondisi)
	{
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(id_rekondisi) as recordsFiltered ");
		$this->db->from("ref_armada_rekondisi_history");
		$this->db->where("id_rekondisi",$id_rekondisi);
		$this->db->where("(tgl_rekondisi  LIKE '%".$cari."%' OR biaya  LIKE '%".$cari."%' OR no_surat  LIKE '%".$cari."%') ");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		$this->db->select(" COUNT(id_rekondisi) as recordsTotal ");
		$this->db->from("ref_armada_rekondisi_history");
		$this->db->where("id_rekondisi",$id_rekondisi);
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;
	}

	public function insert_rekondisi($data)
	{
		$this->db->insert('ref_armada_rekondisi', $data);
		return $this->db->insert_id();
	}

	public function insert_rekondisi_history($data)
	{
		$this->db->insert('ref_armada_rekondisi_history', $data);
		return $this->db->insert_id();
	}

	public function delete_rekondisi($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_rekondisi', $data['id_rekondisi']);
		$this->db->update('ref_armada_rekondisi', array('active' => '2'));
		return $data['id_rekondisi'];
	}

	public function update_rekondisi($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_rekondisi', $data['id_rekondisi']);
		$this->db->where("active != '2' ");
		$this->db->update('ref_armada_rekondisi', $data);
		return $data['id_rekondisi'];
	}

	public function get_rekondisi_by_id($id_rekondisi)
	{
		if(empty($id_rekondisi))
		{
			return array();
		}
		else
		{
			$session = $this->session->userdata('login');
			$this->db->from("ref_armada_rekondisi a");
			$this->db->where('a.id_rekondisi', $id_rekondisi);
			$this->db->where("a.active != '2' ");
			return $this->db->get()->row_array();
		}
	}

	public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

	public function combobox_tahun()
	{
		$session = $this->session->userdata('login');
		$this->db->select("tahun");
		$this->db->from("ref_tahun");
		$this->db->where('active',1);
		$this->db->order_by('tahun', 'DESC');
		return $this->db->get();
	}

	public function combobox_armada($id_bu){
		$this->db->from("ref_armada a");
		$this->db->where('a.id_bu',$id_bu);
		$this->db->where('a.active in (0,1)');
		return $this->db->get();
		
	}

	public function change_active($where, $data)
	{
		$this->db->update("ref_armada_rekondisi", $data, $where);
		return $this->db->affected_rows();
	}

}
