<?php
    class Model_jenis_asuransi extends CI_Model
    {
        public function getAlljenis_asuransi($show=null, $start=null, $cari=null)
        {
            $this->db->select("a.*");
            $this->db->from("ref_jenis_asuransi a");
            $session = $this->session->userdata('login');
            $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
            $this->db->where("(a.nm_jenis_asuransi  LIKE '%".$cari."%' ) ");
            $this->db->where("a.active IN (0, 1) ");
            if ($show == null && $start == null) {
            } else {
                $this->db->limit($show, $start);
            }

            return $this->db->get();
        }
		
		public function get_count_jenis_asuransi($search = null)
		{
			$count = array();
			$session = $this->session->userdata('login');
			
			$this->db->select(" COUNT(id_jenis_asuransi) as recordsFiltered ");
			$this->db->from("ref_jenis_asuransi");
			$this->db->where('id_perusahaan', $session['id_perusahaan']);
			$this->db->where("active != '2' ");
			$this->db->like("nm_jenis_asuransi ", $search);
			$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
			
			$this->db->select(" COUNT(id_jenis_asuransi) as recordsTotal ");
			$this->db->from("ref_jenis_asuransi");
			$this->db->where('id_perusahaan', $session['id_perusahaan']);
			$this->db->where("active != '2' ");
			$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
			
			return $count;
		}
		
		public function insert_jenis_asuransi($data)
        {
            $this->db->insert('ref_jenis_asuransi', $data);
			return $this->db->insert_id();
        }

        public function delete_jenis_asuransi($data)
        {
            $session = $this->session->userdata('login');
            $this->db->where('id_perusahaan', $session['id_perusahaan']);
            $this->db->where('id_jenis_asuransi', $data['id_jenis_asuransi']);
            $this->db->update('ref_jenis_asuransi', array('active' => '2'));
			return $data['id_jenis_asuransi'];
        }
		
        public function update_jenis_asuransi($data)
        {
            $session = $this->session->userdata('login');
            $this->db->where('id_perusahaan', $session['id_perusahaan']);
            $this->db->where('id_jenis_asuransi', $data['id_jenis_asuransi']);
			$this->db->where("active != '2' ");
            $this->db->update('ref_jenis_asuransi', $data);
			return $data['id_jenis_asuransi'];
        }
		
		public function get_jenis_asuransi_by_id($id_jenis_asuransi)
		{
			if(empty($id_jenis_asuransi))
			{
				return array();
			}
			else
			{
				$session = $this->session->userdata('login');
				$this->db->from("ref_jenis_asuransi a");
				$this->db->where('a.id_perusahaan', $session['id_perusahaan']);
				$this->db->where('a.id_jenis_asuransi', $id_jenis_asuransi);
				$this->db->where("a.active != '2' ");
				return $this->db->get()->row_array();
			}
		}

    }
