<?php
class Model_usul_afkir extends CI_Model
{
	public function getAllusul_afkir($show=null, $start=null, $cari=null,$id_bu, $tahun)
	{
		$session = $this->session->userdata('login');

		$this->db->select("a.*");
		$this->db->from("ref_armada_usul_afkir a");
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.status <> 2");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($tahun <> 0){ $this->db->where("a.approvedyear='$tahun' "); }

		if($id_bu<>0){$this->db->where("a.id_bu",$id_bu);}

		$this->db->order_by("cdate","DESC");
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}
	
	public function get_count_usul_afkir($cari = null,$id_bu, $tahun)
	{
		$count = array();
		$session = $this->session->userdata('login');
		
		$this->db->select(" COUNT(id_usul_afkir) as recordsFiltered ");
		$this->db->from("ref_armada_usul_afkir");
		$this->db->where("(kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("status <> 2 ");
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($tahun <> 0){ $this->db->where("approvedyear='$tahun' "); }
		if($id_bu<>0){$this->db->where("id_bu",$id_bu);}
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
		
		$this->db->select(" COUNT(id_usul_afkir) as recordsTotal ");
		$this->db->from("ref_armada_usul_afkir");
		$this->db->where("status <> 2 ");
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($tahun <> 0){ $this->db->where("approvedyear='$tahun' "); }
		if($id_bu<>0){$this->db->where("id_bu",$id_bu);}
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
		
		return $count;
	}

	public function getAllusul_afkir_pusat($show=null, $start=null, $cari=null,$tahun,$id_bu)
	{
		$session = $this->session->userdata('login');

		$this->db->select("a.*");
		$this->db->from("ref_armada_usul_afkir a");
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%') ");
		$this->db->where("a.status in (3,4)");

		if($tahun <> 0){ $this->db->where("approvedyear",$tahun); };
		if($id_bu <> 0){ $this->db->where("a.id_bu",$id_bu); };

		$this->db->order_by("cdate","DESC");
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}
	
	public function get_count_usul_afkir_pusat($cari = null,$tahun,$id_bu)
	{
		$count = array();
		$session = $this->session->userdata('login');
		
		$this->db->select(" COUNT(id_usul_afkir) as recordsFiltered ");
		$this->db->from("ref_armada_usul_afkir");
		$this->db->where("(kd_armada  LIKE '%".$cari."%'  ) ");
		$this->db->where("status in (3,4)");

		if($tahun <> 0){ $this->db->where("approvedyear",$tahun); };
		if($id_bu <> 0){ $this->db->where("id_bu",$id_bu); };

		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
		
		$this->db->select(" COUNT(id_usul_afkir) as recordsTotal ");
		$this->db->from("ref_armada_usul_afkir");
		$this->db->where("status in (3,4)");

		if($tahun <> 0){ $this->db->where("approvedyear",$tahun); };
		if($id_bu <> 0){ $this->db->where("id_bu",$id_bu); };

		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
		
		return $count;
	}
	
	public function insert_usul_afkir($data)
	{
		$this->db->insert('ref_armada_usul_afkir', $data);
		return $this->db->insert_id();
	}

	public function delete_usul_afkir($data)
	{
		$this->db->where('id_usul_afkir', $data['id_usul_afkir']);
		$this->db->update('ref_armada_usul_afkir', array('status' => '2'));
		return $data['id_usul_afkir'];
	}

	public function update_data_armada($data)
	{
		$this->db->where('id_armada', $data['id_armada']);
		$this->db->update('ref_armada', array('active' => 3) );
		return $data['id_armada'];
	}
	
	public function update_usul_afkir($data)
	{
		$this->db->where('id_usul_afkir', $data['id_usul_afkir']);
		$this->db->update('ref_armada_usul_afkir', $data);
		return $data['id_usul_afkir'];
	}
	
	public function get_usul_afkir_by_id($id_usul_afkir)
	{
		if(empty($id_usul_afkir))
		{
			return array();
		}
		else
		{
			$this->db->from("ref_armada_usul_afkir a");
			$this->db->where('a.id_usul_afkir', $id_usul_afkir);
			return $this->db->get()->row_array();
		}
	}

	public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

	public function combobox_tahunXX()
	{
		$session = $this->session->userdata('login');
		$this->db->select("approvedyear as tahun");
		$this->db->from("ref_armada_usul_afkir");
		$this->db->where('status in (3,4)');
		$this->db->where( "approvedyear != YEAR(CURDATE())" );
		$this->db->group_by('approvedyear');
		$this->db->order_by('approvedyear', 'DESC');
		
		return $this->db->get();
	}

	public function combobox_tahun()
	{
		$session = $this->session->userdata('login');
		$this->db->select("tahun");
		$this->db->from("ref_tahun");
		$this->db->where('active',1);
		$this->db->order_by('tahun', 'DESC');
		return $this->db->get();
	}

	public function combobox_cabangxxx()
	{
		$this->db->from("ref_bu a");
		$this->db->where('a.active',1);
		return $this->db->get();
	}

	public function combobox_cabang()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

	public function combobox_armada($id_bu){
		$this->db->from("ref_armada a");
		$this->db->where('a.id_bu',$id_bu);
		$this->db->where('a.active in (0,1)');
		return $this->db->get();
		
	}

	public function change_active($where, $data)
	{
		$this->db->update("ref_armada_usul_afkir", $data, $where);
		return $this->db->affected_rows();
	}

}
