<?php
class Model_lelang extends CI_Model
{
    public function getAlllelang($show=null, $start=null, $cari=null, $id_bu, $tahun)
    {
        $this->db->select("a.*");
        $this->db->from("ref_armada_lelang a");
        $session = $this->session->userdata('login');
        $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
        
        if($id_bu<>0){ $this->db->where('a.id_bu', $id_bu); }
        $this->db->where("a.tahun_ua='$tahun' ");
        $this->db->where("a.id_bu in (3, 7, 8, 17)");

        $this->db->where("(a.nm_bu  LIKE '%".$cari."%' or a.nm_pemenang  LIKE '%".$cari."%' or a.nilai_liduidasi  LIKE '%".$cari."%') ");
        $this->db->order_by("a.id_lelang","ASC");
        if ($show == null && $start == null) {
        } else {
            $this->db->limit($show, $start);
        }

        return $this->db->get();
    }

    public function get_count_lelang($cari = null, $id_bu, $tahun)
    {
     $count = array();
     $session = $this->session->userdata('login');

     $this->db->select(" COUNT(a.id_lelang) as recordsFiltered ");
     $this->db->from("ref_armada_lelang a");
     $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
     $this->db->where("a.id_bu in (3, 7, 8, 17)");
     if($id_bu<>0){ $this->db->where('a.id_bu', $id_bu); }
     $this->db->where("a.tahun_ua='$tahun' ");

     $this->db->where("(a.nm_bu  LIKE '%".$cari."%' or a.nm_pemenang  LIKE '%".$cari."%' or a.nilai_liduidasi  LIKE '%".$cari."%') ");
     $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

     $this->db->select(" COUNT(id_lelang) as recordsTotal ");
     $this->db->from("ref_armada_lelang");
     $this->db->where('id_perusahaan', $session['id_perusahaan']);
     $this->db->where("id_bu in (3, 7, 8, 17)");
     if($id_bu<>0){ $this->db->where('id_bu', $id_bu); }
     $this->db->where("tahun_ua='$tahun' ");
     $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

     return $count;
 }

 public function getAlllelang_detail($show=null, $start=null, $cari=null, $id_lelang)
 {
    $this->db->select("a.*");
    $this->db->from("ref_armada_lelang_detail a");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_lelang', $id_lelang);
    // $this->db->where("(a.kd_armada  LIKE '%".$cari."%') ");
    
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_lelang_detail($cari = null, $id_lelang)
{
    $count = array();
    $session = $this->session->userdata('login');

    $this->db->select(" COUNT(id_lelang_detail) as recordsFiltered ");
    $this->db->from("ref_armada_lelang_detail");
    $this->db->where('id_lelang', $id_lelang);
    $this->db->where("(kd_armada  LIKE '%".$cari."%') ");
    $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

    $this->db->select(" COUNT(id_lelang_detail) as recordsTotal ");
    $this->db->from("ref_armada_lelang_detail");
    $this->db->where('id_lelang', $id_lelang);
    $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

    return $count;
}

public function insert_lelang($data)
{
    $this->db->insert('ref_armada_lelang', $data);
    return $this->db->insert_id();
}

public function insert_lelang_detail($data)
{
    $this->db->insert('ref_armada_lelang_detail', $data);
    return $this->db->insert_id();
}

public function insert_hapus_buku($data)
{
    $this->db->insert('ref_armada_hapus_buku', $data);
    return $this->db->insert_id();
}

public function delete_lelang($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_lelang', $data['id_lelang']);
    $this->db->delete('ref_armada_lelang');
    return $data['id_lelang'];
}

public function delete_lelang_detail($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_lelang_detail', $data['id_lelang_detail']);
    $this->db->delete('ref_armada_lelang_detail');
    return $data['id_lelang_detail'];
}

public function update_lelang($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_lelang', $data['id_lelang']);
    $this->db->update('ref_armada_lelang', $data);
    return $data['id_lelang'];
}

public function update_trayek_detail($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_trayek_detail', $data['id_trayek_detail']);
    $this->db->update('ref_trayek_detail', $data);
    return $data['id_trayek_detail'];
}

public function update_armada($data)
{
    $this->db->where('id_armada', $data['id_armada']);
    $this->db->update('ref_armada', array('active' => '4'));
    return $data['id_armada'];
}

public function get_lelang_by_id($id_lelang)
{
 if(empty($id_lelang))
 {
    return array();
}
else
{
    $session = $this->session->userdata('login');
    $this->db->select("a.*");
    $this->db->from("ref_armada_lelang a");
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('a.id_lelang', $id_lelang);
    $this->db->where("a.active != '2' ");
    return $this->db->get()->row_array();
}
}

public function get_trayek_by_id_detail($id_trayek_detail)
{
    if(empty($id_trayek_detail))
    {
        return array();
    }
    else
    {
        $session = $this->session->userdata('login');
        $this->db->select("a.*");
        $this->db->from("ref_trayek_detail a");
        $this->db->where('a.id_trayek_detail', $id_trayek_detail);
        return $this->db->get()->row_array();
    }
}

public function combobox_point()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_point b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_user()
{
    $this->db->from("ref_user");
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('active', 1);
    $this->db->where('id_level', 4);
    return $this->db->get();
}

public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

  public function combobox_bu_all()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

public function combobox_bu_allxx()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_bu b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_segment()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_segment a");
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('a.active', 1);
    return $this->db->get();
}

public function combobox_armada($id_bu, $approvedyear){
    $this->db->from("ref_armada_usul_afkir a");
    $this->db->where('a.id_bu',$id_bu);
    $this->db->where('a.approvedyear',$approvedyear);
    $this->db->where('a.status',4);
    $this->db->where("a.id_usul_afkir not in (select id_usul_afkir from ref_armada_lelang_detail where id_bu='$id_bu')");
    return $this->db->get(); 
}

public function combobox_tahun()
{
    $session = $this->session->userdata('login');
    $this->db->select("tahun");
    $this->db->from("ref_tahun");
    $this->db->where('active',1);
    $this->db->order_by('tahun', 'DESC');
    return $this->db->get();
}

}
