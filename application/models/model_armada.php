<?php
class Model_armada extends CI_Model
{


    public function getAllarmada($show=null, $start=null, $cari=null, $id_bu)
    {

        $this->db->select("a.*, b.nm_bu, 
           (select y.no_polis from ref_asuransi_detail y 
           left join ref_asuransi z on y.id_asuransi=z.id_asuransi
           where y.id_armada = a.id_armada and y.active=1 and z.active=1
           order by y.id_asuransi_detail desc limit 1 ) as no_polis,
           
           (select y.no_certificate from ref_asuransi_detail y 
           left join ref_asuransi z on y.id_asuransi=z.id_asuransi
           where y.id_armada = a.id_armada and y.active=1 and z.active=1
           order by y.id_asuransi_detail desc limit 1  ) as no_certificate
           ");
        $this->db->from("ref_armada a");
        $this->db->join("ref_bu b", "a.id_bu = b.id_bu","left");
        $session = $this->session->userdata('login');
        $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
        $this->db->where("a.id_bu in (3, 7, 8, 17)");
        if($id_bu<>0){
            $this->db->where('a.id_bu', $id_bu);
        }
        $this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
        $this->db->where("a.active IN (0,3) ");
        if ($show == null && $start == null) {
        } else {
            $this->db->limit($show, $start);
        }

        return $this->db->get();
    }

    public function get_count_armada($search = null, $id_bu)
    {
       $count = array();
       $session = $this->session->userdata('login');

       $this->db->select(" COUNT(id_armada) as recordsFiltered ");
       $this->db->from("ref_armada");
       $this->db->where('id_perusahaan', $session['id_perusahaan']);
       $this->db->where("id_bu in (3, 7, 8, 17)");
       if($id_bu<>0){
           $this->db->where('id_bu', $id_bu);
       }
       $this->db->where("active IN (0, 3) ");
       $this->db->like("kd_armada ", $search);
       $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

       $this->db->select(" COUNT(id_armada) as recordsTotal ");
       $this->db->from("ref_armada");
       $this->db->where('id_perusahaan', $session['id_perusahaan']);
       $this->db->where("id_bu in (3, 7, 8, 17)");
       if($id_bu<>0){
           $this->db->where('id_bu', $id_bu);
       }
       $this->db->where("active IN (0, 3) ");
       $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

       return $count;
   }


   public function getAllmutasiarmada($show=null, $start=null, $cari=null, $id_armada)
   {

    $this->db->select("a.*");
    $this->db->from("ref_armada_mutasi a");
    $this->db->where('a.id_armada', $id_armada);
    $this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
    $this->db->where("a.status != 2 ");
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_mutasiarmada($search = null, $id_armada)
{
   $count = array();
   $session = $this->session->userdata('login');

   $this->db->select(" COUNT(id_mutasi) as recordsFiltered ");
   $this->db->from("ref_armada_mutasi");
   $this->db->where('id_armada', $id_armada);
   $this->db->where("status != '2' ");
   $this->db->like("kd_armada ", $search);
   $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

   $this->db->select(" COUNT(id_mutasi) as recordsTotal ");
   $this->db->from("ref_armada_mutasi");
   $this->db->where('id_armada', $id_armada);
   $this->db->where("status != '2' ");
   $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
   return $count;
}

public function insert_armada($data)
{
    $this->db->insert('ref_armada', $data);
    return $this->db->insert_id();
}

public function delete_armada($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $data['id_armada']);
    $this->db->update('ref_armada', array('active' => '2'));
    return $data['id_armada'];
}

public function update_armada($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $data['id_armada']);
    $this->db->where("active != '2' ");
    $this->db->update('ref_armada', $data);
    return $data['id_armada'];
}



public function get_armada_by_id($id_armada)
{
   if(empty($id_armada))
   {
    return array();
}
else
{
    $session = $this->session->userdata('login');
    $this->db->select("a.*, b.nm_bu");
    $this->db->from("ref_armada a");
    $this->db->join("ref_bu b","a.id_bu = b.id_bu","LEFT");
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('a.id_armada', $id_armada);
    $this->db->where("a.active != '2' ");
    return $this->db->get()->row_array();
}
}


public function get_armada_by_id_stnk($id_armada_stnk)
{
   if(empty($id_armada_stnk))
   {
    return array();
}
else
{
    $session = $this->session->userdata('login');
    $this->db->select("a.*");
    $this->db->from("ref_armada_stnk a");
    $this->db->where('a.id_armada_stnk', $id_armada_stnk);
    return $this->db->get()->row_array();
}
}

public function get_armada_by_id_keur($id_armada_keur)
{
   if(empty($id_armada_keur))
   {
    return array();
}
else
{
    $session = $this->session->userdata('login');
    $this->db->select("a.*");
    $this->db->from("ref_armada_keur a");
    $this->db->where('a.id_armada_keur', $id_armada_keur);
    return $this->db->get()->row_array();
}
}


public function getAllarmadafoto($show=null, $start=null, $cari=null, $id_armada=null)
{

    $this->db->select("a.*");
    $this->db->from("ref_armada_foto a");
            // $this->db->join("ref_bu b", "a.id_bu = b.id_bu","left");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
            // $this->db->where('a.id_bu', $id_bu);
            // $this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
    $this->db->where("a.active",1);
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_armadafoto($search = null, $id_armada=null)
{
   $count = array();
   $session = $this->session->userdata('login');

   $this->db->select(" COUNT(id_armada_foto) as recordsFiltered ");
   $this->db->from("ref_armada_foto");
   $this->db->where('id_perusahaan', $session['id_perusahaan']);
   $this->db->where('id_armada', $id_armada);
   $this->db->where("active = '1' ");
			// $this->db->like("kd_armada ", $search);
   $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

   $this->db->select(" COUNT(id_armada_foto) as recordsTotal ");
   $this->db->from("ref_armada_foto");
   $this->db->where('id_perusahaan', $session['id_perusahaan']);
   $this->db->where('id_armada', $id_armada);
            // $this->db->where('id_bu', $id_bu);
   $this->db->where("active = '1' ");
   $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

   return $count;
}

public function insert_armada_foto($data)
{
    $this->db->insert('ref_armada_foto', $data);
    return $this->db->insert_id();
}

public function get_foto_by_id($id_armada_foto)
{
   if(empty($id_armada_foto))
   {
    return array();
}
else
{
    $session = $this->session->userdata('login');
    $this->db->select("a.*");
    $this->db->from("ref_armada_foto a");
    $this->db->where('a.id_armada_foto', $id_armada_foto);
    return $this->db->get()->row_array();
}
}

public function delete_foto($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada_foto', $data['id_armada_foto']);
    $this->db->delete('ref_armada_foto');
    return $data['id_armada_foto'];
}

public function getAllarmadastnk($show=null, $start=null, $cari=null, $id_armada=null)
{

    $this->db->select("a.*");
    $this->db->from("ref_armada_stnk a");
            // $this->db->join("ref_bu b", "a.id_bu = b.id_bu","left");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
            // $this->db->where('a.id_bu', $id_bu);
            // $this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
    $this->db->where("a.active",1);
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_armadastnk($search = null, $id_armada=null)
{
    $count = array();
    $session = $this->session->userdata('login');

    $this->db->select(" COUNT(id_armada_stnk) as recordsFiltered ");
    $this->db->from("ref_armada_stnk");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("active = '1' ");
            // $this->db->like("kd_armada ", $search);
    $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

    $this->db->select(" COUNT(id_armada_stnk) as recordsTotal ");
    $this->db->from("ref_armada_stnk");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
            // $this->db->where('id_bu', $id_bu);
    $this->db->where("active = '1' ");
    $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

    return $count;
}

public function insert_armada_stnk($data)
{
    $this->db->insert('ref_armada_stnk', $data);
    return $this->db->insert_id();
}

public function update_armada_stnk($data)
{
    $this->db->where('id_armada_stnk', $data['id_armada_stnk']);
    $this->db->update('ref_armada_stnk', $data);
    return $data['id_armada_stnk'];
}

public function update_armada_keur($data)
{
    $this->db->where('id_armada_keur', $data['id_armada_keur']);
    $this->db->update('ref_armada_keur', $data);
    return $data['id_armada_keur'];
}


public function get_stnk_by_id($id_armada_stnk)
{
    if(empty($id_armada_stnk))
    {
        return array();
    }
    else
    {
        $session = $this->session->userdata('login');
        $this->db->select("a.*");
        $this->db->from("ref_armada_stnk a");
        $this->db->where('a.id_armada_stnk', $id_armada_stnk);
        return $this->db->get()->row_array();
    }
}

public function delete_stnk($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada_stnk', $data['id_armada_stnk']);
    $this->db->delete('ref_armada_stnk');
    return $data['id_armada_stnk'];
}



public function getAllarmadakeur($show=null, $start=null, $cari=null, $id_armada=null)
{

    $this->db->select("a.*");
    $this->db->from("ref_armada_keur a");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("a.active",1);
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_armadakeur($search = null, $id_armada=null)
{
    $count = array();
    $session = $this->session->userdata('login');

    $this->db->select(" COUNT(id_armada_keur) as recordsFiltered ");
    $this->db->from("ref_armada_keur");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("active = '1' ");
    $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

    $this->db->select(" COUNT(id_armada_keur) as recordsTotal ");
    $this->db->from("ref_armada_keur");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("active = '1' ");
    $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

    return $count;
}

public function insert_armada_keur($data)
{
    $this->db->insert('ref_armada_keur', $data);
    return $this->db->insert_id();
}

public function get_keur_by_id($id_armada_keur)
{
    if(empty($id_armada_keur))
    {
        return array();
    }
    else
    {
        $session = $this->session->userdata('login');
        $this->db->select("a.*");
        $this->db->from("ref_armada_keur a");
        $this->db->where('a.id_armada_keur', $id_armada_keur);
        return $this->db->get()->row_array();
    }
}

public function delete_keur($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada_keur', $data['id_armada_keur']);
    $this->db->delete('ref_armada_keur');
    return $data['id_armada_keur'];
}






public function getAllarmadaijintrayek($show=null, $start=null, $cari=null, $id_armada=null)
{

    $this->db->select("a.*");
    $this->db->from("ref_armada_ijintrayek a");
    $session = $this->session->userdata('login');
    $this->db->where('a.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("a.active",1);
    if ($show == null && $start == null) {
    } else {
        $this->db->limit($show, $start);
    }

    return $this->db->get();
}

public function get_count_armadaijintrayek($search = null, $id_armada=null)
{
    $count = array();
    $session = $this->session->userdata('login');

    $this->db->select(" COUNT(id_armada_ijintrayek) as recordsFiltered ");
    $this->db->from("ref_armada_ijintrayek");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("active = '1' ");
    $count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

    $this->db->select(" COUNT(id_armada_ijintrayek) as recordsTotal ");
    $this->db->from("ref_armada_ijintrayek");
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada', $id_armada);
    $this->db->where("active = '1' ");
    $count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

    return $count;
}

public function insert_armada_ijintrayek($data)
{
    $this->db->insert('ref_armada_ijintrayek', $data);
    return $this->db->insert_id();
}

public function get_ijintrayek_by_id($id_armada_ijintrayek)
{
    if(empty($id_armada_ijintrayek))
    {
        return array();
    }
    else
    {
        $session = $this->session->userdata('login');
        $this->db->select("a.*");
        $this->db->from("ref_armada_ijintrayek a");
        $this->db->where('a.id_armada_ijintrayek', $id_armada_ijintrayek);
        return $this->db->get()->row_array();
    }
}

public function delete_ijintrayek($data)
{
    $session = $this->session->userdata('login');
    $this->db->where('id_perusahaan', $session['id_perusahaan']);
    $this->db->where('id_armada_ijintrayek', $data['id_armada_ijintrayek']);
    $this->db->delete('ref_armada_ijintrayek');
    return $data['id_armada_ijintrayek'];
}




public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

public function combobox_segment()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_segment b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_trayek($id_bu)
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_trayek b");
    $this->db->where('b.id_bu', $id_bu);
    $this->db->where('b.flag_logistik', 0);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_layout()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_layout b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_merek()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_merek b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_layanan()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_layanan b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}


public function combobox_ukuran()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_ukuran b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

public function combobox_warna()
{
    $session = $this->session->userdata('login');
    $this->db->from("ref_warna b");
    $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    $this->db->where('b.active', 1);

    return $this->db->get();
}

function get_info_2($hasil="",$tabel="",$kolom="",$isi=""){
    if($hasil && $hasil!="*") $this->db->select($hasil);
    $this->db->limit(1);
    $this->db->where("$kolom",$isi);
    $exec = $this->db->get($tabel);
    return $hasil && $hasil!="*" ? $exec->row($hasil) : $exec;
}





}
