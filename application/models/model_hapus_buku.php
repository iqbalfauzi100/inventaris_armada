<?php
class Model_hapus_buku extends CI_Model
{
	public function getAllhapus_buku($show=null, $start=null, $cari=null,$id_bu, $tahun)
	{
		$this->db->select("a.id_hapus_buku, a.tahun as tahun_hapus_buku, a.nm_bu, a.cdate as cdate_hapus_buku, b.*");
		$this->db->from("ref_armada_hapus_buku a");
		$this->db->join("ref_armada b","a.id_armada = b.id_armada", 'left');
		$session = $this->session->userdata('login');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){ $this->db->where('a.id_bu', $id_bu); }
        $this->db->where("a.tahun='$tahun' ");

		$this->db->where("(a.nm_bu  LIKE '%".$cari."%' or a.kd_armada  LIKE '%".$cari."%' ) ");
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_hapus_buku($cari = null,$id_bu, $tahun)
	{
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(id_hapus_buku) as recordsFiltered ");
		$this->db->from("ref_armada_hapus_buku");
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){ $this->db->where('id_bu', $id_bu); }
        $this->db->where("tahun='$tahun' ");
		$this->db->where("(nm_bu  LIKE '%".$cari."%' or kd_armada  LIKE '%".$cari."%' ) ");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		$this->db->select(" COUNT(id_hapus_buku) as recordsTotal ");
		$this->db->from("ref_armada_hapus_buku");
		$this->db->where("id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){ $this->db->where('id_bu', $id_bu); }
        $this->db->where("tahun='$tahun' ");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;
	}

	public function insert_hapus_buku($data)
	{
		$this->db->insert('ref_hapus_buku', $data);
		return $this->db->insert_id();
	}

	public function delete_hapus_buku($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_perusahaan', $session['id_perusahaan']);
		$this->db->where('id_hapus_buku', $data['id_hapus_buku']);
		$this->db->update('ref_hapus_buku', array('active' => '2'));
		return $data['id_hapus_buku'];
	}

	public function update_hapus_buku($data)
	{
		$session = $this->session->userdata('login');
		$this->db->where('id_perusahaan', $session['id_perusahaan']);
		$this->db->where('id_hapus_buku', $data['id_hapus_buku']);
		$this->db->where("active != '2' ");
		$this->db->update('ref_hapus_buku', $data);
		return $data['id_hapus_buku'];
	}

	public function get_hapus_buku_by_id($id_hapus_buku)
	{
		if(empty($id_hapus_buku))
		{
			return array();
		}
		else
		{
			$session = $this->session->userdata('login');
			$this->db->from("ref_hapus_buku a");
			$this->db->where('a.id_perusahaan', $session['id_perusahaan']);
			$this->db->where('a.id_hapus_buku', $id_hapus_buku);
			$this->db->where("a.active != '2' ");
			return $this->db->get()->row_array();
		}
	}

	public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

	public function combobox_tahun()
	{
		$session = $this->session->userdata('login');
		$this->db->select("tahun");
		$this->db->from("ref_tahun");
		$this->db->where('active',1);
		$this->db->order_by('tahun', 'DESC');
		return $this->db->get();
	}

}
