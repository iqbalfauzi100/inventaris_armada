<?php
class Model_home_detail extends CI_Model
{

	//STNK
	public function getAlldata_grafik_stnk_detail($show=null, $start=null, $cari=null,$id_bu,$kategori){

		$this->db->select("a.id_armada, a.kd_armada, a.plat_armada, a.kd_segment, a.status_armada,b.tgl_exp_stnk,b.cdate, c.nm_bu, c.id_divre");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk,cdate from ref_armada_stnk where masa=1 GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("c.id_bu in (3, 7, 8, 17)");
		$session = $this->session->userdata('login');
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) IS NULL");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_stnk_detail($cari = null,$id_bu,$kategori){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}


//KEUR
	public function getAlldata_grafik_keur_detail($show=null, $start=null, $cari=null,$id_bu,$kategori){

		$this->db->select("a.id_armada, a.kd_armada, a.plat_armada, a.kd_segment, a.status_armada,b.tgl_exp,b.cdate, c.nm_bu, c.id_divre");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp,cdate from ref_armada_keur  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$session = $this->session->userdata('login');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_keur_detail($cari = null,$id_bu,$kategori){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}


//IJIN TRAYEK
	public function getAlldata_grafik_ijintrayek_detail($show=null, $start=null, $cari=null,$id_bu,$kategori){

		$this->db->select("a.id_armada, a.kd_armada, a.plat_armada, a.kd_segment, a.id_bu, a.status_armada,b.tgl_exp,b.cdate, c.nm_bu, c.id_divre");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp,cdate from ref_armada_ijintrayek  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$session = $this->session->userdata('login');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_ijintrayek_detail($cari = null,$id_bu,$kategori){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("(SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek  GROUP BY id_armada) b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_bu c", "a.id_bu = c.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30");
		}else if($kategori == 2){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30");
		}else if($kategori == 3){
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0");
		}else {
			$this->db->where("TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) IS NULL");
		}
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}






	//ASURANSI
	public function getAlldata_grafik_asuransi_detail($show=null, $start=null, $cari=null, $tahun ,$id_bu,$kategori){

		$this->db->select("a.id_armada, a.kd_armada, a.plat_armada, a.kd_segment, a.id_bu, a.status_armada,b.no_certificate,b.cdate, d.nm_bu, d.id_divre");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada and b.active=1","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active =1 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");

		$session = $this->session->userdata('login');

		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");

		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_asuransi_detail($cari = null, $tahun ,$id_bu,$kategori){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada and b.active=1","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active =1 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada and b.active=1","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active =1 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}





	//KLAIM
	public function getAlldata_grafik_klaim_detailX($show=null, $start=null, $cari=null, $tahun ,$id_bu,$kategori,$status){

		$this->db->select("a.id_armada, a.kd_armada, a.plat_armada, a.kd_segment, a.id_bu, a.status_armada,
			b.no_certificate,b.no_polis, b.waktu_kejadian ,b.estimasi_perbaikan ,b.klaim_disetujui ,b.tjh_3 ,b.own_risk ,b.nett_klaim ,b.status,b.cdate, 
			d.nm_bu, d.id_divre");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$session = $this->session->userdata('login');

		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		if($status==0){
		}else if($status==1){
			$this->db->where("(b.status IS NULL OR b.status='PROGRESS') ");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");

		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_klaim_detailX($cari = null, $tahun ,$id_bu,$kategori,$status){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(a.id_armada) as recordsFiltered ");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		if($status==0){
		}else if($status==1){
			$this->db->where("(b.status IS NULL OR b.status='PROGRESS') ");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(a.id_armada) as recordsTotal ");
		$this->db->from("ref_armada a");
		$this->db->join("ref_asuransi_detail b", "a.id_armada = b.id_armada","left");
		$this->db->join("ref_asuransi c", "b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun' ","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");

		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}
		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($status==0){
		}else if($status==1){
			$this->db->where("(b.status IS NULL OR b.status='PROGRESS')");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("c.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("c.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("c.id_jenis_asuransi is null");
		}

		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' ) ");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.status_armada <> 'SEWA'");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}



	//KLAIM
	public function getAlldata_grafik_klaim_detail($show=null, $start=null, $cari=null, $tahun ,$id_bu,$kategori,$status){

		$this->db->select("a.id_bu, 
			b.id_armada, b.kd_armada, b.plat_armada, b.no_certificate,b.no_polis, b.waktu_kejadian ,b.estimasi_perbaikan ,b.klaim_disetujui ,b.tjh_3 ,b.own_risk ,b.nett_klaim ,b.status,b.cdate, 
			d.nm_bu, d.id_divre");
		$this->db->from("ref_asuransi a");
		$this->db->join("ref_asuransi_detail b", "a.id_asuransi = b.id_asuransi","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$session = $this->session->userdata('login');

		$this->db->where('a.tahun', $tahun);
		$this->db->where('a.active <>2 ');

		$this->db->where("a.id_bu in (3, 7, 8, 17)");

		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		if($status==0){
			$this->db->where("b.status in ('SELESAI','PROGRESS') ");
		}else if($status==1){
			$this->db->where("(b.status='PROGRESS') ");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("a.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("a.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("a.id_jenis_asuransi is null");
		}

		$this->db->where("(b.kd_armada  LIKE '%".$cari."%' ) ");

		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}

	public function get_count_data_grafik_klaim_detail($cari = null, $tahun ,$id_bu,$kategori,$status){
		
		$count = array();
		$session = $this->session->userdata('login');

		$this->db->select(" COUNT(b.id_armada) as recordsFiltered ");
		$this->db->from("ref_asuransi a");
		$this->db->join("ref_asuransi_detail b", "a.id_asuransi = b.id_asuransi","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$session = $this->session->userdata('login');

		$this->db->where('a.tahun', $tahun);
		$this->db->where('a.active <>2 ');

		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		if($status==0){
			$this->db->where("b.status in ('SELESAI','PROGRESS') ");
		}else if($status==1){
			$this->db->where("(b.status='PROGRESS') ");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("a.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("a.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("a.id_jenis_asuransi is null");
		}

		$this->db->where("(b.kd_armada  LIKE '%".$cari."%' ) ");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

		
		$this->db->select(" COUNT(b.id_armada) as recordsTotal ");
		$this->db->from("ref_asuransi a");
		$this->db->join("ref_asuransi_detail b", "a.id_asuransi = b.id_asuransi","left");
		$this->db->join("ref_bu d", "a.id_bu = d.id_bu","left");
		$session = $this->session->userdata('login');

		$this->db->where('a.tahun', $tahun);
		$this->db->where('a.active <>2 ');

		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}

		if($status==0){
			$this->db->where("b.status in ('SELESAI','PROGRESS') ");
		}else if($status==1){
			$this->db->where("(b.status='PROGRESS') ");
		}else{
			$this->db->where("b.status ='SELESAI' ");
		}

		// if($tahun<>0){
		// 	$this->db->where('c.tahun', $tahun);
		// }

		if($kategori == 0){
			
		}else if($kategori == 1){
			$this->db->where("a.id_jenis_asuransi",1);
		}else if($kategori == 2){
			$this->db->where("a.id_jenis_asuransi in (2,3)");
		}else if($kategori == 3){
			$this->db->where("a.id_jenis_asuransi is null");
		}

		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

		return $count;

	}


}
