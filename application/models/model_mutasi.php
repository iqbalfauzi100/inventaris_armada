<?php
class Model_mutasi extends CI_Model
{
	public function getAllmutasi($show=null, $start=null, $cari=null)
	{
		$session = $this->session->userdata('login');

		$this->db->select("a.*");
		$this->db->from("ref_armada_mutasi a");
		$this->db->where("(a.kd_armada  LIKE '%".$cari."%' OR a.no_surat  LIKE '%".$cari."%' OR a.tgl_mutasi  LIKE '%".$cari."%' ) ");
		$this->db->where("a.status <> 2");
		$this->db->where("a.id_bu_sebelum in (3, 7, 8, 17)");

		$id_level = array("1", "7","13");
		if(in_array($session['id_level'], $id_level)){ }else{ $this->db->where("a.id_bu_sebelum",$session['id_bu']); };

		$this->db->order_by("cdate","DESC");
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}

		return $this->db->get();
	}
	
	public function get_count_mutasi($cari = null)
	{
		$count = array();
		$session = $this->session->userdata('login');
		
		$this->db->select(" COUNT(id_mutasi) as recordsFiltered ");
		$this->db->from("ref_armada_mutasi");
		$this->db->where("(kd_armada  LIKE '%".$cari."%' OR no_surat  LIKE '%".$cari."%' OR tgl_mutasi  LIKE '%".$cari."%' ) ");
		$this->db->where("status <> 2 ");
		$this->db->where("id_bu_sebelum in (3, 7, 8, 17)");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
		
		$this->db->select(" COUNT(id_mutasi) as recordsTotal ");
		$this->db->from("ref_armada_mutasi");
		$this->db->where("status <> 2 ");
		$this->db->where("id_bu_sebelum in (3, 7, 8, 17)");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
		
		return $count;
	}
	
	public function insert_mutasi($data)
	{
		$this->db->insert('ref_armada_mutasi', $data);
		return $this->db->insert_id();
	}

	public function delete_mutasi($data)
	{
		$this->db->where('id_mutasi', $data['id_mutasi']);
		$this->db->update('ref_armada_mutasi', array('status' => '2'));
		return $data['id_mutasi'];
	}

	public function update_data_armada($data)
	{
		$this->db->where('id_armada', $data['id_armada']);
		$this->db->update('ref_armada', array('id_bu' => $data['id_bu_sesudah']) );
		return $data['id_armada'];
	}
	
	public function update_mutasi($data)
	{
		$this->db->where('id_mutasi', $data['id_mutasi']);
		$this->db->update('ref_armada_mutasi', $data);
		return $data['id_mutasi'];
	}
	
	public function get_mutasi_by_id($id_mutasi)
	{
		if(empty($id_mutasi))
		{
			return array();
		}
		else
		{
			$this->db->from("ref_armada_mutasi a");
			$this->db->where('a.id_mutasi', $id_mutasi);
			return $this->db->get()->row_array();
		}
	}

	public function combobox_bu()
  {
    $session = $this->session->userdata('login');
    // $this->db->from("ref_bu_access b");
    $this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
    $this->db->where("a.id_bu in (3, 7, 8, 17)");
    // $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
    // $this->db->where('b.id_user', $session['id_user']);
    // $this->db->where('a.active', 1);

    return $this->db->get();
  }

	public function combobox_cabang()
	{
		$this->db->from("ref_bu a");
		$this->db->where('a.active',1);
		 $this->db->where("a.id_bu in (3, 7, 8, 17)");
		return $this->db->get();
	}

	public function combobox_armada($id_bu){
		$this->db->from("ref_armada a");
		$this->db->where('a.id_bu',$id_bu);
		$this->db->where('a.active in (0,1,3)');
		return $this->db->get();
		
	}

	public function change_active($where, $data)
	{
		$this->db->update("ref_armada_mutasi", $data, $where);
		return $this->db->affected_rows();
	}

}
