<?php

class Model_api extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function databus($kd_armada)
    {

        $this->db->select('a.id_lmb, a.rit, a.kd_armada, a.nm_driver,a.kd_segmen,a.nm_layanan, b.kd_trayek, b.nm_point_awal, b.nm_point_akhir,b.kd_point_awal,b.kd_point_akhir, c.setting_ritase
            ');
        $this->db->from('tr_lmb a');
        $this->db->join('ref_trayek b','a.id_trayek = b.id_trayek', 'left');
        $this->db->join('ref_segment c','a.kd_segmen = c.kd_segment', 'left');
        $this->db->where('a.kd_armada', $kd_armada);
        $this->db->where('a.active', 1);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }
    }

    public function UpdateUser($where, $data)
    {
        $this->db->update('ref_user', $data, $where);
        return $this->db->affected_rows();
    }


    public function databus_rit($kd_armada){
        $this->db->select('a.id_lmb, a.rit, a.kd_armada, a.nm_driver,a.kd_segmen,a.nm_layanan, b.kd_trayek, b.nm_point_awal, b.nm_point_akhir,b.kd_point_awal,b.kd_point_akhir, c.setting_ritase,
            (SELECT max(rit) FROM tr_rit WHERE id_lmb = a.id_lmb and type_rit=1) trip');
        $this->db->from('tr_lmb a');
        $this->db->join('ref_trayek b','a.id_trayek = b.id_trayek', 'left');
        $this->db->join('ref_segment c','a.kd_segmen = c.kd_segment', 'left');
        $this->db->where('a.kd_armada', $kd_armada);
        $this->db->where('a.active', 1);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $trip   = $query->row("trip");
            $id_lmb = $query->row("id_lmb");

            if($trip<=1){
                $result = $query->result();
                return $result;
            }else{
                $result = $this->db->query("
                    SELECT a.id_lmb, a.rit, a.type_rit, a.kd_armada, a.kd_trayek, b.nm_driver, b.kd_segmen, b.nm_layanan, (a.asal) kd_point_awal, (a.tujuan) kd_point_akhir, (c.nm_point) nm_point_awal, (d.nm_point) nm_point_akhir,
                    e.setting_ritase
                    FROM
                    tr_rit a
                    LEFT JOIN tr_lmb b ON a.id_lmb = b.id_lmb
                    LEFT JOIN ref_point c ON a.asal = c.kd_point
                    LEFT JOIN ref_point d ON a.tujuan = d.kd_point 
                    LEFT JOIN ref_segment e ON e.kd_segment = b.kd_segmen 
                    WHERE
                    a.kd_armada = '$kd_armada' 
                    AND a.id_lmb = '$id_lmb'
                    AND a.active = 1 
                    and a.rit=(SELECT max(rit) from tr_rit where id_lmb = '$id_lmb' and type_rit=1)
                    and a.type_rit=1
                    LIMIT 1;
                    ");
                return $result->result();

            }

        } else {
            return false;
        }
    }

    public function databuscabang($id_bu)
    {

        $this->db->select('a.id_lmb, a.rit, a.kd_armada, a.nm_driver, b.kd_trayek, b.nm_point_awal, b.nm_point_akhir, 1 as status');
        $this->db->from('tr_lmb a');
        $this->db->join('ref_trayek b','a.id_trayek = b.id_trayek', 'left');
        $this->db->where('a.id_bu', 3);
        $this->db->where('a.active', 1);
        $query = $this->db->get();

            // if ($query->num_rows() == 1) {
        $result = $query->result_array();
        return $result;
            // } else {
                // return false;
            // }
    }

    public function insert_ritase($data)
    {
        $this->db->insert_batch('tr_rit', $data);
        return $this->db->insert_id();
    }

    public function getAllritlmb($id_lmb)
    {
        $this->db->select("a.*, b.nm_user, c.nm_driver");
        $this->db->from("tr_rit a");
        $this->db->join('ref_user b','a.cuser = b.id_user', 'left');
        $this->db->join('tr_lmb c','a.id_lmb = c.id_lmb', 'left');
        $this->db->where("a.id_lmb", $id_lmb);
        return $this->db->get();
    }

    public function getAllritlmbuser($id_lmb, $rit)
    {
        $this->db->select("a.*, b.nm_user, c.nm_driver,(e.nm_point) nm_point_ppa");
        $this->db->from("tr_rit a");
        $this->db->join('tr_lmb c','a.id_lmb = c.id_lmb', 'left');
        $this->db->join('ref_user b','a.cuser = b.id_user', 'left');
        $this->db->join('ref_point e','e.id_point = a.id_point_ppa', 'left');
        $this->db->where("a.id_lmb", $id_lmb);
        $this->db->where("a.rit", $rit);
        $this->db->order_by("a.type_rit", "ASC");
        return $this->db->get();
    }

    public function getAllritlmbdriver($nik, $tanggal)
    {
        $this->db->select("a.*, b.nm_user, c.nm_driver");
        $this->db->from("tr_rit a");
        $this->db->join('tr_lmb c','a.id_lmb = c.id_lmb', 'left');
        $this->db->join('ref_user b','a.cuser = b.id_user', 'left');
        $this->db->join('hris.ref_pegawai d','c.id_driver = d.id_pegawai', 'left');
        $this->db->where("d.nik_pegawai", $nik);
        $this->db->where("a.tgl_lmb", $tanggal);
            // $this->db->where("date(c.jam_in)", $tanggal);
        return $this->db->get();
    }

    public function getAllritlmbusergroup($id_user, $tanggal)
    {
            // $this->db->select("a.*, b.nm_user, c.nm_driver");
        $this->db->select("a.rit, a.id_lmb,a.kd_segmen,  a.kd_armada, sum(pnp) as pnp, date(c.cdate) as tgl, c.nm_driver, c.id_driver");
        $this->db->from("tr_rit a");
        $this->db->join('tr_lmb c','a.id_lmb = c.id_lmb', 'left');
            // $this->db->join('ref_user b','a.cuser = b.id_user', 'left');
        $this->db->where("a.cuser", $id_user);
        $this->db->where("a.tgl_lmb", $tanggal);
            // $this->db->where("date(a.cdate)", $tanggal);
        $this->db->group_by("a.id_lmb, a.kd_armada, a.rit, date(c.cdate), a.id_bu");
        $this->db->order_by("a.cdate","desc");
        return $this->db->get();
    }

    public function getlmbdriver($id_driver, $tanggal)
    {
        $this->db->select("a.*, b.nm_user");
        $this->db->from("tr_lmb a");
        $this->db->join('ref_user b','a.cuser = b.id_user', 'left');
        $this->db->where("a.cuser", $id_user);
        $this->db->where("a.tgl_lmb", $tanggal);
            // $this->db->where("date(a.cdate)", $tanggal);
        return $this->db->get();
    }

    public function getAlltrayek($id_bu, $kd_trayek)
    {
        $this->db->select("a.*");
        $this->db->from("ref_trayek a");
        $this->db->where("a.id_bu", $id_bu);
        // $this->db->where_not_in('a.kd_trayek', $kd_trayek);
        return $this->db->get();
    }
	
	function mulai_cuci($data){
		$this->db->insert('ref_cucibus', $data);
		$id = $this->db->insert_id();
		if($this->db->affected_rows() > 0){
			$hasil = array(
				  'status'=> 1,
				  'id_cucibus'=> $id,
				);
		}else{
			$hasil = array(
				  'status'=> 0,
				  'id_cucibus'=> '',
				);
		}
		return $hasil;
		
	}
	
	function selesai_cuci($data){
		$this->db->where('id_cucibus', $data['id_cucibus']);
		$this->db->update('ref_cucibus', $data);
		if($this->db->affected_rows() > 0){
			$hasil = array(
				  'status'=> 1,
				);
		}else{
			$hasil = array(
				  'status'=> 0,
				);
		}
		return $hasil;
		
	}
	
	
	
	
}
