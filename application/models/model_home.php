<?php
class Model_home extends CI_Model
{
	public function UpdateUser($id_user, $data)
	{
		$this->db->where('id_user', $id_user);
		$this->db->update('ref_user', $data);
	}

	public function combobox_tahun()
	{
		$session = $this->session->userdata('login');
		$this->db->select("tahun");
		$this->db->from("ref_tahun");
		$this->db->where('active',1);
		$this->db->order_by('tahun', 'DESC');
		return $this->db->get();
	}

	public function combobox_bu()
	{
		$session = $this->session->userdata('login');
		// $this->db->from("ref_bu_access b");
		$this->db->from("ref_bu a", "b.id_bu = a.id_bu", "left");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		// $this->db->where('b.id_perusahaan', $session['id_perusahaan']);
		// $this->db->where('b.id_user', $session['id_user']);
		// $this->db->where('a.active', 1);

		return $this->db->get();
	}

	public function combobox_segmen(){
		$this->db->from ("ref_segment a");
		$this->db->where('a.active', 1);
		return $this->db->get();
	}

	public function get_count_armada(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where("a.status_armada <> 'SEWA' ");

		// if($session['id_level'] == 1 || $session['id_level'] == 7){}else{
		// 	$this->db->where('a.id_bu', $session['id_bu'] );
		// }
		return $this->db->get();

	}

	public function get_count_armada_grafik(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where("a.active in (0,3)");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where("a.status_armada <> 'SEWA' ");

		// if($session['id_level'] == 1 || $session['id_level'] == 7){}else{
		// 	$this->db->where('a.id_bu', $session['id_bu'] );
		// }
		return $this->db->get();

	}

	public function get_count_armada_damri_ua(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where('a.active = 3');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where("a.status_armada not in ('SEWA','KSO') ");
		return $this->db->get();

	}


	public function get_count_armada_usul_afkir(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where('a.active = 3');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		return $this->db->get();

	}

	public function get_count_armada_kso(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where('a.status_armada', 'KSO');
		$this->db->where('a.active in (0,3)');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");

		// if($session['id_level'] == 1 || $session['id_level'] == 7){}else{
		// 	$this->db->where('a.id_bu', $session['id_bu'] );
		// }
		return $this->db->get();

	}

	public function get_count_armada_kso_ua(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where('a.active = 3');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where('a.status_armada', 'KSO');
		return $this->db->get();

	}

	public function get_count_armada_sewa(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where('a.status_armada', 'SEWA');
		$this->db->where('a.active in (0,3)');

		return $this->db->get();

	}

	public function get_count_armada_sewa_ua(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.kd_armada) jumlah_armada");
		$this->db->from("ref_armada a");
		$this->db->where('a.active = 3');
		$this->db->where("a.id_bu in (3, 7, 8, 17)");
		$this->db->where('a.status_armada', 'SEWA');
		return $this->db->get();

	}

	public function get_count_jumlah_cabang_sewa(){
		$session = $this->session->userdata('login');

		$this->db->select("COUNT(a.id_bu) jumlah_cabang");
		$this->db->from("(SELECT count(id_bu)id_bu from ref_armada WHERE status_armada='SEWA' and id_bu in (3, 7, 8, 17) GROUP BY id_bu) a");
		return $this->db->get();

	}

	public function get_data_grafik_usia($divisi){
		$this->db->select("y.nm_bu as cabang, sum(x.satu) as satu, sum(x.dua) as dua, sum(x.tiga) as tiga, sum(x.empat) as empat, sum(x.lima) as lima");
		$this->db->from("(
			select id_bu, count(kd_armada) as satu, '' as dua, '' as tiga, '' as empat, '' as lima from ref_armada where tahun_armada < year(current_date()) and (year(current_date())-tahun_armada) < 6 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, count(kd_armada) as dua, '' as tiga, '' as empat, '' as lima from ref_armada where tahun_armada < year(current_date()) and (year(current_date())-tahun_armada) > 5 and (year(current_date())-tahun_armada) < 11 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, count(kd_armada) as tiga, '' as empat, '' as lima from ref_armada where tahun_armada < year(current_date()) and (year(current_date())-tahun_armada) > 10 and (year(current_date())-tahun_armada) < 16 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, count(kd_armada) as empat, '' as lima from ref_armada where tahun_armada < year(current_date()) and (year(current_date())-tahun_armada) > 15 and (year(current_date())-tahun_armada) < 21 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, '' as empat, count(kd_armada) as lima from ref_armada where tahun_armada < year(current_date()) and (year(current_date())-tahun_armada) > 20 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x");
		$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
		if($divisi == 0 || $divisi == 5){
		}else {
			$this->db->where("y.id_divre = $divisi");
		}
		$this->db->order_by("y.nm_bu");
		return $this->db->get();
	}

	public function get_data_grafik_usia_tahun_perolehan($divisi){
		$this->db->select("y.nm_bu as cabang, sum(x.satu) as satu, sum(x.dua) as dua, sum(x.tiga) as tiga, sum(x.empat) as empat, sum(x.lima) as lima");
		$this->db->from("(
			select id_bu, count(kd_armada) as satu, '' as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) < 6 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, count(kd_armada) as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 5 and (year(current_date())-tahun_perolehan) < 11 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, count(kd_armada) as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 10 and (year(current_date())-tahun_perolehan) < 16 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, count(kd_armada) as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 15 and (year(current_date())-tahun_perolehan) < 21 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, '' as empat, count(kd_armada) as lima from ref_armada where (year(current_date())-tahun_perolehan) > 20 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x");
		$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
		if($divisi == 0 || $divisi == 5){
		}else {
			$this->db->where("y.id_divre = $divisi");
		}
		$this->db->order_by("y.nm_bu");
		return $this->db->get();
	}


	public function getAlldata_grafik_tahun_perolehan($show=null, $start=null, $cari=null, $divisi){
		$this->db->select("y.nm_bu as cabang, y.id_divre, sum(x.satu) as satu, sum(x.dua) as dua, sum(x.tiga) as tiga, sum(x.empat) as empat, sum(x.lima) as lima, SUM(x.satu+x.dua+x.tiga+x.empat+x.lima) total");
		$this->db->from("(
			select id_bu, count(kd_armada) as satu, '' as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) < 6 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, count(kd_armada) as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 5 and (year(current_date())-tahun_perolehan) < 11 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, count(kd_armada) as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 10 and (year(current_date())-tahun_perolehan) < 16 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, count(kd_armada) as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 15 and (year(current_date())-tahun_perolehan) < 21 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, '' as empat, count(kd_armada) as lima from ref_armada where (year(current_date())-tahun_perolehan) > 20 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x");
		$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
		if($divisi == 0 || $divisi == 5){
		}else {
			$this->db->where("y.id_divre = $divisi");
		}
		$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
		$this->db->group_by("x.id_bu");
		$this->db->order_by("y.nm_bu");
		if ($show == null && $start == null) {
		} else {
			$this->db->limit($show, $start);
		}
		return $this->db->get();
	}
	
	public function get_count_data_grafik_tahun_perolehan($search = null, $divisi){
		if($divisi == 0 || $divisi == 5){
			$divre = '';
		} else {
			$divre = "where y.id_divre = '$divisi'";
		}
		$count = array();
		$session = $this->session->userdata('login');
		
		$this->db->select("count(cabang) as recordsFiltered");
		$this->db->from("(
			select y.nm_bu as cabang, sum(x.satu) as satu, sum(x.dua) as dua, sum(x.tiga) as tiga, sum(x.empat) as empat, sum(x.lima) as lima from (
			select id_bu, count(kd_armada) as satu, '' as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) < 6 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, count(kd_armada) as dua, '' as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 5 and (year(current_date())-tahun_perolehan) < 11 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, count(kd_armada) as tiga, '' as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 10 and (year(current_date())-tahun_perolehan) < 16 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, count(kd_armada) as empat, '' as lima from ref_armada where (year(current_date())-tahun_perolehan) > 15 and (year(current_date())-tahun_perolehan) < 21 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, '' as empat, count(kd_armada) as lima from ref_armada where (year(current_date())-tahun_perolehan) > 20 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x
			left join ref_bu y on x.id_bu = y.id_bu
			$divre
			group by x.id_bu
			order by y.nm_bu
			) a");
		$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
		
		$this->db->select("count(cabang) as recordsTotal ");
		$this->db->from("(
			select y.nm_bu as cabang, sum(x.satu) as satu, sum(x.dua) as dua, sum(x.tiga) as tiga, sum(x.empat) as empat, sum(x.lima) as lima from (
			select id_bu, count(kd_armada) as satu, '' as dua, '' as tiga, '' as empat, '' as lima from ref_armada where tahun_perolehan < year(current_date()) and (year(current_date())-tahun_perolehan) < 6 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, count(kd_armada) as dua, '' as tiga, '' as empat, '' as lima from ref_armada where tahun_perolehan < year(current_date()) and (year(current_date())-tahun_perolehan) > 5 and (year(current_date())-tahun_perolehan) < 11 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, count(kd_armada) as tiga, '' as empat, '' as lima from ref_armada where tahun_perolehan < year(current_date()) and (year(current_date())-tahun_perolehan) > 10 and (year(current_date())-tahun_perolehan) < 16 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, count(kd_armada) as empat, '' as lima from ref_armada where tahun_perolehan < year(current_date()) and (year(current_date())-tahun_perolehan) > 15 and (year(current_date())-tahun_perolehan) < 21 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, '' as satu, '' as dua, '' as tiga, '' as empat, count(kd_armada) as lima from ref_armada where tahun_perolehan < year(current_date()) and (year(current_date())-tahun_perolehan) > 20 and active in (0,3) and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x
			left join ref_bu y on x.id_bu = y.id_bu
			$divre
			group by x.id_bu
			order by y.nm_bu
			) a");
		$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
		
		return $count;
	}

	
	public function get_data_grafik_merek($divisi){
		$this->db->select("y.nm_bu as cabang, sum(x.beijing) as beijing, sum(x.daihatsu) as daihatsu, sum(x.gdragon) as gdragon, sum(x.hino) as hino, sum(x.hyundai) as hyundai, sum(x.inobus) as inobus, sum(x.isuzu) as isuzu, sum(x.kinglong) as kinglong, sum(x.mercy) as mercy, sum(x.mitsubishi) as mitsubishi, sum(x.nissan) as nissan, sum(x.toyota) as toyota, sum(x.yutoong) as yutoong, sum(x.zhongtong) as zhongtong, sum(x.aaibus) as aaibus");
		$this->db->from("(
			select id_bu, id_merek, nm_merek, count(id_merek) as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '1' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, count(id_merek) as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '2' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, count(id_merek) as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '3' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, count(id_merek) as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '4' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, count(id_merek) as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '5' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, count(id_merek) as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '6' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, count(id_merek) as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '7' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, count(id_merek) as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '8' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, count(id_merek) as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '9' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, count(id_merek) as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '10' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, count(id_merek) as nissan, '' as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '11' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, count(id_merek) as toyota, '' as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '12' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, count(id_merek) as yutoong, '' as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '13' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, count(id_merek) as zhongtong, '' as aaibus from ref_armada where active in (0,3) and id_merek = '14' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			union all
			select id_bu, id_merek, nm_merek, '' as beijing, '' as daihatsu, '' as gdragon, '' as hino, '' as hyundai, '' as inobus, '' as isuzu, '' as kinglong, '' as mercy, '' as mitsubishi, '' as nissan, '' as toyota, '' as yutoong, '' as zhongtong, count(id_merek) as aaibus from ref_armada where active in (0,3) and id_merek = '16' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
			) x");
$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
if($divisi == 0 || $divisi == 5){
}else {
	$this->db->where("y.id_divre = $divisi");
}
$this->db->order_by("y.nm_bu");
return $this->db->get();
}


public function get_data_grafik_stnk($divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.lebih30) as lebih30, sum(x.kurang30) as kurang30, sum(x.habis_masa) as habis_masa");
	$this->db->from("(

		SELECT id_bu, SUM(lebih30) as lebih30, kurang30, habis_masa
		FROM(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30 and a.status_armada<>'SEWA'  
		and id_bu in (3, 7, 8, 17) GROUP BY a.id_bu
		)a


		UNION ALL

		SELECT id_bu, lebih30, SUM(kurang30) as kurang30, habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30 and a.status_armada<>'SEWA'  
		and id_bu in (3, 7, 8, 17) GROUP BY a.id_bu
		)a

		UNION ALL

		SELECT id_bu, lebih30, kurang30, SUM(habis_masa) as habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0 and a.status_armada<>'SEWA'  
		and id_bu in (3, 7, 8, 17) GROUP BY a.id_bu
		)a

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}


public function getAlldata_grafik_stnk($show=null, $start=null, $cari=null, $divisi){
	$this->db->select("y.id_bu, y.id_divre, y.nm_bu as cabang, IFNULL(sum(x.lebih30),0) as lebih30, IFNULL(sum(x.kurang30),0) as kurang30, IFNULL(sum(x.habis_masa),0) as habis_masa, 
		COALESCE((SELECT COUNT(id_armada) from ref_armada WHERE id_bu=y.id_bu and status_armada<>'SEWA' and active in (0,3)),0)total_armada");
	$this->db->from("(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk)  >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk)  BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'right');
	$this->db->where("y.id_bu in (3,7,8,17)");
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
	$this->db->where("y.active = 1 and y.id_bu not BETWEEN 60 and 65");
	$this->db->group_by("y.id_bu");
	$this->db->order_by("y.nm_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_stnk($search = null, $divisi){
	if($divisi == 0 || $divisi == 5){
		$divre = '';
	} else {
		$divre = "where y.id_divre = '$divisi' and y.id_bu in (3,7,8,17)";
	}
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(cabang) as recordsFiltered");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		) x");
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(cabang) as recordsTotal ");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp_stnk) tgl_exp_stnk from ref_armada_stnk where masa=1 GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp_stnk) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) 
		GROUP BY a.id_bu
		) x");
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}

public function get_data_grafik_keur($divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.lebih30) as lebih30, sum(x.kurang30) as kurang30, sum(x.habis_masa) as habis_masa");
	$this->db->from("(

		SELECT id_bu, SUM(lebih30) as lebih30, kurang30, habis_masa
		FROM(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp FROM ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a

		UNION ALL

		SELECT id_bu, lebih30, SUM(kurang30) as kurang30, habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp FROM ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a

		UNION ALL

		SELECT id_bu, lebih30, kurang30, SUM(habis_masa) as habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp FROM ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}


public function getAlldata_grafik_keur($show=null, $start=null, $cari=null, $divisi){
	$this->db->select("y.id_bu, y.id_divre, y.nm_bu as cabang, IFNULL(sum(x.lebih30),0) as lebih30, IFNULL(sum(x.kurang30),0) as kurang30, IFNULL(sum(x.habis_masa),0) as habis_masa, COALESCE((SELECT COUNT(id_armada) from ref_armada WHERE id_bu=y.id_bu and status_armada<>'SEWA' and active in (0,3)),0)total_armada");
	$this->db->from("(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp)  >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp)  BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'right');
	$this->db->where("y.id_bu in (3,7,8,17)");
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
	$this->db->where("y.active = 1 and y.id_bu not BETWEEN 60 and 65");
	$this->db->group_by("y.id_bu");
	$this->db->order_by("y.nm_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_keur($search = null, $divisi){
	if($divisi == 0 || $divisi == 5){
		$divre = '';
	} else {
		$divre = "where y.id_divre = '$divisi'  and y.id_bu in (3,7,8,17)";
	}
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(cabang) as recordsFiltered");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(cabang) as recordsTotal ");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_keur GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}

public function get_data_grafik_ijintrayek($divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.lebih30) as lebih30, sum(x.kurang30) as kurang30, sum(x.habis_masa) as habis_masa");
	$this->db->from("(

		SELECT id_bu, SUM(lebih30) as lebih30, kurang30, habis_masa
		FROM(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,tgl_exp FROM ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a
		
		UNION ALL

		SELECT id_bu, lebih30, SUM(kurang30) as kurang30, habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,tgl_exp FROM ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a

		UNION ALL

		SELECT id_bu, lebih30, kurang30, SUM(habis_masa) as habis_masa
		FROM(
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,tgl_exp FROM ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		)a

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}


public function getAlldata_grafik_ijintrayek($show=null, $start=null, $cari=null, $divisi){
	$this->db->select("y.id_bu, y.id_divre, y.nm_bu as cabang, IFNULL(sum(x.lebih30),0) as lebih30, IFNULL(sum(x.kurang30),0) as kurang30, IFNULL(sum(x.habis_masa),0) as habis_masa, COALESCE((SELECT COUNT(id_armada) from ref_armada WHERE id_bu=y.id_bu and status_armada<>'SEWA' and active in (0,3)),0)total_armada");
	$this->db->from("(
		SELECT a.id_bu, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp)  >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp)  BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'right');
	$this->db->where("y.id_bu in (3,7,8,17)");
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
	$this->db->where("y.active = 1 and y.id_bu not BETWEEN 60 and 65");
	$this->db->group_by("y.id_bu");
	$this->db->order_by("y.nm_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_ijintrayek($search = null, $divisi){
	if($divisi == 0 || $divisi == 5){
		$divre = '';
	} else {
		$divre = "where y.id_divre = '$divisi'  and y.id_bu in (3,7,8,17)";
	}
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(cabang) as recordsFiltered");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(cabang) as recordsTotal ");
	$this->db->from("(
		SELECT a.id_bu as cabang, COUNT(a.id_armada) as lebih30, '' as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) >30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, COUNT(a.id_armada) as kurang30, '' habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) BETWEEN 0 AND 30 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		UNION ALL
		SELECT a.id_bu, '' as lebih30, '' as kurang30, COUNT(a.id_armada) habis_masa
		FROM ref_armada a
		LEFT JOIN (SELECT id_armada,max(tgl_exp) tgl_exp from ref_armada_ijintrayek GROUP BY id_armada)b on a.id_armada=b.id_armada
		WHERE a.active in (0,3) AND TIMESTAMPDIFF(DAY,(select CURRENT_DATE),tgl_exp) <0 and a.status_armada<>'SEWA' and id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}


public function get_data_grafik_outstanding($tahun,$divisi){
	if($divisi == 0 || $divisi == 5){
		$divre = '';
	} else {
		$divre = "and b.id_divre = '$divisi'";
	}

	$data = $this->db->query("
		SELECT a.id_bu, IFNULL(SUM(a.nilai_premi),0) as nilai_premi, IFNULL(SUM(a.nilai_bayar),0) as nilai_bayar, IFNULL(SUM(a.nilai_outstanding),0) as nilai_outstanding
		FROM ref_asuransi a 
		LEFT JOIN ref_bu b on a.id_bu=b.id_bu
		WHERE a.active !=2 and a.tahun='$tahun' and b.id_bu in (3, 7, 8, 17) $divre
		");
	return $data;
}

public function getAlldata_grafik_outstanding($show=null, $start=null, $cari=null, $tahun, $id_bu){
	$this->db->select("y.id_divre, y.nm_bu as cabang, x.* ");
	
	// $this->db->from("(
	// 	SELECT a.id_bu, IFNULL(SUM(a.nilai_premi),0) as nilai_premi, IFNULL(SUM(a.nilai_bayar),0) as nilai_bayar, IFNULL(SUM(a.nilai_outstanding),0) as nilai_outstanding
	// 	FROM ref_asuransi a WHERE a.active !=2 and a.tahun='$tahun'
	// 	GROUP BY a.id_bu
	// 	) x");

	$this->db->from("ref_asuransi x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	$this->db->where('x.tahun',$tahun);
	if($id_bu<>0){ $this->db->where('y.id_bu',$id_bu); }
	$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
	$this->db->where("y.active = 1 and y.id_bu in (3, 7, 8, 17)");
	$this->db->order_by("y.id_divre");
	$this->db->order_by("y.id_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_outstanding($search = null, $tahun, $id_bu){
	
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(y.id_bu) as recordsFiltered");
	$this->db->from("ref_asuransi x");
	$this->db->join("ref_bu y", "x.id_bu = y.id_bu","left");
	$this->db->where('x.tahun',$tahun);
	$this->db->where("y.id_bu in (3, 7, 8, 17)");
	if($id_bu<>0){ $this->db->where('y.id_bu',$id_bu); }
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(y.id_bu) as recordsTotal");
	$this->db->from("ref_asuransi x");
	$this->db->join("ref_bu y", "x.id_bu = y.id_bu","left");
	$this->db->where('x.tahun',$tahun);
	$this->db->where("y.id_bu in (3, 7, 8, 17)");
	if($id_bu<>0){ $this->db->where('y.id_bu',$id_bu); }
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}





public function get_data_grafik_asuransi($divisi, $tahun){
	$this->db->select("y.nm_bu as cabang, sum(x.pengajuan_cabang) as pengajuan_cabang, sum(x.jaminan_bank) as jaminan_bank");
	$this->db->from("(

		SELECT a.id_bu, COUNT(a.id_armada) as pengajuan_cabang, '' as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi=c.id_asuransi
		WHERE a.active in (0,3) and a.status_armada<>'SEWA'  AND c.active =1 AND b.active =1  AND c.id_jenis_asuransi=1 AND c.tahun='$tahun' 
		and a.id_bu in (3, 7, 8, 17) GROUP BY a.id_bu

		UNION ALL

		SELECT a.id_bu, '' as pengajuan_cabang, COUNT(a.id_armada) as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi=c.id_asuransi
		WHERE a.active in (0,3) and a.status_armada<>'SEWA'  AND c.active =1 AND b.active =1 AND c.id_jenis_asuransi in (2,3) AND c.tahun='$tahun' 
		and a.id_bu in (3, 7, 8, 17) GROUP BY a.id_bu

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}

public function getAlldata_grafik_asuransi($show=null, $start=null, $cari=null, $tahun, $divisi){
	$this->db->select("y.id_bu, y.id_divre, y.nm_bu as cabang, IFNULL(sum(x.pengajuan_cabang),0) as pengajuan_cabang, IFNULL(sum(x.jaminan_bank),0) as jaminan_bank, COALESCE((SELECT COUNT(id_armada) from ref_armada WHERE id_bu=y.id_bu and status_armada<>'SEWA' and active in (0,3)),0)total_armada");
	$this->db->from("(

		SELECT a.id_bu, COUNT(a.id_armada) as pengajuan_cabang, '' as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi=1 AND c.active =1 AND b.active =1
		and a.id_bu in (3, 7, 8, 17) GROUP BY a.id_bu

		UNION ALL
		SELECT a.id_bu, '' as pengajuan_cabang, COUNT(a.id_armada) as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi in (2,3) AND c.active =1 AND b.active =1
		and a.id_bu in (3, 7, 8, 17) GROUP BY a.id_bu

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'right');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->where("(y.nm_bu LIKE '%".$cari."%')");
	$this->db->where("y.active = 1 and y.id_bu in (3, 7, 8, 17)");
	$this->db->group_by("y.id_bu");
	$this->db->order_by("y.nm_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_asuransi($search = null, $tahun, $divisi){
	
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(y.id_bu) as recordsFiltered");
	$this->db->from("(
		SELECT a.id_bu, COUNT(a.id_armada) as pengajuan_cabang, '' as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi=1 and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu

		UNION ALL
		SELECT a.id_bu, '' as pengajuan_cabang, COUNT(a.id_armada) as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi in (2,3) and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$this->db->join("ref_bu y", "x.id_bu = y.id_bu","left");
	if($divisi == 0 || $divisi == 5){
	} else {
		$this->db->where('y.id_divre',$divisi);
	}
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(y.id_bu) as recordsTotal");
	$this->db->from("(
		SELECT a.id_bu, COUNT(a.id_armada) as pengajuan_cabang, '' as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi=1 and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu

		UNION ALL
		SELECT a.id_bu, '' as pengajuan_cabang, COUNT(a.id_armada) as jaminan_bank
		FROM ref_armada a
		LEFT JOIN ref_asuransi_detail b on a.id_armada=b.id_armada
		LEFT JOIN ref_asuransi c on b.id_asuransi = c.id_asuransi and c.active !=2 AND c.tahun='$tahun'
		WHERE a.active in (0,3) and a.status_armada<>'SEWA' and c.id_jenis_asuransi in (2,3) and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_bu
		) x");
	$this->db->join("ref_bu y", "x.id_bu = y.id_bu","left");
	if($divisi == 0 || $divisi == 5){
	} else {
		$this->db->where('y.id_divre',$divisi);
	}
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}


public function get_data_grafik_klaim($divisi, $tahun){
	$this->db->select("y.nm_bu as cabang, sum(x.progress) as progress, sum(x.selesai) as selesai");
	$this->db->from("(

		SELECT b.id_bu, COUNT(a.id_asuransi_detail) progress, ''selesai
		FROM ref_asuransi_detail a
		LEFT JOIN ref_asuransi b on a.id_asuransi=b.id_asuransi
		WHERE b.active <>2 and (a.status='PROGRESS') AND b.tahun='$tahun' and b.id_bu in (3, 7, 8, 17)

		UNION ALL

		SELECT b.id_bu, ''progress, COUNT(a.id_asuransi_detail) selesai
		FROM ref_asuransi_detail a
		LEFT JOIN ref_asuransi b on a.id_asuransi=b.id_asuransi
		WHERE b.active <>2 and a.status='SELESAI' AND b.tahun='$tahun' and b.id_bu in (3, 7, 8, 17)

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}


public function get_data_grafik_foto_armada($divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.depan_kanan) as depan_kanan, sum(x.depan_kiri) as depan_kiri, sum(x.belakang) as belakang, sum(x.dalam) as dalam, sum(x.gesek_rangka) as gesek_rangka, sum(x.gesek_mesin) as gesek_mesin, sum(x.foto_mesin) as foto_mesin");
	$this->db->from("(

		SELECT a.id_bu,a.id_armada, IF(COUNT(b.id_armada_foto)>0,1,0) depan_kanan, '' depan_kiri,'' belakang,'' dalam, '' gesek_rangka,'' gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='DEPAN KANAN' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada

		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, IF(COUNT(b.id_armada_foto)>0,1,0) depan_kiri,'' belakang,'' dalam, '' gesek_rangka,'' gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='DEPAN KIRI' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada

		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, '' depan_kiri,IF(COUNT(b.id_armada_foto)>0,1,0) belakang,'' dalam, '' gesek_rangka,'' gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='BELAKANG' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada
		
		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, '' depan_kiri,'' belakang,IF(COUNT(b.id_armada_foto)>0,1,0) dalam, '' gesek_rangka,'' gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='DALAM' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada
		
		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, '' depan_kiri,'' belakang,'' dalam, IF(COUNT(b.id_armada_foto)>0,1,0) gesek_rangka,'' gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='GESEK RANGKA' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada
		
		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, '' depan_kiri,'' belakang,'' dalam, '' gesek_rangka,IF(COUNT(b.id_armada_foto)>0,1,0) gesek_mesin,'' foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='GESEK MESIN' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada
		
		UNION ALL

		SELECT a.id_bu,a.id_armada, '' depan_kanan, '' depan_kiri,'' belakang,'' dalam, '' gesek_rangka,'' gesek_mesin,IF(COUNT(b.id_armada_foto)>0,1,0) foto_mesin
		FROM ref_armada a
		LEFT JOIN ref_armada_foto b on a.id_armada=b.id_armada and b.nm_armada_foto='FOTO MESIN' 
		WHERE a.active in (0,3) and a.status_armada <> 'SEWA' and a.id_bu in (3, 7, 8, 17)
		GROUP BY a.id_armada

		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	$this->db->where("y.id_bu in (3, 7, 8, 17)");
	// if($divisi == 0 || $divisi == 5){
	// }else {
	// 	$this->db->where("y.id_divre = $divisi");
	// }
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}


public function getAlldata_grafik_foto_armada($show=null, $start=null, $cari=null, $id_bu){
	$this->db->select('a.id_armada, b.id_bu, b.id_divre, b.nm_bu, a.kd_armada, a.plat_armada, a.tahun_armada,
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="DEPAN KANAN" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as depan_kanan,
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="DEPAN KIRI" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as depan_kiri,
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="BELAKANG" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as belakang, 
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="DALAM" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as dalam,

		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="GESEK RANGKA" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as gesek_rangka,
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="GESEK MESIN" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as gesek_mesin, 
		(IF((SELECT COUNT(id_armada_foto) from ref_armada_foto WHERE nm_armada_foto="FOTO MESIN" and id_armada=a.id_armada)>0,"ADA","TIDAK ADA")) as foto_mesin 
		');
	$this->db->from("ref_armada a");
	$this->db->join("ref_bu b", 'a.id_bu = b.id_bu', 'left');
	$this->db->where("b.id_bu in (3, 7, 8, 17)");
	if($id_bu == 0){
	}else {
		$this->db->where("b.id_bu = $id_bu");
	}
	$this->db->where("(b.nm_bu LIKE '%".$cari."%' OR a.kd_armada LIKE '%".$cari."%' )");
	$this->db->where("a.active in (0,3)");
	$this->db->where("a.status_armada <>'SEWA' ");

	$this->db->where("b.active = 1");
	$this->db->order_by("b.id_divre");
	$this->db->order_by("b.id_bu");
	if ($show == null && $start == null) {
	} else {
		$this->db->limit($show, $start);
	}
	return $this->db->get();
}

public function get_count_data_grafik_foto_armada($cari = null, $id_bu){
	
	$count = array();
	$session = $this->session->userdata('login');

	$this->db->select("count(a.id_armada) as recordsFiltered");
	$this->db->from("ref_armada a");
	$this->db->join("ref_bu b", 'a.id_bu = b.id_bu', 'left');
	$this->db->where("b.id_bu in (3, 7, 8, 17)");
	if($id_bu == 0){
	}else {
		$this->db->where("b.id_bu = $id_bu");
	}
	$this->db->where("(b.nm_bu LIKE '%".$cari."%' OR a.kd_armada LIKE '%".$cari."%' )");
	$this->db->where("a.active in (0,3)");
	$this->db->where("a.status_armada <>'SEWA' ");
	$this->db->where("b.active = 1");
	$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];

	$this->db->select("count(a.id_armada) as recordsTotal");
	$this->db->from("ref_armada a");
	$this->db->join("ref_bu b", 'a.id_bu = b.id_bu', 'left');
	if($id_bu == 0){
	}else {
		$this->db->where("b.id_bu = $id_bu");
	}
	$this->db->where("a.active in (0,3)");
	$this->db->where("a.status_armada <>'SEWA' ");

	$this->db->where("b.active = 1");
	$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];

	return $count;
}




public function get_data_grafik_segmentasi( $divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.antarkota) as antarkota, sum(x.perintis) as perintis, sum(x.pemadumoda) as pemadumoda, sum(x.aneg) as aneg, sum(x.biskota) as biskota, sum(x.paket) as paket, sum(x.pariwisata) as pariwisata, sum(x.aglomerasi) as aglomerasi");
	$this->db->from("(
		select id_bu, id_segment, count(id_segment) as antarkota, '' as perintis, '' as pemadumoda, '' as aneg, '' as biskota, '' as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '1' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, count(id_segment) as perintis, '' as pemadumoda, '' as aneg, '' as biskota, '' as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '2' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, count(id_segment) as pemadumoda, '' as aneg, '' as biskota, '' as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '3' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, '' as pemadumoda, count(id_segment) as aneg, '' as biskota, '' as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '4' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, '' as pemadumoda, '' as aneg, count(id_segment) as biskota, '' as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '5' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, '' as pemadumoda, '' as aneg, '' as biskota, count(id_segment) as paket, '' as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '6' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, '' as pemadumoda, '' as aneg, '' as biskota, '' as paket, count(id_segment) as pariwisata, '' as aglomerasi from ref_armada where active in (0,3) and id_segment = '7' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_segment, '' as antarkota, '' as perintis, '' as pemadumoda, '' as aneg, '' as biskota, '' as paket, '' as pariwisata, count(id_segment) as aglomerasi from ref_armada where active in (0,3) and id_segment = '8' and status_armada<>'SEWA' and id_bu in (3, 7, 8, 17) group by id_bu
		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}

public function get_data_grafik_jenis_armada($divisi){
	$this->db->select("y.nm_bu as cabang, sum(x.busbesar) as busbesar, sum(x.busgandeng) as busgandeng, sum(x.busmedium) as busmedium, sum(x.microbus) as microbus, sum(x.boxmini) as boxmini, sum(x.boxmedium) as boxmedium, sum(x.boxbesar) as boxbesar, sum(x.mediumlong) as mediumlong");
	$this->db->from("(
		select id_bu, id_ukuran, nm_ukuran, count(id_ukuran) as busbesar, '' as busgandeng, '' as busmedium, '' as microbus, '' as boxmini, '' as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '1' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran,'' as busbesar,  count(id_ukuran) as busgandeng, '' as busmedium, '' as microbus, '' as boxmini, '' as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '2' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng,  count(id_ukuran) as busmedium, '' as microbus, '' as boxmini, '' as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '3' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng, '' as busmedium,  count(id_ukuran) as microbus, '' as boxmini, '' as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '4' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng, '' as busmedium, '' as microbus,  count(id_ukuran) as boxmini, '' as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '5' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng, '' as busmedium, '' as microbus, '' as boxmini,  count(id_ukuran) as boxmedium, '' as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '11' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng, '' as busmedium, '' as microbus, '' as boxmini, '' as boxmedium,  count(id_ukuran) as boxbesar, '' as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '12' and id_bu in (3, 7, 8, 17) group by id_bu
		union all
		select id_bu, id_ukuran, nm_ukuran, '' as busbesar, '' as busgandeng, '' as busmedium, '' as microbus, '' as boxmini, '' as boxmedium, '' as boxbesar,  count(id_ukuran) as mediumlong from ref_armada where active in (0,3) and status_armada<>'SEWA' and id_ukuran = '13' and id_bu in (3, 7, 8, 17) group by id_bu
		) x");
	$this->db->join("ref_bu y", 'x.id_bu = y.id_bu', 'left');
	if($divisi == 0 || $divisi == 5){
	}else {
		$this->db->where("y.id_divre = $divisi");
	}
	$this->db->order_by("y.nm_bu");
	return $this->db->get();
}



}
