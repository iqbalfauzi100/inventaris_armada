<?php
    class Model_tahun extends CI_Model
    {
        public function getAlltahun($show=null, $start=null, $cari=null)
        {
            $this->db->select("a.*");
            $this->db->from("ref_tahun a");
            $session = $this->session->userdata('login');
            $this->db->where("(a.tahun  LIKE '%".$cari."%' ) ");
            $this->db->where("a.active IN (0, 1) ");
            if ($show == null && $start == null) {
            } else {
                $this->db->limit($show, $start);
            }

            return $this->db->get();
        }
		
		public function get_count_tahun($search = null)
		{
			$count = array();
			$session = $this->session->userdata('login');
			
			$this->db->select(" COUNT(id_tahun) as recordsFiltered ");
			$this->db->from("ref_tahun");
			$this->db->where("active != '2' ");
			$this->db->like("tahun ", $search);
			$count['recordsFiltered'] = $this->db->get()->row_array()['recordsFiltered'];
			
			$this->db->select(" COUNT(id_tahun) as recordsTotal ");
			$this->db->from("ref_tahun");
			$this->db->where("active != '2' ");
			$count['recordsTotal'] = $this->db->get()->row_array()['recordsTotal'];
			
			return $count;
		}
		
		public function insert_tahun($data)
        {
            $this->db->insert('ref_tahun', $data);
			return $this->db->insert_id();
        }

        public function delete_tahun($data)
        {
            $session = $this->session->userdata('login');
            $this->db->where('id_tahun', $data['id_tahun']);
            $this->db->update('ref_tahun', array('active' => '2'));
			return $data['id_tahun'];
        }
		
        public function update_tahun($data)
        {
            $session = $this->session->userdata('login');
            $this->db->where('id_tahun', $data['id_tahun']);
			$this->db->where("active != '2' ");
            $this->db->update('ref_tahun', $data);
			return $data['id_tahun'];
        }
		
		public function get_tahun_by_id($id_tahun)
		{
			if(empty($id_tahun))
			{
				return array();
			}
			else
			{
				$session = $this->session->userdata('login');
				$this->db->from("ref_tahun a");
				$this->db->where('a.id_tahun', $id_tahun);
				$this->db->where("a.active != '2' ");
				return $this->db->get()->row_array();
			}
		}

    }
