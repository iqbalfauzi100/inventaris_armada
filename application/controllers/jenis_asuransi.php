<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class jenis_asuransi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_jenis_asuransi");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S09";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $this->load->view('asuransi/jenis_asuransi', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->jenis_asuransi(1) != null) {
                $url = $this->uri->jenis_asuransi(1);
                $url = $url.' '.$this->uri->jenis_asuransi(2);
                $url = $url.' '.$this->uri->jenis_asuransi(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_jenis_asuransi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S09";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

            $start = $this->input->post('start');
            $draw = $this->input->post('draw');
            $length = $this->input->post('length');
            $cari = $this->input->post('search', true);
            $data = $this->model_jenis_asuransi->getAlljenis_asuransi($length, $start, $cari['value'])->result_array();
            $count = $this->model_jenis_asuransi->get_count_jenis_asuransi($cari['value']);

            echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->jenis_asuransi(1) != null) {
                $url = $this->uri->jenis_asuransi(1);
                $url = $url.' '.$this->uri->jenis_asuransi(2);
                $url = $url.' '.$this->uri->jenis_asuransi(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
	
	public function ax_set_data()
	{
		if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S09";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

            $id_jenis_asuransi = $this->input->post('id_jenis_asuransi');
            $nm_jenis_asuransi = $this->input->post('nm_jenis_asuransi');
    		$active = $this->input->post('active');
    		$session = $this->session->userdata('login');
    		$data = array(
                'id_jenis_asuransi' => $id_jenis_asuransi,
                'nm_jenis_asuransi' => $nm_jenis_asuransi,
    			'active' => $active,
    			'id_perusahaan' => $session['id_perusahaan'],
                'cuser' => $session['id_user']
    		);
    		
    		if(empty($id_jenis_asuransi))
    			$data['id_jenis_asuransi'] = $this->model_jenis_asuransi->insert_jenis_asuransi($data);
    		else
    			$data['id_jenis_asuransi'] = $this->model_jenis_asuransi->update_jenis_asuransi($data);
    		
    		echo json_encode(array('status' => 'success', 'data' => $data));

        } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->jenis_asuransi(1) != null) {
                $url = $this->uri->jenis_asuransi(1);
                $url = $url.' '.$this->uri->jenis_asuransi(2);
                $url = $url.' '.$this->uri->jenis_asuransi(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
	}
	
	public function ax_unset_data()
	{
		if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S09";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

            $id_jenis_asuransi = $this->input->post('id_jenis_asuransi');
    		
    		$data = array('id_jenis_asuransi' => $id_jenis_asuransi);
    		
    		if(!empty($id_jenis_asuransi))
    			$data['id_jenis_asuransi'] = $this->model_jenis_asuransi->delete_jenis_asuransi($data);
    		
    		echo json_encode(array('status' => 'success', 'data' => $data));

        } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->jenis_asuransi(1) != null) {
                $url = $this->uri->jenis_asuransi(1);
                $url = $url.' '.$this->uri->jenis_asuransi(2);
                $url = $url.' '.$this->uri->jenis_asuransi(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
	}
	
	public function ax_get_data_by_id()
	{
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S09";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

    		$id_jenis_asuransi = $this->input->post('id_jenis_asuransi');
    		
    		if(empty($id_jenis_asuransi))
    			$data = array();
    		else
    			$data = $this->model_jenis_asuransi->get_jenis_asuransi_by_id($id_jenis_asuransi);
    		
    		echo json_encode($data);

        } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->jenis_asuransi(1) != null) {
                $url = $this->uri->jenis_asuransi(1);
                $url = $url.' '.$this->uri->jenis_asuransi(2);
                $url = $url.' '.$this->uri->jenis_asuransi(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
	}
}
