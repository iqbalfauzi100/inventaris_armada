<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class rekondisi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_rekondisi");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_rekondisi->combobox_bu();
                $data['combobox_tahun'] = $this->model_rekondisi->combobox_tahun();
                $this->load->view('armada/rekondisi', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_rekondisi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                $tahun = $this->input->post('tahun');
                $active = $this->input->post('active');

                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_rekondisi->getAllrekondisi($length, $start, $cari['value'],$id_bu,$tahun,$active)->result_array();
                $count = $this->model_rekondisi->get_count_rekondisi($cari['value'],$id_bu,$tahun,$active);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_rekondisi_history()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_rekondisi = $this->input->post('id_rekondisi');

                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_rekondisi->getAllrekondisi_history($length, $start, $cari['value'],$id_rekondisi)->result_array();
                $count = $this->model_rekondisi->get_count_rekondisi_history($cari['value'],$id_rekondisi);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_rekondisi = $this->input->post('id_rekondisi');
                $tahun = $this->input->post('tahun');
                $id_bu = $this->input->post('id_bu');
                $id_armada = $this->input->post('id_armada');
                $biaya = $this->input->post('biaya');
                $no_surat = $this->input->post('no_surat');
                $tgl_rekondisi = $this->input->post('tgl_rekondisi');


                $session = $this->session->userdata('login');
                $data = array(
                    'id_rekondisi' => $id_rekondisi,
                    'tahun' => $tahun,
                    'id_bu' => $id_bu,
                    'id_armada' => $id_armada,
                    'biaya' => $biaya,
                    'no_surat' => $no_surat,
                    'tgl_rekondisi' => $tgl_rekondisi,
                    'cuser' => $session['id_user'],
                    );

                if(empty($id_rekondisi)){

                   $data['id_rekondisi'] = $this->model_rekondisi->insert_rekondisi($data);

                   $data_history = array(
                    'id_rekondisi' => $data['id_rekondisi'],
                    'biaya' => $biaya,
                    'no_surat' => $no_surat,
                    'tgl_rekondisi' => $tgl_rekondisi,
                    'cuser' => $session['id_user'],
                    );
                   $data['id_rekondisi'] = $this->model_rekondisi->insert_rekondisi_history($data_history);

               }else{
                   $data['id_rekondisi'] = $this->model_rekondisi->update_rekondisi($data);
               }

               echo json_encode(array('status' => 'success', 'data' => $data));

           } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_rekondisi = $this->input->post('id_rekondisi');

                $data = array('id_rekondisi' => $id_rekondisi);

                if(!empty($id_rekondisi))
                   $data['id_rekondisi'] = $this->model_rekondisi->delete_rekondisi($data);

               echo json_encode(array('status' => 'success', 'data' => $data));

           } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_get_data_by_id()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_rekondisi = $this->input->post('id_rekondisi');

              if(empty($id_rekondisi))
               $data = array();
           else
               $data = $this->model_rekondisi->get_rekondisi_by_id($id_rekondisi);

           echo json_encode($data);

       } else {
        echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
    }
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_get_armada()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $id_bu = $this->input->post('id_bu');
                $data = $this->model_rekondisi->combobox_armada($id_bu);
                $html = "<option value='0'>--KD Armada--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_armada."'>".$row->kd_armada." - (".$row->plat_armada.") - ".$row->nm_segment."</option>"; 
                }
                $callback = array('data_armada'=>$html);
                echo json_encode($callback);



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_status()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $active = $this->input->post('active');

                if($active==0 || $active==1){
                    $html = "
                    <option value='3'>Rekomendasi Divre</option>
                    <option value='1'>Cancel</option>
                    ";
                }else if($active==3){
                   $html = " 
                    <option value='4'>Persetujuan</option>
                    <option value='1'>Cancel</option>
                    ";
                }else if($active==4){
                    $html = "
                    <option value='5'>Pengerjaan</option>
                    <option value='1'>Cancel</option>
                    ";
                }else if($active==5){
                    $html = " 
                    <option value='6'>Selesai</option>  
                    <option value='1'>Cancel</option>
                    ";
                }else if($active==6){
                    $html = "
                    <option value='6'>Selesai</option>
                    ";
                }
                
                $callback = array('data_status'=>$html);
                echo json_encode($callback);



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_change_active()
    {
        $session = $this->session->userdata('login');
        $id_rekondisi = $this->input->post('id_rekondisi');
        $active = $this->input->post('active');

        $change_active;
        if ($active==1) {
            $change_active = 2;
        }else if ($active==2) {
            $change_active = 3;
        }else{
            $change_active = 1;
        }

        //INSERT REF_ARMADA_REKONDISI_HISTORY
        $data_rekondisi = $this->model_rekondisi->get_rekondisi_by_id($id_rekondisi);
        $data_history = array(
            'id_rekondisi' => $id_rekondisi,
            'no_surat' => $data_rekondisi['no_surat'],
            'biaya' => $data_rekondisi['biaya'],
            'tgl_rekondisi' => $data_rekondisi['tgl_rekondisi'],
            'active' => $active,
            'cuser' => $session['id_user'],
            );
        $data['id_rekondisi'] = $this->model_rekondisi->insert_rekondisi_history($data_history);

        //UPDATE REF_ARMADA_REKONDISI
        $data = array(
            'active' => $active
            );
        $this->model_rekondisi->change_active(array('id_rekondisi' => $id_rekondisi), $data);

        echo json_encode(array("status" => TRUE));

    }


    public function ax_set_data_update()
    {

        $session = $this->session->userdata('login');
        $id_rekondisi = $this->input->post('id_rekondisi');
        $active = $this->input->post('active');

        $no_surat       = $this->input->post('no_surat');
        $biaya          = $this->input->post('biaya');
        $tgl_rekondisi  = $this->input->post('tgl_rekondisi');

        //UPDATE REF_ARMADA_REKONDISI
        $data = array(
            'active'    => $active,
            'no_surat'  => $no_surat,
            'biaya'     => $biaya,
            'tgl_rekondisi' => $tgl_rekondisi,
            );
        $this->model_rekondisi->change_active(array('id_rekondisi' => $id_rekondisi), $data);

        //INSERT REF_ARMADA_REKONDISI_HISTORY
        $data_history = array(
            'id_rekondisi' => $id_rekondisi,
            'no_surat' => $no_surat,
            'biaya' => $biaya,
            'tgl_rekondisi' => $tgl_rekondisi,
            'active' => $active,
            'cuser' => $session['id_user'],
            );
        $data['id_rekondisi'] = $this->model_rekondisi->insert_rekondisi_history($data_history);

        echo json_encode(array('status' => 'success', 'data' => $data,'active' => $active));

    }

}
