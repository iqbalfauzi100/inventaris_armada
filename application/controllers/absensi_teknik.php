<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}
class absensi_teknik extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("model_absensi_teknik");
		$this->load->model("model_menu");
		///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
	}

	public function index()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$data['id_user'] = $session['id_user'];
				$data['nm_user'] = $session['nm_user'];
				$data['session_level'] = $session['id_level'];
				$data['combobox_bu'] = $this->model_absensi_teknik->combobox_bu();
				$data['combobox_segment'] = $this->model_absensi_teknik->combobox_segment();

				$this->load->view('absensi_armada/teknik', $data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function index_new()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$data['id_user'] = $session['id_user'];
				$data['nm_user'] = $session['nm_user'];
				$data['session_level'] = $session['id_level'];
				$data['combobox_bu'] = $this->model_absensi_teknik->combobox_bu();
				$data['combobox_segment'] = $this->model_absensi_teknik->combobox_segment();

				$this->load->view('absensi_armada/teknik_new', $data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function teknik_km()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$data['id_user'] = $session['id_user'];
				$data['nm_user'] = $session['nm_user'];
				$data['session_level'] = $session['id_level'];
				$data['combobox_bu'] = $this->model_absensi_teknik->combobox_bu();
				$data['combobox_segment'] = $this->model_absensi_teknik->combobox_segment();

				$this->load->view('absensi_armada/teknik_km', $data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function teknik_service()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$data['id_user'] = $session['id_user'];
				$data['nm_user'] = $session['nm_user'];
				$data['session_level'] = $session['id_level'];
				$data['combobox_bu'] = $this->model_absensi_teknik->combobox_bu();
				$data['combobox_segment'] = $this->model_absensi_teknik->combobox_segment();

				$this->load->view('absensi_armada/teknik_service', $data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_data_absensi_teknik()
	{

		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$id_segment = $this->input->post('id_segment');
				$tanggal = $this->input->post('tanggal');
				$start = $this->input->post('start');
				$draw = $this->input->post('draw');
				$length = $this->input->post('length');
				$cari = $this->input->post('search', true);
				$data = $this->model_absensi_teknik->getAllabsensi_teknik($length, $start, $cari['value'], $id_bu, $tanggal, $id_segment)->result_array();
				$count = $this->model_absensi_teknik->get_count_absensi_teknik($cari['value'], $id_bu, $tanggal, $id_segment);

				echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_data_absensi_teknik_new()
	{

		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$id_segment = $this->input->post('id_segment');
				$tanggal = $this->input->post('tgl_absensi');
				$start = $this->input->post('start');
				$draw = $this->input->post('draw');
				$length = $this->input->post('length');
				$cari = $this->input->post('search', true);
				$data = $this->model_absensi_teknik->getAllabsensi_teknik_new($length, $start, $cari['value'], $id_bu, $tanggal, $id_segment)->result_array();
				$count = $this->model_absensi_teknik->get_count_absensi_teknik_new($cari['value'], $id_bu, $tanggal, $id_segment);

				echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_data_km_service_armada()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$start = $this->input->post('start');
				$draw = $this->input->post('draw');
				$length = $this->input->post('length');
				$cari = $this->input->post('search', true);
				$data = $this->model_absensi_teknik->getAllkm_armada_simateknik($id_bu);
				$isi = $data->data;
				// print_r($data);die;

				// echo json_encode(array('recordsTotal' => '50', 'recordsFiltered' => '50', 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
				echo json_encode(array('data' => $isi));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_data_km_service_armada_2()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				// print_r('nanda');die;
				$id_bu = $this->input->post('id_bu');
				$start = $this->input->post('start');
				$draw = $this->input->post('draw');
				$length = $this->input->post('length');
				$cari = $this->input->post('search', true);
				$data = $this->model_absensi_teknik->getAllkm_armada_simateknik_2($cari['value'], $id_bu)->result_array();
				// $count = $this->model_absensi_teknik->get_count_km_armada_simateknik_2($cari['value'], $id_bu);
				// print_r($data);die;
				
				echo json_encode(array('data' => $data));
				// echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_data_km_service_armada_3()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$start = $this->input->post('start');
				$draw = $this->input->post('draw');
				$length = $this->input->post('length');
				$cari = $this->input->post('search', true);
				$datas = $this->model_absensi_teknik->getAllkm_armada_simateknik($id_bu);
				$isi = $datas->data;
				
				$this->db->query("DROP TABLE IF EXISTS temp_km_service_simateknik");
				$this->db->query("CREATE TEMPORARY TABLE temp_km_service_simateknik (armada varchar(255), nm_item varchar(255), tgl_akhir_service varchar(255), total_km varchar(255) )");
				foreach ($isi as $row){
					$this->db->query("INSERT INTO temp_km_service_simateknik VALUES ('$row->armada', '$row->nm_item', '$row->tgl_akhir_service', '$row->total')");
				}
				
				$data = $this->model_absensi_teknik->getAllkm_armada_simateknik_3($cari['value'], $id_bu)->result_array();
				// $count = $this->model_absensi_teknik->get_count_km_armada_simateknik_3($cari['value'], $id_bu);
				
				echo json_encode(array('data' => $data));
				// echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
				
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_set_data()
	{

		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$session   = $this->session->userdata('login');
				$kd_armada = $this->input->post('kd_armada');
				$status    = $this->input->post('status');

				$data = array(
					'kd_armada'     => $this->input->post('kd_armada'),
					'id_bu'         => $this->input->post('id_cabang'),
					'status'        => $this->input->post('status'),
					'tgl_absensi'   => $this->input->post('tanggal'),
					'keterangan'    => $this->input->post('keterangan'),
					'cuser'         => $session['id_user']
				);


				$query = $this->model_absensi_teknik->insert_absensi_teknik($data);

				if ($query) {
					echo json_encode(array('status' => 'success', 'data' => $query));
				} else {
					echo json_encode(array('status' => 'fail', 'data' => $query));
				}
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_set_data_new()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$session   = $this->session->userdata('login');
				$kd_armada = $this->input->post('kd_armada');
				$status    = $this->input->post('status');

				$data = array(
					'kd_armada'     => $this->input->post('kd_armada'),
					'id_bu'         => $this->input->post('id_bu'),
					'id_segment'    => $this->input->post('id_segment'),
					'status'        => $this->input->post('status'),
					'tgl_absensi'   => $this->input->post('tgl_absensi'),
					'odometer_awal' => $this->input->post('odometer_awal'),
					'odometer_akhir' => $this->input->post('odometer_akhir'),
					'km_tempuh'		=> $this->input->post('km_tempuh'),
					'status_jadwal' => 1,
					// 'keterangan'    => $this->input->post('keterangan'),
					'cdate'         => date('Y-m-d H:i:s'),
					'cuser'         => $session['id_user']
				);
				$tgl_absensimin1 = date('Y-m-d', strtotime($this->input->post('tgl_absensi') . ' -1 day'));
				$sql = $this->db->query("select max(tgl_absensi) as max_tgl_absensi from tr_absensi_armada_teknik where kd_armada = '$kd_armada' and tgl_absensi = '$tgl_absensimin1'");
				$max_tgl_absensi = $sql->row()->max_tgl_absensi;
				// print_r($max_tgl_absensi);die;
				if (!empty($max_tgl_absensi) || $this->input->post('tgl_absensi') == '2020-11-01') {
					$query = $this->model_absensi_teknik->insert_absensi_teknik_new($data);
					echo json_encode(array('status' => 'success', 'data' => $query));
				} else {
					echo json_encode(array('status' => 'failed'));
				}

				// if($query){

				// }else{
				// echo json_encode(array('status' => 'fail', 'data' => $query));
				// }
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_recall_data_km_service_simateknik()
	{

		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$session   = $this->session->userdata('login');
				$id_bu = $this->input->post('id_bu');

				$data = array(
					'id_bu'     => $this->input->post('id_bu'),
				);

				// $query = $this->model_absensi_teknik->insert_absensi_teknik($data);

				if (!empty($id_bu)) {
					// $sql = $this->db->query("SELECT * FROM tr_absensi_armada_teknik a WHERE a.id_bu =  '$id_bu' GROUP BY a.kd_armada;");
					
					$data = $this->model_absensi_teknik->getAllkm_armada_simateknik($id_bu);
					$isi = $data->data;
					
					foreach ($isi as $row){
						
                        // $this->db->query("insert into tr_jadwal_ujian values ('', '$row->id_jadwal', '$row->id_ujian', '$row->id_ujian_detail', '$row->durasi_ujian', '', '$row->id_template', '$row->tgl_jadwal', '', '', '0', '1', '77')");
                    }
					echo json_encode(array('status' => 'success', 'data' => $data));
				} else {
					echo json_encode(array('status' => 'fail', 'data' => $data));
				}
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_unset_data()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_pegawai = $this->input->post('id_pegawai');

				$data = array('id_pegawai' => $id_pegawai);

				if (!empty($id_pegawai))
					$data['id_pegawai'] = $this->model_absensi_teknik->delete_absensi_teknik($data);

				echo json_encode(array('status' => 'success', 'data' => $data));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_unset_data_new()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_absensi_armada = $this->input->post('id_absensi_armada');

				$data = array('id_absensi_armada' => $id_absensi_armada);

				if (!empty($id_absensi_armada))
					$data['id_absensi_armada'] = $this->model_absensi_teknik->delete_absensi_teknik_new($data);

				echo json_encode(array('status' => 'success', 'data' => $data));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_data_by_id()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_pegawai = $this->input->post('id_pegawai');
				$id_bu = $this->input->post('id_bu');

				if (empty($id_pegawai))
					$data = array();
				else
					$data = $this->model_absensi_teknik->get_absensi_teknik_by_id($id_pegawai, $id_bu);

				echo json_encode($data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_trayek()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$id_cabang = $this->input->post('id_cabang');
				$data = $this->model_absensi_teknik->list_trayek($id_cabang);
				$html = "<option value='0'>--Trayek--</option>";
				foreach ($data->result() as $row) {
					$html .= "<option value='" . $row->kd_trayek . "'>" . $row->nm_point_awal . " - " . $row->nm_point_akhir . "</option>";
				}
				$callback = array('data_trayek' => $html);
				echo json_encode($callback);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_armada()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {
				$id_cabang = $this->input->post('id_cabang');
				$data = $this->model_absensi_teknik->list_armada($id_cabang);
				$html = "<option value='0'>--Trayek--</option>";
				foreach ($data->result() as $row) {
					$html .= "<option value='" . $row->kd_armada . "'>" . $row->kd_armada . "</option>";
				}
				$callback = array('data_armada' => $html);
				echo json_encode($callback);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_odometer_auto()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$kd_armada = $this->input->post('kd_armada');
				$tgl_absensi = $this->input->post('tgl_absensi');

				if (empty($kd_armada))
					$data = array();
				else
					$data = $this->model_absensi_teknik->get_odometer_auto($kd_armada, $tgl_absensi);

				if (empty($data)) {
					$data = array(
						'odometer_akhir' => 0,
					);
					echo json_encode($data);
				} else {
					// print_r('isi');die;
					echo json_encode($data);
				}
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_segment_auto()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$kd_armada = $this->input->post('kd_armada');

				if (empty($kd_armada))
					$data = array();
				else
					$data = $this->model_absensi_teknik->get_segment_auto($kd_armada);

				if (empty($data)) {
					$data = array(
						'id_segment' => 0,
					);
					echo json_encode($data);
				} else {
					// print_r('isi');die;
					echo json_encode($data);
				}
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_ua_auto()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$tgl_absensi = $this->input->post('tgl_absensi');
				// $data = $this->model_absensi_teknik->get_odometer_auto($kd_armada, $tgl_absensi);
				$cek = $this->db->query("select * from tr_absensi_armada_teknik where id_bu = '$id_bu' and status = '6' and tgl_absensi = DATE_ADD('$tgl_absensi', INTERVAL -1 DAY)");
				// print_r($sql);die;
				if ($cek->num_rows > 0) {
					$sql = $this->db->query("insert ignore into tr_absensi_armada_teknik select '' as id_absensi_armada, kd_armada, id_bu, id_segment, nm_segment, status, '$tgl_absensi' as tgl_absensi, odometer_awal, odometer_akhir, km_tempuh, status_jadwal, keterangan, cdate, cuser, cnm_user from tr_absensi_armada_teknik where id_bu = '$id_bu' and status = '6' and tgl_absensi = DATE_ADD('$tgl_absensi', INTERVAL -1 DAY)");
					echo json_encode(array('status' => 'success', 'data' => $sql));
				} else {
					echo json_encode(array('status' => 'failed'));
				}
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_get_data_absensi_armada_teknik_by_id()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_absensi_armada = $this->input->post('id_absensi_armada');
				if (empty($id_absensi_armada))
					$data = array();
				else
					$data = $this->model_absensi_teknik->get_absensi_armada_teknik_by_id($id_absensi_armada);

				// print_r($data);die;
				echo json_encode($data);
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_copy_absensi_teknik()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$username   = $session['username'];
				$id_cabang  = $this->input->post('id_cabang');
				$id_segment  = $this->input->post('id_segment');
				$tanggal_from  = $this->input->post('tanggal_from');
				$tanggal_to    = $this->input->post('tanggal_to');

				$data = array(
					'id_cabang' => $id_cabang,
					'id_segment' => $id_segment,
					'tanggal'   => $tanggal_from,
					'tanggal_to' => $tanggal_to,
				);

				$status = $this->model_absensi_teknik->copyAbsensiArmada($id_cabang, $id_segment, $tanggal_from, $tanggal_to);
				echo json_encode(array('status' => 'success', 'tanggal_to' => $tanggal_to));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}

	public function ax_unset_data_all_absent()
	{
		if ($this->session->userdata('login')) {
			$session = $this->session->userdata('login');
			$menu_kd_menu_details = "U07";  //custom by database
			$access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
			if (!empty($access['id_menu_details'])) {

				$id_bu = $this->input->post('id_bu');
				$id_segment = $this->input->post('id_segment');
				$tanggal = $this->input->post('tanggal');

				if ($id_segment == 0) {
					$status = $this->db->delete('tr_absensi_armada_teknik', array('tgl_absensi' => $tanggal, 'id_bu' => $id_bu));
				} else {
					$status = $this->db->delete('tr_absensi_armada_teknik', array('tgl_absensi' => $tanggal, 'id_bu' => $id_bu, 'id_segment' => $id_segment));
				}
				echo json_encode(array('status' => $status));
			} else {
				echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
			}
		} else {
			if ($this->uri->segment(1) != null) {
				$url = $this->uri->segment(1);
				$url = $url . ' ' . $this->uri->segment(2);
				$url = $url . ' ' . $this->uri->segment(3);
				redirect('welcome/relogin/?url=' . $url . '', 'refresh');
			} else {
				redirect('welcome/relogin', 'refresh');
			}
		}
	}
}
