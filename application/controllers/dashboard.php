<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_menu");
        $this->load->model("model_home");
    }
    public function usia()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];

            $this->load->view('dashboard/usia', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }
    public function tahun_perolehan()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];

            $this->load->view('dashboard/tahun_perolehan', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function stnk()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();

            $this->load->view('dashboard/stnk', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function keur()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();

            $this->load->view('dashboard/keur', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function ijintrayek()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();

            $this->load->view('dashboard/ijintrayek', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function segment()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];

            $this->load->view('dashboard/segment', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function merek()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];

            $this->load->view('dashboard/merek', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }
    
    public function jenis_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];

            $this->load->view('dashboard/jenis_armada', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function outstanding()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();
            $data['combobox_tahun']    = $this->model_home->combobox_tahun();

            $this->load->view('dashboard/outstanding', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function asuransi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();
            $data['combobox_tahun']    = $this->model_home->combobox_tahun();

            $this->load->view('dashboard/asuransi', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function klaim()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();
            $data['combobox_tahun']    = $this->model_home->combobox_tahun();

            $this->load->view('dashboard/klaim', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }

    public function foto_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu']    = $this->model_home->combobox_bu();

            $this->load->view('dashboard/foto_armada', $data);

        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }


    public function ax_data_datatable_usia(){
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_datatable_usia($length, $start, $cari['value'])->result_array();
        $count = $this->model_home->get_count_data_datatable_usia($cari['value']);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }




}
