<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class armada extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_armada");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_armada->combobox_bu();
                $data['combobox_segment'] = $this->model_armada->combobox_segment();
                $data['combobox_merek'] = $this->model_armada->combobox_merek();
                $data['combobox_layanan'] = $this->model_armada->combobox_layanan();
                $data['combobox_ukuran'] = $this->model_armada->combobox_ukuran();
                $data['combobox_warna'] = $this->model_armada->combobox_warna();
                $data['combobox_layout'] = $this->model_armada->combobox_layout();
                
                $this->load->view('armada/index', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    

    public function ax_data_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllarmada($length, $start, $cari['value'], $id_bu)->result_array();
                $count = $this->model_armada->get_count_armada($cari['value'], $id_bu);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_mutasi_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllmutasiarmada($length, $start, $cari['value'], $id_armada)->result_array();
                $count = $this->model_armada->get_count_mutasiarmada($cari['value'], $id_armada);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $id_layout = $this->input->post('id_layout');
                $kd_armada = $this->input->post('kd_armada');
                $rangka_armada = $this->input->post('rangka_armada');
                $plat_armada = $this->input->post('plat_armada');
                $mesin_armada = $this->input->post('mesin_armada');
                $tahun_armada = $this->input->post('tahun_armada');
                $tahun_perolehan = $this->input->post('tahun_perolehan');
                $id_merek = $this->input->post('id_merek');
                $tipe_armada = $this->input->post('tipe_armada');
                $silinder_armada = $this->input->post('silinder_armada');
                $id_ukuran = $this->input->post('id_ukuran');
                $seat_armada = $this->input->post('seat_armada');
                $id_segment = $this->input->post('id_segment');
                $id_warna = $this->input->post('id_warna');
                $id_layanan = $this->input->post('id_layanan');
                $id_segment = $this->input->post('id_segment');
                $status_armada = $this->input->post('status_armada');
                $id_bu = $this->input->post('id_bu');

                $no_kontrak = $this->input->post('no_kontrak');
                $masa_berlaku_kontrak = $this->input->post('masa_berlaku_kontrak');
                $active = $this->input->post('active');
                $session = $this->session->userdata('login');
                $data = array(

                    'id_armada' => $id_armada,
                    'id_layout' => $id_layout,
                    'kd_armada' => $kd_armada,
                    'rangka_armada' => $rangka_armada,
                    'plat_armada' => $plat_armada,
                    'mesin_armada' => $mesin_armada,
                    'tahun_armada' => $tahun_armada,
                    'tahun_perolehan' => $tahun_perolehan,
                    'id_merek' => $id_merek,
                    'tipe_armada' => $tipe_armada,
                    'silinder_armada' => $silinder_armada,
                    'id_ukuran' => $id_ukuran,
                    'seat_armada' => $seat_armada,
                    'id_segment' => $id_segment,
                    'id_layanan' => $id_layanan,
                    'id_warna' => $id_warna,
                    'status_armada' => $status_armada,
                    'id_bu' => $id_bu,

                    'no_kontrak' => $no_kontrak,
                    'masa_berlaku_kontrak' => $masa_berlaku_kontrak,
                    'active' => $active,
                    'id_perusahaan' => $session['id_perusahaan'],
                    'cuser' => $session['id_user'],

                    );

                if($id_armada == 0){
                 $data['id_armada'] = $this->model_armada->insert_armada($data);
             }
             else{
                 $data['id_armada'] = $this->model_armada->update_armada($data);
             }

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');

                $data = array('id_armada' => $id_armada);

                if(!empty($id_armada))
                 $data['id_armada'] = $this->model_armada->delete_armada($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}



public function ax_get_data_by_id()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_armada = $this->input->post('id_armada');

              if(empty($id_armada))
                 $data = array();
             else
                 $data = $this->model_armada->get_armada_by_id($id_armada);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}


public function ax_data_foto()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllarmadafoto($length, $start, $cari['value'], $id_armada)->result_array();
                $count = $this->model_armada->get_count_armadafoto($cari['value'], $id_armada);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_upload_data_foto()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


            $config['upload_path']="./uploads/armada/foto/"; //path folder file upload
            $config['allowed_types']='jpeg|jpg'; //type file yang boleh di upload
            $config['encrypt_name'] = TRUE; //enkripsi file name upload
            $config['max_size'] = 500; //enkripsi file name upload

            $this->load->library('upload',$config); //call library upload 
            if($this->upload->do_upload("file")){ //upload file
                $data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $nm_armada_foto = $this->input->post('nm_armada_foto'); //get judul image
                $id_armada = $this->input->post('id_armada'); //get judul image
                // $nm_upload = $this->input->post('nm_upload'); //get judul image
                $upload = $data['upload_data']['file_name']; //set file name ke variable image
                $url = './uploads/armada/foto/'.$upload; 

                $session = $this->session->userdata('login');
                $data = array(
                    'nm_armada_foto' => $nm_armada_foto,
                    'id_armada' => $id_armada,
                    'attachment' => $upload,
                    'active' => 1,
                    'cuser' => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan'],


                    );
                // print_r($data);exit();
                if (file_exists($url)) {
                 $data['id_armada_foto'] = $this->model_armada->insert_armada_foto($data);
                 echo json_encode(array('status' => 'success', 'data' => $data));
             } 

                // echo json_encode(array('status' => 'success', 'data' => $data));
         }

     } else {
        echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
    }
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_unset_foto()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada_foto = $this->input->post('id_armada_foto');

                $data = array('id_armada_foto' => $id_armada_foto);


                $datas = $this->model_armada->get_foto_by_id($id_armada_foto);
                $this->load->helper("file");
                $url = './uploads/armada/foto/'.$datas['attachment'];
                
                if (file_exists($url)) {
                    unlink($url);
                    $data['id_armada_foto'] = $this->model_armada->delete_foto($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                } else {
                    $data['id_armada_foto'] = $this->model_armada->delete_foto($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                }
                



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_stnk()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllarmadastnk($length, $start, $cari['value'], $id_armada)->result_array();
                $count = $this->model_armada->get_count_armadastnk($cari['value'], $id_armada);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_upload_data_stnk()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

            $config['upload_path']="./uploads/armada/stnk/"; //path folder file upload
            $config['allowed_types']='jpeg|jpg'; //type file yang boleh di upload
            $config['encrypt_name'] = TRUE; //enkripsi file name upload
            $config['max_size'] = 500; //enkripsi file name upload

            $id_armada_stnk     = $this->input->post('id_armada_stnk'); 
            $id_armada          = $this->input->post('id_armadastnk'); //get judul image
            $tgl_exp_stnk       = $this->input->post('tgl_exp_stnk'); //get judul image
            $masa               = $this->input->post('masa'); //get judul image


            if(!empty($id_armada_stnk)){
                //EDIT DATA STNK
                $data = array(
                    'id_armada_stnk'=> $id_armada_stnk,
                    'id_armada'     => $id_armada,
                    'tgl_exp_stnk'  => $tgl_exp_stnk,
                    'masa'          => $masa
                    );

                $data['id_armada_stnk'] = $this->model_armada->update_armada_stnk($data);
                echo json_encode(array('status' => 'success', 'data' => $data));

            }else{
                //INSERT DATA STNK

            $this->load->library('upload',$config); //call library upload 
            if($this->upload->do_upload("file")){ //upload file
                $data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                // $nm_upload = $this->input->post('nm_upload'); //get judul image
                $upload = $data['upload_data']['file_name']; //set file name ke variable image
                $url = './uploads/armada/stnk/'.$upload; 

                $session = $this->session->userdata('login');
                $data = array(
                    'tgl_exp_stnk' => $tgl_exp_stnk,
                    'id_armada' => $id_armada,
                    'attachment' => $upload,
                    'masa' => $masa,
                    'active' => 1,
                    'cuser' => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan'],
                    );
                if (file_exists($url)) {
                 $data['id_armada_stnk'] = $this->model_armada->insert_armada_stnk($data);
                 echo json_encode(array('status' => 'success', 'data' => $data));
             }
         }else{
            echo json_encode(array('status' => 'gagal'));
        }
    }

} else {
    echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
}
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_get_data_by_id_stnk()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_armada_stnk = $this->input->post('id_armada_stnk');

              if(empty($id_armada_stnk))
                 $data = array();
             else
                 $data = $this->model_armada->get_armada_by_id_stnk($id_armada_stnk);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_stnk()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada_stnk = $this->input->post('id_armada_stnk');

                $data = array('id_armada_stnk' => $id_armada_stnk);


                $datas = $this->model_armada->get_stnk_by_id($id_armada_stnk);
                $this->load->helper("file");
                $url = './uploads/armada/stnk/'.$datas['attachment'];
                
                if (file_exists($url)) {
                    unlink($url);
                    $data['id_armada_stnk'] = $this->model_armada->delete_stnk($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                } else {
                    $data['id_armada_stnk'] = $this->model_armada->delete_stnk($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                }
                



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }




    //KEUR
    public function ax_data_keur()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllarmadakeur($length, $start, $cari['value'], $id_armada)->result_array();
                $count = $this->model_armada->get_count_armadakeur($cari['value'], $id_armada);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_upload_data_keur()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

            $config['upload_path']="./uploads/armada/keur/"; //path folder file upload
            $config['allowed_types']='jpeg|jpg|pdf'; //type file yang boleh di upload
            $config['encrypt_name'] = TRUE; //enkripsi file name upload
            $config['max_size'] = 500; //enkripsi file name upload

            $id_armada_keur = $this->input->post('id_armada_keur');
            $id_armada = $this->input->post('id_armadakeur');
            $tgl_exp = $this->input->post('tgl_exp_keur');

            if(!empty($id_armada_keur)){
                //EDIT DATA STNK
                $data = array(
                    'id_armada_keur'=> $id_armada_keur,
                    'id_armada'     => $id_armada,
                    'tgl_exp'       => $tgl_exp
                    );

                $data['id_armada_keur'] = $this->model_armada->update_armada_keur($data);
                echo json_encode(array('status' => 'success', 'data' => $data));

            }else{
                //INSERT DATA STNK

            $this->load->library('upload',$config); //call library upload 
            if($this->upload->do_upload("file")){ //upload file
                $data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $upload = $data['upload_data']['file_name']; //set file name ke variable image
                $url = './uploads/armada/keur/'.$upload; 

                $session = $this->session->userdata('login');
                $data = array(
                    'id_armada' => $id_armada,
                    'attachment' => $upload,
                    'tgl_exp' => $tgl_exp,
                    'active' => 1,
                    'cuser' => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan'],
                    );

                if (file_exists($url)) {
                 $data['id_armada_keur'] = $this->model_armada->insert_armada_keur($data);
                 echo json_encode(array('status' => 'success', 'data' => $data));
             } 
         }else{
            echo json_encode(array('status' => 'gagal'));
        }
     }

 } else {
    echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
}
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_get_data_by_id_keur()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_armada_keur = $this->input->post('id_armada_keur');

              if(empty($id_armada_keur))
                 $data = array();
             else
                 $data = $this->model_armada->get_armada_by_id_keur($id_armada_keur);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_keur()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada_keur = $this->input->post('id_armada_keur');

                $data = array('id_armada_keur' => $id_armada_keur);


                $datas = $this->model_armada->get_keur_by_id($id_armada_keur);
                $this->load->helper("file");
                $url = './uploads/armada/keur/'.$datas['attachment'];
                
                if (file_exists($url)) {
                    unlink($url);
                    $data['id_armada_keur'] = $this->model_armada->delete_keur($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                } else {
                    $data['id_armada_keur'] = $this->model_armada->delete_keur($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }









        //IJIN TRAYEK
    public function ax_data_ijintrayek()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada = $this->input->post('id_armada');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_armada->getAllarmadaijintrayek($length, $start, $cari['value'], $id_armada)->result_array();
                $count = $this->model_armada->get_count_armadaijintrayek($cari['value'], $id_armada);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_upload_data_ijintrayek()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                // echo json_encode($this->input->post('id_trayek'));
                // echo json_encode($this->input->post('tgl_exp_ijintrayek'));
                // // echo json_encode($this->input->post('id_armadaijintrayek'));
                // exit();

            $config['upload_path']="./uploads/armada/ijintrayek/"; //path folder file upload
            $config['allowed_types']='jpeg|jpg|pdf'; //type file yang boleh di upload
            $config['encrypt_name'] = TRUE; //enkripsi file name upload
            $config['max_size'] = 500; //enkripsi file name upload

            $this->load->library('upload',$config); //call library upload 
            if($this->upload->do_upload("file")){ //upload file
                $data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id_armada = $this->input->post('id_armadaijintrayek'); //get judul image
                $id_trayek = $this->input->post('id_trayek');
                $tgl_exp = $this->input->post('tgl_exp_ijintrayek');
                $upload = $data['upload_data']['file_name']; //set file name ke variable image
                $url = './uploads/armada/ijintrayek/'.$upload; 

                $session = $this->session->userdata('login');
                $data = array(
                    'id_armada' => $id_armada,
                    'attachment' => $upload,
                    'id_trayek' => $id_trayek,
                    'tgl_exp' => $tgl_exp,
                    'active' => 1,
                    'cuser' => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan'],
                    );


                // print_r($data);exit();
                if (file_exists($url)) {
                 $data['id_armada_keur'] = $this->model_armada->insert_armada_ijintrayek($data);
                 echo json_encode(array('status' => 'success', 'data' => $data));
             } 

                // echo json_encode(array('status' => 'success', 'data' => $data));
         }

     } else {
        echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
    }
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_get_trayek(){
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_bu = $this->input->post('id_bu');
                $data = $this->model_armada->combobox_trayek($id_bu);
                $html = "<option value='0'>--Trayek--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_trayek."'>".$row->nm_trayek." - (".$row->kd_segment.")</option>"; 
                }
                $callback = array('data_trayek'=>$html);
                echo json_encode($callback);

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_unset_ijintrayek()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_armada_ijintrayek = $this->input->post('id_armada_ijintrayek');

                $data = array('id_armada_ijintrayek' => $id_armada_ijintrayek);


                $datas = $this->model_armada->get_ijintrayek_by_id($id_armada_ijintrayek);
                $this->load->helper("file");
                $url = './uploads/armada/ijintrayek/'.$datas['attachment'];
                
                if (file_exists($url)) {
                    unlink($url);
                    $data['id_armada_ijintrayek'] = $this->model_armada->delete_ijintrayek($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                } else {
                    $data['id_armada_ijintrayek'] = $this->model_armada->delete_ijintrayek($data);
                    echo json_encode(array('status' => 'success', 'data' => $url));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }



    

    

}
