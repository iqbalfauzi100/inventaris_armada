<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class mutasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_mutasi");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_mutasi->combobox_bu();
                $data['combobox_cabang'] = $this->model_mutasi->combobox_cabang();
                $this->load->view('armada/mutasi', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_mutasi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_mutasi->getAllmutasi($length, $start, $cari['value'])->result_array();
                $count = $this->model_mutasi->get_count_mutasi($cari['value']);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_set_data()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_mutasi = $this->input->post('id_mutasi');
                $id_bu_sebelum = $this->input->post('id_bu_sebelum');
                $id_armada = $this->input->post('id_armada');
                $id_bu_sesudah = $this->input->post('id_bu_sesudah');
                $jenis_surat = $this->input->post('jenis_surat');
                $no_surat = $this->input->post('no_surat');
                $tgl_mutasi = $this->input->post('tgl_mutasi');


                $session = $this->session->userdata('login');
                $data = array(
                    'id_mutasi'     => $id_mutasi,
                    'id_bu_sebelum' => $id_bu_sebelum,
                    'id_armada'     => $id_armada,
                    'id_bu_sesudah' => $id_bu_sesudah,
                    'jenis_surat'  => $jenis_surat,
                    'no_surat'      => $no_surat,
                    'tgl_mutasi'    => $tgl_mutasi,
                    'cuser'         => $session['id_user'],
                    );

                if(empty($id_mutasi)){
                    $data['id_mutasi'] = $this->model_mutasi->insert_mutasi($data);
                    #$this->model_mutasi->update_data_armada($data);
                }else{
                    $data['id_mutasi'] = $this->model_mutasi->update_mutasi($data);
                    #$this->model_mutasi->update_data_armada($data);
                }

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_unset_data()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_mutasi = $this->input->post('id_mutasi');

                $data = array('id_mutasi' => $id_mutasi);

                if(!empty($id_mutasi))
                    $data['id_mutasi'] = $this->model_mutasi->delete_mutasi($data);

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_get_data_by_id()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_mutasi = $this->input->post('id_mutasi');

                if(empty($id_mutasi))
                    $data = array();
                else
                    $data = $this->model_mutasi->get_mutasi_by_id($id_mutasi);

                echo json_encode($data);

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $id_bu = $this->input->post('id_bu');
                $data = $this->model_mutasi->combobox_armada($id_bu);
                $html = "<option value='0'>--KD Armada--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_armada."'>".$row->kd_armada." - (".$row->plat_armada.") - ".$row->nm_segment."</option>"; 
                }
                $callback = array('data_armada'=>$html);
                echo json_encode($callback);



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }


    public function ax_change_ajukan_mutasi()
    {
        $id_mutasi = $this->input->post('id_mutasi');
        $status = $this->input->post('status');

        $change_status = 3;
        $data = array(
            'status' => $change_status
            );
        $this->model_mutasi->change_active(array('id_mutasi' => $id_mutasi), $data);

        echo json_encode(array("status" => TRUE));

    }

    public function ax_change_reject()
    {
        $id_mutasi = $this->input->post('id_mutasi');
        $status = $this->input->post('status');

        $change_status = 1;
        $data = array(
            'status' => $change_status
            );
        $this->model_mutasi->change_active(array('id_mutasi' => $id_mutasi), $data);

        echo json_encode(array("status" => TRUE));

    }

    public function ax_change_approved()
    {
        $id_mutasi = $this->input->post('id_mutasi');
        $id_bu_sebelum = $this->input->post('id_bu_sebelum');
        $id_armada = $this->input->post('id_armada');
        $id_bu_sesudah = $this->input->post('id_bu_sesudah');

        $change_status = 4;
        $data = array(
            'status' => $change_status
            );
        $this->model_mutasi->change_active(array('id_mutasi' => $id_mutasi), $data);


        $datax = array(
            'id_mutasi'     => $id_mutasi,
            'id_bu_sebelum' => $id_bu_sebelum,
            'id_armada'     => $id_armada,
            'id_bu_sesudah' => $id_bu_sesudah
            );
        $this->model_mutasi->update_data_armada($datax);


        echo json_encode(array("status" => TRUE));

    }


}
