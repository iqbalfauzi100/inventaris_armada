<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class hapus_buku extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_hapus_buku");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_hapus_buku->combobox_bu();
                $data['combobox_tahun'] = $this->model_hapus_buku->combobox_tahun();
                $this->load->view('armada/hapus_buku', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_hapus_buku()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

               $id_bu = $this->input->post('id_bu');
               $tahun = $this->input->post('tahun');
               $start = $this->input->post('start');
               $draw = $this->input->post('draw');
               $length = $this->input->post('length');
               $cari = $this->input->post('search', true);
               $data = $this->model_hapus_buku->getAllhapus_buku($length, $start, $cari['value'],$id_bu, $tahun)->result_array();
               $count = $this->model_hapus_buku->get_count_hapus_buku($cari['value'],$id_bu, $tahun);

               echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
           } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_set_data()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_hapus_buku = $this->input->post('id_hapus_buku');
                $kd_hapus_buku = $this->input->post('kd_hapus_buku');
                $nm_hapus_buku = $this->input->post('nm_hapus_buku');
                $active = $this->input->post('active');
                $session = $this->session->userdata('login');
                $data = array(
                    'id_hapus_buku' => $id_hapus_buku,
                    'kd_hapus_buku' => $kd_hapus_buku,
                    'nm_hapus_buku' => $nm_hapus_buku,
                    'active' => $active,
                    'id_perusahaan' => $session['id_perusahaan']
                    );

                if(empty($id_hapus_buku))
                 $data['id_hapus_buku'] = $this->model_hapus_buku->insert_hapus_buku($data);
             else
                 $data['id_hapus_buku'] = $this->model_hapus_buku->update_hapus_buku($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_hapus_buku = $this->input->post('id_hapus_buku');

                $data = array('id_hapus_buku' => $id_hapus_buku);

                if(!empty($id_hapus_buku))
                 $data['id_hapus_buku'] = $this->model_hapus_buku->delete_hapus_buku($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_get_data_by_id()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_hapus_buku = $this->input->post('id_hapus_buku');

              if(empty($id_hapus_buku))
                 $data = array();
             else
                 $data = $this->model_hapus_buku->get_hapus_buku_by_id($id_hapus_buku);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}
}
