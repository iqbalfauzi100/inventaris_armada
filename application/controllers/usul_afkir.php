<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class usul_afkir extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_usul_afkir");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_usul_afkir->combobox_bu();
                $data['combobox_tahun'] = $this->model_usul_afkir->combobox_tahun();
                $data['combobox_cabang'] = $this->model_usul_afkir->combobox_cabang();

                // $data_level = array("1", "7","13");
                // if(in_array($session['id_level'], $data_level)){
                //     $this->load->view('armada/usul_afkir_pusat', $data);
                // }else{
                $this->load->view('armada/usul_afkir', $data);
                // }
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_usul_afkir()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                $tahun = $this->input->post('tahun');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_usul_afkir->getAllusul_afkir($length, $start, $cari['value'],$id_bu, $tahun)->result_array();
                $count = $this->model_usul_afkir->get_count_usul_afkir($cari['value'],$id_bu, $tahun);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }


    public function ax_data_usul_afkir_pusat()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $tahun = $this->input->post('tahun');
                $id_bu = $this->input->post('id_bu');

                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_usul_afkir->getAllusul_afkir_pusat($length, $start, $cari['value'],$tahun,$id_bu)->result_array();
                $count = $this->model_usul_afkir->get_count_usul_afkir_pusat($cari['value'],$tahun,$id_bu);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_set_data()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_usul_afkir = $this->input->post('id_usul_afkir');
                $id_bu = $this->input->post('id_bu');
                $id_armada = $this->input->post('id_armada');

                $session = $this->session->userdata('login');
                $data = array(
                    'id_usul_afkir'     => $id_usul_afkir,
                    'id_bu'         => $id_bu,
                    'id_armada'     => $id_armada,
                    'cuser'         => $session['id_user'],
                    );

                if(empty($id_usul_afkir)){
                    $data['id_usul_afkir'] = $this->model_usul_afkir->insert_usul_afkir($data);
                }else{
                    $data['id_usul_afkir'] = $this->model_usul_afkir->update_usul_afkir($data);
                }

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_unset_data()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_usul_afkir = $this->input->post('id_usul_afkir');

                $data = array('id_usul_afkir' => $id_usul_afkir);

                if(!empty($id_usul_afkir))
                    $data['id_usul_afkir'] = $this->model_usul_afkir->delete_usul_afkir($data);

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_get_data_by_id()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_usul_afkir = $this->input->post('id_usul_afkir');

                if(empty($id_usul_afkir))
                    $data = array();
                else
                    $data = $this->model_usul_afkir->get_usul_afkir_by_id($id_usul_afkir);

                echo json_encode($data);

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $id_bu = $this->input->post('id_bu');
                $data = $this->model_usul_afkir->combobox_armada($id_bu);
                $html = "<option value='0'>--KD Armada--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_armada."'>".$row->kd_armada." - (".$row->plat_armada.") - ".$row->nm_segment."</option>"; 
                }
                $callback = array('data_armada'=>$html);
                echo json_encode($callback);



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }


    public function ax_change_ajukan_usul_afkir()
    {
        $id_usul_afkir = $this->input->post('id_usul_afkir');
        $status = $this->input->post('status');

        $change_status = 3;
        $data = array(
            'status' => $change_status
            );
        $this->model_usul_afkir->change_active(array('id_usul_afkir' => $id_usul_afkir), $data);

        echo json_encode(array("status" => TRUE));

    }

    public function ax_change_reject()
    {
        $id_usul_afkir = $this->input->post('id_usul_afkir');
        $status = $this->input->post('status');

        $change_status = 1;
        $data = array(
            'status' => $change_status
            );
        $this->model_usul_afkir->change_active(array('id_usul_afkir' => $id_usul_afkir), $data);

        echo json_encode(array("status" => TRUE));

    }

    public function ax_change_approved()
    {
        $id_usul_afkir = $this->input->post('id_usul_afkir');
        $approvedyear = $this->input->post('approvedyear');
        $id_armada = $this->input->post('id_armada');

        $change_status = 4;
        $data = array(
            'status' => $change_status,
            'approvedyear' => $approvedyear
            );
        $this->model_usul_afkir->change_active(array('id_usul_afkir' => $id_usul_afkir), $data);

        //UPDATE REF_ARMADA
        $datax = array(
            'id_armada'     => $id_armada,
            'active'        => 3
            );
        $this->model_usul_afkir->update_data_armada($datax);


        echo json_encode(array("status" => TRUE));

    }


}
