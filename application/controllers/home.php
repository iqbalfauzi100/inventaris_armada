<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_menu");
        $this->load->model("model_home");
        $this->load->model("model_home_detail");
    }
    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $data['nm_user'] = $session['nm_user'];
            $data['id_user'] = $session['id_user'];
            $data['session_level']  = $session['id_level'];
            $id_cabang              = $session['id_bu'];
            $data['combobox_bu'] = $this->model_home->combobox_bu($id_cabang);
            $data['combobox_tahun'] = $this->model_home->combobox_tahun();
            $data['combobox_segmen'] = $this->model_home->combobox_segmen();
            
            // $this->load->view('home_dashboard', $data);
            // $os = array("1", "7");

            // if (in_array($session['id_level'], $os)) {
            $this->load->view('home_dashboard', $data);
            // }else{
            //     $this->load->view('home', $data);
            // }

            
        } else {
            redirect('welcome/relogin', 'refresh');
        }

    }


    public function Update()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');

            $a = $this->input->post('password_lama');
            $b = $this->input->post('password_baru');
            if (empty($a)or empty($b)) {
                echo "<script>alert('Data Masih Ada Yang Kosong');window.location.href='javascript:history.back(-1);'</script>";
            } else {
                $c = do_hash($this->input->post('password_lama'), 'md5');
                $d = $session['password'];
                if ($d != $c) {
                    echo "<script>alert('Password Lama Salah');window.location.href='javascript:history.back(-1);'</script>";
                } else {
                    $id_user = $session['id_user'];
                    $data = array(

                        'password' => do_hash($this->input->post('password_baru'), 'md5'),

                        );
                    $this->model_home->UpdateUser($id_user, $data);
                    redirect('welcome/logout');
                }
            }
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }

    public function get_data_dashboard_usia()
    {
        $session = $this->session->userdata('login');
        $session_bu = $session['id_bu'];

        $divisi = $this->input->post('id_divisi');
        $grafik_usia = $this->model_home->get_data_grafik_usia($divisi)->result_array();

        //USIA
        $dibawah5 = array();
        $antara6dan10 = array();
        $antara11dan15 = array();
        $antara16dan20 = array();
        $diatas20 = array();
        foreach($grafik_usia as $row)
        {
            array_push($dibawah5,(int)$row['satu']);
            array_push($antara6dan10,(int)$row['dua']);
            array_push($antara11dan15,(int)$row['tiga']);
            array_push($antara16dan20,(int)$row['empat']);
            array_push($diatas20,(int)$row['lima']);
        }


        echo json_encode(array(
            'dibawah5' => $dibawah5,'antara6dan10' => $antara6dan10,'antara11dan15'=>$antara11dan15,'antara16dan20'=>$antara16dan20,'diatas20'=>$diatas20
            ));

    }

    public function get_data_dashboard_usia_tahun_perolehan()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_usia = $this->model_home->get_data_grafik_usia_tahun_perolehan($divisi)->result_array();

        //USIA
        $dibawah5 = array();
        $antara6dan10 = array();
        $antara11dan15 = array();
        $antara16dan20 = array();
        $diatas20 = array();
        foreach($grafik_usia as $row)
        {
            array_push($dibawah5,(int)$row['satu']);
            array_push($antara6dan10,(int)$row['dua']);
            array_push($antara11dan15,(int)$row['tiga']);
            array_push($antara16dan20,(int)$row['empat']);
            array_push($diatas20,(int)$row['lima']);
        }

        echo json_encode(array(
            'dibawah5' => $dibawah5,'antara6dan10' => $antara6dan10,'antara11dan15'=>$antara11dan15,'antara16dan20'=>$antara16dan20,'diatas20'=>$diatas20
            ));

    }

    public function ax_data_datatable_tahun_perolehan(){
        $divisi = $this->input->post('divisi');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_tahun_perolehan($length, $start, $cari['value'],$divisi)->result_array();
        $count = $this->model_home->get_count_data_grafik_tahun_perolehan($cari['value'],$divisi);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }


    public function get_data_dashboard_merek()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_merek = $this->model_home->get_data_grafik_merek($divisi)->result_array();

        //MEREK
        $beijing = array();
        $daihatsu = array();
        $gdragon = array();
        $hino = array();
        $hyundai = array();
        $inobus = array();
        $isuzu = array();
        $kinglong = array();
        $mercy = array();
        $mitsubishi = array();
        $nissan = array();
        $toyota = array();
        $yutoong = array();
        $zhongtong = array();
        $aaibus = array();
        foreach($grafik_merek as $row)
        {
            array_push($beijing,(int)$row['beijing']);
            array_push($daihatsu,(int)$row['daihatsu']);
            array_push($gdragon,(int)$row['gdragon']);
            array_push($hino,(int)$row['hino']);
            array_push($hyundai,(int)$row['hyundai']);
            array_push($inobus,(int)$row['inobus']);
            array_push($isuzu,(int)$row['isuzu']);
            array_push($kinglong,(int)$row['kinglong']);
            array_push($mercy,(int)$row['mercy']);
            array_push($mitsubishi,(int)$row['mitsubishi']);
            array_push($nissan,(int)$row['nissan']);
            array_push($toyota,(int)$row['toyota']);
            array_push($yutoong,(int)$row['yutoong']);
            array_push($zhongtong,(int)$row['zhongtong']);
            array_push($aaibus,(int)$row['aaibus']);
        }

        echo json_encode(array(
            'beijing'=>$beijing, 'daihatsu'=>$daihatsu, 'gdragon'=>$gdragon, 'hino'=>$hino, 'hyundai'=>$hyundai, 'inobus'=>$inobus, 'isuzu'=>$isuzu, 'kinglong'=>$kinglong, 'mercy'=>$mercy, 'mitsubishi'=>$mitsubishi, 'nissan'=>$nissan, 'toyota'=>$toyota, 'yutoong'=>$yutoong, 'zhongtong'=>$zhongtong, 'aaibus'=>$aaibus
            ));

    }

    public function get_data_dashboard_jenis_armada()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_jenis_armada = $this->model_home->get_data_grafik_jenis_armada($divisi)->result_array();

        //JENIS
        $busbesar = array();
        $busgandeng = array();
        $busmedium = array();
        $microbus = array();
        $boxmini = array();
        $boxmedium = array();
        $boxbesar = array();
        $mediumlong = array();

        foreach($grafik_jenis_armada as $row)
        {
            array_push($busbesar,(int)$row['busbesar']);
            array_push($busgandeng,(int)$row['busgandeng']);
            array_push($busmedium,(int)$row['busmedium']);
            array_push($microbus,(int)$row['microbus']);
            array_push($boxmini,(int)$row['boxmini']);
            array_push($boxmedium,(int)$row['boxmedium']);
            array_push($boxbesar,(int)$row['boxbesar']);
            array_push($mediumlong,(int)$row['mediumlong']);
        }

        echo json_encode(array(
            'busbesar' => $busbesar,'busgandeng' => $busgandeng,'busmedium'=>$busmedium,'microbus'=>$microbus,'boxmini'=>$boxmini,'boxmedium'=>$boxmedium,'boxbesar'=>$boxbesar,'mediumlong'=>$mediumlong
            ));

    }


    public function get_data_dashboard_segmentasi()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_jenis_armada = $this->model_home->get_data_grafik_segmentasi($divisi)->result_array();

        //JENIS
        $antarkota = array();
        $perintis = array();
        $pemadumoda = array();
        $aneg = array();
        $biskota = array();
        $paket = array();
        $pariwisata = array();
        $aglomerasi = array();

        foreach($grafik_jenis_armada as $row)
        {
            array_push($antarkota,(int)$row['antarkota']);
            array_push($perintis,(int)$row['perintis']);
            array_push($pemadumoda,(int)$row['pemadumoda']);
            array_push($aneg,(int)$row['aneg']);
            array_push($biskota,(int)$row['biskota']);
            array_push($paket,(int)$row['paket']);
            array_push($pariwisata,(int)$row['pariwisata']);
            array_push($aglomerasi,(int)$row['aglomerasi']);
        }

        echo json_encode(array(
            'antarkota' => $antarkota,'perintis' => $perintis,'pemadumoda'=>$pemadumoda,'aneg'=>$aneg,'biskota'=>$biskota,'paket'=>$paket,'pariwisata'=>$pariwisata,'aglomerasi'=>$aglomerasi
            ));

    }


    public function get_data_dashboard_stnk()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_stnk = $this->model_home->get_data_grafik_stnk($divisi)->result_array();

        //JUMLAH ARMADA DAMRI
        $total_armada   = $this->model_home->get_count_armada_grafik()->row("jumlah_armada");

        //USIA
        $lebih30 = array();
        $kurang30 = array();
        $habis_masa = array();
        $belum_upload = array();
        foreach($grafik_stnk as $row)
        {
            $belum_upload_stnk = $total_armada-($row['lebih30']+$row['kurang30']+$row['habis_masa']);
            array_push($lebih30,(int)$row['lebih30']);
            array_push($kurang30,(int)$row['kurang30']);
            array_push($habis_masa,(int)$row['habis_masa']);
            array_push($belum_upload,(int)$belum_upload_stnk);
        }

        echo json_encode(array('lebih30' => $lebih30,'kurang30' => $kurang30,'habis_masa'=>$habis_masa,'belum_upload'=>$belum_upload));

    }

    public function ax_data_datatable_stnk(){
        $divisi = $this->input->post('divisi');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_stnk($length, $start, $cari['value'],$divisi)->result_array();
        $count = $this->model_home->get_count_data_grafik_stnk($cari['value'],$divisi);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function ax_data_datatable_stnk_detail_armada(){

        $id_bu = $this->input->post('id_bu');
        $kategori = $this->input->post('kategori');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home_detail->getAlldata_grafik_stnk_detail($length, $start, $cari['value'],$id_bu,$kategori)->result_array();
        $count = $this->model_home_detail->get_count_data_grafik_stnk_detail($cari['value'],$id_bu,$kategori);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }


    public function get_data_dashboard_keur()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_stnk = $this->model_home->get_data_grafik_keur($divisi)->result_array();

        //JUMLAH ARMADA DAMRI
        $total_armada   = $this->model_home->get_count_armada_grafik()->row("jumlah_armada");

        //USIA
        $lebih30 = array();
        $kurang30 = array();
        $habis_masa = array();
        $belum_upload = array();
        foreach($grafik_stnk as $row)
        {
            $belum_upload_stnk = $total_armada-($row['lebih30']+$row['kurang30']+$row['habis_masa']);
            array_push($lebih30,(int)$row['lebih30']);
            array_push($kurang30,(int)$row['kurang30']);
            array_push($habis_masa,(int)$row['habis_masa']);
            array_push($belum_upload,(int)$belum_upload_stnk);
        }

        echo json_encode(array('lebih30' => $lebih30,'kurang30' => $kurang30,'habis_masa'=>$habis_masa,'belum_upload'=>$belum_upload));

    }
    public function ax_data_datatable_keur(){
        $divisi = $this->input->post('divisi');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_keur($length, $start, $cari['value'],$divisi)->result_array();
        $count = $this->model_home->get_count_data_grafik_keur($cari['value'],$divisi);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function ax_data_datatable_keur_detail_armada(){

        $id_bu = $this->input->post('id_bu');
        $kategori = $this->input->post('kategori');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home_detail->getAlldata_grafik_keur_detail($length, $start, $cari['value'],$id_bu,$kategori)->result_array();
        $count = $this->model_home_detail->get_count_data_grafik_keur_detail($cari['value'],$id_bu,$kategori);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function get_data_dashboard_ijintrayek()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_stnk = $this->model_home->get_data_grafik_ijintrayek($divisi)->result_array();

        //JUMLAH ARMADA DAMRI
        $total_armada   = $this->model_home->get_count_armada_grafik()->row("jumlah_armada");

        //USIA
        $lebih30 = array();
        $kurang30 = array();
        $habis_masa = array();
        $belum_upload = array();
        foreach($grafik_stnk as $row)
        {
            $belum_upload_stnk = $total_armada-($row['lebih30']+$row['kurang30']+$row['habis_masa']);
            array_push($lebih30,(int)$row['lebih30']);
            array_push($kurang30,(int)$row['kurang30']);
            array_push($habis_masa,(int)$row['habis_masa']);
            array_push($belum_upload,(int)$belum_upload_stnk);
        }

        echo json_encode(array('lebih30' => $lebih30,'kurang30' => $kurang30,'habis_masa'=>$habis_masa,'belum_upload'=>$belum_upload));

    }
    public function ax_data_datatable_ijintrayek(){
        $divisi = $this->input->post('divisi');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_ijintrayek($length, $start, $cari['value'],$divisi)->result_array();
        $count = $this->model_home->get_count_data_grafik_ijintrayek($cari['value'],$divisi);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function ax_data_datatable_ijintrayek_detail_armada(){

        $id_bu = $this->input->post('id_bu');
        $kategori = $this->input->post('kategori');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home_detail->getAlldata_grafik_ijintrayek_detail($length, $start, $cari['value'],$id_bu,$kategori)->result_array();
        $count = $this->model_home_detail->get_count_data_grafik_ijintrayek_detail($cari['value'],$id_bu,$kategori);
        
        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }


    public function get_data_dashboard_outstanding()
    {
        $tahun              = $this->input->post('tahun');
        $divisi             = $this->input->post('id_divisi');
        $grafik_outstanding = $this->model_home->get_data_grafik_outstanding($tahun,$divisi)->result_array();

        //OUTSANDING
        $nilai_premi = array();
        $nilai_bayar = array();
        $nilai_outstanding = array();
        foreach($grafik_outstanding as $row)
        {
            array_push($nilai_premi,(int)$row['nilai_premi']);
            array_push($nilai_bayar,(int)$row['nilai_bayar']);
            array_push($nilai_outstanding,(int)$row['nilai_outstanding']);
        }

        echo json_encode(array('nilai_premi' => $nilai_premi, 'nilai_bayar' => $nilai_bayar, 'nilai_outstanding'=> $nilai_outstanding));

    }

    public function ax_data_datatable_outstanding(){
        $tahun = $this->input->post('tahun');
        $id_bu = $this->input->post('id_bu');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_outstanding($length, $start, $cari['value'],$tahun,$id_bu)->result_array();
        $count = $this->model_home->get_count_data_grafik_outstanding($cari['value'],$tahun,$id_bu);

        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function get_data_dashboard_asuransi()
    {
        $divisi = $this->input->post('id_divisi');
        $tahun = $this->input->post('tahun');
        $grafik_stnk = $this->model_home->get_data_grafik_asuransi($divisi, $tahun)->result_array();

        //JUMLAH ARMADA DAMRI
        $total_armada   = $this->model_home->get_count_armada_grafik()->row("jumlah_armada");

        //USIA
        $pengajuan_cabang = array();
        $jaminan_bank = array();
        $belum_terasuransi = array();
        foreach($grafik_stnk as $row)
        {
            $belum_asuransi = $total_armada-($row['pengajuan_cabang']+$row['jaminan_bank']);
            array_push($pengajuan_cabang,(int)$row['pengajuan_cabang']);
            array_push($jaminan_bank,(int)$row['jaminan_bank']);
            array_push($belum_terasuransi,(int)$belum_asuransi);
        }

        echo json_encode(array('pengajuan_cabang' => $pengajuan_cabang,'jaminan_bank' => $jaminan_bank,'belum_terasuransi'=>$belum_terasuransi));

    }

    public function ax_data_datatable_asuransi(){
        $tahun = $this->input->post('tahun');
        $divisi = $this->input->post('divisi');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_asuransi($length, $start, $cari['value'],$tahun,$divisi)->result_array();
        $count = $this->model_home->get_count_data_grafik_asuransi($cari['value'],$tahun,$divisi);

        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function ax_data_datatable_asuransi_detail_armada(){

        $tahun = $this->input->post('tahun');
        $id_bu = $this->input->post('id_bu');
        $kategori = $this->input->post('kategori');

        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home_detail->getAlldata_grafik_asuransi_detail($length, $start, $cari['value'],$tahun,$id_bu,$kategori)->result_array();
        $count = $this->model_home_detail->get_count_data_grafik_asuransi_detail($cari['value'],$tahun,$id_bu,$kategori);

        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    public function get_data_dashboard_klaim()
    {
        $divisi = $this->input->post('id_divisi');
        $tahun = $this->input->post('tahun');
        $grafik_klaim = $this->model_home->get_data_grafik_klaim($divisi, $tahun)->result_array();

        //USIA
        $progress = array();
        $selesai = array();
        foreach($grafik_klaim as $row)
        {
            array_push($progress,(int)$row['progress']);
            array_push($selesai,(int)$row['selesai']);
        }

        echo json_encode(array('progress' => $progress,'selesai' => $selesai));

    }

    public function ax_data_datatable_klaim_detail_armada(){

        $tahun = $this->input->post('tahun');
        $id_bu = $this->input->post('id_bu');
        $kategori = $this->input->post('kategori');
        $status = $this->input->post('status');

        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home_detail->getAlldata_grafik_klaim_detail($length, $start, $cari['value'],$tahun,$id_bu,$kategori,$status)->result_array();
        $count = $this->model_home_detail->get_count_data_grafik_klaim_detail($cari['value'],$tahun,$id_bu,$kategori,$status);

        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }


    public function get_data_dashboard_foto_armada()
    {
        $divisi = $this->input->post('id_divisi');
        $grafik_foto_armada = $this->model_home->get_data_grafik_foto_armada($divisi)->result_array();

        //JUMLAH ARMADA DAMRI
        $total_armada   = $this->model_home->get_count_armada_grafik()->row("jumlah_armada");

        //USIA
        $depan_kanan    = array();
        $depan_kiri     = array();
        $belakang       = array();
        $dalam          = array();
        $gesek_rangka   = array();
        $gesek_mesin    = array();
        $foto_mesin     = array();

        $depan_kanan_belum_upload   = array();
        $depan_kiri_belum_upload    = array();
        $belakang_belum_upload      = array();
        $dalam_belum_upload         = array();
        $gesek_rangka_belum_upload  = array();
        $gesek_mesin_belum_upload   = array();
        $foto_mesin_belum_upload    = array();

        foreach($grafik_foto_armada as $row)
        {
            $depan_kanan_belum_upload_     = $total_armada-$row['depan_kanan'];
            $depan_kiri_belum_upload_      = $total_armada-$row['depan_kiri'];
            $belakang_belum_upload_        = $total_armada-$row['belakang'];
            $dalam_belum_upload_           = $total_armada-$row['dalam'];

            $gesek_rangka_belum_upload_       = $total_armada-$row['gesek_rangka'];
            $gesek_mesin_belum_upload_        = $total_armada-$row['gesek_mesin'];
            $foto_mesin_belum_upload_         = $total_armada-$row['foto_mesin'];

            array_push($depan_kanan,(int)$row['depan_kanan']);
            array_push($depan_kiri,(int)$row['depan_kiri']);
            array_push($belakang,(int)$row['belakang']);
            array_push($dalam,(int)$row['dalam']);
            array_push($gesek_rangka,(int)$row['gesek_rangka']);
            array_push($gesek_mesin,(int)$row['gesek_mesin']);
            array_push($foto_mesin,(int)$row['foto_mesin']);

            array_push($depan_kanan_belum_upload,(int)$depan_kanan_belum_upload_);
            array_push($depan_kiri_belum_upload,(int)$depan_kiri_belum_upload_);
            array_push($belakang_belum_upload,(int)$depan_kanan_belum_upload_);
            array_push($dalam_belum_upload,(int)$dalam_belum_upload_);
            array_push($gesek_rangka_belum_upload,(int)$gesek_rangka_belum_upload_);
            array_push($gesek_mesin_belum_upload,(int)$gesek_mesin_belum_upload_);
            array_push($foto_mesin_belum_upload,(int)$foto_mesin_belum_upload_);
        }

        echo json_encode(array(
            'depan_kanan' => $depan_kanan,
            'depan_kiri' => $depan_kiri,
            'belakang' => $belakang,
            'dalam' => $dalam,
            'gesek_rangka' => $gesek_rangka,
            'gesek_mesin' => $gesek_mesin,
            'foto_mesin' => $foto_mesin,
            'depan_kanan_belum_upload' => $depan_kanan_belum_upload,
            'depan_kiri_belum_upload' => $depan_kiri_belum_upload,
            'belakang_belum_upload' => $belakang_belum_upload,
            'dalam_belum_upload' => $dalam_belum_upload,
            'gesek_rangka_belum_upload' => $gesek_rangka_belum_upload,
            'gesek_mesin_belum_upload' => $gesek_mesin_belum_upload,
            'foto_mesin_belum_upload' => $foto_mesin_belum_upload,
            ));

    }

    public function ax_data_datatable_foto_armada(){
        $id_bu = $this->input->post('id_bu');
        $start = $this->input->post('start');
        $draw = $this->input->post('draw');
        $length = $this->input->post('length');
        $cari = $this->input->post('search', true);
        $data = $this->model_home->getAlldata_grafik_foto_armada($length, $start, $cari['value'],$id_bu)->result_array();
        $count = $this->model_home->get_count_data_grafik_foto_armada($cari['value'],$id_bu);

        echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'],'data' => $data ));
    }

    

    public function get_data_jumlah_armada()
    {
        $session = $this->session->userdata('login');

        $tot_all_armada     = $this->model_home->get_count_armada()->row("jumlah_armada");
        $tot_armada_ua     = $this->model_home->get_count_armada_usul_afkir()->row("jumlah_armada");
        $tot_armada_kso     = $this->model_home->get_count_armada_kso()->row("jumlah_armada");
        $tot_armada_kso_ua   = $this->model_home->get_count_armada_kso_ua()->row("jumlah_armada");
        $tot_armada_sewa    = $this->model_home->get_count_armada_sewa()->row("jumlah_armada");
        $tot_armada_sewa_ua   = $this->model_home->get_count_armada_sewa_ua()->row("jumlah_armada");
        $tot_armada_damri     = $tot_all_armada-$tot_armada_kso;
        $tot_armada_damri_ua   = $this->model_home->get_count_armada_damri_ua()->row("jumlah_armada");

        $tot_jumlah_cabang_sewa   = $this->model_home->get_count_jumlah_cabang_sewa()->row("jumlah_cabang");

        echo json_encode(array(
            'total_armada' => $tot_all_armada,'armada_kso' => $tot_armada_kso,'armada_sewa' => $tot_armada_sewa,'armada_damri' => $tot_armada_damri,'armada_ua' => $tot_armada_ua,'armada_damri_ua' => $tot_armada_damri_ua,'armada_kso_ua' => $tot_armada_kso_ua,'armada_sewa_ua' => $tot_armada_sewa_ua,'jumlah_cabang_sewa' => $tot_jumlah_cabang_sewa
            ));
    }

    public function get_data_tahun_perolehan($id_divisi)
    {
        if($id_divisi==0 ||$id_divisi==5){
            $query_divisi = "";
        }else{
            $query_divisi = "AND b.id_divre=$id_divisi";
        }
        $query = $this->db->query("
           SELECT id_bu,divre,tahun_perolehan,SUM(jumlah_armada) jumlah
           FROM
           (
           SELECT a.id_bu, b.id_divre as divre, a.tahun_perolehan,count(kd_armada) jumlah_armada 
           FROM ref_armada a 
           LEFT JOIN ref_bu b on a.id_bu=b.id_bu
           where a.active<>2 $query_divisi
           group by a.id_bu,a.tahun_perolehan ORDER BY a.id_bu
           )x
           GROUP BY tahun_perolehan,divre
           ORDER BY divre,tahun_perolehan
           ");

        $data = $query->result();

        echo json_encode($data);
    }

}
