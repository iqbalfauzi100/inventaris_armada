<?php if (! defined('BASEPATH')) {
  exit('No direct script access allowed');
}
class apicucibus extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model("model_login");
    $this->load->model("model_api");
  }

  function cekversi(){
    $id_apps 	= $this->input->post("id_apps");
    $qry = "select * FROM ref_apps where id_apps = '".$id_apps."'";
    $data = $this->db->query($qry)->result();
    echo json_encode($data);
  }

	function login(){  
		$username 		= $this->input->post("username");
		$password 		= $this->input->post("password");
		$result = $this->model_login->logincucibus($username, $password);

			//jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
		if ($result) {
		  foreach ($result as $row) {
			$hasil = array(
			  'nm_user'=> $row->nm_user,
			  'id_user'=> $row->id_user,
			  'username' => $row->username,
			  'id_level' => $row->id_level,
			  'id_bu' => $row->id_bu,
			  'status' => 1,
			  );
		  }
		}else{

		 $hasil = array(
		  'nm_user'=> '',
		  'id_user'=> '',
		  'username' => '',
		  'id_level' => '',
		  'id_bu' => '',
		  'status' => 0,
		  );
	   }
	   echo json_encode($hasil);
	}
	
	
	function listLokasi(){
		$id_bu	= $this->input->post("id_bu");
		$qry	= "SELECT id_pool,kd_pool, nm_pool FROM ref_pool where id_bu = '".$id_bu."'";
		$data 	= $this->db->query($qry)->result();
		echo json_encode($data);
	}
	
	
	function detailArmada(){
		$kd_armada	= $this->input->post("kd_armada");
		$qry		= "SELECT id_armada,plat_armada, tahun_armada, nm_merek, nm_layanan, nm_segment FROM ref_armada where kd_armada = '".$kd_armada."'";
		$data 		= $this->db->query($qry)->result();
		echo json_encode($data);
	}
	
	function detailHarga(){
		$id_bu		= $this->input->post("id_bu");
		$qry		= "SELECT id_harga,kd_harga, harga nm_merek  FROM ref_harga_cuci where id_bu = '".$id_bu."'";
		$data 		= $this->db->query($qry)->result();
		echo json_encode($data);
	}
	
	
	
	function mulaiCuci(){
		$id_bu		= $this->input->post("id_bu");
		$id_pool	= $this->input->post("id_pool");
		$id_armada	= $this->input->post("id_armada");
		$id_harga	= $this->input->post("id_harga");
		$id_user	= $this->input->post("id_user");
		$data 		= array(
						'id_bu' => $id_bu,
						'id_pool' => $id_pool,
						'id_armada' => $id_armada,
						'id_harga' => $id_harga,
						'id_user' => $id_user,
						'status' => '0',
						'jam_mulai' => DATE("Y-m-d H:i:s"),
					);
		$status = $this->model_api->mulai_cuci($data);
		echo json_encode($status);
	}
	
	function selesaiCuci(){
		$id		= $this->input->post("id_cucibus");
		$data 		= array(
						'id_cucibus' => $id,
						'status' => '1',
						'jam_akhir' => DATE("Y-m-d H:i:s"),
					);
		$status = $this->model_api->selesai_cuci($data);
		echo json_encode($status);
	}
	
	
	

 function update_user(){  
  $id_user = $this->input->post("id_user");
  $data = array(
    'password' => do_hash($this->input->post("password_baru"), 'md5')
    );

  $result =  $this->model_api->UpdateUser(array('id_user' => $this->input->post('id_user')), $data);
  // $result = $this->model_api->UpdateUser($id_user, $data);

  if ($result) {
   $hasil = array(

    'hasil' => $result,
    'status' => 1,
    );

 }else{

   $hasil = array(

    'hasil' => $result,
    'status' => 0,
    );
 }

 echo json_encode($hasil);   

}

function databus(){  
  $kd_armada 		= $this->input->post("kd_armada");
  $result = $this->model_api->databus($kd_armada);

        //jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
  if ($result) {
    foreach ($result as $row) {
      $hasil = array(
        'id_lmb'=> $row->id_lmb,
        'kd_trayek'=> $row->kd_trayek,
        'kd_armada' => $row->kd_armada,
        'kd_point_awal'=> $row->kd_point_awal,
        'kd_point_akhir' => $row->kd_point_akhir,
        'nm_point_awal' => $row->nm_point_awal,
        'nm_point_akhir' => $row->nm_point_akhir,
        'kd_segmen' => $row->kd_segmen,
        'nm_layanan' => $row->nm_layanan,
        'nm_driver' => $row->nm_driver,
        'rit' => $row->rit,
        'setting_ritase' => $row->setting_ritase,
        'status' => 1,
        );
    }
  }else{

   $hasil = array(
    'id_lmb'=> '',
    'kd_trayek'=>'',
    'kd_armada' => '',
    'kd_point_awal'=> '',
    'kd_point_akhir' => '',
    'nm_point_awal' => '',
    'nm_point_akhir' => '',
    'kd_segmen' => '',
    'nm_layanan' => '',
    'nm_driver' => '',
    'rit' => '',
    'setting_ritase' => '',
    'status' => 0,
    );
 }


 echo json_encode($hasil);
}

function databus_rit(){  
  $kd_armada    = $this->input->post("kd_armada");
  $result = $this->model_api->databus_rit($kd_armada);

        //jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
  if ($result) {
    foreach ($result as $row) {
      $hasil = array(
        'id_lmb'=> $row->id_lmb,
        'kd_trayek'=> $row->kd_trayek,
        'kd_armada' => $row->kd_armada,
        'kd_point_awal'=> $row->kd_point_awal,
        'kd_point_akhir' => $row->kd_point_akhir,
        'nm_point_awal' => $row->nm_point_awal,
        'nm_point_akhir' => $row->nm_point_akhir,
        'kd_segmen' => $row->kd_segmen,
        'nm_layanan' => $row->nm_layanan,
        'nm_driver' => $row->nm_driver,
        'rit' => $row->rit,
        'setting_ritase' => $row->setting_ritase,
        'status' => 1,
        );
    }
  }else{

   $hasil = array(
    'id_lmb'=> '',
    'kd_trayek'=>'',
    'kd_armada' => '',
    'kd_point_awal'=> '',
    'kd_point_akhir' => '',
    'nm_point_awal' => '',
    'nm_point_akhir' => '',
    'kd_segmen' => '',
    'nm_layanan' => '',
    'nm_driver' => '',
    'rit' => '',
    'setting_ritase' => '',
    'status' => 0,
    );
 }


 echo json_encode($hasil);
}

function databuscabang(){  
  $id_bu      = $this->input->post("id_bu");
  $result = $this->model_api->databuscabang($id_bu);

        //jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
        // if ($result) {
        //     foreach ($result as $row) {
        //         $hasil = array(
        //             'id_lmb'=> $row->id_lmb,
        //             'kd_trayek'=> $row->kd_trayek,
        //             'kd_armada' => $row->kd_armada,
        //             'nm_point_awal' => $row->nm_point_awal,
        //             'nm_point_akhir' => $row->nm_point_akhir,
        //             'nm_driver' => $row->nm_driver,
        //             'rit' => $row->rit,
        //             'status' => 1,
        //         );
        //     }
        // }else{

        //     $hasil = array(
        //             'id_lmb'=> '',
        //             'kd_trayek'=>'',
        //             'kd_armada' => '',
        //             'nm_point_awal' => '',
        //             'nm_point_akhir' => '',
        //             'nm_driver' => '',
        //             'rit' => '',
        //             'status' => 0,
        //         );
        // }


  echo json_encode($result);
}


function datalmb(){  

  $id_lmb      = $this->input->post("id_lmb");
  $result = $this->model_api->getAllritlmb($id_lmb)->result_array();

  echo json_encode($result);
}

function datarit(){  

  $id_lmb      = $this->input->post("id_lmb");
  $rit      = $this->input->post("rit");
  $result = $this->model_api->getAllritlmbuser($id_lmb, $rit)->result_array();

  echo json_encode($result);
}

function dataritdriver(){  

  $nik      = $this->input->post("nik");
  $tanggal      = $this->input->post("tanggal");
  $result = $this->model_api->getAllritlmbdriver($nik, $tanggal)->result_array();

  echo json_encode($result);
}

function dataritgroup(){  

  $id_user      = $this->input->post("id_user");
  $tanggal      = $this->input->post("tanggal");
  $result = $this->model_api->getAllritlmbusergroup($id_user, $tanggal)->result_array();

  echo json_encode($result);
}
     // My damri
function datalmbdriver(){  

  $id_driver    = $this->input->post("id_driver");
  $tanggal      = $this->input->post("tanggal");
  $result = $this->model_api->getlmbdriver($id_driver, $tanggal)->result_array();

  echo json_encode($result);
}

function datatrayek(){  

  $id_bu          = $this->input->post("id_bu");
  $kd_trayek      = $this->input->post("kd_trayek");
  $result         = $this->model_api->getAlltrayek($id_bu, $kd_trayek)->result_array();

  echo json_encode($result);
}


function saveritase(){  
  $data         = $this->input->post("data");
		// $id_lmb 		= $this->input->post("id_lmb");
		// $id_user 		= $this->input->post("id_user");
		// $pnp 		= $this->input->post("pnp");
  //       $kd_trayek        = $this->input->post("kd_trayek");
  //       $type_rit        = $this->input->post("type_rit");
  //       $note        = $this->input->post("note");
  //       $data = array(
  //               'id_lmb' => $id_lmb,
  //   			'cuser' => $id_user,
  //               'pnp' => $pnp,
  //               'kd_trayek' => $kd_trayek,
  //               'type_rit' => $type_rit,
  //   			'note' => $note,
  //   		);

  $profile = json_decode($data, TRUE);
  $result = $this->model_api->insert_ritase($profile);

  if ($result) {
   $hasil = array(

    'hasil' => $result,
    'status' => 1,
    );

 }else{

   $hasil = array(

    'hasil' => $result,
    'status' => 0,
    );
 }

 echo json_encode($hasil);   

}

function adi(){
  $data         = $this->input->post("data");
  $profile = json_decode($data, TRUE);
  var_dump($profile);
}


function test(){
  $data = array(array(
    'id_lmb' => 1,
    'cuser' => 1,
    'pnp' => 1,
    'kd_trayek' => "bbd-ygy",
    'type_rit' => 1,
    'note' => "",
    ),array(
    'id_lmb' => 1,
    'cuser' => 1,
    'pnp' => 1,
    'kd_trayek' => "bbd-ygy",
    'type_rit' => 1,
    'note' => "",
    ),array(
    'id_lmb' => 1,
    'cuser' => 1,
    'pnp' => 1,
    'kd_trayek' => "bbd-ygy",
    'type_rit' => 1,
    'note' => "",
    ))
  ;
  echo json_encode($data);   
}


function ads(){
 echo "<script>location.replace('https://play.google.com/store/apps/details?id=com.simadamri.damriapps')</script>";
}

function jadwal_armada(){
  $id_bu      =$this->input->post('id_bu');
  $kd_trayek  =$this->input->post('kd_trayek');
  $armada     =$this->input->post('armada');
  $tanggal    =$this->input->post('tanggal');

  $this->db->select("a.id_jadwal,a.unik,a.id_pool,a.id_cabang,a.bis,a.armada,a.tanggal,a.kd_trayek,a.kd_segmen,a.asal,a.tujuan,a.harga,a.driver1,a.driver2,a.nm_cabang,a.nm_trayek,a.nm_segmen,a.nm_asal,a.nm_tujuan,a.kd_trayek,b.km_trayek,b.km_empty");
  $this->db->from("ms_jadwal a");
  $this->db->join("ref_trayek b", "a.kd_trayek = b.kd_trayek","left");

  if($kd_trayek!=null || $kd_trayek!='' || $kd_trayek!=0){
    $this->db->where('a.kd_trayek', $kd_trayek);
  }
  if($armada!=null || $armada!='' || $armada!=0){
    $this->db->where('a.armada', $armada);
  }
  if($tanggal!=null || $tanggal!='' || $tanggal!=0){
    $this->db->where('a.tanggal', $tanggal);
  }
  $this->db->where('a.id_cabang', $id_bu);
  $data = $this->db->get()->result_array();

  echo json_encode($data); 
}

public function point_ppa()
{
  $this->db->select("a.*"); 
  $this->db->from("ref_point a");
  $this->db->where('a.set_ppa', 1);
  $this->db->where('a.flag', 1);
  $data = $this->db->get()->result_array();
  echo json_encode($data);
}


public function asuransi()
{
  $data_asuransi = $this->db->query("
    SELECT IFNULL(a.id_armada,0) id_armada  
    from ref_asuransi_detail a 
    LEFT JOIN ref_asuransi b on a.id_asuransi=b.id_asuransi
    WHERE (select CURRENT_DATE) > b.tgl_akhir
    ");

  if($data_asuransi->num_rows()>0){

    foreach ($data_asuransi->result() as $row) {
      $id_armada = $row->id_armada;

      $update_armada = $this->db->query("UPDATE ref_armada set active=1 WHERE id_armada='$id_armada' ");
    }

  }

}

}
