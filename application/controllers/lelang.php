<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class lelang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_lelang");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combobox_bu'] = $this->model_lelang->combobox_bu();
                $data['combobox_bu_all'] = $this->model_lelang->combobox_bu_all();
                $data['combobox_tahun'] = $this->model_lelang->combobox_tahun();
                $data['combobox_segment'] = $this->model_lelang->combobox_segment();

                $this->load->view('armada/lelang', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    

    public function ax_data_lelang()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                $tahun = $this->input->post('tahun');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_lelang->getAlllelang($length, $start, $cari['value'],$id_bu,$tahun)->result_array();
                $count = $this->model_lelang->get_count_lelang($cari['value'],$id_bu,$tahun);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_lelang_detail()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang = $this->input->post('id_lelang');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_lelang->getAlllelang_detail($length, $start, $cari['value'],$id_lelang)->result_array();
                $count = $this->model_lelang->get_count_lelang_detail($cari['value'],$id_lelang);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang = $this->input->post('id_lelang');
                $id_bu = $this->input->post('id_bu');
                $nm_pemenang = $this->input->post('nm_pemenang');
                $nilai_liduidasi = $this->input->post('nilai_liduidasi');
                $nilai_lelang = $this->input->post('nilai_lelang');
                $no_risalah = $this->input->post('no_risalah');
                $tahun_ua   = $this->input->post('tahun_ua');
                $tgl_lelang = $this->input->post('tgl_lelang');
                $active = $this->input->post('active');
                $session = $this->session->userdata('login');
                $data = array(
                    'id_lelang'     => $id_lelang,
                    'id_bu'         => $id_bu,
                    'nm_pemenang'   => $nm_pemenang,
                    'nilai_liduidasi' => $nilai_liduidasi,
                    'nilai_lelang'  => $nilai_lelang,
                    'no_risalah'    => $no_risalah,
                    'tahun_ua'      => $tahun_ua,
                    'tgl_lelang'    => $tgl_lelang,
                    'active'        => 1,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan']
                    );
                // echo json_encode($data);
                // exit();

                if(empty($id_lelang))
                 $data['id_lelang'] = $this->model_lelang->insert_lelang($data);
             else
                 $data['id_lelang'] = $this->model_lelang->update_lelang($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_set_data_detail()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang      = $this->input->post('id_lelang');
                $id_usul_afkir  = $this->input->post('id_usul_afkir');

                // echo json_encode($id_usul_afkir);
                // exit();

                $session        = $this->session->userdata('login');

                $data = array(
                    'id_lelang'     => $id_lelang,
                    'id_usul_afkir' => $id_usul_afkir,
                    'active'        => 1,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan']
                    );

                $data['id_lelang'] = $this->model_lelang->insert_lelang_detail($data);

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_unset_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang = $this->input->post('id_lelang');

                $data = array('id_lelang' => $id_lelang);

                if(!empty($id_lelang))
                 $data['id_lelang'] = $this->model_lelang->delete_lelang($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data_detail()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang_detail = $this->input->post('id_lelang_detail');

                $data = array('id_lelang_detail' => $id_lelang_detail);

                if(!empty($id_lelang_detail))
                    $data['id_lelang_detail'] = $this->model_lelang->delete_lelang_detail($data);

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function checkouthapusbuku()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang    = $this->input->post('id_lelang');

                //CEK JUMLAH ARMADA DI LELANG DETAIL
                $this->db->select("COUNT(a.id_lelang_detail) jumlah");
                $this->db->from("ref_armada_lelang_detail a");
                $this->db->where('a.id_lelang', $id_lelang);
                $jumlah_armada_lelang = $this->db->get()->row("jumlah");


                if($jumlah_armada_lelang>0){

                    //CEK DATA ARMADA DI LELANG DETAIL
                    $this->db->select("a.*,b.tahun_ua");
                    $this->db->from("ref_armada_lelang_detail a");
                    $this->db->join("ref_armada_lelang b","a.id_lelang = b.id_lelang", 'left');
                    $this->db->where('a.id_lelang', $id_lelang);
                    $data_armada_lelang = $this->db->get()->result();

                    foreach ($data_armada_lelang as $row) {

                        $data = array(
                            'id_armada' => $row->id_armada, 
                            'kd_armada' => $row->kd_armada,
                            'id_bu'     => $row->id_bu,
                            'tahun'     => $row->tahun_ua,
                            'cuser'     => $session['id_user'],
                            );

                        //INSERT REF_ARMADA_HAPUS_BUKU
                        $this->model_lelang->insert_hapus_buku($data);
                        //UPDATE REF_ARMADA
                        $this->model_lelang->update_armada($data);

                    }

                    //UPDATE REF_ARMADA_LELANG
                    $data_lelang = array('id_lelang' => $id_lelang, 'active' => 2);
                    $this->model_lelang->update_lelang($data_lelang);

                    echo json_encode(array('status' => 'success', 'data' => $data));

                }else{
                    echo json_encode(array('status' => 'gagal'));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    
    public function ax_get_data_by_id()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_lelang = $this->input->post('id_lelang');

              if(empty($id_lelang))
                 $data = array();
             else
                 $data = $this->model_lelang->get_lelang_by_id($id_lelang);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_get_data_by_id_detail()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_lelang_in = $this->input->post('id_lelang_in');

                if(empty($id_lelang_in))
                    $data = array();
                else
                    $data = $this->model_lelang->get_lelang_by_id_detail($id_lelang_in);

                echo json_encode($data);

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $id_bu = $this->input->post('id_bu');
                $approvedyear = $this->input->post('approvedyear');
                $data = $this->model_lelang->combobox_armada($id_bu, $approvedyear);
                $html = "<option value='0'>--KD Armada--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_usul_afkir."'>".$row->kd_armada."</option>"; 
                }
                $callback = array('data_armada'=>$html);
                echo json_encode($callback);



            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

}
