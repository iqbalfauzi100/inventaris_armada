<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class asuransi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_asuransi");
        $this->load->model("model_menu");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level'] = $session['id_level'];
                $data['combo_armada'] = $this->model_asuransi->combo_armada();
                $data['combobox_bu'] = $this->model_asuransi->combobox_cabang();
                $data['combobox_tahun'] = $this->model_asuransi->combobox_tahun();
                $data['combobox_jenis_asuransi'] = $this->model_asuransi->combobox_jenis_asuransi();
                $this->load->view('asuransi/index', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    

    public function ax_data_asuransi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                $tahun = $this->input->post('tahun');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_asuransi->getAllasuransi($length, $start, $cari['value'],$id_bu, $tahun)->result_array();
                $count = $this->model_asuransi->get_count_asuransi($cari['value'],$id_bu, $tahun);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_asuransi_detail()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_asuransi->getAllasuransiDetail($length, $start, $cari['value'],$id_asuransi)->result_array();
                $count = $this->model_asuransi->get_count_asuransi_detail($cari['value'],$id_asuransi);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_asuransi_bayar()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_asuransi->getAllasuransiBayar($length, $start, $cari['value'],$id_asuransi)->result_array();
                $count = $this->model_asuransi->get_count_asuransi_bayar($cari['value'],$id_asuransi);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_asuransi_history_premi()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_asuransi->getAllasuransiHistoryPremi($length, $start, $cari['value'],$id_asuransi)->result_array();
                $count = $this->model_asuransi->get_count_asuransi_history_premi($cari['value'],$id_asuransi);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_data_asuransi_documents()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_asuransi->getAllasuransiDocuments($length, $start, $cari['value'],$id_asuransi)->result_array();
                $count = $this->model_asuransi->get_count_asuransi_documents($cari['value'],$id_asuransi);

                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');
                $tahun = $this->input->post('tahun');
                $id_bu = $this->input->post('id_bu');
                $id_jenis_asuransi = $this->input->post('id_jenis_asuransi');
                $insured = $this->input->post('insured');
                $no_polis = $this->input->post('no_polis');
                $tgl_awal = $this->input->post('tgl_awal');
                $tgl_akhir = $this->input->post('tgl_akhir');
                $nilai_premi = $this->input->post('nilai_premi');
                $active = $this->input->post('active');
                $session = $this->session->userdata('login');
                $data = array(
                    'id_asuransi'   => $id_asuransi,
                    'tahun'         => $tahun,
                    'id_bu'         => $id_bu,
                    'id_jenis_asuransi'  => $id_jenis_asuransi,
                    'insured'       => $insured,
                    'no_polis'      => $no_polis,
                    'tgl_awal'      => $tgl_awal,
                    'tgl_akhir'     => $tgl_akhir,
                    'nilai_premi'   => $nilai_premi,
                    'active'        => $active,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan']
                    );

                // $data_update_premi = array(
                //     'id_asuransi'   => $id_asuransi,
                //     'nilai_premi'   => $nilai_premi,
                //     'keterangan'    => $keterangan,
                //     'active'        => $active,
                //     'cuser'         => $session['id_user'],
                //     'id_perusahaan' => $session['id_perusahaan']
                //     );

                if(empty($id_asuransi)){
                 $data['id_asuransi'] = $this->model_asuransi->insert_asuransi($data);
                 // $data['id_asuransi'] = $this->model_asuransi->insert_asuransi_history_premi($data_update_premi);
             }else{
               $data['id_asuransi'] = $this->model_asuransi->update_asuransi($data);
                 // $data['id_asuransi'] = $this->model_asuransi->insert_asuransi_history_premi($data_update_premi);
           }

           echo json_encode(array('status' => 'success', 'data' => $data));

       } else {
        echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
    }
} else {
    if ($this->uri->segment(1) != null) {
        $url = $this->uri->segment(1);
        $url = $url.' '.$this->uri->segment(2);
        $url = $url.' '.$this->uri->segment(3);
        redirect('welcome/relogin/?url='.$url.'', 'refresh');
    } else {
        redirect('welcome/relogin', 'refresh');
    }
}
}

public function ax_set_data_detail()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi_detail = $this->input->post('id_asuransi_detail');
                $id_asuransi = $this->input->post('id_asuransi');
                $no_certificate = $this->input->post('no_certificate');
                $id_armada = $this->input->post('id_armada');

                $waktu_kejadian = $this->input->post('waktu_kejadian');
                $estimasi_perbaikan = $this->input->post('estimasi_perbaikan');
                $klaim_disetujui = $this->input->post('klaim_disetujui');
                $tjh_3 = $this->input->post('tjh_3');
                $own_risk = $this->input->post('own_risk');
                $status = $this->input->post('status');

                $active = $this->input->post('active');
                $session = $this->session->userdata('login');

                $data = array(
                    'id_asuransi_detail' => $id_asuransi_detail,
                    'id_asuransi' => $id_asuransi,
                    'no_certificate' => $no_certificate,
                    'id_armada'     => $id_armada,

                    'waktu_kejadian' => $waktu_kejadian,
                    'estimasi_perbaikan' => $estimasi_perbaikan,
                    'klaim_disetujui'     => $klaim_disetujui,
                    'tjh_3' => $tjh_3,
                    'own_risk' => $own_risk,
                    'status' => $status,

                    'active'        => $active,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan'],
                    );

                if(empty($id_asuransi_detail))
                 $data['id_asuransi_detail'] = $this->model_asuransi->insert_asuransi_detail($data);
             else
                 $data['id_asuransi_detail'] = $this->model_asuransi->update_asuransi_detail($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_set_data_bayar()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi    = $this->input->post('id_asuransi_header_bayar');
                $tgl_bayar      = $this->input->post('tgl_bayar');
                $total_bayar    = $this->input->post('total_bayar');
                $session        = $this->session->userdata('login');
                $data = array(
                    'id_asuransi'   => $id_asuransi,
                    'tgl_bayar'     => $tgl_bayar,
                    'total_bayar'   => $total_bayar,
                    'active'        => 1,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan']
                    );

                //QUERY NILAI PREMI SEKARANG
                $this->db->select("IFNULL(nilai_premi,0) nilai_premi");
                $this->db->from("ref_asuransi");
                $this->db->where('id_asuransi',$id_asuransi);
                $nilai_premi = $this->db->get()->row("nilai_premi");


                //QUERY JUMLAH BAYAR SEKARANG
                $this->db->select("IFNULL(SUM(total_bayar),0) total_bayar");
                $this->db->from("ref_asuransi_bayar");
                $this->db->where('id_asuransi',$id_asuransi);
                $total_all_bayar = $this->db->get()->row("total_bayar");

                // echo $total_bayar;
                // exit();
                if($nilai_premi>= ($total_all_bayar+$total_bayar) ){
                    $data['id_asuransi_bayar'] = $this->model_asuransi->insert_asuransi_bayar($data);
                    echo json_encode(array('status' => 'success', 'data' => $data));
                }else{
                    echo json_encode(array('status' => 'gagal', 'data' => ($nilai_premi-$total_all_bayar) ));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }


    public function ax_set_data_premi()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi        = $this->input->post('id_asuransi_header_history_premi');
                $nilai_premi_tambah = $this->input->post('nilai_premi_tambah');
                $nilai_premi_kurang = $this->input->post('nilai_premi_kurang');
                $keterangan        = $this->input->post('keterangan');
                $session            = $this->session->userdata('login');
                $data = array(
                    'id_asuransi'        => $id_asuransi,
                    'nilai_premi_tambah' => $nilai_premi_tambah,
                    'nilai_premi_kurang' => $nilai_premi_kurang,
                    'keterangan'         => $keterangan,
                    'active'        => 1,
                    'cuser'         => $session['id_user'],
                    'id_perusahaan' => $session['id_perusahaan']
                    );

                $data['id_asuransi'] = $this->model_asuransi->insert_asuransi_history_premi($data);

                echo json_encode(array('status' => 'success', 'data' => $data));

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_unset_data()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi = $this->input->post('id_asuransi');

                $data = array('id_asuransi' => $id_asuransi);

                if(!empty($id_asuransi))
                 $data['id_asuransi'] = $this->model_asuransi->delete_asuransi($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data_detail()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi_detail = $this->input->post('id_asuransi_detail');

                $data = array('id_asuransi_detail' => $id_asuransi_detail);

                if(!empty($id_asuransi_detail))
                 $data['id_asuransi_detail'] = $this->model_asuransi->delete_asuransi_detail($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data_bayar()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi_bayar = $this->input->post('id_asuransi_bayar');

                $data = array('id_asuransi_bayar' => $id_asuransi_bayar);

                if(!empty($id_asuransi_bayar))
                 $data['id_asuransi_bayar'] = $this->model_asuransi->delete_asuransi_bayar($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_unset_data_history_premi()
{
  if ($this->session->userdata('login')) {
    $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi_history_premi = $this->input->post('id_asuransi_history_premi');

                $data = array('id_asuransi_history_premi' => $id_asuransi_history_premi);

                if(!empty($id_asuransi_history_premi))
                 $data['id_asuransi_history_premi'] = $this->model_asuransi->delete_asuransi_history_premi($data);

             echo json_encode(array('status' => 'success', 'data' => $data));

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_get_data_by_id()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

              $id_asuransi = $this->input->post('id_asuransi');

              if(empty($id_asuransi))
                 $data = array();
             else
                 $data = $this->model_asuransi->get_asuransi_by_id($id_asuransi);

             echo json_encode($data);

         } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

public function ax_get_data_by_id_detail()
{
    if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U05";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_asuransi_detail = $this->input->post('id_asuransi_detail');

                if(empty($id_asuransi_detail))
                 $data = array();
             else
                $data = $this->model_asuransi->get_asuransi_by_id_detail($id_asuransi_detail);

            echo json_encode($data);

        } else {
            echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
        }
    } else {
        if ($this->uri->segment(1) != null) {
            $url = $this->uri->segment(1);
            $url = $url.' '.$this->uri->segment(2);
            $url = $url.' '.$this->uri->segment(3);
            redirect('welcome/relogin/?url='.$url.'', 'refresh');
        } else {
            redirect('welcome/relogin', 'refresh');
        }
    }
}

}
