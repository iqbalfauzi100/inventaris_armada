<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class setoran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_setoran");
        $this->load->model("model_menu");
        $this->load->model("model_jadwal");
        ///constructor yang dipanggil ketika memanggil ro.php untuk melakukan pemanggilan pada model : ro.php yang ada di folder models
    }
    
    public function index()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $data['id_user'] = $session['id_user'];
                $data['nm_user'] = $session['nm_user'];
                $data['session_level']      = $session['id_level'];
                $data['combobox_bu']        = $this->model_setoran->combobox_bu();
                $data['combobox_segmen']    = $this->model_jadwal->combobox_segmen();
                
                $this->load->view('setoran/index', $data);
                $this->load->view('setoran/pertelaan', $data);
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_data_setoran()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $active = $this->input->post('active');
                $id_bu = $this->input->post('id_bu');
                $id_pool = $this->input->post('id_pool');
                $tanggal = $this->input->post('tanggal');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran($length, $start, $cari['value'], $id_bu,$id_pool,$tanggal, $active)->result_array();
                $count = $this->model_setoran->get_count_setoran($cari['value'], $id_bu,$id_pool,$tanggal, $active);
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    function ax_data_setoran_detail()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran_detail($length, $start, $cari['value'], $id_setoran)->result_array();
                $count = $this->model_setoran->get_count_setoran_detail($cari['value'], $id_setoran);
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    function ax_data_setoran_detail_pnp()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran_detail_pnp($length, $start, $cari['value'], $id_setoran,$id_setoran_detail)->result_array();
                $count = $this->model_setoran->get_count_setoran_detail_pnp($cari['value'], $id_setoran,$id_setoran_detail);

                $count_all = $this->db->query("SELECT COALESCE(SUM(jumlah),0) as jum,COALESCE(SUM(bagasi_pnp),0) as bagasi,COALESCE(SUM(total),0) as tot from ref_setoran_detail_pnp where id_setoran_detail='$id_setoran_detail'");

                $penumpang = $count_all->row("jum");
                $bagasi = $count_all->row("bagasi");
                $total = $count_all->row("tot");

                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data,'penumpang' => $penumpang,'bagasi' => $bagasi,'total' => $total,));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    function ax_data_setoran_detail_pnp_pertelaan()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_pnp = $this->input->post('id_setoran_pnp');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran_detail_pnp_pertelaan($length, $start, $cari['value'], $id_setoran_pnp)->result_array();
                $count = $this->model_setoran->get_count_setoran_detail_pnp_pertelaan($cari['value'], $id_setoran_pnp);
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    function ax_data_setoran_detail_pend()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran_detail_pend($length, $start, $cari['value'], $id_setoran,$id_setoran_detail)->result_array();
                $count = $this->model_setoran->get_count_setoran_detail_pend($cari['value'], $id_setoran,$id_setoran_detail);

                $count_all = $this->db->query("SELECT COALESCE(SUM(total),0) as tot from ref_setoran_detail_pend where id_setoran_detail='$id_setoran_detail'");
                $total = $count_all->row("tot");
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data, 'total' => $total));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    function ax_data_setoran_detail_beban()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->getAllsetoran_detail_beban($length, $start, $cari['value'], $id_setoran,$id_setoran_detail)->result_array();
                $count = $this->model_setoran->get_count_setoran_detail_beban($cari['value'], $id_setoran,$id_setoran_detail);

                $count_all = $this->db->query("SELECT COALESCE(SUM(total),0) as tot from ref_setoran_detail_beban where id_setoran_detail='$id_setoran_detail'");
                $total = $count_all->row("tot");
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data, 'total' => $total));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_set_data_header()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran = $this->input->post('id_setoran');
                $armada = $this->input->post('armada');
                $tanggal = $this->input->post('tanggal');
                $id_bu = $this->input->post('id_bu');
                $id_pool = $this->input->post('id_pool');
                
                $session = $this->session->userdata('login');
                $data = array(
                    'id_setoran' => $id_setoran,
                    'armada' => $armada,
                    'tgl_setoran' => $tanggal,
                    'id_bu' => $id_bu,
                    'id_pool' => $id_pool,
                    'id_user' => $session['id_user'],
                    );
                
                if($id_setoran == 0){
                    $status = $this->model_setoran->insert_setoran($data);
                }else{
                    $status = $this->model_setoran->update_setoran($data);
                }
                
                echo json_encode(array('status' => $status));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_set_data_detail()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran_header');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $id_jadwal = $this->input->post('id_jadwal');
                $armada = $this->input->post('armada');
                $tanggal = $this->input->post('tanggal');
                $kd_segmen = $this->input->post('kd_segmen');
                $kd_trayek = $this->input->post('kd_trayek');
                $driver1 = $this->input->post('driver1');
                $driver2 = $this->input->post('driver2');
                $id_layanan = $this->input->post('id_layanan');
                
                $session = $this->session->userdata('login');
                $data = array(
                    'id_setoran' => $id_setoran,
                    'id_jadwal' => $id_jadwal,
                    'id_setoran_detail' => $id_setoran_detail,
                    'tanggal'   => $tanggal,
                    'armada'    => $armada,
                    'kd_segmen' => $kd_segmen,
                    'kd_trayek' => $kd_trayek,
                    'driver1'   => $driver1,
                    'driver2'    => $driver2,
                    'id_layanan' => $id_layanan,
                    'id_user'   => $session['id_user'],
                    );
                
                if($id_setoran_detail == 0){

                    $status = $this->model_setoran->insert_setoran_detail($data);
                }
                else{
                    $status = $this->model_setoran->update_setoran_detail($data);
                }
                
                echo json_encode(array('status' => $status));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_set_data_detail_pnp()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran             = $this->input->post('id_setoran_header');
                $id_setoran_detail      = $this->input->post('id_setoran_detail');
                $id_setoran_detail_pnp  = $this->input->post('id_setoran_detail_pnp');
                $armada                 = $this->input->post('armada');
                $tanggal                = $this->input->post('tanggal');
                $kd_trayek              = $this->input->post('kd_trayek');
                $jumlah                 = $this->input->post('jumlah_pnp');
                $harga                  = $this->input->post('harga_pnp');
                $rit                    = $this->input->post('rit_pnp');
                $bagasi_pnp             = $this->input->post('bagasi_pnp');
                $jenis_penjualan_pnp    = $this->input->post('jenis_penjualan_pnp');
                $mata_uang              = $this->input->post('mata_uang');
                $km_trayek              = $this->input->post('km_trayek');

                $dataArr = explode("-",$this->input->post('kd_trayek'));
                
                $session = $this->session->userdata('login');
                $data = array(
                    'id_setoran_pnp' => $id_setoran_detail_pnp,
                    'id_setoran' => $id_setoran,
                    'id_setoran_detail' => $id_setoran_detail,
                    'tanggal'   => $tanggal,
                    'armada'    => $armada,
                    'kd_trayek' => $kd_trayek,
                    'asal'      => $dataArr[0],
                    'tujuan'    => $dataArr[1],
                    'jumlah'    => $jumlah,
                    'harga'     => $harga,
                    'rit'       => $rit,
                    'bagasi_pnp'=> $bagasi_pnp,
                    'jenis_penjualan_pnp' => $jenis_penjualan_pnp,
                    'mata_uang' => $mata_uang,
                    'km_trayek' => $km_trayek?$km_trayek:0,
                    'id_user'   => $session['id_user'],
                    );
                
                if($id_setoran_detail_pnp == 0){
                    $data['id_setoran_pnp'] = $this->model_setoran->insert_setoran_detail_pnp($data);
                }
                else{
                    $status = $this->model_setoran->update_setoran_detail_pnp($data);
                }

                // $jmlkoli        = $this->input->post('jmlkoli');
                // $koli           = $this->input->post('koli');
                // $harga_koli     = $this->input->post('harga_koli');

                // if(!empty($harga_koli)){
                //     $koliarr = explode(",",$koli);
                //     $harga_koliarr = explode(",",$harga_koli);

                //     $data_koli = array(
                //         'koli'          => $koliarr,
                //         'harga_koli'    => $harga_koliarr
                //         );

                //     $h = -1;
                //     foreach($koliarr as $data_koli) {

                //         $h++;
                //         $datah = array(
                //             'id_setoran_pnp' => $data['id_setoran_pnp'],
                //             'koli'          => $koliarr["$h"]?$koliarr["$h"]:0,
                //             'harga_koli'    => $harga_koliarr["$h"]?$harga_koliarr["$h"]:0,
                //             'cuser' => $session['id_user'],
                //             );
                //         $datah['id_setoran_pnp'] = $this->model_setoran->insert_setoran_detail_pnp_bagasi($datah);

                //     }
                // }

                // echo json_encode(array($jmlkoli,$koli,$harga_koli,$mata_uang,$data));
                // exit();

                echo json_encode(array('status' => 1));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_set_data_detail_pend()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran             = $this->input->post('id_setoran_header');
                $id_setoran_detail      = $this->input->post('id_setoran_detail');
                $id_setoran_detail_pend = $this->input->post('id_setoran_detail_pend');
                $armada                 = $this->input->post('armada');
                $tanggal                = $this->input->post('tanggal');
                $id_jenis               = $this->input->post('id_jenis');
                $jumlah                 = $this->input->post('jumlah_pend');
                $harga                  = $this->input->post('harga_pend');
                
                $session = $this->session->userdata('login');

                if($id_jenis==6){
                    $data = array(
                        'id_setoran_pend' => $id_setoran_detail_pend,
                        'id_setoran' => $id_setoran,
                        'id_setoran_detail' => $id_setoran_detail,
                        'tanggal'   => $tanggal,
                        'armada'    => $armada,
                        'id_jenis' => $id_jenis,
                        'jumlah'    => $jumlah,
                        'harga'     => $harga,
                        'total'     => $jumlah*$harga,
                        'id_user'   => $session['id_user'],
                        );
                }else{
                    $data = array(
                        'id_setoran_pend' => $id_setoran_detail_pend,
                        'id_setoran' => $id_setoran,
                        'id_setoran_detail' => $id_setoran_detail,
                        'tanggal'   => $tanggal,
                        'armada'    => $armada,
                        'id_jenis' => $id_jenis,
                        'jumlah'    => $jumlah,
                        'harga'     => $harga,
                        'id_user'   => $session['id_user'],
                        );
                }
                
                if($id_setoran_detail_pend == 0){
                    $status = $this->model_setoran->insert_setoran_detail_pend($data);
                }
                else{
                    $status = $this->model_setoran->update_setoran_detail_pend($data);
                }
                
                echo json_encode(array('status' => $status));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_set_data_detail_beban()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran              = $this->input->post('id_setoran_header');
                $id_setoran_detail       = $this->input->post('id_setoran_detail');
                $id_setoran_detail_beban = $this->input->post('id_setoran_detail_beban');
                $armada                  = $this->input->post('armada');
                $tanggal                 = $this->input->post('tanggal');
                $id_jenis                = $this->input->post('id_jenis');
                $jumlah                  = $this->input->post('jumlah_beban');
                $harga                   = $this->input->post('harga_beban');
                $status_jenis_beban      = $this->input->post('status_jenis_beban');
                
                $session = $this->session->userdata('login');

                if($id_jenis==7){
                    $data = array(
                        'id_setoran_beban' => $id_setoran_detail_beban,
                        'id_setoran' => $id_setoran,
                        'id_setoran_detail' => $id_setoran_detail,
                        'tanggal'   => $tanggal,
                        'armada'    => $armada,
                        'id_jenis'  => $id_jenis,
                        'jumlah'    => $jumlah,
                        'harga'     => $harga,
                        'total'     => $this->input->post('total_beban'),
                        'status_jenis_beban' => $status_jenis_beban,
                        'id_user'   => $session['id_user'],
                        );
                }else{
                    $data = array(
                        'id_setoran_beban' => $id_setoran_detail_beban,
                        'id_setoran' => $id_setoran,
                        'id_setoran_detail' => $id_setoran_detail,
                        'tanggal'   => $tanggal,
                        'armada'    => $armada,
                        'id_jenis'  => $id_jenis,
                        'jumlah'    => $jumlah,
                        'harga'     => $harga,
                        'status_jenis_beban' => $status_jenis_beban,
                        'id_user'   => $session['id_user'],
                        );
                }
                
                if($id_setoran_detail_beban == 0){
                    $status = $this->model_setoran->insert_setoran_detail_beban($data);
                }
                else{
                    $status = $this->model_setoran->update_setoran_detail_beban($data);
                }
                
                echo json_encode(array('status' => $status));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_unset_data()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "S10";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran = $this->input->post('id_setoran');
                
                $data = array('id_setoran' => $id_setoran);
                
                if(!empty($id_setoran))
                    $status = $this->model_setoran->delete_setoran_pnp($data);
                
                echo json_encode(array('status' => $status));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_unset_data_setoran_header()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran = $this->input->post('id_setoran');
                if(!empty($id_setoran))
                    $status = $this->model_setoran->delete_setoran_header($id_setoran);
                echo json_encode(array('status' => $status));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_unset_data_setoran_pnp()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran_pnp = $this->input->post('id_setoran_pnp');
                if(!empty($id_setoran_pnp))
                    $status = $this->model_setoran->delete_setoran_pnp($id_setoran_pnp);
                echo json_encode(array('status' => $status));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_unset_data_setoran_detail()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                if(!empty($id_setoran_detail))
                    $status = $this->model_setoran->delete_setoran_detail($id_setoran_detail);
                echo json_encode(array('status' => $status));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_unset_data_setoran_pend(){
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran_pend = $this->input->post('id_setoran_pend');
                
                if(!empty($id_setoran_pend))
                    $status = $this->model_setoran->delete_setoran_pend($id_setoran_pend);
                echo json_encode(array('status' => $status));
            } else {

                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_unset_data_setoran_beban(){
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if(!empty($access['id_menu_details'])){
                $id_setoran_beban = $this->input->post('id_setoran_beban');
                if(!empty($id_setoran_beban))
                    $status = $this->model_setoran->delete_setoran_beban($id_setoran_beban);
                echo json_encode(array('status' => $status));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_unset_data_setoran_pertelaan()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id_setoran_pnp_prt = $this->input->post('id_setoran_pnp_prt');
                if(!empty($id_setoran_pnp_prt)){
                    $status = $this->db->delete('ref_setoran_detail_pnp_pertelaan', array('id_setoran_pnp_prt' => $id_setoran_pnp_prt)); 
                }
                echo json_encode(array('status' => $status));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_get_data_by_id()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran = $this->input->post('id_setoran');
                
                if(empty($id_setoran))
                    $data = array();
                else
                    $data = $this->model_setoran->get_setoran_by_id($id_setoran);
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    function ax_get_jadwal(){
        $id_bu = $this->input->post('id_bu');
        $armada = $this->input->post('armada');
        $data = $this->model_setoran->listJadwal($id_bu,$armada);
        $html = "<option value='0'>--Data Bus--</option>";
        foreach ($data->result() as $row) {
            $html .= "<option value='".$row->id_jadwal."'>".$row->tgl_lmb." | ".$row->nm_trayek." | ".$row->kd_trayek."</option>"; 
        }
        $callback = array('data_jadwal'=>$html);
        echo json_encode($callback);
    }
    
    function ax_get_jadwal_rinci(){
        $id_jadwal = $this->input->post("id_jadwal");
        $data = $this->model_setoran->getjadwalrinci($id_jadwal)->result_array();
        echo json_encode($data);
    }
    
    function ax_get_trayek(){
        $id_bu = $this->input->post('id_bu');
        $id_trayek = $this->input->post('id_trayek');
        $layanan = $this->input->post('layanan');
        $data = $this->model_setoran->listTrayek($id_bu,$id_trayek,$layanan);
        $html = "<option value='0'>--Trayek--</option>";
        foreach ($data->result() as $row) {
            $html .= "<option value='".$row->kd_trayek."~".$row->mata_uang."'>".$row->nm_point_awal." - ".$row->nm_point_akhir." (".$row->mata_uang." - ".$row->harga.")</option>"; 
        }
        $callback = array('data_trayek'=>$html);
        echo json_encode($callback);
    }
    function ax_get_point_trayek(){
        $id_bu = $this->input->post('id_bu');
        $id_trayek = $this->input->post('id_trayek');
        $data = $this->model_setoran->listPointTrayek($id_bu,$id_trayek);
        $html = "<option value='0'>--Trayek--</option>";
        foreach ($data->result() as $row) {
            $html .= "<option value='".$row->point."'>".$row->point."</option>"; 
        }
        $callback = array('data_trayek'=>$html);
        echo json_encode($callback);
    }
    
    function ax_get_trayek_harga(){
        $id_bu     = $this->input->post('id_bu');
        $id_trayek = $this->input->post('id_trayek');
        $kd_trayek = $this->input->post('kd_trayek');
        $layanan   = $this->input->post('layanan');
        $mata_uang = $this->input->post('mata_uang');
        $data = $this->model_setoran->hargaTrayek($id_bu,$id_trayek,$kd_trayek,$layanan,$mata_uang);
        echo json_encode($data);
    }
    function ax_get_harga_beban(){
        $id_jenis_beban = $this->input->post('id_jenis_beban');
        $id_bu = $this->input->post('id_bu');
        $data = $this->model_setoran->hargaBeban($id_jenis_beban,$id_bu);
        echo json_encode($data);
    }
    
    function ax_get_jenis_pend(){
        $id_bu = $this->input->post('id_bu');
        $data = $this->model_setoran->listJenisPend($id_bu);
        $html = "<option value='0'>--Jenis--</option>";
        foreach ($data->result() as $row) {
            $html .= "<option value='".$row->id_komponen."'>".$row->keterangan."</option>"; 
        }
        $callback = array('data_jenis'=>$html);
        echo json_encode($callback);
    }
    
    function ax_get_jenis_beban(){
        $id_bu = $this->input->post('id_bu');
        $data = $this->model_setoran->listJenisBeban($id_bu);
        $html = "<option value='0'>--Jenis--</option>";
        foreach ($data->result() as $row) {
            $html .= "<option value='".$row->id_komponen."'>".$row->nm_komponen."</option>"; 
        }
        $callback = array('data_jenis'=>$html);
        echo json_encode($callback);
    }
    
    function ax_get_bu(){
        $id_bu = $this->input->post("id_bu");
        $data = $this->model_setoran->getidbu($id_bu)->result_array();
        echo json_encode($data);
    }
    
    public function ax_change_active()
    {
        $id = $this->input->post('id_setoran');
        $active = $this->input->post('active');
        
        $penumpang = $this->model_setoran->sum_penumpang($id)->penumpang;
        $beban = $this->model_setoran->sum_beban($id)->beban;
        
        // if($penumpang != null && $beban != null){
        if($beban != null){
            $change_active;
            if ($active==1) {
                $change_active = 2;
            }else if ($active==2) {
                $change_active = 1;
            }
            $data = array(
                'active' => $change_active
                );
            $this->model_setoran->change_active(array('id_setoran' => $id), $data);
            
            echo json_encode(array("status" => TRUE));
        }else{
            echo json_encode(array("status" => FALSE));
        }
        
    }

    public function ax_change_status_udj()
    {
        $id_setoran_detail  = $this->input->post('id_setoran_detail');
        $status_udj         = $this->input->post('status_udj');

        $change_status;
        if($status_udj=='true'){
            $change_status=1;
        }else{
            $change_status=0;
        }

        $data = array(
            'status_udj' => $change_status
            );

        $this->db->update('ref_setoran_detail', $data, array('id_setoran_detail' => $id_setoran_detail));
        echo json_encode(array("status" => TRUE, "change"=>$change_status));
    }

    public function ax_change_status_udj_bagasi()
    {
        $id_setoran_detail  = $this->input->post('id_setoran_detail');
        $status_udj_bagasi         = $this->input->post('status_udj_bagasi');

        $change_status;
        if($status_udj_bagasi=='true'){
            $change_status=1;
        }else{
            $change_status=0;
        }

        $data = array(
            'status_udj_bagasi' => $change_status
            );

        $this->db->update('ref_setoran_detail', $data, array('id_setoran_detail' => $id_setoran_detail));
        echo json_encode(array("status" => TRUE, "change"=>$change_status));
    }

    public function ax_get_armada()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_cabang = $this->input->post('id_cabang');
                $tanggal = $this->input->post('tanggal');
                $id_pool = $this->input->post('id_pool');
                $data = $this->model_setoran->combobox_armada_($id_cabang);
                $html = "<option value='' disabled selected>--Armada--</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->kd_armada."'>".$row->kd_armada."</option>"; 
                }
                
                
                $data1 = $this->model_setoran->combobox_armada_jadwal($id_cabang, $tanggal, $id_pool);
                $html1 = "<option value='' disabled selected>--Armada--</option>";
                foreach ($data1->result() as $row1) {
                    $html1 .= "<option value='".$row1->kd_armada."'>".$row1->kd_armada."</option>"; 
                }
                $callback = array('data_armada'=>$html, 'data_jadwal'=>$html1);
                echo json_encode($callback);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_get_pool()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {


                $id_cabang = $this->input->post('id_cabang');
                $data = $this->model_setoran->list_pool($id_cabang);
                $html = "<option value='0'>-- Pool --</option>";
                foreach ($data->result() as $row) {
                    $html .= "<option value='".$row->id_pool."'>".$row->nm_pool."</option>"; 
                }
                $callback = array('data_pool'=>$html);
                echo json_encode($callback);
                
                
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_button_lmb()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_bu = $this->input->post('id_bu');
                if(empty($id_bu)){
                    $data = array();
                }
                else{
                    $data = $this->model_setoran->get_button_lmb($id_bu);
                }

                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_get_data_api_ticket_old()
    {
        $session = $this->session->userdata('login');
        $data = array(
            'tanggal'=> '2020-02-01',
            'armada'=> '4937'
            // 'tanggal'=> $this->input->post('tanggal'),
            // 'armada'=> $this->input->post('armada')
            );
        $url = 'newtiket.damri.co.id/apps/operasi/dataPenumpang';
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $responseJson = curl_exec( $ch );
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {

            foreach (json_decode($responseJson)->data as $row){

                $data_array = array(
                    'id_setoran_detail' => $this->input->post('id_setoran_detail'),
                    'id_setoran'        => $this->input->post('id_setoran'),
                    'rit'               => 1,
                    'tanggal'           => $this->input->post('tanggal'),
                    // 'tanggal'           => '2020-02-01',
                    'armada'            => $row->armada,
                    'kd_trayek'         => $row->trayek,
                    'jumlah'            => $row->jumlah,
                    'harga'             => $row->harga,
                    'jenis_penjualan_pnp'=> 1,
                    'id_user'           => $session['id_user']
                    );
                $data_final [] = $data_array;
            }
            $status = $this->db->insert_batch('ref_setoran_detail_pnp', $data_final);
        }
        echo json_encode(array('status' => $responseJson));
    }
    
    public function ax_get_data_api_ticket()
    {
        $session = $this->session->userdata('login');

        $this->db->select("b.id_cabang,a.id_bu, a.nm_bu");
        $this->db->from("ref_bu a");
        $this->db->join("ref_bu_ticketing b", "b.nm_cabang = a.nm_bu","left");
        $this->db->where('a.id_bu', $this->input->post('id_bu'));
        $this->db->limit(1);
        $query = $this->db->get();
        $id_cabang = $query->num_rows()>=1 ? $query->row("id_cabang") : "0";

        $data = array(
            // 'tanggal'=> '2020-02-01',
            // 'armada'=> '4937'
            'tanggal'=> $this->input->post('tanggal'),
            'armada'=> $this->input->post('armada'),
            'id_cabang'=> $id_cabang
            );
        $url = 'newtiket.damri.co.id/apps/operasi/dataPenumpang';
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $responseJson = curl_exec( $ch );
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $id_setoran_detail = $this->input->post('id_setoran_detail');
            $id_setoran        = $this->input->post('id_setoran');
            $tanggal           = $this->input->post('tanggal');
            foreach (json_decode($responseJson)->data as $row){
                $armada     = $row->armada;
                $trayek     = $row->trayek;
                $jumlah     = $row->jumlah;
                $harga      = $row->harga;
                $pend       = $row->pend;
                
                $jum_apps     = $row->jum_apps;        
                $nilai_apps   = $row->nilai_apps;
                $jum_ota      = $row->jum_ota;
                $nilai_ota    = $row->nilai_ota;
                $jum_agen     = $row->jum_agen;
                $nilai_agen   = $row->nilai_agen;
                $jum_loket    = $row->jum_loket;
                $nilai_loket  = $row->nilai_loket;
                $kd_uang       = $row->kd_uang;
                
                $this->insert_eticket($id_setoran_detail,$id_setoran,$tanggal,$armada,$trayek,$jumlah,$harga,$pend,$jum_apps,$nilai_apps,$jum_ota,$nilai_ota,$jum_agen,$nilai_agen,$jum_loket,$nilai_loket,$kd_uang);
            }
        }
        echo json_encode(array('status' => $responseJson));
    }
    
    public function insert_eticket($id_setoran_detail,$id_setoran,$tanggal,$armada,$trayek,$jumlah,$harga,$pend,$jum_apps,$nilai_apps,$jum_ota,$nilai_ota,$jum_agen,$nilai_agen,$jum_loket,$nilai_loket,$kd_uang)
    {
        $exp_trayek = explode("-",$trayek);
        $asal = $exp_trayek[0];
        $tujuan = $exp_trayek[1];

        $session = $this->session->userdata('login');

        if($jum_apps=='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket=='0'){
            $data_array = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jumlah,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array);
        }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket=='0'){
            $data_array = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array);
        }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket=='0'){
            $data_array = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array);
        }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket=='0'){
            $data_array = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_agen,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 3,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array);
        }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket!='0'){
            $data_array = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_loket,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 4,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array);
        }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket=='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket=='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_agen,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 3,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_loket,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 4,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket=='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_agen,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 3,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_loket,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 4,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_agen,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 3,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_loket,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 4,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
        }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket=='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_3 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_agen,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 3,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_3);
        }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_3 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_loket,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 4,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_3);
        }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_agen,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 3,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $data_array_3 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_loket,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 4,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_3);
        }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_agen,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 3,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $data_array_3 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_loket,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 4,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_3);

        }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket!='0'){
            $data_array_1 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_apps,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 1,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_2 = array(
                'id_setoran_detail' => $id_setoran_detail,
                'id_setoran'        => $id_setoran,
                'rit'               => 1,
                'tanggal'           => $tanggal,
                'armada'            => $armada,
                'kd_trayek'         => $trayek,
                'jumlah'            => $jum_ota,
                'harga'             => $harga,
                'jenis_penjualan_pnp'=> 2,
                'asal'              => $asal,
                'tujuan'            => $tujuan,
                'mata_uang'         => $kd_uang,
                'id_user'           => $session['id_user']
                );
            $data_array_3 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_agen,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 3,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $data_array_4 = array(
               'id_setoran_detail' => $id_setoran_detail,
               'id_setoran'        => $id_setoran,
               'rit'               => 1,
               'tanggal'           => $tanggal,
               'armada'            => $armada,
               'kd_trayek'         => $trayek,
               'jumlah'            => $jum_loket,
               'harga'             => $harga,
               'jenis_penjualan_pnp'=> 4,
               'asal'              => $asal,
               'tujuan'            => $tujuan,
               'mata_uang'         => $kd_uang,
               'id_user'           => $session['id_user']
               );
            $this->db->insert('ref_setoran_detail_pnp',$data_array_1);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_2);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_3);
            $this->db->insert('ref_setoran_detail_pnp',$data_array_4);
        }else{

        }
        return "sukses";
    }
    
    public function datatable_ticket()
    {
        $session = $this->session->userdata('login');
        $data = array(
            // 'tanggal'=> '2020-02-01',
            // 'armada'=> '4937'
            'tanggal'=> $this->input->post('tanggal'),
            'armada'=> $this->input->post('armada')
            );
        $url = 'newtiket.damri.co.id/apps/operasi/dataPenumpang';
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $responseJson = curl_exec( $ch );
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $cRet ='';
            $tanggal_e_ticket = $this->input->post('tanggal');
            
            foreach (json_decode($responseJson)->data as $row){
                $armada     = $row->armada;
                $trayek     = $row->trayek;
                $jumlah     = $row->jumlah;
                $harga      = $row->harga;
                $pend       = $row->pend;
                
                $jum_apps     = $row->jum_apps;        
                $nilai_apps   = $row->nilai_apps;
                $jum_ota      = $row->jum_ota;
                $nilai_ota    = $row->nilai_ota;
                $jum_agen     = $row->jum_agen;
                $nilai_agen   = $row->nilai_agen;
                $jum_loket    = $row->jum_loket;
                $nilai_loket  = $row->nilai_loket;
                
                if($jum_apps=='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>$harga</td>
                        <td>$jumlah</td>
                        <td align='center'>$pend</td>
                        <td></td>
                    </tr>
                    ";
                }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                    
                    //2 select
                }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen=='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                    
                    //3 select
                }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket=='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                }else if($jum_apps!='0' && $jum_ota!='0' && $jum_agen=='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                }else if($jum_apps=='0' && $jum_ota!='0' && $jum_agen!='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                    
                    //4 select
                }else if($jum_apps!='0' && $jum_ota=='0' && $jum_agen!='0' && $jum_loket!='0'){
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_apps,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_apps,0,',','.')."</td>
                        <td>DAMRI Apps</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_ota,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_ota,0,',','.')."</td>
                        <td>OTA</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_agen,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_agen,0,',','.')."</td>
                        <td>Agen</td>
                    </tr>
                    ";
                    $cRet  .="
                    <tr>
                        <td align='center'>$tanggal_e_ticket</td>
                        <td>$armada</td>
                        <td>".ucwords($trayek)."</td>
                        <td align='center'>".number_format($jum_loket,0,',','.')."</td>
                        <td align='center'>".number_format($harga,0,',','.')."</td>
                        <td align='center'>".number_format($nilai_loket,0,',','.')."</td>
                        <td>Loket</td>
                    </tr>
                    ";
                }else{
                    $cRet .="";
                }
            }
            // echo $cRet;
            echo json_encode(array("detail" => $cRet));
            
        }
    }
    
    
    public function ax_get_data_lmb()
    {
        $id_jadwal         = $this->input->post('id_jadwal');
        $id_setoran        = $this->input->post('id_setoran');
        $id_setoran_detail = $this->input->post('id_setoran_detail');
        // echo json_encode(array($id_jadwal,$id_setoran,$id_setoran_detail));
        // exit();
        
        $status = $this->model_setoran->insertSetoranLmb($id_jadwal,$id_setoran,$id_setoran_detail);
        echo json_encode(array('status' => $status));
    }
    
    
    public function ax_data_lmb()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $start = $this->input->post('start');
                $draw = $this->input->post('draw');
                $length = $this->input->post('length');
                $id_jadwal = $this->input->post('id_jadwal');
                $cari = $this->input->post('search', true);
                $data = $this->model_setoran->get_data_lmb($length, $start, $cari['value'],$id_jadwal)->result_array();
                $count = $this->model_setoran->get_count_lmb($cari['value'],$id_jadwal);
                
                echo json_encode(array('recordsTotal' => $count['recordsTotal'], 'recordsFiltered' => $count['recordsFiltered'], 'draw' => $draw, 'search' => $cari['value'], 'data' => $data));
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    public function ax_get_data_by_id_setoran_pnp()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {
                $id = $this->input->post('id');
                if(empty($id))
                    $data = array();
                else
                    $data = $this->model_setoran->get_jadwal_by_id_setoran_pnp($id);
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_data_by_id_setoran_pend()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id = $this->input->post('id');
                
                if(empty($id)){
                    $data = array();
                }else{
                    $data = $this->model_setoran->get_jadwal_by_id_setoran_pend($id);
                }
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_data_by_id_setoran_beban()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id = $this->input->post('id');
                
                if(empty($id)){
                    $data = array();
                }else{
                    $data = $this->model_setoran->get_jadwal_by_id_setoran_beban($id);
                }
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_rp_km()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $kd_trayek = $this->input->post('kd_trayek');
                
                if(empty($kd_trayek)){
                    $data = array();
                }else{
                    $this->db->from("ref_trayek a");
                    $this->db->where('a.kd_trayek', $kd_trayek);
                    $this->db->limit(1);
                    $data = $this->db->get()->row_array();
                }
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    
    
    public function ax_set_data_update_png(){
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_pnp     = $this->input->post('id_setoran_pnp_edit');
                $rit                = $this->input->post('rit_edit_pnp');
                $jumlah             = $this->input->post('jumlah_edit_pnp');
                $bagasi_pnp         = $this->input->post('bagasi_edit_pnp');
                $jenis_penjualan_pnp = $this->input->post('jenis_penjualan_edit_pnp');
                $session            = $this->session->userdata('login');

                // $dataArr = explode("~",$this->input->post('kd_trayek_edit_pnp')); 
                // $dataArrTrayek = explode("-",$dataArr[0]);
                
                $data = array(
                    // 'kd_trayek'             => $dataArr[0],
                    // 'asal'                  => $dataArrTrayek[0],
                    // 'tujuan'                => $dataArrTrayek[1],
                    'rit'                   => $rit,
                    'jumlah'                => $this->input->post('jumlah_edit_pnp'),
                    'harga'                 => $this->input->post('harga_edit_pnp'),
                    // 'jumlah'                => $jumlah,
                    'bagasi_pnp'            => $bagasi_pnp,
                    'jenis_penjualan_pnp'   => $jenis_penjualan_pnp,
                    // 'mata_uang'             => $dataArr[1]
                    );
                
                $this->model_setoran->update_setoran_detail_pnp(array('id_setoran_pnp' => $id_setoran_pnp), $data);
                echo json_encode(array("status" => TRUE));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data_update_pend(){
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_pend    = $this->input->post('id_setoran_pend_edit');
                $jumlah             = $this->input->post('jumlah_edit_pend');
                $harga              = $this->input->post('harga_edit_pend');
                $session            = $this->session->userdata('login');                                        
                
                $data = array(
                    'jumlah'    => $jumlah,
                    'harga'     => $harga
                    );
                $this->db->update('ref_setoran_detail_pend', $data, array('id_setoran_pend' => $id_setoran_pend));
                echo json_encode(array("status" => TRUE));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    public function ax_set_data_update_beban(){
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_beban    = $this->input->post('id_setoran_beban_edit');
                $id_jenis           = $this->input->post('id_jenis_beban_edit');
                $session            = $this->session->userdata('login');

                $arr = array("22", "23", "24");
                if(in_array($id_jenis, $arr)){
                    $data = array(
                        'jumlah'             => $this->input->post('jumlah_beban_edit'),
                        'harga'              => $this->input->post('harga_beban_edit'),
                        'status_jenis_beban' => $this->input->post('status_jenis_beban_edit')
                        );
                }else if($id_jenis==7){
                    $data = array(
                        'jumlah'             => $this->input->post('jumlah_bbm_beban_edit'),
                        'harga'              => $this->input->post('harga_bbm_beban_edit'),
                        'total'              => $this->input->post('total_bbm_beban_edit')
                        );
                }else{
                    $data = array(
                        'jumlah'             => $this->input->post('jumlah_beban_edit'),
                        'harga'              => $this->input->post('harga_beban_edit')
                        );
                }
                
                $this->db->update('ref_setoran_detail_beban', $data, array('id_setoran_beban' => $id_setoran_beban));
                echo json_encode(array("status" => TRUE));
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data_pertelaan()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_pnp = $this->input->post('id_setoran_pnp');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $id_setoran = $this->input->post('id_setoran');
                $jumlah     = $this->input->post('jumlah'); //detailPNP
                $total      = $this->input->post('total'); //detailTotalPNP

                $kd_trayek  = $this->input->post('kd_trayek');
                $harga      = $this->input->post('harga');
                $jum        = $this->input->post('jum');

                $jumlah_pnp_now = $this->model_setoran->cek_jumlah_pnp($id_setoran_pnp);
                $jumlah_total_now = $this->model_setoran->cek_jumlah_total($id_setoran_pnp);

                if(($jumlah_pnp_now+$jum)<=$jumlah && ($jumlah_total_now+($jum*$harga))<=$total){
                    $session = $this->session->userdata('login');
                    $data = array(
                        'id_setoran_pnp'    => $id_setoran_pnp,
                        'id_setoran_detail' => $id_setoran_detail,
                        'id_setoran'        => $id_setoran,
                        'kd_trayek'         => $kd_trayek,
                        'jumlah'            => $jum,
                        'harga'             => $harga,
                        'id_user'           => $session['id_user'],
                        'id_bu'             => $this->input->post('id_bu')
                        );
                    $this->db->insert('ref_setoran_detail_pnp_pertelaan',$data);
                    echo json_encode(array('status' => TRUE));
                }else{
                    echo json_encode(array('status' => 'melebihisisa','jumlah' => $jumlah));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_set_data_pertelaan_update()
    {
      if ($this->session->userdata('login')) {
        $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id_setoran_pnp_prt = $this->input->post('id_setoran_pnp_prt');
                $id_setoran_pnp = $this->input->post('id_setoran_pnp');
                $id_setoran_detail = $this->input->post('id_setoran_detail');
                $id_setoran = $this->input->post('id_setoran');
                $jumlah     = $this->input->post('jumlah'); //detailPNP
                $total      = $this->input->post('total'); //detailTotalPNP

                $kd_trayek  = $this->input->post('kd_trayek');
                $harga      = $this->input->post('harga');
                $jum        = $this->input->post('jum');

                $jumlah_pnp_now = $this->model_setoran->cek_jumlah_pnp($id_setoran_pnp);
                $jumlah_total_now = $this->model_setoran->cek_jumlah_total($id_setoran_pnp);

                $data_pertelaan = $this->model_setoran->get_data_by_id_setoran_pnp_pertelaan($id_setoran_pnp_prt);
                $a=$data_pertelaan['jumlah'];
                $b=$data_pertelaan['total'];
                // echo $id_setoran_pnp_prt;
                // exit();

                if((($jumlah_pnp_now-$a)+$jum)<=$jumlah && (($jumlah_total_now-$b)+($jum*$harga))<=$total){
                    $session = $this->session->userdata('login');
                    $data = array(
                        'kd_trayek'         => $kd_trayek,
                        'jumlah'            => $jum,
                        'harga'             => $harga,
                        'id_user'           => $session['id_user'],
                        'id_bu'             => $this->input->post('id_bu')
                        );
                    $this->db->update('ref_setoran_detail_pnp_pertelaan',$data, array('id_setoran_pnp_prt' => $id_setoran_pnp_prt));
                    echo json_encode(array('status' => TRUE));
                }else{
                    echo json_encode(array('status' => 'melebihisisa','jumlah' => $jumlah));
                }

            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }

    public function ax_get_data_by_id_setoran_pnp_pertelaan()
    {
        if ($this->session->userdata('login')) {
            $session = $this->session->userdata('login');
            $menu_kd_menu_details = "U01";  //custom by database
            $access = $this->model_menu->selectaccess($session['id_level'], $menu_kd_menu_details);
            if (!empty($access['id_menu_details'])) {

                $id = $this->input->post('id');
                
                if(empty($id))
                    $data = array();
                else
                    $data = $this->model_setoran->get_data_by_id_setoran_pnp_pertelaan($id);
                
                echo json_encode($data);
                
            } else {
                echo "<script>alert('Anda tidak mendapatkan access menu ini');window.location.href='javascript:history.back(-1);'</script>";
            }
        } else {
            if ($this->uri->segment(1) != null) {
                $url = $this->uri->segment(1);
                $url = $url.' '.$this->uri->segment(2);
                $url = $url.' '.$this->uri->segment(3);
                redirect('welcome/relogin/?url='.$url.'', 'refresh');
            } else {
                redirect('welcome/relogin', 'refresh');
            }
        }
    }
    public function printLaporan()
    {
        $session = $this->session->userdata('login');
        $id_cabang  = $this->input->post('id_cabang_print');
        $id_segment = $this->input->post('segment_print');
        $id_armada  = $this->input->post('armada_print');
        $tanggal    = $this->input->post('tanggal_print');
        
        $data_cabang    = $this->model_setoran->get_cabang($id_cabang);
        $data_segment   = $this->model_setoran->get_segment($id_segment);
        $data_armada    = $this->model_setoran->get_armada($id_armada);
        
        $general_manager   = $this->model_setoran->general_manager($id_cabang);
        
        $data_setoran = $this->model_setoran->get_data_setoran($id_cabang,$id_armada,$tanggal);
        
        if($data_setoran){
            $id_setoran          = $data_setoran->id_setoran;
            $id_setoran_detail   = $data_setoran->id_setoran_detail;
            
            $nm_point_awal = $this->model_setoran->get_trayek_by_kd_trayek($data_setoran->kd_trayek)->nm_point_awal;
            $nm_point_akhir = $this->model_setoran->get_trayek_by_kd_trayek($data_setoran->kd_trayek)->nm_point_akhir;
            $km_trayek = $this->model_setoran->get_trayek_by_kd_trayek($data_setoran->kd_trayek)->km_trayek;
            $driver1 = $data_setoran->driver1;
            $driver2 =$data_setoran->driver2;
            
            if($id_setoran_detail){
                $data_setoran_detail_pnp = $this->model_setoran->get_setoran_detail_pnp($id_setoran_detail);
                $data_setoran_detail_beban_1 = $this->model_setoran->get_setoran_detail_beban_1($id_setoran_detail);
                $data_setoran_detail_beban_2 = $this->model_setoran->get_setoran_detail_beban_2($id_setoran_detail);
                $data_setoran_detail_beban_3 = $this->model_setoran->get_setoran_detail_beban_3($id_setoran_detail);
                
                $data_agen_pph = $this->model_setoran->sum_total_agen_setoran_detail_pnp($id_setoran_detail);
            }else{
                $data_agen_pph = '0';
                
                $data_setoran_detail_pnp = array();
                $data_setoran_detail_beban_1 = array();
                $data_setoran_detail_beban_2 = array();
                $data_setoran_detail_beban_3 = array();
            }
        }else{

            $nm_point_awal='';
            $nm_point_akhir='';
            $km_trayek='';
            $driver1='-';
            $driver2='-';
            $data_agen_pph = '0';
            
            $data_setoran_detail_pnp = array();
            $data_setoran_detail_beban_1 = array();
            $data_setoran_detail_beban_2 = array();
            $data_setoran_detail_beban_3 = array();
        }
        
        
        $cRet ='';
        $cRet .= "
        <link rel=\"icon\" type=\"image/x-icon\" href=\"".base_url('assets/img/survei.png')."\">
        <title>REPORT AK/13</title>
        <table style=\"border-collapse:collapse;\" width=\"100%\" align=\"center\" border=\"0\">
            <tr>
                <td width=\"22%\" align=\"center\" style=\"font-size:12px;\">
                    <b>SEGMEN : ".strtoupper($data_segment->nm_segment)."</b><br />
                </td>
                <td width=\"50%\" align=\"center\" style=\"font-size:12px;\">
                    <b>PERUSAHAAN UMUM DAMRI (PERUM DAMRI)</b><br />
                    <b>KANTOR CABANG : ".strtoupper($data_cabang->nm_bu)."</b><br />
                </td>
                <td width=\"20%\" align=\"center\" style=\"font-size:18px;\">
                    <img src=\"".base_url('assets/img/logos.png')."\" alt=\"Perum DAMRI\" height=\"20\" width=\"100\">
                </td>
            </tr>
        </table><br/>
        <table style=\"border-collapse:collapse;\" width=\"100%\" align=\"center\" border=\"0\">
            <tr>
                <td width=\"1%\" align=\"center\" style=\"font-size:12px;\"></td>
                <td width=\"50%\" align=\"center\" style=\"font-size:14px;\">
                    <b>DAFTAR PENJUALAN KARCIS (DPK) AK/13</b><br />
                    NOMOR : ...........................<br />
                </td>
            </tr>
        </table><br/>
        <table style=\"border-collapse:collapse;\" border=\"1\" width=\"100%\">
            <thead>
                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">Bus Code &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;: $data_armada->kd_armada</td>
                    <td rowspan=\"2\" colspan=\"4\" style=\"font-size:10px;text-align:center;\">AP/3 Dari UPT No.</td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:center;\">Penjualan Karcis Di Perjalanan Oleh Awak Bus</td>
                </tr>
                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">Jurusan &emsp;&emsp;&emsp;&emsp;&emsp;&nbsp; : ".$nm_point_awal." - ".$nm_point_akhir."</td>
                    <td style=\"font-size:10px;text-align:center;\">No. Awal</td>
                    <td style=\"font-size:10px;text-align:center;\">No. Akhir</td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\">Jml. Lembar</td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\">Jml. Rp</td>
                </tr>

                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">KM Tempuh &emsp;&emsp;&emsp;&nbsp; : $km_trayek</td>
                    <td colspan=\"4\"style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                </tr>
                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">Tanggal &emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;: ".tgl_indo($tanggal)."</td>
                    <td colspan=\"4\"style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                </tr>
                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">Driver 1 &emsp;&emsp;&emsp;&emsp;&emsp; : ".ucwords($driver1)."</td>
                    <td colspan=\"4\"style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                </tr>
                <tr>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right:none;\">Driver 2 &emsp;&emsp;&emsp;&emsp;&emsp; : $driver2</td>
                    <td colspan=\"4\"style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;\"></td>
                </tr>

                <tr>
                    <td rowspan=\"2\" style=\"font-size:10px;text-align:center;\">NO</td>
                    <td colspan=\"4\" style=\"font-size:10px;text-align:center;\">Jurusan</td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:center;\">Karcis di Jual Oleh</td>
                    <td colspan=\"3\" style=\"font-size:10px;text-align:center;\">UPP</td>
                    <td rowspan=\"2\" style=\"font-size:10px;text-align:center;\">Jumlah Pnp Km 4x11</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">Dari</td>
                    <td style=\"font-size:10px;text-align:center;\">Ke</td>
                    <td style=\"font-size:10px;text-align:center;\">Km</td>
                    <td style=\"font-size:10px;text-align:center;\">Rit</td>
                    <td style=\"font-size:10px;text-align:center;\">DAMRI Apps</td>
                    <td style=\"font-size:10px;text-align:center;\">OTA</td>
                    <td style=\"font-size:10px;text-align:center;\">Agen</td>
                    <td style=\"font-size:10px;text-align:center;\">Loket</td>
                    <td style=\"font-size:10px;text-align:center;width: 2px;\">Awak Bus</td>
                    <td style=\"font-size:10px;text-align:center;\">Jumlah Karcis</td>
                    <td style=\"font-size:10px;text-align:center;\">Pnp</td>
                    <td style=\"font-size:10px;text-align:center;\">Bgs</td>
                    <td style=\"font-size:10px;text-align:center;\">Jumlah</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>1</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>2</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>3</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>4</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>5</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>6</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>7</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>8</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>9</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>10</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>11</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>12</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>13</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>14</i></td>
                    <td style=\"font-size:10px;text-align:center;background-color:#d5d5e3;\"><i>15</i></td>
                </tr>";

                $nomor = 0;
                $total_km=0;
                $total_rit=0;
                $total_karcis=0;
                $total_upp_pnp=0;
                $total_upp_bagasi=0;
                $total_pendapatan=0;
                foreach ($data_setoran_detail_pnp as $row) {
                    $nomor = $nomor+1;

                    $cRet.="
                    <tr>
                        <td style=\"font-size:10px;text-align:center;\">$nomor</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->nm_point_awal</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->nm_point_akhir</td>
                        <td style=\"font-size:10px;text-align:center;\">".intval($row->km_trayek)."</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->rit</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->damri_apps</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->ota</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->agen</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->loket</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->awak_bus</td>
                        <td style=\"font-size:10px;text-align:center;\">$row->jumlah</td>
                        <td style=\"font-size:10px;text-align:right;\">".number_format(($row->jumlah*$row->harga),0,",",".")."</td>
                        <td style=\"font-size:10px;text-align:right;\">".number_format($row->bagasi_pnp,0,",",".")."</td>
                        <td style=\"font-size:10px;text-align:right;\">".number_format($row->total,0,",",".")."</td>
                        <td style=\"font-size:10px;text-align:center;\">".number_format($row->km_trayek*$row->jumlah,0,",",".")."</td>
                    </tr>";
                    $total_km           +=$row->km_trayek;
                    $total_rit          =$row->rit;
                    $total_karcis       +=$row->jumlah;
                    $total_upp_pnp      +=($row->jumlah*$row->harga);
                    $total_upp_bagasi   +=$row->bagasi_pnp;
                    $total_pendapatan   +=($row->total);
                }


                $cRet.="
                <tr>
                    <td colspan=\"3\" style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;height:13px\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                </tr>
                <tr>
                    <td colspan=\"3\" style=\"font-size:10px;text-align:center;\"><b>Total Pendapatan Semua Rit</b></td>
                    <td style=\"font-size:10px;text-align:center;\"><b>$total_km</b></td>
                    <td style=\"font-size:10px;text-align:center;\"><b>$total_rit</b></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"><b>".number_format($total_karcis,0,",",".")."</b></td></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td style=\"font-size:10px;text-align:right;\"><b>Rp. ".number_format($total_pendapatan,0,",",".")."</b></td></td>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">1</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Total Pendapatan Penumpang</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($total_upp_pnp,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">2</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Total Pendapatan Bagasi</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($total_upp_bagasi,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah UPP Penumpang dan Bagasi</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($total_pendapatan,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">1</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Titipan Asuransi Jasa Raharja (@Rp. 60,00 * Jumlah Karcis)</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format(60*$total_karcis,0,",",".")."</td>
                </tr>";
                $nomor_beban_1      = 1;
                $total_beban_1      = 0;
                $jml_biaya_titipan  = 0;
                $jml_pnp_dan_upp_bus    = 0;
                $jml_upp_kurang_komisi  = 0;
                foreach ($data_setoran_detail_beban_1 as $row) {
                    $nomor_beban_1 = $nomor_beban_1+1;
                    $cRet.="
                    <tr>
                        <td style=\"font-size:10px;text-align:center;\">$nomor_beban_1</td>
                        <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">$row->nm_komponen</td>
                        <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($row->total,0,",",".")."</td>
                    </tr>";
                    $total_beban_1+=$row->total;
                }

                $jml_biaya_titipan=(60*$total_karcis)+$total_beban_1;
                $jml_pnp_dan_upp_bus=$total_pendapatan-$jml_biaya_titipan;
                $jml_upp_kurang_komisi=$jml_pnp_dan_upp_bus-$data_agen_pph;

                $cRet.="
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah Biaya Titipan</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($jml_biaya_titipan,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;\"><b>Jumlah Penumpang dan UPP Km Bus</b></td>
                    <td colspan=\"1\" style=\"font-size:10px;text-align:center;border-left-style:none;\">$total_karcis</td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:center;border-right-style:none;\"></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($jml_pnp_dan_upp_bus,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"14\" style=\"font-size:10px;text-align:left;\"><b>Biaya Operasi</b></td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">1</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Komisi yang diterima Agen (10%)</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($data_agen_pph,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">2</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Komisi Transit</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: - </td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">3</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">Discount/Potongan Harga</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: - </td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah Komisi</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($data_agen_pph,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah UPP Setelah dikurangi Komisi</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($jml_upp_kurang_komisi,0,",",".")."</td>
                </tr>";
                $nomor_beban = 0;
                $total_beban_2 = 0;
                $UDJ_7_persen = 0;
                $jml_biaya_langsung = 0;
                $surplus_minus_operasi = 0;
                foreach ($data_setoran_detail_beban_2 as $row_beban) {
                    $nomor_beban = $nomor_beban+1;
                    $cRet .= "
                    <tr>
                        <td style=\"font-size:10px;text-align:center;\">$nomor_beban</td>
                        <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">$row_beban->nm_komponen</td>
                        <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($row_beban->total,0,",",".")." </td>
                    </tr>
                    ";
                    $total_beban_2+=$row_beban->total;
                }
                $UDJ_7_persen = 0.07*$jml_upp_kurang_komisi;
                $jml_biaya_langsung = $UDJ_7_persen+$total_beban_2;
                $surplus_minus_operasi = $jml_upp_kurang_komisi-$jml_biaya_langsung;

                $cRet .= "
                <tr>
                    <td style=\"font-size:10px;text-align:center;\">".($nomor_beban+1)."</td>
                    <td colspan=\"5\" style=\"font-size:10px;text-align:left;border-right-style:none;\">UDJ 7% dari UPP setelah dikurangi Komisi</td>
                    <td colspan=\"9\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($UDJ_7_persen,0,",",".")." </td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah Biaya Langsung</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($jml_biaya_langsung,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Surplus/Minus Operasi</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($surplus_minus_operasi,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Penerimaan Kembali Titipan</b></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-left-style:none;border-right-style:none;\"><b>1. Asuransi Jasa Raharja</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format(60*$total_karcis,0,",",".")."</td>
                </tr>";

                $nomor_beban_3 = 1;
                $total_beban_3 = 0;
                $penerimaan_kembali_pph = 0;
                $penerimaan_kembali_jumlah = 0;
                $setoran_final =0;
                foreach ($data_setoran_detail_beban_3 as $row) {
                    $nomor_beban_3 = $nomor_beban_3+1;
                    $cRet.="
                    <tr>
                        <td style=\"font-size:10px;text-align:center;\"></td>
                        <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b></b></td>
                        <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-left-style:none;border-right-style:none;\"><b>".$nomor_beban_3.". ".$row->nm_komponen."</b></td>
                        <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($row->total,0,",",".")."</td>
                    </tr>";
                    $total_beban_3+=$row->total;
                }
                $penerimaan_kembali_pph=$data_agen_pph*0.02;
                $penerimaan_kembali_jumlah = (60*$total_karcis)+$total_beban_3+$penerimaan_kembali_pph;
                $setoran_final = $surplus_minus_operasi+$penerimaan_kembali_jumlah;

                $cRet.="
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b></b></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-left-style:none;border-right-style:none;\"><b>".($nomor_beban_3+1).". PPH (2%)</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($penerimaan_kembali_pph,0,",",".")."</td>
                </tr>
                <tr>
                    <td style=\"font-size:10px;text-align:center;\"></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b></b></td>
                    <td colspan=\"6\" style=\"font-size:10px;text-align:left;border-left-style:none;border-right-style:none;\"><b>Jumlah.....</b></td>
                    <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: Rp. ".number_format($penerimaan_kembali_jumlah,0,",",".")."</td>
                </tr>

                <td style=\"font-size:10px;text-align:center;\"></td>
                <td colspan=\"14\" style=\"font-size:10px;text-align:left;border-right-style:none;height:15px\"></td>
            </tr>

            <td style=\"font-size:10px;text-align:center;\"></td>
            <td colspan=\"12\" style=\"font-size:10px;text-align:left;border-right-style:none;\"><b>Jumlah Yang Di Setorkan Oleh Awak Bos</b></td>
            <td colspan=\"2\" style=\"font-size:10px;text-align:left;border-left-style:none;\">: <b>Rp. ".number_format($setoran_final,0,",",".")."</b></td>
        </tr>
    </thead>";
        // $sql = $this->db->query("");
    $no = 0;
        // foreach ($sql->result() as $row){
    $no = $no+1;

    $cRet .="
    <tbody>
        <tr>
            <td style=\"font-size:10px;text-align:center;\"></td>
            <td colspan=\"14\" style=\"font-size:10px;text-align:left;\"><b></b></td>
        </tr>
    </tbody>";
            // } $no++;
    $cRet.="
</table>

<br /><table style=\"border-collapse:collapse;\" width=\"100%\" align=\"center\" border=\"0\">
<tr>
    <td width=\"33%\" align=\"left\" style=\"font-size:10px;\">
        <b>Sumber Data Dari :</b><br />
        <b>AP/1, AP/2, AP/3, dan AK/16</b><br />
        <b>Dibuat Rangkap 3 (tiga) :</b>
        <b>1. Lembar kesatu untuk Kantor Cabang</b>
        <b>2. Lembar kedua untuk Rekonsiliasi di Kantor Pusat</b>
        <b>3. Lembar ketiga untuk Arsip sesi Usaha</b>
    </td>
    <td width=\"33%\" align=\"center\" style=\"font-size:11px;\">
        <b>MENGETAHUI :</b>
        <b></br>GENERAL MANAGER</b><br /><br /><br />
        <b></br><u>".strtoupper($general_manager['nm_pegawai'])."</u></b>
    </td>
    <td width=\"33%\" align=\"center\" style=\"font-size:11px;\">
        <b>".$data_cabang->kota." , ".tgl_hari_ini()."</b><br />
        <b>PEMBUAT DAFTAR</b><br /><br />
        <b><br /><u>".strtoupper($session['nm_user'])."</u></b>
    </td>
</tr>
</table><br/>
";
$format = $this->input->post('print_data_laporan');
if($format == 0){
    echo $cRet;
                // } else if ($format == 1) {
                    //  $this->_mpdf('',$cRet,10,10,10,'L');
} else {
    $data['prev']= $cRet;
    header("Cache-Control: no-cache, no-store, must-revalidate");
    header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename= Laporan_AK/13_".tgl_indo($tanggal).".xls");
    $this->load->view('report/excel', $data);
}
}

public function laporan_ak13($id_bu,$tgl) {
    $session = $this->session->userdata('login');
    $nama_bu = $this->model_setoran->get_cabang($id_bu)->nm_bu;
    $cRet ='';
    $cRet .= "
    <table style=\"border-collapse:collapse;\" width=\"100%\" align=\"center\" border=\"0\">
        <tr>
            <td width=\"33%\" align=\"left\" style=\"font-size:11px;\">
                SEGMENT BUS KOTA/PEMADU MODA<br /><br/><br /><br/>
                <b>JURUSAN : ".$nama_bu."</b><br />
                <b>PANJANG TRAYEK : ...................................<br />
                    <b>TANGGAL : ".tgl_indo($tgl)."</b><br />
                </td>
                <td width=\"33%\" align=\"center\" style=\"font-size:18px;\">
                    <b>PERUSAHAAN UMUM DAMRI</b><br />
                    <b>(PERUM DAMRI)</b><br />
                    <b>KANTOR CABANG : ".strtoupper($nama_bu)."</b><br />

                </td>
                <td width=\"33%\" align=\"center\" style=\"font-size:18px;\">
                    <img src=\"".base_url('assets/img/logos.png')."\" alt=\"Perum DAMRI\" height=\"15\" width=\"100\">
                    <p style=\"font-size:12px;text-align:center;\">NOMOR</p>
                    <table style=\"border-collapse:collapse;\" align=\"right\" border=\"1\" style=\"font-size:12px;\">
                        <tr>
                            <td rowspan=\"3\" style=\"font-size:11px;text-align:center;\">JENIS PELAYANAN</td>
                            <td colspan=\"3\" style=\"font-size:11px;text-align:center;\">SERIE KARCIS</td>
                        </tr>
                        <tr>
                            <td colspan=\"2\" style=\"font-size:11px;text-align:center;\">NOMOR</td>
                            <td rowspan=\"2\" style=\"font-size:11px;text-align:center;\">JUMLAH</td>
                        </tr>
                        <tr>
                            <td style=\"font-size:11px;text-align:center;\">AWAL</td>
                            <td style=\"font-size:11px;text-align:center;\">AKHIR</td>
                        </tr>
                        <tr>
                            <td height=\"30\"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table><br/>
        <table style=\"border-collapse:collapse;\" border=\"1\" width=\"100%\">
            <thead>
                <tr>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">NO</td>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">KODE BUS</td>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">NOMOR POLISI</td>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">NOMOR AP/1 & AP/2</td>
                    <td colspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">NAMA</td>
                    <td colspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">BANYAKNYA</td>
                    <td colspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">JENIS PENUMPANG</td>
                    <td colspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">KARCIS TERJUAL</td>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">BAGASI COLLY/KG</td>
                    <td rowspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">TARIF PNP-KM,PNP-ORG/UMUM/PM</td>
                    <td colspan=\"3\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">PENDAPATAN</td>
                </tr>
                <tr>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">PENGEMUDI</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">KONDEKTUR</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">RIT</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">KM</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">UMUM</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">P.M</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">JML</td>
                    <td colspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">NOMOR</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">JUMLAH</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">PNP</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">BGS</td>
                    <td rowspan=\"2\" style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">JUMLAH</td>
                </tr>
                <tr>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">AWAL</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">AKHIR</td>
                </tr>
                <tr>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">1</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">2</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\"></td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">3</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">4</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">5</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">6</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">7</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">8</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">9</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">10</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">11</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">12</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">13</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">14</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">15</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">16</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">17</td>
                    <td style=\"font-size:12px;text-align:center;background-color:#d5d5e3;\">18</td>
                </tr>
            </thead>";
            $sql = $this->model_setoran->laporan_ak13($id_bu,$tgl);
            $no = 0;
            foreach ($sql->result() as $row){
                $no = $no+1;

                $cRet .="
                <tbody>
                    <tr>
                        <td style=\"font-size:12px;text-align:center;\">".$no++."</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->armada</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->plat_armada</td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\">$row->driver1</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->driver2</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->rit</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->km_trayek</td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\"></td>
                        <td style=\"font-size:12px;text-align:center;\">$row->pendapatan_pnp</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->bagasi</td>
                        <td style=\"font-size:12px;text-align:center;\">$row->total_pendapatan</td>
                    </tbody>";
                } $no++;
                $cRet.="<tfoot>
                <tr>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                    <td style=\"font-size:12px;text-align:right;\"></td>
                </tfoot>
            </table>

            <br /><table style=\"border-collapse:collapse;\" width=\"100%\" align=\"center\" border=\"0\">
            <tr>
                <td width=\"50%\" align=\"center\" style=\"font-size:11px;\">
                    MENGETAHUI :
                </br>MANAGER USAHA<br /><br />
                <b><br />(..................................)</b>
            </td>
            <td width=\"50%\" align=\"center\" style=\"font-size:11px;\">
                <br />
                PEMBUAT DAFTAR<br /><br />
                <br /><u>".strtoupper($session['nm_user'])."</u>
            </td>
        </tr>
    </table><br/>
    ";

                // if($format == 0){
    echo $cRet;
                    // } else if ($format == 1) {
                        //  $this->_mpdf('',$cRet,10,10,10,'L');
                        // } else {
                            //  $data['prev']= $cRet;
                            //  header("Cache-Control: no-cache, no-store, must-revalidate");
                            //  header("Content-Type: application/vnd.ms-excel");
                            //  header("Content-Disposition: attachment; filename= Rincian_Gaji_".$nm_cabang.".xls");
                            //  $this->load->view('report/excel', $data);
                            // }
}

}
