<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$combo 		= $this->input->get('combo');
	$tanggal 	= $this->input->get("tanggal");
	$tanggal 	= $tanggal ? $tanggal : date("Y-m-d");

	if($combo==2){
		$bulan 			= $this->input->get("bulan");
		$bulan_periode 	= "(month(a.tanggal)='$bulan')";
		$tahun 			= explode("-",$tanggal)[0];
		$tPERIODE 		= "Bulan";
		$vPERIODE 		= bulan($bulan)." ".$tahun;
	}elseif($combo==1){
		$periode_awal 	= $this->input->get("periode_awal");
		$periode_akhir 	= $this->input->get("periode_akhir");
		$bulan_periode 	= " (a.tanggal between '$periode_awal' and '$periode_akhir')";
		$tPERIODE 		= "Periode";
		$vPERIODE 		= tgl_indo($periode_awal)." s/d ".tgl_indo($periode_akhir);
	}
	$nm_segment = $this->input->get("id_segment");
	// $nm_segment = $this->model_reports->get_info("nm_segment","ref_segment","id_segment",$id_segment);
	
	$kd_trayek 	= $this->input->get("kd_trayek");
	$tryk 		= $this->input->get("kd_trayek");

	if($kd_trayek !='0' || $kd_trayek !=''){
		$nm_point_awal 	= $this->model_reports->get_info("nm_point_awal","ref_trayek","kd_trayek",$kd_trayek);
		$nm_point_akhir = $this->model_reports->get_info("nm_point_akhir","ref_trayek","kd_trayek",$kd_trayek);
		$km_trayek 		= $this->model_reports->get_info("km_trayek","ref_trayek","kd_trayek",$kd_trayek);
	}

	if($kd_trayek=='0'){
		$query_trayek = "";
	}else if($kd_trayek==''){
		$query_trayek = "";
	}else{
		$query_trayek = "AND a.kd_trayek='$kd_trayek'";
	}

	$session = $this->session->userdata('login');
	if($session['id_bu']=='60'){
		$query_id_bu = "";
	}else{
		$query_id_bu = "AND b.id_bu=".$session['id_bu'];
	}

	$general_manager   = $this->model_reports->general_manager($session['id_bu']);
	$manager_usaha     = $this->model_reports->manager_usaha($session['id_bu']);
	;?>
	<title>AP/5 - <?=strtoupper($nm_segment);?> (usaha)</title>
	<style>
		th{
			background : #6495ED;
		}
	</style>
</head>
<body>
	<h3 align="center">
		<span>JUMLAH KENDARAAN <?=strtoupper($nm_segment);?></span>
		<br/>JUMLAH RIT / HARI
		<br/>TARIP / PNP
	</h3>
	<table width="100%" style="font-size:14px">
		<tr>
			<td style="font-weight:bold;width:10%"><?=$tPERIODE?></td>
			<td style="font-weight:bold;width:1%">:</td>
			<td style="font-weight:bold;width:90%"><?=$vPERIODE?></td>
		</tr>

		<?php if($tryk=="0" || $tryk==""){ ?>
		<tr>
			<td style="font-weight:bold">Segmen</td>
			<td style="font-weight:bold">:</td>
			<td style="font-weight:bold"><?=ucwords($nm_segment);?></td>
		</tr>
		<?php }else{ ?>
		<tr>
			<td style="font-weight:bold">Trayek</td>
			<td style="font-weight:bold">:</td>
			<td style="font-weight:bold"><?=$nm_point_awal." - ".$nm_point_akhir?> (PP)</td>
		</tr>
		<tr>
			<td style="font-weight:bold">Panjang</td>
			<td style="font-weight:bold">:</td>
			<td style="font-weight:bold"><?=$km_trayek?> KM</td>
		</tr>
		<?php } ?>

	</table>
	<table width="100%" rules="all" border="1" cellpadding="0" cellspacing="0" style="font-size:12px">
		<tr>
			<th rowspan="3">NO</th>
			<th rowspan="3">TGL</th>
			<th rowspan="3">TRAYEK</th>
			<th colspan="2">NOMOR</th>
			<th rowspan="3">DAYA MUAT</th>
			<th colspan="3">BANYAKNYA</th>
			<th rowspan="3">TARIF PNP - ORG/UMUM</th>
			<th colspan="3">PENDAPATAN</th>
			<!-- <th rowspan="3">ASURANSI PNP</th> -->
			<th rowspan="3">UPP BERSIH</th>
		</tr>
		<tr>
			<th rowspan="2">CODE BUS</th>
			<th rowspan="2">POLISI</th>
			<th rowspan="2">RIT</th>
			<th rowspan="2">KM</th>
			<th>JUMLAH</th>
			<th rowspan="2">PNP</th>
			<th rowspan="2">BGS</th>
			<th rowspan="2">JUMLAH</th>
		</tr>
		<tr>
			<th>PNP-ORG</th>
		</tr>
		<tr>
			<th width="3%">1</th>
			<th width="4%">2</th>
			<th width="5%">3</th>
			<th width="3%">4</th>
			<th width="7%">5</th>
			<th width="3%">6</th>
			<th width="2%">7</th>
			<th width="2%">8</th>
			<th width="5%">9</th>
			<th width="5%">10</th>
			<th width="10%">11</th>
			<th width="5%">12</th>
			<th width="10%">13</th>
			<!-- <th width="5%">13</th> -->
			<th width="10%">14</th>
		</tr>

		<?php 
		$no=0;
		// $query = "select a.id_lmb,a.kd_trayek,day(a.tgl_lmb)tgl,a.rit,a.kd_armada,a.pnp,a.harga,a.total,b.plat_armada,b.seat_armada,COALESCE(c.km_trayek, 0 ) AS km_trayek
		// from tr_rit a
		// left join ref_armada b on a.kd_armada=b.kd_armada
		// left join ref_trayek c on a.kd_trayek=c.kd_trayek
		// where 
		// $bulan_periode 
		// AND a.kd_armada IS NOT NULL AND b.id_segment=$id_segment
		// $query_trayek $query_id_bu 
		// order by tgl, a.kd_armada,  CASE
		// WHEN a.rit='1' THEN 1
		// WHEN a.rit='2' THEN 2
		// WHEN a.rit='3' THEN 3
		// WHEN a.rit='4' THEN 4
		// WHEN a.rit='5' THEN 5
		// WHEN a.rit='6' THEN 6
		// ELSE 7
		// END
		// ";

		$query = "select a.rit, a.tanggal, a.armada, a.kd_trayek, a.km_trayek, a.jumlah, a.harga, a.bagasi_pnp,a.total,
		c.plat_armada,c.seat_armada
		from ref_setoran_detail_pnp a 
		left join ref_setoran_detail b on a.id_setoran_detail = b.id_setoran_detail 
		left join ref_armada c on a.armada = c.kd_armada 
		where 
		$bulan_periode 
		AND a.armada IS NOT NULL AND b.kd_segmen='$nm_segment'
		$query_trayek $query_id_bu
		order by a.tanggal, a.armada,  CASE
		WHEN a.rit='1' THEN 1
		WHEN a.rit='2' THEN 2
		WHEN a.rit='3' THEN 3
		WHEN a.rit='4' THEN 4
		WHEN a.rit='5' THEN 5
		WHEN a.rit='6' THEN 6
		ELSE 7
		END
		";
		$jml_pnp = $jml_pnp_tarif = $jml_bagasi = $jml_total = $jml_asuransi = $jml_total_asuransi = 0;
		$sql = $this->db->query($query);
		foreach ($sql->result() as $row) { ?>
		<tr>
			<td align="center"><?=$no+=1;?></td>
			<td align="center"><?=$row->tanggal;?></td>
			<td align="center"><?=$row->kd_trayek;?></td>
			<td align="center"><?=$row->armada;?></td>
			<td align="center"><?=$row->plat_armada;?></td>
			<td align="right"><?=$row->seat_armada;?></td>
			<td align="right"><?=$row->rit;?></td>
			<td align="right"><?=$row->km_trayek;?></td>
			<td align="right"><?=number_format($row->jumlah,0,',','.');?></td>
			<td align="right"><?=number_format($row->harga,0,',','.');?></td>
			<td align="right"><?=number_format($row->jumlah*$row->harga,0,',','.');?></td>
			<td align="right"><?=number_format($row->bagasi_pnp,0,',','.');?></td>
			<td valign="top" align="right"><?=number_format($row->total,0,',','.');?></td>
			<!-- <td valign="top" align="right"><?=number_format(($row->pnp*60),0,',','.');?></td> -->
			<td valign="top" align="right"><?=number_format($row->total,0,',','.');?></td>
		</tr>
		<?php
		$jml_pnp 		+= $row->jumlah;
		$jml_pnp_tarif 	+= $row->jumlah*$row->harga;
		$jml_bagasi 	+= $row->bagasi_pnp;
		$jml_total 		+= $row->total;
		$jml_asuransi 	+= ($row->jumlah*60);
		$jml_total_asuransi += ($row->total+($row->jumlah*60));
	} ?>
	<tr>
		<td align="center" colspan="6"><b>JUMLAH</b></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="right"><b><?=number_format($jml_pnp,0,',','.');?></b></td>
		<td align="center"></td>
		<td align="right"><b><?=number_format($jml_pnp_tarif,0,',','.');?></b></td>
		<td align="right"><b><?=number_format($jml_bagasi,0,',','.');?></b></td>
		<td valign="top" align="right"><b><?=number_format($jml_total,0,',','.');?></b></td>
		<!-- <td valign="top" align="right"><b><?=number_format($jml_asuransi,0,',','.');?></b></td> -->
		<td valign="top" align="right"><b><?=number_format($jml_total_asuransi,0,',','.');?></b></td>
	</tr>
</table>
<table width="100%" align="center"  cellspacing="0">
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" valign="top">Mengetahui <br>General Manager</td>
		<td align="center" valign="top">Menyetujui<br><?=$manager_usaha['posisi'];?></td>
		<td align="center" valign="top"><?=ucwords($cabang_kota).", ".tgl_indo($tanggal);?><br>Pembuat Daftar</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><u><b><?=$general_manager['nm_pegawai'];?></b></u><br>NIK. <?=$general_manager['nik_pegawai'];?></td>
		<td align="center"><u><b><?=$manager_usaha['nm_pegawai'];?></b></u><br>NIK. <?=$manager_usaha['nik_pegawai'];?></td>
		<td align="center"><u><b><?=$session['nm_user'];?></b></u><br>NIK. <?=$session['username'];?></td>
	</tr>
</table>
</body>
<html>