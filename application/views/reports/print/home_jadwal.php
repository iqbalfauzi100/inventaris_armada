<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$tanggal = $this->input->get("tanggal");
	$tanggal = $tanggal ? $tanggal : tgl_hari_ini();

	$bulan 			= $this->input->get("bulan");
	$bulan_periode 	= " (month(tanggal)='$bulan')";
	$tahun 			= $this->input->get("tahun");
	$tPERIODE 		= "Bulan";
	$vPERIODE 		= bulan($bulan)." ".$tahun;
	$jumlah_hari 	= date("t",mktime(0,0,0,$bulan,1,$tahun));
	$jumlah_sampai 	= $jumlah_hari;
	$awal_hari = 1;

	$divre = $this->input->get("divre");
	if($divre == 0){
		$dvr = "";
	}else{
		$dvr = "and b.id_divre = '$divre'";
	}

	?>

	<title>Rekapitulasi Input Jadwal</title>
	<style>
		th{
			background : #ccc;
		}
	</style>
</head>
<body>
	<h3 align="center">
		<span>REKAPITULASI INPUT JADWAL ARMADA</span>
		<br/>
	</h3>
	<table width="100%" style="font-size:14px">
		<tr>
			<td style="font-weight:bold;width:10%"><?=$tPERIODE?></td>
			<td style="font-weight:bold;width:1%">:</td>
			<td style="font-weight:bold;width:90%"><?=$vPERIODE?></td>
		</tr>
	</table>
	<table border="1" rules="all" width="100%" style="font-size:12px">
		<tr>
			<th rowspan="2" width="3%">No</th>
			<th rowspan="2" width="7%">Cabang</th>
			<th rowspan="2" width="7%">Divre</th>
			<th colspan="<?=$jumlah_hari?>" width="60%">Tanggal</th>
			<th rowspan="2" width="3%">Total</th>
			<th rowspan="2" width="3%">HTM/HTP</th>
			<th rowspan="2" width="3%">HTSK</th>
			<th rowspan="2" width="3%">HJ</th>
			<th rowspan="2" width="3%">S.O</th>
			<th rowspan="2" width="3%">Keterangan</th>
		</tr>
		<tr>
			<?php for($x=$awal_hari;$x<=$jumlah_sampai;$x++){ ?>
			<th width="2%"><?=$x?></th>
			<?php } ?>
		</tr>
		<?php
		$qry_hadir = "select a.*,day(a.tanggal)hari 
		from ms_jadwal a 
		LEFT JOIN ref_bu b on a.id_cabang=b.id_bu
		where $bulan_periode $dvr order by a.id_cabang";
		$sql_hadir = $this->db->query($qry_hadir);
		$exec_hadir = $sql_hadir->result_array();
		foreach($exec_hadir as $data_hadir){
			extract($data_hadir);
			$absen[$id_cabang][$hari] 	= $id_jadwal;
			$info[$id_cabang][$hari] 	= $armada;
		}
		$qry_bu = "select b.* from ref_bu b where b.id_bu not between 60 and 65 $dvr order by b.id_divre asc";
		$sql_bu = $this->db->query($qry_bu);
		$exec_bu = $sql_bu->result_array();
		$urut = 0;
		$total_ = $total_all = 0;
		foreach($exec_bu as $data_bu){
			$urut++;
			extract($data_bu);
			?>
			<tr>
				<td align="center"><?=$urut?></td>
				<td><?=$nm_bu?></td>
				<td><?=$id_divre?></td>

				<!-- <?php
				$tot[$kd_armada] = $htp[$kd_armada] = $hj[$kd_armada] = $htsk[$kd_armada] = $t_ket[$kd_armada] = 0;
				for($x=$awal_hari;$x<=$jumlah_sampai;$x++){
					$hari = $x;
					$poin = isset($absen[$kd_armada][$hari]) ? $absen[$kd_armada][$hari] : 0;
					if($poin==1){
						$kehadiran = "HTP";
						$info_hadir = "HTM/HTP";
						$htp[$kd_armada] += 1;
					}
					if($poin==2){
						$kehadiran = "HJ";
						$info_hadir = "HJ";
						$hj[$kd_armada] += 1;
					}
					if($poin==3){
						$kehadiran = "HTSK";
						$info_hadir = "HTSK";
						$htsk[$kd_armada] += 1;
					}
					if($poin==0){
						$kehadiran = "-";
						$info_hadir = "Tanpa Keterangan";
						$t_ket[$kd_armada] += 1;
					}

					$tot[$kd_armada] += 1;
					$informasi = isset($info[$kd_armada][$hari]) ? $info[$kd_armada][$hari] : "";
					?>
					<td align=center title="<?=$info_hadir." : ".$informasi?>"><label><?=$kehadiran?></label></td>
					<?php } ?> -->
					
				</tr>
				<?php

			}
			?>
			<!-- <tr>
				<td></td>
				<td colspan="2" align="center"><b>TOTAL</b></td>
				<td colspan="<?=$jumlah_sampai;?>"></td>
				<td></td>
				<td align=right><b><?=$total_all_htm;?></b></td>
				<td align=right><b><?=$total_all_htsk;?></b></td>
				<td align=right><b><?=$total_all_hj;?></b></td>
				<td align=right><b><?=$total_all_SO;?></b></td>
				<td align=right></td>
			</tr> -->
		</table>

	</body>
	<html>