<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$id_armada = $this->input->get("id_armada");
	$kd_armada = $this->input->get("kd_armada");
	$id_bu = $this->input->get("id_bu");
	$nama_cabang = $this->model_reports->get_info_2("nm_bu","ref_bu","id_bu",$id_bu);
	$plat_armada = $this->model_reports->get_info_2("plat_armada","ref_armada","id_armada",$id_armada);

	?>

	<title>Data Armada <?=ucwords($kd_armada);?></title>
	<style>
		th{
			background : #ccc;
		}
	</style>
</head>
<body>
	<!-- <h5 align="left">
		<span>KODE BUS : <?=strtoupper($kd_armada);?> - <?=strtoupper($nama_cabang);?></span><br>
		<span>PLAT ARMADA : <?=strtoupper($plat_armada);?></span>
	</h5> -->

	<table style="border-collapse:collapse;" width="100%" border="0">
		<tr>
			<td width="20%" align="left" style="font-size:12px;"><b>CABANG</b></td>
			<td width="80%" align="left" style="font-size:12px;"><b>: <?=strtoupper($nama_cabang);?></b></td>
		</tr>
		<tr>
			<td width="20%" align="left" style="font-size:12px;"><b>KD ARMADA</b></td>
			<td width="80%" align="left" style="font-size:12px;"><b>: <?=strtoupper($kd_armada);?></b></td>
		</tr>
		<tr>
			<td width="20%" align="left" style="font-size:12px;"><b>PLAT ARMADA</b></td>
			<td width="80%" align="left" style="font-size:12px;"><b>: <?=strtoupper($plat_armada);?></b></td>
		</tr>
	</table><br/>

	<?php
	$this->db->select("a.*");
	$this->db->from("(SELECT * from ref_armada_foto WHERE id_armada='$id_armada' ORDER BY cdate DESC)a");
	$this->db->group_by('a.nm_armada_foto');
	$this->db->order_by("
		CASE
		WHEN a.nm_armada_foto='DEPAN KANAN' THEN 1
		WHEN a.nm_armada_foto='DEPAN KIRI' THEN 2
		WHEN a.nm_armada_foto='BELAKANG' THEN 3
		WHEN a.nm_armada_foto='GESEK RANGKA' THEN 4
		WHEN a.nm_armada_foto='GESEK MESIN' THEN 5
		WHEN a.nm_armada_foto='FOTO MESIN' THEN 6
		ELSE 7
		END
		");
	$data = $this->db->get();
	$no=0;?>
	
	<table border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<?php foreach ($data->result_array() as $data_armada) { 
				extract($data_armada);

				$array_foto = array("DEPAN KANAN", "DEPAN KIRI", "BELAKANG", "DALAM");
				if (in_array($nm_armada_foto, $array_foto)) {
					?>
					<td style="font-size:14px;text-align:center;"><b><?=strtoupper($nm_armada_foto);?></b></td>
					<?php 
				} 
			} ?>
		</tr>

		<tr>
			<?php
			foreach ($data->result_array() as $data_armada) { 
				extract($data_armada);

				$array_foto = array("DEPAN KANAN", "DEPAN KIRI", "BELAKANG", "DALAM");
				if (in_array($nm_armada_foto, $array_foto)) {
					?>
					<td style="font-size:12px;text-align:center;"><img src="<?=base_url()?>uploads/armada/foto/<?=$attachment?>" alt="Foto Armada" width="300" height="300"></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php 
				} 
			} ?>
		</tr>
	</table>

	<br>

	<table border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<?php foreach ($data->result_array() as $data_armada) { 
				extract($data_armada);
				$array_foto2 = array("GESEK RANGKA", "GESEK MESIN", "FOTO MESIN");
				if (in_array($nm_armada_foto, $array_foto2)) {
					?>
					<td style="font-size:14px;text-align:center;"><b><?=strtoupper($nm_armada_foto);?></b></td>
					<?php 
				} 
			} ?>
		</tr>

		<tr>
			<?php
			foreach ($data->result_array() as $data_armada) { 
				extract($data_armada);

				$array_foto2 = array("GESEK RANGKA", "GESEK MESIN", "FOTO MESIN");
				if (in_array($nm_armada_foto, $array_foto2)) {
					?>
					<td style="font-size:12px;text-align:center;"><img src="<?=base_url()?>uploads/armada/foto/<?=$attachment?>" alt="Foto Armada" width="300" height="300"></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php 
				} 
			} ?>
		</tr>
	</table>

	<br>

	<?php 
	$this->db->select("a.*");
	$this->db->from("(SELECT * from ref_armada_stnk WHERE id_armada='$id_armada' ORDER BY tgl_exp_stnk DESC LIMIT 2)a");
	$data_stnk = $this->db->get();

	$jumlah_stnk = $data_stnk->num_rows();
	?>

	<table border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="font-size:14px;text-align:center;" colspan="<?=$jumlah_stnk;?>"><b>STNK</b></td>
		</tr>
		<tr>
			<?php 
			foreach ($data_stnk->result_array() as $data_stnk) { 
				extract($data_stnk);
				?>
				<td style="font-size:12px;text-align:center;"><img src="<?=base_url()?>uploads/armada/stnk/<?=$attachment?>" alt="Foto STNK" width="400" height="250"></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php } ?>
			</tr>
		</table>

	</body>
	<html>