<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$id_bu = $this->input->get("id_bu");
	$tahun = $this->input->get("tahun");

	if($id_bu==0){
		$nama_cabang="SEMUA CABANG";
	}else{
		$nama_cabang=$cabang_nama;
	}

	?>

	<title>Data Armada Rekondisi <?=ucwords($nama_cabang);?></title>
	<style>
		th{
			background : #ccc;
		}
	</style>
</head>
<body>
	<h3 align="center">
		<span>LAPORAN REKONDISI <?=strtoupper($nama_cabang);?> TAHUN <?=$tahun;?></span>
		<br/>
	</h3>
	
	<table border="1" width="100%" style="border-collapse:collapse;">
		<tr>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>CABANG</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>KODE ARMADA</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>MEREK</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>TYPE</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>JENIS</b></td>

			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>BIAYA REKONDISI</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>STATUS</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO SURAT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>TANGGAL</b></td>
		</tr>
		<?php
		$this->db->select("a.*,b.nm_merek,b.tipe_armada,b.nm_ukuran");
		$this->db->from("ref_armada_rekondisi a");
		$this->db->join("ref_armada b","a.id_armada = b.id_armada", 'left');
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}else{
			$this->db->where("a.id_bu in (3, 7, 8, 17)");
		}
		$this->db->where('a.tahun', $tahun);
		$this->db->where('a.active != 2');

		$this->db->order_by("a.id_bu","ASC");
		$this->db->order_by("a.active","ASC");
		$data = $this->db->get();

		$no=0; $total_biaya_rekondisi = 0;
		foreach ($data->result() as $row) {

			?> 
			<tr>
				<td style="font-size:12px;text-align:center;"><?=($no+=1);?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->nm_bu;?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->kd_armada;?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->nm_merek;?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->tipe_armada;?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->nm_ukuran;?></td>
				<td style="font-size:12px;text-align:right;"><?=number_format($row->biaya,0,'.',',');?></td>

				<?php if($row->active==0){ ?>
				<td style="font-size:12px;text-align:center;background-color:#8be0e8;">Pengajuan</td>
				<?php }else if($row->active==1){ ?>
				<td style="font-size:12px;text-align:center;background-color:#fa9119;">Reject</td>
				<?php }else if($row->active==2){ ?>
				<td style="font-size:12px;text-align:center;background-color:#d5d5e3;">Deleted</td>
				<?php }else if($row->active==3){ ?>
				<td style="font-size:12px;text-align:center;background-color:#1991fa;">Rekomendasi Divre</td>
				<?php }else if($row->active==4){ ?>
				<td style="font-size:12px;text-align:center;background-color:#47a6ff;">Persetujuan</td>
				<?php }else { ?>
				<td style="font-size:12px;text-align:center;background-color:#47ff60;">Selesai</td>
				<?php } ?>

				<td style="font-size:12px;text-align:left;"><?=$row->no_surat;?></td>
				<td style="font-size:12px;text-align:left;"><?=$row->tgl_rekondisi;?></td>

			</tr>
			<?php 
			$total_biaya_rekondisi += $row->biaya;
		} ?>
		<tr>
			<td colspan="6" align="center" style="font-size:12px;text-align:center;"><b>TOTAL</b></td>
			<td style="font-size:12px;text-align:right;"><b><?=number_format($total_biaya_rekondisi,0,'.',',');?></b></td>
			<td style="font-size:12px;text-align:center;"></td>
			<td style="font-size:12px;text-align:center;"></td>
			<td style="font-size:12px;text-align:center;"></td>
		</tr>

	</table>

</body>
<html>