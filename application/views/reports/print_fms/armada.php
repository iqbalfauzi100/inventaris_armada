<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$id_bu = $this->input->get("id_bu");

	if($id_bu==0){
		$nama_cabang="SEMUA CABANG";
	}else{
		$nama_cabang=$cabang_nama;
	}

	?>

	<title>Data Armada <?=ucwords($nama_cabang);?></title>
	<style>
		th{
			background : #ccc;
		}
	</style>
</head>
<body>
	<h3 align="center">
		<span>DATA ARMADA <?=strtoupper($nama_cabang);?></span>
		<br/>
	</h3>
	
	<table border="1" rules="all" width="100%">
		<tr>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>CABANG</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>KD ARMADA</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>RANGKA</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>MESIN</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>PLAT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>TAHUN PEMBUATAN</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>TAHUN PEROLEHAN</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>MEREK</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>TYPE</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>WARNA</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>SILINDER</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>UKURAN</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>SEAT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>LAYOUT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>SEGMENT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>LAYANAN</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO.POLIS</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO.CERT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>STATUS</b></td>
		</tr>
		<?php
		$this->db->select("a.*,b.nm_bu,
			(select y.no_polis from ref_asuransi_detail y 
           left join ref_asuransi z on y.id_asuransi=z.id_asuransi
           where y.id_armada = a.id_armada and y.active=1 and z.active=1
           order by y.id_asuransi_detail desc limit 1 ) as no_polis,
           
           (select y.no_certificate from ref_asuransi_detail y 
           left join ref_asuransi z on y.id_asuransi=z.id_asuransi
           where y.id_armada = a.id_armada and y.active=1 and z.active=1
           order by y.id_asuransi_detail desc limit 1  ) as no_certificate
			");
		$this->db->from("ref_armada a");
		$this->db->join("ref_bu b","a.id_bu = b.id_bu", 'left');
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}else{
			$this->db->where("a.id_bu in (3, 7, 8, 17)");
		}
		$this->db->where("a.active IN (0, 3) ");
		$this->db->order_by("b.id_divre","ASC");
		$this->db->order_by("b.id_bu","ASC");
		$this->db->order_by("a.status_armada","ASC");
		$data = $this->db->get();

		$no=0;
		foreach ($data->result() as $row) { ?> 
		<tr>
			<td style="font-size:12px;text-align:center;"><?=($no+=1);?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_bu;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->kd_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->rangka_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->mesin_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->plat_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->tahun_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->tahun_perolehan;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_merek;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->tipe_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_warna;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->silinder_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_ukuran;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->seat_armada;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_layout;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->kd_segment;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->nm_layanan;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->no_polis;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->no_certificate;?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->status_armada;?></td>
		</tr>
			
		<?php } ?>
		
	</table>

</body>
<html>