<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<?=base_url("assets/img")?>/survei.png" type="image/x-icon">
	<?php
	$id_bu = $this->input->get("id_bu");
	$tahun = $this->input->get("tahun");

	if($id_bu==0){
		$nama_cabang="SEMUA CABANG";
	}else{
		$nama_cabang=$cabang_nama;
	}

	?>

	<title>Data Armada Lelang <?=ucwords($nama_cabang);?></title>
	<style>
		th{
			background : #ccc;
		}
	</style>
</head>
<body>
	<h3 align="center">
		<span>DATA ARMADA LELANG <?=strtoupper($nama_cabang);?> TAHUN <?=$tahun;?></span>
		<br/>
	</h3>
	
	<table border="1" width="100%" style="border-collapse:collapse;">
		<tr>
			<td rowspan="2" style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NO</b></td>
			<td rowspan="2" style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>KANTOR CABANG</b></td>
			<td rowspan="2" style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NAMA PEMENANG</b></td>
			<td colspan="2" style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>PENILAIAN APPRAISAL</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>HASIL LELANG</b></td>
			<td rowspan="2" style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NOMOR RISALAH</b></td>
		</tr>
		<tr>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>JUMLAH UNIT</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>NILAI LIKUIDASI</b></td>
			<td style="font-size:14px;text-align:center;background-color:#d5d5e3;"><b>HARGA TERJUAL</b></td>
		</tr>
		<?php
		$this->db->select("a.*");
		$this->db->from("ref_armada_lelang a");
		if($id_bu<>0){
			$this->db->where('a.id_bu', $id_bu);
		}else{
			$this->db->where("a.id_bu in (3, 7, 8, 17)");
		}
		$this->db->where('a.tahun_ua', $tahun);
		// $this->db->where("a.active IN (0, 3) ");
		$this->db->order_by("a.id_bu","ASC");
		$data = $this->db->get();

		$no=0; $total_jumlah_armada = $total_nilai_liduidasi = $total_nilai_lelang = 0;
		foreach ($data->result() as $row) { ?> 
		<tr>
			<td style="font-size:12px;text-align:center;"><?=($no+=1);?></td>
			<td style="font-size:12px;text-align:left;"><?=$row->nm_bu;?></td>
			<td style="font-size:12px;text-align:left;"><?=$row->nm_pemenang;?></td>
			<td style="font-size:12px;text-align:right;"><?=number_format($row->jumlah_armada,0,'.',',');?></td>
			<td style="font-size:12px;text-align:right;"><?=number_format($row->nilai_liduidasi,0,'.',',');?></td>
			<td style="font-size:12px;text-align:right;"><?=number_format($row->nilai_lelang,0,'.',',');?></td>
			<td style="font-size:12px;text-align:center;"><?=$row->no_risalah;?></td>
		</tr>
			
		<?php 
		 $total_jumlah_armada += $row->jumlah_armada; 
		 $total_nilai_liduidasi += $row->nilai_liduidasi; 
		 $total_nilai_lelang += $row->nilai_lelang; 
		} ?>
		<tr>
			<th colspan="3" align="center" style="font-size:12px;text-align:center;">TOTAL</th>
			<th style="font-size:12px;text-align:right;"><?=number_format($total_jumlah_armada,0,'.',',');?></th>
			<th style="font-size:12px;text-align:right;"><?=number_format($total_nilai_liduidasi,0,'.',',');?></th>
			<th style="font-size:12px;text-align:right;"><?=number_format($total_nilai_lelang,0,'.',',');?></th>
			<th style="font-size:12px;text-align:center;"></th>
		</tr>
		
	</table>

</body>
<html>