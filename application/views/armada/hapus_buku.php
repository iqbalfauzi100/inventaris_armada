<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Arsip Hapus Buku</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
									<!-- <button class="btn btn-primary" onclick='ViewData(0)'>
										<i class='fa fa-plus'></i> Add Hapus Buku
									</button> -->
									<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="Form-add-bu" id="addModalLabel">Form Add Hapus Buku</h4>
												</div>
												<div class="modal-body">
													<input type="hidden" id="id_hapus_buku" name="id_hapus_buku" value='' />
													

													<div class="form-group">
														<label>Kode</label>
														<input type="text" id="kd_hapus_buku" name="kd_hapus_buku" class="form-control" placeholder="Kode">
													</div>

													<div class="form-group">
														<label>Name</label>
														<input type="text" id="nm_hapus_buku" name="nm_hapus_buku" class="form-control" placeholder="Name">
													</div>
													

													<div class="form-group">
														<label>Active</label>
														<select class="form-control" id="active" name="active">
															<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
															<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
														</select>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">

									<div class="row">
										<div class="col-lg-12">
											<div class="form-group col-lg-3">
												<label>Tahun</label>
												<select class="form-control select2 " style="width: 100%;" id="tahun_filter" name="id_bu_filter">
													<?php
													foreach ($combobox_tahun->result() as $rowmenu) {
														?>
														<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
														<?php
													}
													?>
												</select>
											</div>
											<div class="form-group col-lg-4">
												<label>Cabang</label>
												<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
													<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
													<option value="0">--All Cabang--</option>
													<?php } ?>
													<?php
													foreach ($combobox_bu->result() as $rowmenu) {
														?>
														<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
														<?php
													}
													?>
												</select>
											</div>
											<div class="form-group col-md-3">
												<label>Print</label>
												<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
													<option value="html" >1. HTML</option>
													<option value="pdf" >2. PDF</option>
													<option value="excell" >3. Excell</option>
												</select>
											</div>
											<div class="form-group col-md-2">
												<p style="height: 15px"></p>
												<button class="btn btn-info" title="Print" onclick='printCetak()'>
													<i class='fa fa-copy'></i> Print
												</button>
											</div>
										</div>
									</div>

									<div class="dataTable_wrapper">
										<table class="table table-striped table-bordered table-hover" id="buTable">
											<thead>
												<tr>
													<th>Options</th>
													<th>#</th>
													<th>Cabang</th>
													<th>Kode</th>
													<th>Rangka</th>
													<th>Mesin</th>
													<th>Plat</th>
													<th>Tahun Pembuatan</th>
													<th>Tahun Perolehan</th>
													<th>Merek</th>
													<th>Type</th>
													<th>Warna</th>
													<th>Silinder</th>
													<th>Ukuran</th>
													<th>Seat</th>
													<th>Layout</th>
													<th>Segment</th>
													<th>Layanan</th>
													<th>Status</th>
													<th>CUser</th>
													<th>CDate</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>




					<!-- FOTO ARMADA -->
					<div class="row" >
						<div class="col-lg-12">
							<div class="modal fade" id="addModalfotoarmada" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="form-add" id="addModalLabel">Form List Foto Armada</h4>


										</div>
										<div class="modal-body">
											<div class="row">
												<form id="submit">
													<input type="hidden" id="id_armada_foto" name="id_armada_foto" class="form-control">
													<input type="hidden" id="id_armada" name="id_armada" class="form-control">
													<div class="form-group col-lg-3">

														<label>Sisi</label>
														<select class="form-control" id="nm_armada_foto" name="nm_armada_foto">
															<option value="DEPAN KANAN" >DEPAN KANAN</option>
															<option value="DEPAN KIRI" >DEPAN KIRI</option>
															<option value="BELAKANG" >BELAKANG</option>
															<option value="DALAM" >DALAM</option>
														</select>
													</div>
													<div class="form-group col-lg-6">
														<label>Nama Attachment</label>
														<input type="file" name="file" id="file_attachment" accept=".jpeg,.jpeg" class="form-control">
														<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
													</div>
													<div class="form-group col-lg-3">
														<label>_</label>
													</div>
												</form>

											</div>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="attachmentTable">
													<thead>
														<tr>
															<th>Aksi</th>
															<th>#</th>
															<th>Sisi</th>
															<th>Foto Armada</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>
										<div class="modal-footer">

											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- FOTO STNK -->
					<div class="row" >
						<div class="col-lg-12">
							<div class="modal fade" id="addModalfotostnk" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="form-add" id="addModalLabel">Form List Foto STNK</h4>


										</div>
										<div class="modal-body">
											<div class="row">
												<form id="submit_stnk">
													<input type="hidden" id="id_armada_stnk" name="id_armada_stnk" class="form-control">
													<input type="hidden" id="id_armadastnk" name="id_armadastnk" class="form-control">
													<div class="form-group col-lg-3">

														<label>Tanggal EXP</label>
														<input type="text" id="tgl_exp_stnk" name="tgl_exp_stnk" class="form-control" placeholder="yyyy-mm-dd">
													</div>
													<div class="form-group col-lg-2">
														<label>Masa</label>
														<select class="form-control " style="width: 100%;" id="masa" name="masa">
															<option value="1">1</option>
															<option value="5">5</option>
														</select>
													</div>
													<div class="form-group col-lg-4">
														<label>Nama Attachment</label>
														<input type="file" name="file" id="file_attachment_stnk" accept=".jpeg,.jpeg" class="form-control">
														<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
													</div>
													<div class="form-group col-lg-3">
														<label>_</label>
													</div>
												</form>

											</div>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="stnkTable">
													<thead>
														<tr>
															<th>Aksi</th>
															<th>#</th>
															<th>Tanggal Kadaluarsa</th>
															<th>Masa</th>
															<th>Foto STNK</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>
										<div class="modal-footer">

											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- FOTO KEUR -->
					<div class="row" >
						<div class="col-lg-12">
							<div class="modal fade" id="addModalfotokeur" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="form-add" id="addModalLabel">Form List Foto KEUR</h4>


										</div>
										<div class="modal-body">
											<div class="row">
												<form id="submit_keur">
													<input type="hidden" id="id_armada_keur" name="id_armada_keur" class="form-control">
													<input type="hidden" id="id_armadakeur" name="id_armadakeur" class="form-control">
													<div class="form-group col-lg-3">
														<label>Tanggal EXP</label>
														<input type="text" id="tgl_exp_keur" name="tgl_exp_keur" class="form-control" placeholder="yyyy-mm-dd">
													</div>
												<!-- <div class="form-group col-lg-4">
													<label>Masa Berlaku (Tahun)</label>
													<select class="form-control " style="width: 100%;" id="masa_keur" name="masa_keur">
														<option value="1">1</option>
														<option value="5">5</option>
													</select>
												</div> -->
												<div class="form-group col-lg-4">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment_keur" accept=".jpg, .JPG, .jpeg, .pdf, .png" class="form-control">
													<p class="help-block">Upload Foto Keur. Format File: Jpg/PDF  Size Max: 500KB   </p>
												</div>
												<div class="form-group col-lg-3">
													<label>_</label>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="keurTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Tgl Exp</th>
														<th>Foto Keur</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>





				<!-- HISTORY MUTASI -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalmutasiarmada" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">List History Mutasi Armada</h4>
									</div>
									<div class="modal-body">
										<div class="dataTable_wrapper">
											<input type="hidden" id="id_armada_mutasi" name="id_armada_mutasi" class="form-control">
											<table class="table table-striped table-bordered table-hover" id="mutasiarmadaTable">
												<thead>
													<tr>
														<th>No</th>
														<th>KD Armada</th>
														<th>Cabang Penerima</th>
														<th>No. SK Mutasi</th>
														<th>Tgl Mutasi</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- IJIN TRAYEK -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalijintrayek" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">Form List Ijin Trayek</h4>


									</div>
									<div class="modal-body">
										<div class="row">
											<form id="submit_ijintrayek">
												<input type="hidden" id="id_armada_ijintrayek" name="id_armada_ijintrayek" class="form-control">
												<input type="hidden" id="id_armadaijintrayek" name="id_armadaijintrayek" class="form-control">

												<div class="form-group col-lg-5">
													<label>Trayek</label>
													<select class="form-control select2" style="width: 100%;" id="id_trayek" name="id_trayek">
													</select>
												</div>

												<div class="form-group col-lg-2">
													<label>Berlaku Sampai Tgl</label>
													<input type="text" id="tgl_exp_ijintrayek" name="tgl_exp_ijintrayek" class="form-control" placeholder="yyyy-mm-dd">
												</div>
												<div class="form-group col-lg-3">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment_ijintrayek" accept=".jpg, .JPG, .jpeg, .pdf, .png" class="form-control">
													<p class="help-block">Upload Foto Ijin Trayek. Format File: Jpg.pdf  Size Max: 500KB </p>
												</div>
												<div class="form-group col-lg-2">
													<label>_</label>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="ijintrayekTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Nm Trayek</th>
														<th>Tanggal Berlaku</th>
														<th>Foto</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



			</section>
		</div>
	</div>
	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		var buTable = $('#buTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],
					ajax: 
					{
						url: "<?= base_url()?>hapus_buku/ax_data_hapus_buku/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_bu": $("#id_bu_filter").val(),
								"tahun": $("#tahun_filter").val(),
							});
						}
					},
					columns: 
					[

					{
						data: "id_armada", render: function(data, type, full, meta){

							

							var id1 = "'"+data+"','"+full['kd_armada']+"','"+full['id_bu']+"'";
							var str = '';
							str += '<div class="btn-group">';
							str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							str += '<ul class="dropdown-menu">';
							
							var kd = "Foto('"+data+"')";
							str += '<li><a onclick="'+ kd +'"><i class="fa fa-truck"></i> Foto Armada</a></li>';
							str += '<li><a onclick="Stnk(' + data + ')"><i class="fa fa-truck"></i> Foto STNK</a></li>';
							str += '<li><a onclick="Keur(' + data + ')"><i class="fa fa-truck"></i> Foto Keur</a></li>';
							str += '<li><a onclick="Ijin(' + id1 + ')"><i class="fa fa-truck"></i> Foto Ijin Trayek</a></li>';

							str += '<li><a onclick="historymutasi(' + data + ')"><i class="fa fa-list"></i> History Mutasi</a></li>';

							// if(session_level==1 || session_level==7){
							// 	str += '<li><a onClick="PrintArmada(' + id1 + ')"><i class="fa fa-print"></i> Print Armada</a></li>';
							// }
							str += '</ul>';
							str += '</div>';

							// if(session_level != 13){
								return str;
							// }else{
							// 	return '';
							// }

						}
					},
					{ data: "id_hapus_buku" },
					{ data: "nm_bu" },
					{ data: "kd_armada" },

					{ class:'intro', data: "rangka_armada" },
					{ class:'intro', data: "mesin_armada" },
					{ class:'intro', data: "plat_armada" },
					{ class:'intro', data: "tahun_armada" },
					{ class:'intro', data: "tahun_perolehan" },
					{ class:'intro', data: "nm_merek" },
					{ class:'intro', data: "tipe_armada" },
					{ class:'intro', data: "nm_warna" },
					{ class:'intro', data: "silinder_armada" },
					{ class:'intro', data: "nm_ukuran" },
					{ class:'intro', data: "seat_armada" },
					{ class:'intro', data: "nm_layout" },
					{ class:'intro', data: "nm_segment" },
					{ class:'intro', data: "nm_layanan" },
					{ class:'intro', data: "status_armada" },
					{ class:'intro', data: "cuser" },
					{ class:'intro', data: "cdate_hapus_buku" }

					]
				});


		$('#btnSave').on('click', function () {
			if($('#nm_hapus_buku').val() == '')
			{
				alertify.alert("Warning", "Please fill hapus_buku Name.");
			}
			else
			{
				var url = '<?=base_url()?>hapus_buku/ax_set_data';
				var data = {
					id_hapus_buku: $('#id_hapus_buku').val(),
					kd_hapus_buku: $('#kd_hapus_buku').val(),
					nm_hapus_buku: $('#nm_hapus_buku').val(),

					active: $('#active').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Saved.");
						$('#addModal').modal('hide');
						buTable.ajax.reload();
					}
				});
			}
		});

		function ViewData(id_hapus_buku)
		{
			if(id_hapus_buku == 0)
			{
				$('#addModalLabel').html('Add hapus_buku');
				$('#id_hapus_buku').val('');
				$('#kd_hapus_buku').val('');
				$('#nm_hapus_buku').val('');
				$('#active').val('1');
				$('#addModal').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>hapus_buku/ax_get_data_by_id';
				var data = {
					id_hapus_buku: id_hapus_buku
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Edit hapus_buku');
					$('#id_hapus_buku').val(data['id_hapus_buku']);
					$('#kd_hapus_buku').val(data['kd_hapus_buku']);
					$('#nm_hapus_buku').val(data['nm_hapus_buku']);
					$('#select2-id_kota-container').html(data['nm_kota']);
					$('#id_kota').val(data['id_kota']);
					$('#active').val(data['active']);
					$('#addModal').modal('show');
				});
			}
		}

		function DeleteData(id_hapus_buku)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>hapus_buku/ax_unset_data';
					var data = {
						id_hapus_buku: id_hapus_buku
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						buTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		$('#tahun_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});


		function Foto(id_armada)
		{
			$('#addModalfotoarmada').modal('show');
			$('#id_armada').val(id_armada);
			attachmentTable.ajax.reload();

			setTimeout(function(){ attachmentTable.columns.adjust().draw(); }, 1000);
		}

		function Stnk(id_armada)
		{
			$('#addModalfotostnk').modal('show');
			$('#id_armada_stnk').val('');
			$('#tgl_exp_stnk').val('');
			$('#masa').val('1');

			$('#id_armadastnk').val(id_armada);
			stnkTable.ajax.reload();

			setTimeout(function(){ stnkTable.columns.adjust().draw(); }, 1000);
		}

		function Keur(id_armada)
		{
			$('#addModalfotokeur').modal('show');
			$('#id_armada_keur').val('');
			$('#tgl_exp_keur').val('');

			$('#id_armadakeur').val(id_armada);
			keurTable.ajax.reload();

			setTimeout(function(){ keurTable.columns.adjust().draw(); }, 1000);
		}

		function Ijin(id_armada,kd_armada,id_bu)
		{
			$('#addModalijintrayek').modal('show');
			$('#id_armadaijintrayek').val(id_armada);
			// combo_trayek(id_bu);
			ijintrayekTable.ajax.reload();

			setTimeout(function(){ ijintrayekTable.columns.adjust().draw(); }, 1000);
		}

		function historymutasi(id_armada)
		{
			$('#addModalmutasiarmada').modal('show');
			$('#id_armada_mutasi').val(id_armada);
			mutasiarmadaTable.ajax.reload();

			setTimeout(function(){ mutasiarmadaTable.columns.adjust().draw(); }, 1000);
		}

		var attachmentTable = $('#attachmentTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_foto/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_armada": $("#id_armada").val(),
					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_foto", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onClick="DeleteFoto(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					
						return '';
					
				}
			},
			{ class:'intro', data: "id_armada_foto" },
			{ data: "nm_armada_foto" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/foto/"+data;
				var op =  "window.open('"+url+"', '_blank');";
				return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';
			}
		},
		],
	});

		var stnkTable = $('#stnkTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_stnk/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_armada": $("#id_armadastnk").val(),
					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_stnk", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onclick="EditStnk(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
					str += '<li><a onClick="DeleteStnk(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					
						return '';
					
				}
			},
			{ class:'intro', data: "id_armada_stnk" },
			{ data: "tgl_exp_stnk" },
			{ data: "masa" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/stnk/"+data;
				var op =  "window.open('"+url+"', '_blank');";
				return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';
			}
		},
		],
	});



		var keurTable = $('#keurTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_keur/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_armada": $("#id_armadakeur").val(),
					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_keur", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onclick="EditKeur(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
					str += '<li><a onClick="DeleteKeur(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					
						return '';
					
				}
			},
			{ class:'intro', data: "id_armada_keur" },
			{ data: "tgl_exp" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/keur/"+data;
				var op =  "window.open('"+url+"', '_blank');";
				return '<a href="'+url+'" target="_blank">Lihat attachment</a>';
			}
		},
		],
	});



		var ijintrayekTable = $('#ijintrayekTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_ijintrayek/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_armada": $("#id_armadaijintrayek").val(),
					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_ijintrayek", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onClick="Deleteijintrayek(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					
						return '';
					
				}
			},

			{ class:'intro', data: "id_armada_ijintrayek" },
			{ data: "nm_trayek" },
			{ data: "tgl_exp" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/ijintrayek/"+data;
				var op =  "window.open('"+url+"', '_blank');";
				return '<a href="'+url+'" target="_blank">Lihat attachment</a>';
			}
		},
		],
	});



		var mutasiarmadaTable = $('#mutasiarmadaTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_mutasi_armada/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, {
						"id_armada": $("#id_armada_mutasi").val(),
					});
				}
			},
			columns: 
			[
			{
				data: "id",
				render: function(data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}},
				{ class:'intro', data: "kd_armada" },
				{ data: "nm_bu_sebelum" },
				{ data: "no_surat" },
				{ data: "tgl_mutasi" },
				],
			});


		</script>
	</body>
	</html>
