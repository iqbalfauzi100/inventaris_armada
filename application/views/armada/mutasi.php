<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Mutasi Armada</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>

							

							<div class="panel-body">
								<div class="row">
									<input type="hidden" id="id_mutasi" name="id_mutasi" value='' />
									<div class="form-group  col-md-4">
										<label>Cabang Sebelumnya</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_sebelum" name="id_bu_sebelum">
											<option value="0">--Cabang--</option>
											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group  col-md-4">
										<label>KD Armada</label>
										<select class="form-control select2" style="width: 100%;" id="id_armada">
											<option value="0">--KD Armada--</option>	
										</select>
									</div>

									<div class="form-group  col-md-4">
										<label>Cabang Penerima</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_sesudah" name="id_bu_sesudah">
											<option value="0">--Cabang--</option>
											<?php
											foreach ($combobox_cabang->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-3">
										<label>Jenis Surat</label>
										<select class="form-control " style="width: 100%;" id="jenis_surat" name="jenis_surat">
											<option value="1">1. Surat Pengambilan</option>
											<option value="2">2. Surat Keputusan (SK)</option>
											<option value="3">3. Kerjasama</option>
										</select>
									</div>
									<div class="form-group  col-md-3">
										<label>No. Surat</label>
										<input type="text" id="no_surat" name="no_surat" class="form-control" placeholder="Nomor Surat" autocomplete="off">
									</div>

									<div class="form-group  col-md-3">
										<label>Tgl Surat</label>
										<input type="text" id="tgl_mutasi" name="tgl_mutasi" class="form-control" placeholder="Tgl Surat" autocomplete="off">
									</div>

									<?php if($session_level != 13){ ?>
									<div class="form-group  col-md-3">
										<p style="height: 10px"></p>
										<button type="button" class="btn btn-primary" id='btnSave'><i class="fa fa-floppy-o"></i> Save</button>
									</div>
									<?php } ?>

									

								</div>



								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="buTable">
										<thead>
											<tr>
												<th width="150px">Options</th>
												<th>#</th>
												<th>KD Armada</th>
												<th>Jenis Surat</th>
												<th>No. Surat</th>
												<th>Dari</th>
												<th>Ke</th>
												<th>Tanggal Surat</th>
												<th>Status Approved</th>
												<th>Cdate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		$(document).ready(function() {

			$("#id_bu_sebelum").change(function() {
				$("#id_armada").hide();
				var id_bu_sebelum = $("#id_bu_sebelum").val();
				armadalist(id_bu_sebelum, 0, '--KD Armada--');
			});

		});

		var session_level = '<?=$session_level;?>';

		var buTable = $('#buTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>mutasi/ax_data_mutasi/",
				type: 'POST'
			},
			columns: 
			[
			{
				data: "id_mutasi", render: function(data, type, full, meta){

					var id1 = "'"+data+"','"+full['id_bu_sebelum']+"','"+full['id_armada']+"','"+full['id_bu_sesudah']+"'";

					var str = '';
					str += '<div class="btn-group">';
					

					var id_level = ["1", "7"];
					if( id_level.includes(session_level) ){
						if(full['status'] == 4){
							str += '<a type="button" class="btn btn-sm btn-success"><i class="fa fa-check">Approved</i> </a>';
						}else{
							str += '<a type="button" title="Edit Data" class="btn btn-sm btn-default" onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> </a>';	
							str += '<a type="button" class="btn btn-sm btn-danger" title="Delete Data" onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> </a>';
							str += '<a type="button" class="btn btn-sm btn-info" onClick="changeAjukanMutasi(' + data + ','+full['status']+')" title="Ajukan Mutasi"><i class="fa fa-sign-out"></i> </a>';
							str += '<a type="button" title="Reject" class="btn btn-sm btn-warning" onclick="changeReject(' + data + ')"><i class="fa fa-times"></i> </a>';
							str += '<a type="button" title="Approved" class="btn btn-sm bg-olive" onclick="changeApproved(' + id1 + ')"><i class="fa fa-check"></i> </a>';
						}
					}else{

						if(full['status']==0 || full['status']==1){
							str += '<a type="button" title="Edit Data" class="btn btn-sm btn-info" onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> </a>';	
							str += '<a type="button" class="btn btn-sm btn-danger" title="Delete Data" onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> </a>';
							str += '<a type="button" class="btn btn-sm btn-warning" onClick="changeAjukanMutasi(' + data + ','+full['status']+')" title="Ajukan Mutasi"><i class="fa fa-sign-out"></i> </a>';
						}

					}
					str += '</div>';
					if(session_level != 13){
						return str;
					}else{
						return '';
					}


				}
			},
			{ data: "id_mutasi" },
			{ data: "kd_armada" },
			{ data: "jenis_surat", render: function(data, type, full, meta){
				if(data == 1){
					return 'Surat Pengambilan' ;
				}if(data == 2){
					return 'Surat Keputusan' ;
				}else{
					return 'Kerjasama' ;
				}

			}},
			{ data: "no_surat" },
			{ data: "nm_bu_sebelum" },
			{ data: "nm_bu_sesudah" },
			{ data: "tgl_mutasi" },
			{ data: "status", render: function(data, type, full, meta){
				if(data == 0){
					return '<span class="label label-default">Draft</span>' ;
				}else if(data == 1){
					return '<span class="label label-warning">Reject</span>';
				}else if(data == 2){
					return '<span class="label label-danger">Deleted</span>';
				}else if(data == 3){
					return '<span class="label label-info">Waiting Approval</span>';
				}else{
					return '<span class="label label-success">Approved</span>';
				}

			}},
			{ data: "cdate" },
			]
		});

		$('#btnSave').on('click', function () {
			if($('#id_bu_sebelum').val() == '0')
			{
				alertify.alert("Warning", "Pilih Cabang Sebelumnya.");
			}else if($('#id_armada').val() == '0')
			{
				alertify.alert("Warning", "Pilih Armada.");
			}else if($('#id_bu_sesudah').val() == '0')
			{
				alertify.alert("Warning", "Pilih Cabang Penerima.");
			}else if($('#tgl_mutasi').val() == '')
			{
				alertify.alert("Warning", "Tanggal Surat tidak boleh kosong.");
			}
			else
			{
				var url = '<?=base_url()?>mutasi/ax_set_data';
				var data = {
					id_mutasi: $('#id_mutasi').val(),
					id_bu_sebelum: $('#id_bu_sebelum').val(),
					id_armada: $('#id_armada').val(),
					id_bu_sesudah: $('#id_bu_sesudah').val(),
					jenis_surat: $('#jenis_surat').val(),
					no_surat: $('#no_surat').val(),
					tgl_mutasi 	: $('#tgl_mutasi').val(),
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Saved.");
						$('#addModal').modal('hide');
						buTable.ajax.reload();


						$('#id_mutasi').val('');
						$('#id_bu_sebelum').val('0').trigger('change');
						$('#id_armada').val('0').trigger('change');
						$('#id_bu_sesudah').val('0').trigger('change');
						$('#jenis_surat').val('1');
						$('#no_surat').val('');
						$('#tgl_mutasi').val('');

					}
				});
			}
		});

		function ViewData(id_mutasi)
		{
			if(id_mutasi == 0)
			{
				$('#addModalLabel').html('Add Mutasi');
				$('#id_mutasi').val('');
				$('#id_bu_sebelum').val('0').trigger('change');
				$('#id_armada').val('0').trigger('change');
				$('#id_bu_sesudah').val('0').trigger('change');
				$('#jenis_surat').val('1');
				$('#no_surat').val('');
				$('#tgl_mutasi').val('');
				$('#addModal').modal('show');
				
			}
			else
			{
				var url = '<?=base_url()?>mutasi/ax_get_data_by_id';
				var data = {
					id_mutasi: id_mutasi
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Edit Mutasi');
					$('#id_mutasi').val(data['id_mutasi']);

					$('#id_bu_sebelum').val(data['id_bu_sebelum']).trigger('change');
					$('#id_bu_sesudah').val(data['id_bu_sesudah']).trigger('change');
					$('#select2-id_bu_sebelum-container').html(data['nm_bu_sebelum']);
					$('#select2-id_bu_sesudah-container').html(data['nm_bu_sesudah']);
					armadalist(data['id_bu_sebelum'], data['id_armada'], data['kd_armada']);

					$('#jenis_surat').val(data['jenis_surat']);
					$('#no_surat').val(data['no_surat']);
					$('#tgl_mutasi').val(data['tgl_mutasi']);
					$('#addModal').modal('show');
				});
			}
		}

		function DeleteData(id_mutasi)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>mutasi/ax_unset_data';
					var data = {
						id_mutasi: id_mutasi
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						buTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}



		function armadalist(id_bu,id_armada,kd_armada){
			$.ajax({
				type: "POST", 
				url: "<?= base_url() ?>mutasi/ax_get_armada", 
				data: {id_bu : id_bu}, 
				dataType: "json",
				beforeSend: function(e) {
					if(e && e.overrideMimeType) {
						e.overrideMimeType("application/json;charset=UTF-8");
					}
				},
				success: function(response){ 

					$("#id_armada").html(response.data_armada).show();
					$('#select2-id_armada-container').html(kd_armada);
					$('#id_armada').val(id_armada).trigger('change');
					// $('#id_armada').val(id_armada);
				},
				error: function (xhr, ajaxOptions, thrownError) { 
					alert(thrownError); 
				}
			});
		}

		function changeAjukanMutasi(id_mutasi, status){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Ajukan Mutasi .... Approval dari Pusat?', 
				function(){
					var url = '<?=base_url()?>mutasi/ax_change_ajukan_mutasi';
					var data = {
						id_mutasi: id_mutasi,
						status : status
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Ajukan Mutasi");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal diajukan Mutasi");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}


		function changeReject(id_mutasi, status){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Reject Mutasi?', 
				function(){
					var url = '<?=base_url()?>mutasi/ax_change_reject';
					var data = {
						id_mutasi: id_mutasi,
						status : status
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Reject");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal diReject");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}


		function changeApproved(id_mutasi, id_bu_sebelum,id_armada, id_bu_sesudah){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Approved Mutasi?', 
				function(){
					var url = '<?=base_url()?>mutasi/ax_change_approved';
					var data = {
						id_mutasi: id_mutasi,
						id_bu_sebelum : id_bu_sebelum,
						id_armada: id_armada,
						id_bu_sesudah : id_bu_sesudah
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Approved");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal Approved");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}


		$( "#tgl_mutasi").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#tgl_mutasi" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});
	</script>
</body>
</html>
