<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Rekondisi</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<?php if($session_level != 13){ ?>
								<button class="btn btn-primary" onclick='ViewData(0)'>
									<i class='fa fa-plus'></i> Add Rekondisi
								</button>
								<?php } ?>
								<div class="modal fade" id="addModal" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabel">Form Add Rekondisi</h4>
											</div>
											<div class="modal-body">
												<input type="hidden" id="id_rekondisi" name="id_rekondisi" value='' />

												<div class="form-group">
													<label>Tahun</label>
													<select class="form-control select2 " style="width: 100%;" id="tahun" name="tahun">
														<?php
														foreach ($combobox_tahun->result() as $rowmenu) {
															?>
															<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
															<?php } ?>
														</select>
													</div>

													<div class="form-group">
														<label>Cabang</label>
														<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
															<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
															<option value="0">--Cabang--</option>
															<?php } ?>
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group">
														<label>KD Armada</label>
														<select class="form-control select2" style="width: 100%;" id="id_armada">
															<option value="0">--KD Armada--</option>	
														</select>
													</div>

													<div class="form-group">
														<label>Biaya Rekondisi</label>
														<input type="number" id="biaya" name="biaya" class="form-control" placeholder="Biaya Rekondisi" autocomplete="off">
													</div>

													<div class="form-group">
														<label>No.Surat</label>
														<input type="text" id="no_surat" name="no_surat" class="form-control" placeholder="No.Surat" autocomplete="off">
													</div>
													<div class="form-group">
														<label>Tgl Surat</label>
														<input type="text" id="tgl_rekondisi" name="tgl_rekondisi" class="form-control" placeholder="Tgl Rekondisi" autocomplete="off">
													</div>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="form-group col-md-2">
											<label>Tahun</label>
											<select class="form-control select2 " style="width: 100%;" id="tahun_filter" name="tahun_filter">
												<?php
												foreach ($combobox_tahun->result() as $rowmenu) {
													?>
													<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
													<?php } ?>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label>Tahun</label>
												<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
													<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
													<option value="0">--All Cabang--</option>
													<?php } ?>
													<?php
													foreach ($combobox_bu->result() as $rowmenu) {
														?>
														<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-2">
													<label>Status</label>
													<select class="form-control select2 " style="width: 100%;" id="active_filter" name="active_filter">
														<option value="all">--All--</option>
														<option value="0">Pengajuan</option>
														<option value="1">Reject</option>
														<option value="3">Rekomendasi Divre</option>
														<option value="4">Persetujuan</option>
														<option value="5">Selesai</option>
													</select>
												</div>
												<div class="form-group col-md-2">
													<label>Print</label>
													<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
														<option value="html" >1. HTML</option>
														<option value="pdf" >2. PDF</option>
														<option value="excell" >3. Excell</option>
													</select>
												</div>
												<div class="form-group col-md-2">
													<p style="height: 15px"></p>
													<button class="btn btn-info" title="Print" onclick='printCetak()'>
														<i class='fa fa-copy'></i> Print
													</button>
												</div>

											</div>
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="buTable">
													<thead>
														<tr>
															<th>Options</th>
															<th>#</th>
															<th>Cabang</th>
															<th>Armada</th>
															<th>Biaya</th>
															<th>No.Surat</th>
															<th>Tanggal</th>
															<th>Status</th>
															<th>Cdate</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>


				<!-- HISTORY REKONDISI -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalRekondisi" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">History Rekondisi</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<input type="hidden" id="id_rekondisi_head" name="id_rekondisi_head" class="form-control">
											<input type="hidden" id="active_history" name="active_history" class="form-control">
											<div class="form-group col-lg-2">
												<label>No.Surat</label>
												<input type="text" id="no_surat_history" name="no_surat_history" class="form-control" placeholder="No.Surat" autocomplete="off">
											</div>
											<div class="form-group col-lg-3">
												<label>Biaya Rekondisi</label>
												<input type="number" id="biaya_history" name="biaya_history" class="form-control" placeholder="Biaya Rekondisi" autocomplete="off">
											</div>
											<div class="form-group col-lg-2">
												<label>Tgl Surat</label>
												<input type="text" id="tgl_rekondisi_history" name="tgl_rekondisi_history" class="form-control" placeholder="yyyy-mm-dd" autocomplete="off">
											</div>
											<div class="form-group col-lg-3">
												<label>Status</label>
												<select class="form-control" style="width: 100%;" id="status_history" name="status_history">
													<!-- <option value="0">1. Pengajuan</option>  
													<option value="3">2. Rekomendasi Divre</option>  
													<option value="4">3. Persetujuan</option>  
													<option value="5">4. Pengerjaan</option>  
													<option value="6">5. Selesai</option>  
													<option value="1">6. Cancel</option> -->
												</select>
											</div>
											<div class="form-group col-lg-2">
												<label>_</label>
												<button type="button" class="form-control btn btn-success" id='btnSaveHistory'>Update</button>
											</div>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="historyRekondisiTable">
												<thead>
													<tr>
														<th>#</th>
														<th>No.Surat</th>
														<th>Biaya</th>
														<th>Tgl.Surat</th>
														<th>Status</th>
														<th>User</th>
														<th>Cdate</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?= $this->load->view('basic_js'); ?>
				<script type='text/javascript'>

					var session_level = '<?=$session_level;?>';

					$(document).ready(function() {


						var id_bu_filter = $("#id_bu").val();
						armadalist(id_bu_filter, 0, '--KD Armada--');

						$("#id_bu").change(function() {
							$("#id_armada").hide();
							var id_bu = $("#id_bu").val();
							armadalist(id_bu, 0, '--KD Armada--');
						});

					});

					$('#id_bu_filter').select2({
						'allowClear': true
					}).on("change", function (e) {
						buTable.ajax.reload();
					});

					$('#tahun_filter').select2({
						'allowClear': true
					}).on("change", function (e) {
						buTable.ajax.reload();
					});

					$('#active_filter').select2({
						'allowClear': true
					}).on("change", function (e) {
						buTable.ajax.reload();
					});


					$( "#tgl_rekondisi, #tgl_rekondisi_history").datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: "yy-mm-dd"
					});

					$( "#tgl_rekondisi, #tgl_rekondisi_history" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

					var session_level = '<?=$session_level;?>';


					var buTable = $('#buTable').DataTable({
						"ordering" : false,
						"scrollX": true,
						"processing": true,
						"serverSide": true,

						dom: 'Bfrtip',
						lengthMenu: [
						[ 10, 25, 50, 100, 10000 ],
						[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
						],
						buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],
					ajax: 
					{
						url: "<?= base_url()?>rekondisi/ax_data_rekondisi/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_bu": $("#id_bu_filter").val(),
								"tahun": $("#tahun_filter").val(),
								"active": $("#active_filter").val(),
							});
						}
					},
					columns:
					[
					{
						data: "id_rekondisi", render: function(data, type, full, meta){
							var str = '';
							str += '<div class="btn-group">';
							str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							str += '<ul class="dropdown-menu">';

							// if(full['active']== 0 || full['active']== 1){
							// 	str += '<li><a onclick="ChangeStatus(' + data + ',' + "'3'" + ',' + "'Rekomendasi Divre'" +')"><i class="fa fa-sign-out"></i> Rekomendasi Divre</a></li>';

							// 	str += '<li><a onclick="ChangeStatusUpdate(' + data + ',' + "'1'" + ',' + "'Reject'" +')"><i class="fa fa-sign-out"></i> Reject</a></li>';
							// 	str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
							// 	str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							// }else if(full['active']== 3){
							// 	str += '<li><a onclick="ChangeStatus(' + data + ',' + "'4'" + ',' + "'Persetujuan'" +')"><i class="fa fa-sign-out"></i> Persetujuan</a></li>';

							// 	str += '<li><a onclick="ChangeStatusUpdate(' + data + ',' + "'1'" + ',' + "'Reject'" +')"><i class="fa fa-sign-out"></i> Reject</a></li>';
							// }else if(full['active']== 4){
							// 	str += '<li><a onclick="ChangeStatusUpdate(' + data + ',' + "'5'" + ',' + "'Selesai'" +')"><i class="fa fa-sign-out"></i> Selesai</a></li>';
							// }else{
							// 	str += '<li><a onclick="ChangeStatus(' + data + ',' + "'5'" + ',' + "'DONE'" +')">DONE</a></li>';
							// }

							if(full['active']== 0 || full['active']== 1){
								str += '<li><a onclick="ChangeStatus(' + data + ',' + full['active'] + ',' + "'Reject'" +')"><i class="fa fa-sign-out"></i> Edit</a></li>';
								str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							}else if(full['active']== 6){
								str += '<li><a onclick="ChangeStatus(' + data + ',' + full['active'] + ',' + "'DONE'" +')">DONE</a></li>';
							}else{
								str += '<li><a onclick="ChangeStatus(' + data + ',' + full['active'] + ',' + "'Reject'" +')"><i class="fa fa-sign-out"></i> Edit</a></li>';
							}

							str += '</ul>';
							str += '</div>';
							if(session_level != 13){
								return str;
							}else{
								return '';
							}
						}
					},

					{ data: "id_rekondisi" },
					{ data: "nm_bu" },
					{ data: "kd_armada" },
					{ data: "biaya" },
					{ data: "no_surat" },
					{ data: "tgl_rekondisi" },

					{ data: "active", render: function(data, type, full, meta){
						if(data == 0){
							return '<span class="label label-info">Pengajuan</span>';
						}else if(data == 1){
							return '<span class="label label-warning">Reject</span>';
						}else if(data == 2){
							return '<span class="label label-danger">Deleted</span>';
						}else if(data == 3){
							return '<span class="label label-default">Rekomendasi Divre</span>';
						}else if(data == 4){
							return '<span class="label label-primary">Persetujuan</span>';
						}else if(data == 5){
							return '<span class="label bg-purple">Pengerjaan</span>';
						}else{
							return '<span class="label label-success">Selesai</span>';
						} 
					}},
					{ data: "cdate" }

					]
				});

$('#btnSave').on('click', function () {
	if($('#id_bu').val() == '0')
	{
		alertify.alert("Warning", "Please Choose Cabang.");
	}else if($('#id_armada').val() == '0')
	{
		alertify.alert("Warning", "Please Choose Armada.");
	}else if($('#biaya').val() == '')
	{
		alertify.alert("Warning", "Biaya tidak boleh kosong.");
	}else if($('#no_surat').val() == '')
	{
		alertify.alert("Warning", "No.Surat boleh kosong.");
	}else if($('#tgl_rekondisi').val() == '')
	{
		alertify.alert("Warning", "Tgl Rekondisi tidak boleh kosong.");
	}
	else
	{
		var url = '<?=base_url()?>rekondisi/ax_set_data';
		var data = {
			id_rekondisi: $('#id_rekondisi').val(),
			tahun 		: $('#tahun').val(),
			id_bu 		: $('#id_bu').val(),
			id_armada 	: $('#id_armada').val(),
			biaya 		: $('#biaya').val(),
			no_surat 	: $('#no_surat').val(),
			tgl_rekondisi: $('#tgl_rekondisi').val(),
		};

		$.ajax({
			url: url,
			method: 'POST',
			data: data
		}).done(function(data, textStatus, jqXHR) {
			var data = JSON.parse(data);
			if(data['status'] == "success")
			{
				alertify.success("Data Saved.");
				$('#addModal').modal('hide');
				buTable.ajax.reload();
			}
		});
	}
});

function ViewData(id_rekondisi)
{
	if(id_rekondisi == 0)
	{
		$('#addModalLabel').html('Add Rekondisi');
		$('#id_rekondisi').val('');
		$('#id_bu').val('0').trigger('change');
		$('#id_armada').val('0').trigger('change');
		$('#biaya').val('');
		$('#no_surat').val('');
		$('#tgl_rekondisi').val('');
		$('#addModal').modal('show');
	}
	else
	{
		var url = '<?=base_url()?>rekondisi/ax_get_data_by_id';
		var data = {
			id_rekondisi: id_rekondisi
		};

		$.ajax({
			url: url,
			method: 'POST',
			data: data
		}).done(function(data, textStatus, jqXHR) {
			var data = JSON.parse(data);
			$('#addModalLabel').html('Edit Rekondisi');
			$('#id_rekondisi').val(data['id_rekondisi']);

			$('#tahun').val(data['tahun']).trigger('change');
			$('#select2-tahun-container').html(data['tahun']);

			$('#id_bu').val(data['id_bu']).trigger('change');
			$('#select2-id_bu-container').html(data['nm_bu']);
			armadalist(data['id_bu'], data['id_armada'], data['kd_armada']);

			$('#biaya').val(data['biaya']);
			$('#no_surat').val(data['no_surat']);
			$('#tgl_rekondisi').val(data['tgl_rekondisi']);
			$('#addModal').modal('show');
		});
	}
}

function DeleteData(id_rekondisi)
{
	alertify.confirm(
		'Confirmation', 
		'Are you sure you want to delete this data?', 
		function(){
			var url = '<?=base_url()?>rekondisi/ax_unset_data';
			var data = {
				id_rekondisi: id_rekondisi
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				buTable.ajax.reload();
				alertify.error('Data deleted.');
			});
		},
		function(){ }
		);
}

function armadalist(id_bu,id_armada,kd_armada){
	$.ajax({
		type: "POST", 
		url: "<?= base_url() ?>rekondisi/ax_get_armada", 
		data: {id_bu : id_bu}, 
		dataType: "json",
		beforeSend: function(e) {
			if(e && e.overrideMimeType) {
				e.overrideMimeType("application/json;charset=UTF-8");
			}
		},
		success: function(response){ 

			$("#id_armada").html(response.data_armada).show();
			$('#select2-id_armada-container').html(kd_armada);
			$('#id_armada').val(id_armada).trigger('change');
		},
		error: function (xhr, ajaxOptions, thrownError) { 
			alert(thrownError); 
		}
	});
}

function ChangeStatusUpdate(id_rekondisi,active, keterangan){
	alertify.confirm(
		'Confirmation', 
		'Apakah anda yakin ingin Ubah status ke '+keterangan+' ?', 
		function(){
			var url = '<?=base_url()?>rekondisi/ax_change_active';
			var data = {
				id_rekondisi: id_rekondisi,
				active : active,
			};
			$.ajax({
				url: url,
				method: 'POST',
				data: data,
				statusCode: {
					500: function() {
						alertify.alert("Warning","Data Tidak Berhasil di Setting");
					}
				}
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);

				if(data['status']==true)
				{
					alertify.success("Data Berhasil diubah status ke "+status);
					buTable.ajax.reload();
				}
							// alertify.success('CheckOut Setoran Berhasil.');
						});
		},
		function(){ }
		);

}

function printCetak() {
	var url 		= "<?=site_url("reports/print_fms")?>";

	var tahun 		= $('#tahun_filter').val();
	var id_bu 		= $('#id_bu_filter').val();
	var format 		= $('#jenis_print').val();

	var REQ = "?id_bu="+id_bu+"&tahun="+tahun+"&format="+format+"&uk=F4-P"+"&name=rekondisi";
	open(url+REQ);
}



</script>
<script type="text/javascript">
	function ChangeStatus(id_rekondisi,active, keterangan)
	{
		$('#addModalRekondisi').modal('show');
		$('#id_rekondisi_head').val(id_rekondisi);

		$('#active_history').val(active);
		statuslist(active);

		$('#no_surat_history').val('');
		$('#biaya_history').val('');
		$('#tgl_rekondisi_history').val('');

		if(keterangan=='DONE'){
			$('#btnSaveHistory').hide('');
		}else{
			$('#btnSaveHistory').show(''); 
		}

		historyRekondisiTable.ajax.reload();
		setTimeout(function(){ historyRekondisiTable.columns.adjust().draw(); }, 1000);
	}

	var historyRekondisiTable = $('#historyRekondisiTable').DataTable({
		"ordering" : false,
		"scrollX": true,
		"processing": true,
		"serverSide": true,

		dom: 'Bfrtip',
		lengthMenu: [
		[ 10, 25, 50, 100, 10000 ],
		[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
		],
		buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],
					ajax: 
					{
						url: "<?= base_url()?>rekondisi/ax_data_rekondisi_history/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_rekondisi": $("#id_rekondisi_head").val(),
							});
						}
					},
					columns:
					[

					{ data: "id_rekondisi_history" },
					{ data: "no_surat" },
					{ data: "biaya" },
					{ data: "tgl_rekondisi" },
					{ data: "active", render: function(data, type, full, meta){
						if(data == 0){
							return '<span class="label label-info">Pengajuan</span>';
						}else if(data == 1){
							return '<span class="label label-warning">Reject</span>';
						}else if(data == 2){
							return '<span class="label label-danger">Deleted</span>';
						}else if(data == 3){
							return '<span class="label label-default">Rekomendasi Divre</span>';
						}else if(data == 4){
							return '<span class="label label-primary">Persetujuan</span>';
						}else if(data == 5){
							return '<span class="label bg-purple">Pengerjaan</span>';
						}else{
							return '<span class="label label-success">Selesai</span>';
						} 
					}},
					{ data: "nm_user" },
					{ data: "cdate" }

					]
				});

	$('#btnSaveHistory').on('click', function () {
						// if($('#biaya_history').val() == '')
						// {
						// 	alertify.alert("Warning", "Biaya tidak boleh kosong.");
						// }else if($('#no_surat_history').val() == '')
						// {
						// 	alertify.alert("Warning", "No.Surat boleh kosong.");
						// }else if($('#tgl_rekondisi_history').val() == '')
						// {
						// 	alertify.alert("Warning", "Tgl Rekondisi tidak boleh kosong.");
						// }
						// else
						// {
							

							var url = '<?=base_url()?>rekondisi/ax_set_data_update';
							var data = {
								id_rekondisi: $('#id_rekondisi_head').val(),
								active 		: $('#status_history').val(),

								no_surat 	: $('#no_surat_history').val(),
								biaya 		: $('#biaya_history').val(),
								tgl_rekondisi: $('#tgl_rekondisi_history').val(),
							};

							$.ajax({
								url: url,
								method: 'POST',
								data: data
							}).done(function(data, textStatus, jqXHR) {
								var data = JSON.parse(data);
								if(data['status'] == "success")
								{
									alertify.success("Data Saved.");
									// $('#addModalRekondisi').modal('hide');
									statuslist(data['active']);

									$('#no_surat_history').val('');
									$('#biaya_history').val('');
									$('#tgl_rekondisi_history').val('');

									historyRekondisiTable.ajax.reload();
									buTable.ajax.reload();
								}
							});

						// }
						
					});

	function statuslist(active){
		$.ajax({
			type: "POST", 
			url: "<?= base_url() ?>rekondisi/ax_get_status", 
			data: {active : active}, 
			dataType: "json",
			beforeSend: function(e) {
				if(e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response){ 

				$("#status_history").html(response.data_status).show();
								// $('#select2-status_history-container').html(kd_armada);
								// $('#status_history').val(id_armada).trigger('change');
							},
							error: function (xhr, ajaxOptions, thrownError) { 
								alert(thrownError); 
							}
						});
	}

</script>
</body>
</html>
