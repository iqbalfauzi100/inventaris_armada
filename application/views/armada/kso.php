<!DOCTYPE html> 
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<?php
			if($this->session->flashdata('msg')==TRUE):
				echo '<div class="alert alert-success" role="alert">';
			echo $this->session->flashdata('msg');
			echo '</div>';
			endif;
			?>
			<section class="content-header">
				<h1>KSO</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<?php //if($this->session->userdata('login')['id_level'] == 1 || $this->session->userdata('login')['id_level'] == 6){ ?>
								<?php //}?>
								<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabel">Form Add Armada</h4>
											</div>
											<div class="modal-body">
												<input type="hidden" id="id_armada" name="id_armada" value='0' />
												<div class="row">

													<div class="form-group col-lg-4" id="div_no_kontrak">
														<label>No. Kontrak <small><font color="red">* Untuk Armada KSO</font></small></label>
														<input type="text" id="no_kontrak" name="no_kontrak" class="form-control" placeholder="Nomor Kontrak">
													</div>
													<div class="form-group col-lg-4" id="div_masa_berlaku_kontrak">
														<label>Masa Berlaku Kontrak <small><font color="red">* Untuk Armada KSO</font></small></label>
														<input type="text" id="masa_berlaku_kontrak" name="masa_berlaku_kontrak" class="form-control" placeholder="Masa Berlaku Kontrak">
													</div>

													<div class="form-group col-lg-4">
														<label>Active </label>
														<select class="form-control" id="active" name="active">
															<option value="0" <?php echo set_select('myselect', '0', TRUE); ?> >Active</option>
															<option value="1" <?php echo set_select('myselect', '1'); ?> >Not Active</option>
														</select>
													</div>
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-8">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-2">
										<label>Print</label>
										<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
											<option value="html" >1. HTML</option>
											<option value="pdf" >2. PDF</option>
											<option value="excell" >3. Excell</option>
										</select>
									</div>
									<div class="form-group col-md-2">
										<p style="height: 15px"></p>
										<button class="btn btn-info" title="Print" onclick='printCetak()'>
											<i class='fa fa-copy'></i> Print
										</button>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="armadaTable">
										<thead>
											<tr>
												<th>Options</th>
												<th>#</th>
												<th>Nomor Kontrak</th>
												<th>Masa Berlaku Kontrak</th>
												<th>Cabang</th>
												<th>Kode</th>
												<th>Rangka</th>
												<th>Mesin</th>
												<th>Plat</th>
												<th>Tahun</th>
												<th>Merek</th>
												<th>Type</th>
												<th>Warna</th>
												<th>Silinder</th>
												<th>Ukuran</th>
												<th>Seat</th>
												<th>Layout</th>
												<th>Segment</th>
												<th>Layanan</th>
												<th>Status</th>
												<th>Active</th>
												<th>CUser</th>
												<th>CDate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


			</section>


		</div>
	</div>


	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		$( "#masa_berlaku_kontrak").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#masa_berlaku_kontrak" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		var armadaTable = $('#armadaTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],

					ajax: 
					{
						url: "<?= base_url()?>armada_kso/ax_data_armada_kso/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, {
								"id_bu": $("#id_bu_filter").val()
							});
						}
					},
					columns: 
					[
					{
						data: "id_armada", render: function(data, type, full, meta){
							var str = '';
							str += '<div class="btn-group">';
							str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							str += '<ul class="dropdown-menu">';
							str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
							var kd = "Foto('"+data+"')";
							str += '</ul>';
							str += '</div>';

							if(session_level != 13){
								return str;
							}else{
								return '';
							}
						}
					},
					
					{ class:'intro', data: "id_armada" },
					{ class:'intro', data: "no_kontrak" },
					{ class:'intro', data: "masa_berlaku_kontrak" },
					{ class:'intro', data: "nm_bu" },
					{ class:'intro', data: "kd_armada" },
					{ class:'intro', data: "rangka_armada" },
					{ class:'intro', data: "mesin_armada" },
					{ class:'intro', data: "plat_armada" },
					{ class:'intro', data: "tahun_armada" },
					{ class:'intro', data: "nm_merek" },
					{ class:'intro', data: "tipe_armada" },
					{ class:'intro', data: "nm_warna" },
					{ class:'intro', data: "silinder_armada" },
					{ class:'intro', data: "nm_ukuran" },
					{ class:'intro', data: "seat_armada" },
					{ class:'intro', data: "nm_layout" },
					{ class:'intro', data: "nm_segment" },
					{ class:'intro', data: "nm_layanan" },
					{ class:'intro', data: "status_armada" },
					{ data: "active", render: function(data, type, full, meta){
							if(data == 0)
								return "Active";
							else return "Not Active";
						}
					},
					{ class:'intro', data: "cuser" },
					{ class:'intro', data: "cdate" },
					
					]
				});



		$('#btnSave').on('click', function () {
			if($('#kd_armada').val() == '')
			{
				alertify.alert("Warning", "ID Armada Belum di Isi.");
			}
			else
			{
				var url = '<?=base_url()?>armada_kso/ax_set_data_kso';
				var data = {
					id_armada 			: $('#id_armada').val(),
					no_kontrak 			: $('#no_kontrak').val(),
					active 				: $('#active').val(),
					masa_berlaku_kontrak: $('#masa_berlaku_kontrak').val(),
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Armada KSO Disimpan.");
						$('#addModal').modal('hide');
						armadaTable.ajax.reload();
					}
				});
			}
		});




		function ViewData(id_armada)
		{
			var url = '<?=base_url()?>armada/ax_get_data_by_id';
			var data = {
				id_armada: id_armada
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				$('#addModalLabel').html('Form Edit Armada');
				$('#id_armada').val(id_armada);
				$('#no_kontrak').val(data['no_kontrak']);
				$('#active').val(data['active']);
				$('#masa_berlaku_kontrak').val(data['masa_berlaku_kontrak']);
				$('#addModal').modal('show');
			});
		}

		$(document).ready(function() {

			$("#tahun_armada, #seat_armada").keydown(function (e) {

				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

					(e.keyCode == 65 && e.ctrlKey === true) ||

					(e.keyCode == 67 && e.ctrlKey === true) ||

					(e.keyCode == 88 && e.ctrlKey === true) ||

					(e.keyCode >= 35 && e.keyCode <= 39)) {

					return;
			}

			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});


		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			armadaTable.ajax.reload();
		});

		function printCetak() {
			var url 		= "<?=site_url("reports/print_fms")?>";

			var id_bu 		= $('#id_bu_filter').val();
			var format 		= $('#jenis_print').val();

			var REQ = "?id_bu="+id_bu+"&format="+format+"&uk=F4-L"+"&name=armada_kso";
			open(url+REQ);
		}

	</script>
</body>
</html>
