<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Usul Afkir</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
							</div>

							<div class="panel-body">
								<div class="row">
									<input type="hidden" id="id_usul_afkir" name="id_usul_afkir" value='' />
									<div class="form-group  col-md-6">
										<label>Tahun</label>
										<select class="form-control select2 " style="width: 100%;" id="tahun" name="tahun">
											<option value="0">--Tahun--</option>
											<!-- <option value="<?=date('Y');?>"  ><?=date('Y');?></option> -->
											<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
											<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group  col-md-6">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
											<option value="0">--Cabang--</option>
											<?php foreach ($combobox_bu->result() as $rowmenu) { ?>
											<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
											<?php } ?>
										</select>
									</div>
									<!-- <div class="form-group  col-md-4">
										<label>KD Armada</label>
										<select class="form-control select2" style="width: 100%;" id="id_armada">
											<option value="0">--KD Armada--</option>	
										</select>
									</div>
									<div class="form-group  col-md-2">
										<label>Tgl Pengajuan</label>
										<input type="text" id="tgl_pengajuan" name="tgl_pengajuan" class="form-control" placeholder="Tgl Pengajuan" autocomplete="off" value="<?=date('Y-m-d');?>">
									</div>

									<div class="form-group  col-md-2">
										<p style="height: 15px"></p>
										<button type="button" class="btn btn-primary" id='btnSave'><i class="fa fa-floppy-o"></i> Save</button>
									</div> -->

									

								</div>



								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="buTable">
										<thead>
											<tr>
												<th width="150px">Options</th>
												<th>#</th>
												<th>KD Armada</th>
												<th>Cabang</th>
												<th>Status Approved</th>
												<th>Cdate</th>
												<th>Tahun Approved</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>


	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="modalUsulAfkir" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h5 class="Form-add-bu" id="barangModalLabel">Tahun Approval</h5>
							<input type="hidden" id="id_usul_afkir_approval" name="id_usul_afkir_approval" class="form-control">
							<input type="hidden" id="id_armada_approval" name="id_armada_approval" class="form-control">
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Tahun</label>
								<select class="form-control select2 " style="width: 100%;" id="tahun_approval" name="tahun_approval">
									<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
									<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id='changeApproved'>Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		$(document).ready(function() {

			$("#id_bu").change(function() {
				$("#id_armada").hide();
				var id_bu = $("#id_bu").val();
				armadalist(id_bu, 0, '--KD Armada--');
			});

		});

		var session_level = '<?=$session_level;?>';

		var buTable = $('#buTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>usul_afkir/ax_data_usul_afkir_pusat/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"tahun": $("#tahun").val(),
						"id_bu": $("#id_bu").val()
					});
				}
			},
			columns: 
			[
			{
				data: "id_usul_afkir", render: function(data, type, full, meta){

					var id1 = "'"+data+"','"+full['id_bu']+"','"+full['id_armada']+"'";

					var str = '';
					str += '<div class="btn-group">';

					if(full['status'] == 4){
						str += '<a type="button" class="btn btn-sm btn-success"><i class="fa fa-check">Approved</i> </a>';
					}else{
						// str += '<a type="button" title="Edit Data" class="btn btn-sm btn-default" onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> </a>';	
						// str += '<a type="button" class="btn btn-sm btn-danger" title="Delete Data" onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> </a>';
						// str += '<a type="button" class="btn btn-sm btn-info" onClick="changeAjukanusul_afkir(' + data + ','+full['status']+')" title="Ajukan Usul Afkir"><i class="fa fa-sign-out"></i> </a>';

						str += '<a type="button" title="Reject" class="btn btn-sm btn-warning" onclick="changeReject(' + data + ')"><i class="fa fa-times"></i> </a>';
						str += '<a type="button" title="Approved" class="btn btn-sm bg-olive" onclick="changeApprovedModal(' + id1 + ')"><i class="fa fa-check"></i> </a>';
					}
					
					str += '</div>';
					if(session_level != 13){
						return str;
					}else{
						return '';
					}


				}
			},
			{ data: "id_usul_afkir" },
			{ data: "kd_armada" },
			{ data: "nm_bu" },
			{ data: "status", render: function(data, type, full, meta){
				if(data == 0){
					return '<span class="label label-default">Draft</span>' ;
				}else if(data == 1){
					return '<span class="label label-warning">Reject</span>';
				}else if(data == 2){
					return '<span class="label label-danger">Deleted</span>';
				}else if(data == 3){
					return '<span class="label label-info">Waiting Approval</span>';
				}else{
					return '<span class="label label-success">Approved</span>';
				}

			}},
			{ data: "cdate" },
			{ data: "approvedyear" },
			]
		});

		$('#btnSave').on('click', function () {
			if($('#id_bu').val() == '0')
			{
				alertify.alert("Warning", "Pilih Cabang.");
			}else if($('#id_armada').val() == '0')
			{
				alertify.alert("Warning", "Pilih Armada.");
			}
			else
			{
				var url = '<?=base_url()?>usul_afkir/ax_set_data';
				var data = {
					id_usul_afkir: $('#id_usul_afkir').val(),
					id_bu: $('#id_bu').val(),
					id_armada: $('#id_armada').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Saved.");
						$('#addModal').modal('hide');
						buTable.ajax.reload();


						$('#id_usul_afkir').val('');
						$('#id_bu').val('0').trigger('change');
						$('#id_armada').val('0').trigger('change');

					}
				});
			}
		});

		function ViewData(id_usul_afkir)
		{
			if(id_usul_afkir == 0)
			{
				$('#addModalLabel').html('Add usul_afkir');
				$('#id_usul_afkir').val('');
				$('#id_bu').val('0').trigger('change');
				$('#id_armada').val('0').trigger('change');
				$('#addModal').modal('show');
				
			}
			else
			{
				var url = '<?=base_url()?>usul_afkir/ax_get_data_by_id';
				var data = {
					id_usul_afkir: id_usul_afkir
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Edit usul_afkir');
					$('#id_usul_afkir').val(data['id_usul_afkir']);

					$('#id_bu').val(data['id_bu']).trigger('change');
					$('#select2-id_bu-container').html(data['nm_bu']);
					armadalist(data['id_bu'], data['id_armada'], data['kd_armada']);
					$('#addModal').modal('show');
				});
			}
		}

		function DeleteData(id_usul_afkir)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>usul_afkir/ax_unset_data';
					var data = {
						id_usul_afkir: id_usul_afkir
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						buTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}



		function armadalist(id_bu,id_armada,kd_armada){
			$.ajax({
				type: "POST", 
				url: "<?= base_url() ?>usul_afkir/ax_get_armada", 
				data: {id_bu : id_bu}, 
				dataType: "json",
				beforeSend: function(e) {
					if(e && e.overrideMimeType) {
						e.overrideMimeType("application/json;charset=UTF-8");
					}
				},
				success: function(response){ 

					$("#id_armada").html(response.data_armada).show();
					$('#select2-id_armada-container').html(kd_armada);
					$('#id_armada').val(id_armada).trigger('change');
					// $('#id_armada').val(id_armada);
				},
				error: function (xhr, ajaxOptions, thrownError) { 
					alert(thrownError); 
				}
			});
		}

		function changeAjukanusul_afkir(id_usul_afkir, status){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Ajukan Usul Afkir .... Approval dari Pusat?', 
				function(){
					var url = '<?=base_url()?>usul_afkir/ax_change_ajukan_usul_afkir';
					var data = {
						id_usul_afkir: id_usul_afkir,
						status : status
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Ajukan usul_afkir");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal diajukan usul_afkir");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}


		function changeReject(id_usul_afkir, status){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Reject Usul Afkir?', 
				function(){
					var url = '<?=base_url()?>usul_afkir/ax_change_reject';
					var data = {
						id_usul_afkir: id_usul_afkir,
						status : status
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Reject");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal diReject");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}

		function changeApprovedModal(id_usul_afkir, id_bu, id_armada){
			$('#id_usul_afkir_approval').val(id_usul_afkir);
			$('#id_armada_approval').val(id_armada);
			$('#modalUsulAfkir').modal('show');
		}


		$('#changeApproved').on('click', function () {
			if($('#tahun_approval').val() == '0'){
				alertify.alert("Warning", "Pilih Tahun.");
			}else {
				var url = '<?=base_url()?>usul_afkir/ax_change_approved';
				var data = {
					id_usul_afkir 	: $("#id_usul_afkir_approval").val(),
					approvedyear 	: $("#tahun_approval").val(),
					id_armada   	: $("#id_armada_approval").val()
				};
				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning","Data Duplicate");
						}}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Approved");
							$('#modalUsulAfkir').modal('hide');
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal Approved");
							buTable.ajax.reload();
						}
					});
				}
			});



		function changeApproved(){
			alertify.confirm(
				'Konfirmasi', 
				'Apakah anda yakin ingin Approved Usul Afkir?', 
				function(){
					var url = '<?=base_url()?>usul_afkir/ax_change_approved';
					var data = {
						id_usul_afkir: $("#id_usul_afkir_approval").val(),
						approvedyear : $("#tahun_approval").val(),
						id_armada: id_armada
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);

						if(data['status']==true)
						{
							alertify.success("Data Berhasil di Approved");
							buTable.ajax.reload();
						}else{
							alertify.alert("Warning","Data Gagal Approved");
							buTable.ajax.reload();
						}

							// alertify.success('CheckOut Setoran Berhasil.');
						});
				},
				function(){ }
				);

		}

		$('#tahun').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});

		$('#id_bu').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});
	</script>
</body>
</html>
