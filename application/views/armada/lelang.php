<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Lelang </h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

								<div class="modal fade" id="addModal"  tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabel">Form Add Lelang</h4>
											</div>
											<div class="modal-body">
												<input type="hidden" id="id_lelang" name="id_lelang" value='' />
												<div class="form-group">
													<label>Tahun Usulan Usul Afkir</label>
													<select class="form-control select2 " style="width: 100%;" id="tahun_ua" name="id_bu_filter">
														<?php
														foreach ($combobox_tahun->result() as $rowmenu) {
															?>
															<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
															<?php
														}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>Cabang</label>
													<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
														<option value="0">--Cabang--</option>
														<?php
														foreach ($combobox_bu_all->result() as $rowmenu) {
															?>
															<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
															<?php
														}
														?>
													</select>
												</div>

												<div class="form-group">
													<label>Tgl Lelang</label>
													<input type="text" id="tgl_lelang" name="tgl_lelang" class="form-control" placeholder="Tgl Lelang" autocomplete="off">
												</div>

												<div class="form-group">
													<label>Nm Pemenang</label>
													<input type="text" id="nm_pemenang" name="nm_pemenang" class="form-control" placeholder="Nm Pemenang">
												</div>

												<div class="form-group">
													<label>Nilai Likuidasi</label>
													<input type="number" class="form-control"  data-decimals="2" min="0" id="nilai_liduidasi" name="nilai_liduidasi" value='' placeholder="Nilai Likuidasi" />
												</div>

												<div class="form-group">
													<label>Nilai Lelang</label>
													<input type="number" class="form-control"  data-decimals="2" min="0" id="nilai_lelang" name="nilai_lelang" value='' placeholder="Nilai Lelang" />
												</div>

												<div class="form-group">
													<label>No Risalah</label>
													<input type="text" id="no_risalah" name="no_risalah" class="form-control" placeholder="No Risalah">
												</div>

												<div class="form-group">
													<label>Active</label>
													<select class="form-control" id="active" name="active">
														<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
														<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
													</select>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
											</div>
										</div>
									</div>
								</div>


								<div class="modal fade" id="addModalDetail"  tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabelDetail">Form Add Lelang Detail</h4>
											</div>
											<div class="modal-body">
												
												<input type="hidden" id="id_lelang_detail" name="id_lelang_detail" value='' />

												<div class="form-group">
													<label>Mata Uang</label>
													<select class="form-control select2" id="mata_uang" name="mata_uang" style="width: 100%;" >
														<option value="IDR">Rupiah - Indonesia (IDR)</option>
														<option value="MYR">Ringgit - Malaysia (MYR)</option>
														<option value="BND">Dollar - Brunei (BND)</option>
													</select>
												</div>

												<div class="form-group">
													<label>Harga</label>
													<input type="number" class="form-control"  data-decimals="2" min="0" id="harga_detail" name="harga_detail" value='' placeholder="Harga" />
												</div>

												<div class="form-group">
													<label>Layanan</label>
													<select class="form-control select2" id="layanan_detail" name="layanan_detail" style="width: 100%;" >
														<option value="1">Royal (RYL)</option>
														<option value="2">Eksekutif (EKS)</option>
														<option value="3">Bisnis (BNS)</option>
														<option value="4">Ekonomi (EKN)</option>
													</select>
												</div>
												<div class="form-group">
													<label>KM Trayek</label>
													<input type="number" class="form-control"  data-decimals="2" min="0" id="km_trayek_detail" name="km_trayek_detail" value='' placeholder="KM Trayek" />
												</div>

												<div class="form-group">
													<label>Active</label>
													<select class="form-control" id="active_detail" name="active_detail">
														<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
														<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
													</select>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSaveDetail21'>Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-body">
								<div class="nav-tabs-custom">

									<ul class="nav nav-tabs">
										<li class="active disabled"><a href="#tab_1" class="disabled" data-toggle="tab" aria-expanded="true">Data Lelang</a></li>
										<li class=" disabled"><a href="#tab_2" class="disabled" aria-expanded="false">Data Lelang Detail</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1">

											<div class="row">
												<div class="col-lg-12">
													<div class="form-group col-lg-2">
														<label>Tahun</label>
														<select class="form-control select2 " style="width: 100%;" id="tahun_filter" name="id_bu_filter">
															<?php
															foreach ($combobox_tahun->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
																<?php
															}
															?>
														</select>
													</div>
													<div class="form-group col-lg-4">
														<label>Cabang</label>
														<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
															<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
															<option value="0">--All Cabang--</option>
															<?php } ?>
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php
															}
															?>
														</select>
													</div>
													<div class="form-group col-md-2">
														<label>Print</label>
														<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
															<option value="html" >1. HTML</option>
															<option value="pdf" >2. PDF</option>
															<option value="excell" >3. Excell</option>
														</select>
													</div>
													<div class="form-group col-md-2">
														<p style="height: 15px"></p>
														<button class="btn btn-info" title="Print" onclick='printCetak()'>
															<i class='fa fa-copy'></i> Print
														</button>
													</div>
													<div class="form-group col-lg-2 pull-right">
														<p style="height: 15px">.</p>
														<?php if($session_level != 13){ ?>
														<button class="btn btn-primary" onclick='ViewData(0)'>
															<i class='fa fa-plus'></i> Add Lelang
														</button>
														<?php } ?>
													</div>
												</div>
											</div>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="buTable">
													<thead>
														<tr>
															<th>Options</th>
															<th>#</th>
															<th>Cabang</th>
															<th>Pemenang</th>
															<th width="75px">Jml Armada</th>
															<th width="100px">Nilai Likuidasi</th>
															<th width="100px">Nilai Lelang</th>
															<th>No Risalah</th>
															<th>Tahun Usulan UA</th>
															<th width="80px">Tgl Lelang</th>
															<th>Cdate</th>
														</tr>
													</thead>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>JUMLAH</th>
															<th width="75px" id="tfood_nilai_likuidasi" style="background-color: #81f781">1.000.000</th>
															<th width="100px" id="tfood_nilai_lelang" style="background-color: #81f781">1.000.000</th>
															<th width="100px"></th>
															<th></th>
															<th width="80px"></th>
															<th></th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>


										<div class="tab-pane" id="tab_2">
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<p id="detail_keterangan_lelang"></p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<input type="hidden" id="id_lelang_" name="id_lelang_" value='' />
													<input type="hidden" id="tahun_approval" name="tahun_approval" value='' />
													<input type="hidden" id="active_head" name="active_head" value='' />
													<div class="form-group col-lg-8">
														<label>KD Armada</label>
														<select class="form-control select2" style="width: 100%;" id="id_armada">
															<option value="0">--KD Armada--</option>	
														</select>
													</div>
													<div class="col-lg-2">
														<div class="form-group">
															<p style="height: 13px"></p>
															<button type="button" class="btn btn-success" id='btnSaveDetail'>
																<i class='fa fa-plus'></i> Add Lelang
															</button>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="form-group pull-right">
															<p style="height: 13px"></p>
															<button type="button" class="btn bg-purple btn-default" onClick='closeTab()'><i class="fa  fa-arrow-circle-left"></i> Kembali</button>
														</div>
													</div>
												</div>
											</div>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="detailTable">
													<thead>
														<tr>
															<th>Options</th>
															<th>#</th>
															<th>KD Armada</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>
	var session_level = '<?=$session_level;?>';

		var buTable = $('#buTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],
					ajax: 
					{
						url: "<?= base_url()?>lelang/ax_data_lelang/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_bu": $("#id_bu_filter").val(),
								"tahun": $("#tahun_filter").val(),
							});
						}
					},
					columns: 
					[
					{
						data: "id_lelang", render: function(data, type, full, meta){
							var id1 = "'"+data+"','"+full['nm_pemenang']+"','"+full['tahun_ua']+"','"+full['id_bu']+"','"+full['nm_bu']+"','"+full['active']+"'";
							var str = '';
							str += '<div class="btn-group">';
							str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							str += '<ul class="dropdown-menu">';
							str += '<li><a onclick="DetailData(' + id1 + ')"><i class="fa fa-list"></i> Lelang Detail</a></li>';

							if(full['active']!=2){
								str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
								str += '<li><a onclick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete Data</a></li>';
								str += '<li><a onclick="CheckOutData(' + data + ')"><i class="fa fa-sign-out"></i> Checkout Hapus Buku</a></li>';
							}

							str += '</ul>';
							str += '</div>';
							if(session_level != 13){
								return str;
							}else{
								return '';
							}
						}
					},

					{ data: "id_lelang" },
					{ data: "nm_bu" },
					{ data: "nm_pemenang" },
					{ data: "jumlah_armada", render: $.fn.dataTable.render.number( ',', '.',0 ) },
					{ data: "nilai_liduidasi", render: $.fn.dataTable.render.number( ',', '.',2 ) },
					{ data: "nilai_lelang", render: $.fn.dataTable.render.number( ',', '.',2 ) },
					{ data: "no_risalah" },
					{ data: "tahun_ua" },
					{ data: "tgl_lelang" },
					{ data: "cdate" }
					],

					"footerCallback": function ( row, data, start, end, display ) {
						var api = this.api(), data;

						var intVal = function ( i ) {
							return typeof i === 'string' ?
							i.replace(/[\$,]/g, '')*1 :
							typeof i === 'number' ?
							i : 0;
						};

						likuidasi = api
						.column( 5 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );

						lelang = api
						.column( 6 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );

						$( api.column( 5 ).footer() ).html( formatNumber(parseInt(likuidasi).toFixed(2)));
						$( api.column( 6 ).footer() ).html( formatNumber(parseInt(lelang).toFixed(2)));
					}

				});

		var detailTable = $('#detailTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],

					ajax: 
					{
						url: "<?= base_url()?>lelang/ax_data_lelang_detail/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_lelang": $("#id_lelang_").val()
							});
						}
					},
					columns: 
					[
					{
						data: "id_lelang_detail", render: function(data, type, full, meta){
							var str = '';
							var str = '<div class="btn-group">';
							str += '<a type="button" class="btn btn-sm btn-danger" onClick="DeleteDataDetail(' + data + ')"><i class="fa fa-trash"></i> </a>';
							str += '</div>';

							if($("#active_head").val()!=2){
								return str;
							}else{
								return "";
							}
						}
					},

					{ data: "id_lelang_detail" },
					{ data: "kd_armada" }
					]
				});


		$('#btnSave').on('click', function () {
			if($('#id_bu').val() == '0')
			{
				alertify.alert("Warning", "Please fill Cabang.");
			}else if($('#nilai_liduidasi').val() == '')
			{
				alertify.alert("Warning", "Please fill Nilai Likuidasi.");
			}
			else
			{
				var url = '<?=base_url()?>lelang/ax_set_data';
				var data = {
					id_lelang: $('#id_lelang').val(),
					id_bu: $('#id_bu').val(),
					nm_pemenang: $('#nm_pemenang').val(),
					nilai_liduidasi: $('#nilai_liduidasi').val(),
					nilai_lelang: $('#nilai_lelang').val(),
					no_risalah: $('#no_risalah').val(),
					tahun_ua: $('#tahun_ua').val(),
					tgl_lelang: $('#tgl_lelang').val(),
					active: $('#active').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning","Data tidak boleh sama/sudah ada di database sistem.");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data saved.");
						$('#addModal').modal('hide');
						buTable.ajax.reload();
					}
				});

			}
		});


		$('#btnSaveDetail').on('click', function () {
			if($('#id_armada').val() == '0')
			{
				alertify.alert("Warning", "Please fill KD Armada.");
			}
			else
			{
				var url = '<?=base_url()?>lelang/ax_set_data_detail';
				var data = {
					id_lelang 		: $('#id_lelang_').val(),
					id_usul_afkir  	: $('#id_armada').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning","Data tidak boleh sama/sudah ada di database sistem.");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						detailTable.ajax.reload();
						alertify.success('Data saved.');
					}
				});

			}
		});

		$('#tahun_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			buTable.ajax.reload();
		});

		$( "#tgl_lelang").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#tgl_lelang" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		$("#nilai_liduidasi").inputSpinner();
		$("#nilai_lelang").inputSpinner();

		function ViewData(id_lelang)
		{
			if(id_lelang == 0)
			{
				$('#addModalLabel').html('Add Lelang');
				$('#id_lelang').val('');
				$('#select2-id_bu-container').html('--Cabang--');
				$('#id_bu').val('0');
				$('#nm_pemenang').val('');
				$('#nilai_liduidasi').val('');
				$('#nilai_lelang').val('');
				$('#no_risalah').val('');
				$('#tgl_lelang').val('');
				$('#active').val('1');
				$('#addModal').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>lelang/ax_get_data_by_id';
				var data = {
					id_lelang: id_lelang
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Edit Lelang');
					$('#id_lelang').val(data['id_lelang']);
					$('#id_bu').val(data['id_bu']);
					$('#select2-id_bu-container').html(data['nm_bu']);

					$('#nm_pemenang').val(data['nm_pemenang']);
					$('#nilai_liduidasi').val(data['nilai_liduidasi']);
					$('#nilai_lelang').val(data['nilai_lelang']);
					$('#no_risalah').val(data['no_risalah']);

					$('#tahun_ua').val(data['tahun_ua']).trigger('change');

					$('#tgl_lelang').val(data['tgl_lelang']);
					$('#active').val(data['active']);
					$('#addModal').modal('show');
				});
			}
		}

		function ViewDataDetail(id_trayek_in)
		{
			if(id_trayek_in == 0)
			{
				$('#addModalLabelDetail').html('Add Trayek Detail');
				$('#id_trayek_in').val('');
				$('#select2-id_point_awal_detail-container').html('--Asal--');
				$('#select2-id_point_akhir_detail-container').html('--Tujuan--');
				$('#select2-layanan_detail-container').html('--Layanan--');
				$('#id_point_awal_detail').val('0').trigger('change');
				$('#id_point_akhir_detail').val('0').trigger('change');
				$('#layanan_detail').val('1').trigger('change');
				$('#mata_uang').val('IDR').trigger('change');

				$('#harga_detail').val('');
				$('#km_trayek_detail').val('');
				$('#active_detail').val('1');
				$('#addModalDetail').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>trayek/ax_get_data_by_id_detail';
				var data = {
					id_trayek_in: id_trayek_in
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);

					$('#addModalLabelDetail').html('Edit Trayek');
					$('#id_trayek_in').val(data['id_trayek_detail']);
					$('#select2-id_point_awal_detail-container').html(data['id_point_awal']);
					$('#select2-id_point_akhir_detail-container').html(data['id_point_akhir']);
					$('#select2-layanan_detail-container').html(data['layanan']);

					$('#id_point_awal_detail').val(data['id_point_awal']).trigger('change');
					$('#id_point_akhir_detail').val(data['id_point_akhir']).trigger('change');
					$('#layanan_detail').val(data['layanan']).trigger('change');
					$('#mata_uang').val(data['mata_uang']).trigger('change');

					$('#harga_detail').val(data['harga']);
					$('#km_trayek_detail').val(data['km_trayek']);
					$('#active').val(data['active']);
					$('#addModalDetail').modal('show');
				});
			}
		}

		function DeleteData(id_lelang)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>lelang/ax_unset_data';
					var data = {
						id_lelang: id_lelang
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						buTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function DeleteDataDetail(id_lelang_detail)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>lelang/ax_unset_data_detail';
					var data = {
						id_lelang_detail: id_lelang_detail
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						detailTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function armadalist(id_bu, tahun_approval){
			$.ajax({
				type: "POST", 
				url: "<?= base_url() ?>lelang/ax_get_armada", 
				data: {id_bu : id_bu, approvedyear : tahun_approval}, 
				dataType: "json",
				beforeSend: function(e) {
					if(e && e.overrideMimeType) {
						e.overrideMimeType("application/json;charset=UTF-8");
					}
				},
				success: function(response){ 

					$("#id_armada").html(response.data_armada).show();
					$('#select2-id_armada-container').html('-KD Armada--');
				},
				error: function (xhr, ajaxOptions, thrownError) { 
					alert(thrownError); 
				}
			});
		}

		function CheckOutData(id_lelang){

			alertify.confirm(
				'Konfirmasi', 
				'CheckOut ke Hapus Bukuan?', 
				function(){
					var url = '<?=base_url()?>lelang/checkouthapusbuku';
					var data = {
						id_lelang	: id_lelang,
					};
					$.ajax({
						url: url,
						method: 'POST',
						data: data,
						statusCode: {
							500: function() {
								alertify.alert("Warning","Data Tidak Berhasil di Setting");
							}
						}
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						if(data['status'] == "success")
						{
							buTable.ajax.reload();
							alertify.success('CheckOut Hapus Buku Berhasil.');
						}else{
							alertify.error('Detail lelang tidak boleh kosong.');
						}
					});
				},
				function(){ }
				);
		}

		function closeTab(){
			$('.nav-tabs a[href="#tab_1"]').tab('show');
			buTable.columns.adjust().draw();
		}

		function DetailData(id_lelang,nm_pemenang,tahun_ua,id_bu,nm_bu, active){
			$('#detail_keterangan_lelang').html('<code>'+nm_pemenang+' - ('+nm_bu+')</code>');
			$('#id_lelang_').val(id_lelang);
			$('#tahun_approval').val(tahun_ua);
			$('#active_head').val(active);
			armadalist(id_bu, tahun_ua);

			$('.nav-tabs a[href="#tab_2"]').tab('show');
			detailTable.columns.adjust().draw();
		}

		function printCetak() {
			var url 		= "<?=site_url("reports/print_fms")?>";

			var tahun 		= $('#tahun_filter').val();
			var id_bu 		= $('#id_bu_filter').val();
			var format 		= $('#jenis_print').val();

			var REQ = "?id_bu="+id_bu+"&tahun="+tahun+"&format="+format+"&uk=F4-P"+"&name=lelang";
			open(url+REQ);
		}

		function formatNumber(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
		}


	</script>
</body>
</html>
