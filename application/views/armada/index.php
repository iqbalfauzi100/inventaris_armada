<!DOCTYPE html> 
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<?php
			if($this->session->flashdata('msg')==TRUE):
				echo '<div class="alert alert-success" role="alert">';
			echo $this->session->flashdata('msg');
			echo '</div>';
			endif;
			?>
			<section class="content-header">
				<h1>Armada</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

								<?php if($session_level == 1 || $session_level == 7){ ?>
								<button class="btn btn-primary" onclick='ViewData(0)'>
									<i class='fa fa-plus'></i> Add Armada
								</button>
								<?php } ?>

								<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabel">Form Add Armada</h4>
											</div>
											<div class="modal-body">
												<!-- <input type="hidden" id="id_armada" name="id_armada" value='0' /> -->
												<div class="row">

													<div class="form-group col-lg-6">
														<label>Cabang</label>
														<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
															<option value="0">--Cabang--</option>
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Kode Armada</label>
														<input type="text" id="kd_armada" name="kd_armada" class="form-control" placeholder="Kode Armda">
													</div>

													<div class="form-group col-lg-6">
														<label>No. Rangka</label>
														<input type="text" id="rangka_armada" name="rangka_armada" class="form-control" placeholder="Rangka">
													</div>

													<div class="form-group col-lg-6">
														<label>No. Mesin</label>
														<input type="text" id="mesin_armada" name="mesin_armada" class="form-control" placeholder="Mesin">
													</div>

													<div class="form-group col-lg-6">
														<label>Plat</label>
														<input type="text" id="plat_armada" name="plat_armada" class="form-control" placeholder="Plat">
													</div>

													<div class="form-group col-lg-6">
														<label>Type</label>
														<input type="text" id="tipe_armada" name="tipe_armada" class="form-control" placeholder="Type">
													</div>

													<div class="form-group col-lg-6">
														<label>Tahun Pembuatan</label>
														<input type="text" id="tahun_armada" name="tahun_armada" class="form-control" placeholder="Tahun">
													</div>
													<div class="form-group col-lg-6">
														<label>Tahun Perolehan</label>
														<input type="number" id="tahun_perolehan" name="tahun_perolehan" class="form-control" placeholder="Tahun Perolehan">
													</div>

													

													<div class="form-group col-lg-6">
														<label>Silinder</label>
														<input type="text" id="silinder_armada" name="silinder_armada" class="form-control" placeholder="Silinder">
													</div>

													<div class="form-group col-lg-6">
														<label>Seat</label>
														<input type="text" id="seat_armada" name="seat_armada" class="form-control" placeholder="Seat">
													</div>

													<div class="form-group col-lg-6">
														<label>Layout</label>
														<select class="form-control select2 " style="width: 100%;" id="id_layout" name="id_layout">
															<option value="0">--Layout--</option>
															<?php
															foreach ($combobox_layout->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_layout?>"  ><?= $rowmenu->nm_layout?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Segment</label>
														<select class="form-control select2 " style="width: 100%;" id="id_segment" name="id_segment">
															<option value="0">--Segment--</option>
															<?php
															foreach ($combobox_segment->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_segment?>"  ><?= $rowmenu->nm_segment?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Merek</label>
														<select class="form-control select2 " style="width: 100%;" id="id_merek" name="id_merek">
															<option value="0">--Merek--</option>
															<?php
															foreach ($combobox_merek->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_merek?>"  ><?= $rowmenu->nm_merek?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Layanan</label>
														<select class="form-control select2 " style="width: 100%;" id="id_layanan" name="id_layanan">
															<option value="0">--Layanan--</option>
															<?php
															foreach ($combobox_layanan->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_layanan?>"  ><?= $rowmenu->nm_layanan?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Ukuran</label>
														<select class="form-control select2 " style="width: 100%;" id="id_ukuran" name="id_ukuran">
															<option value="0">--Ukuran--</option>
															<?php
															foreach ($combobox_ukuran->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_ukuran?>"  ><?= $rowmenu->nm_ukuran?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Warna</label>
														<select class="form-control select2 " style="width: 100%;" id="id_warna" name="id_warna">
															<option value="0">--Warna--</option>
															<?php
															foreach ($combobox_warna->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_warna?>"  ><?= $rowmenu->nm_warna?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group col-lg-6">
														<label>Status</label>
														<select class="form-control " style="width: 100%;" id="status_armada" name="status_armada" onchange="status_armada_kso()">
															<option value="DAMRI">DAMRI</option>
															<option value="KSO">KSO</option>
															<option value="SEWA">SEWA</option>
														</select>
													</div>
													<div class="form-group col-lg-6"></div>
													<div class="form-group col-lg-6" id="div_no_kontrak">
														<label>No. Kontrak <small><font color="red">* Untuk Armada KSO</font></small></label>
														<input type="text" id="no_kontrak" name="no_kontrak" class="form-control" placeholder="Nomor Kontrak">
													</div>
													<div class="form-group col-lg-6"></div>
													<div class="form-group col-lg-6" id="div_masa_berlaku_kontrak">
														<label>Masa Berlaku Kontrak <small><font color="red">* Untuk Armada KSO</font></small></label>
														<input type="text" id="masa_berlaku_kontrak" name="masa_berlaku_kontrak" class="form-control" placeholder="Masa Berlaku Kontrak">
													</div>

													
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-8">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-2">
										<label>Print</label>
										<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
											<option value="html" >1. HTML</option>
											<option value="pdf" >2. PDF</option>
											<option value="excell" >3. Excell</option>
										</select>
									</div>
									<div class="form-group col-md-2">
										<p style="height: 15px"></p>
										<button class="btn btn-info" title="Print" onclick='printCetak()'>
											<i class='fa fa-copy'></i> Print
										</button>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="armadaTable">
										<thead>
											<tr>
												<th>Options</th>
												<th>#</th>
												<th>Cabang</th>
												<th>Kode</th>
												<th>Rangka</th>
												<th>Mesin</th>
												<th>Plat</th>
												<th>Tahun Pembuatan</th>
												<th>Tahun Perolehan</th>
												<th>Merek</th>
												<th>Type</th>
												<th>Warna</th>
												<th>Silinder</th>
												<th>Ukuran</th>
												<th>Seat</th>
												<th>Layout</th>
												<th>Segment</th>
												<th>Layanan</th>

												<th>No.Polis</th>
												<th>No.Certificate</th>
												<th>Status</th>
												<th>CUser</th>
												<th>CDate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- FOTO ARMADA -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalfotoarmada" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">Form List Foto Armada</h4>


									</div>
									<div class="modal-body">
										<div class="row">
											<form id="submit">
												<input type="hidden" id="id_armada_foto" name="id_armada_foto" class="form-control">
												<input type="hidden" id="id_armada" name="id_armada" class="form-control">
												<div class="form-group col-lg-3">

													<label>Sisi</label>
													<select class="form-control" id="nm_armada_foto" name="nm_armada_foto">
														<option value="DEPAN KANAN" >DEPAN KANAN</option>
														<option value="DEPAN KIRI" >DEPAN KIRI</option>
														<option value="BELAKANG" >BELAKANG</option>
														<option value="DALAM" >DALAM</option>
														<option value="GESEK RANGKA">GESEK RANGKA</option>
														<option value="GESEK MESIN">GESEK MESIN</option>
														<option value="FOTO MESIN">FOTO MESIN</option> 
													</select>
												</div>
												<div class="form-group col-lg-6">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment" accept=".jpg, .JPG, .jpeg" class="form-control">
													<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
												</div>
												<div class="form-group col-lg-3">
													<label>_</label>
													<button type="submit" class="form-control btn btn-success" id=''>Save</button>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="attachmentTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Sisi</th>
														<th>Foto Armada</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- FOTO STNK -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalfotostnk" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">Form List Foto STNK</h4>


									</div>
									<div class="modal-body">
										<div class="row">
											<form id="submit_stnk">
												<input type="hidden" id="id_armada_stnk" name="id_armada_stnk" class="form-control">
												<input type="hidden" id="id_armadastnk" name="id_armadastnk" class="form-control">
												<div class="form-group col-lg-3">

													<label>Tanggal EXP</label>
													<input type="text" id="tgl_exp_stnk" name="tgl_exp_stnk" class="form-control" placeholder="yyyy-mm-dd">
												</div>
												<div class="form-group col-lg-2">
													<label>Masa</label>
													<select class="form-control " style="width: 100%;" id="masa" name="masa">
														<option value="1">1</option>
														<option value="5">5</option>
													</select>
												</div>
												<div class="form-group col-lg-4">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment_stnk" accept=".jpg, .JPG, .jpeg" class="form-control">
													<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
												</div>
												<div class="form-group col-lg-3">
													<label>_</label>
													<button type="submit" class="form-control btn btn-success" id=''>Save</button>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="stnkTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Tanggal Kadaluarsa</th>
														<th>Masa</th>
														<th>Foto STNK</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- FOTO KEUR -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalfotokeur" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">Form List Foto KEUR</h4>


									</div>
									<div class="modal-body">
										<div class="row">
											<form id="submit_keur">
												<input type="hidden" id="id_armada_keur" name="id_armada_keur" class="form-control">
												<input type="hidden" id="id_armadakeur" name="id_armadakeur" class="form-control">
												<div class="form-group col-lg-3">
													<label>Tanggal EXP</label>
													<input type="text" id="tgl_exp_keur" name="tgl_exp_keur" class="form-control" placeholder="yyyy-mm-dd">
												</div>
												<!-- <div class="form-group col-lg-4">
													<label>Masa Berlaku (Tahun)</label>
													<select class="form-control " style="width: 100%;" id="masa_keur" name="masa_keur">
														<option value="1">1</option>
														<option value="5">5</option>
													</select>
												</div> -->
												<div class="form-group col-lg-4">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment_keur" accept=".jpg, .JPG, .jpeg, .pdf, .png" class="form-control">
													<p class="help-block">Upload Foto Keur. Format File: Jpg/PDF  Size Max: 500KB   </p>
												</div>
												<div class="form-group col-lg-3">
													<label>_</label>
													<button type="submit" class="form-control btn btn-success" id=''>Save</button>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="keurTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Tgl Exp</th>
														<th>Foto Keur</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>





				<!-- HISTORY MUTASI -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalmutasiarmada" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">List History Mutasi Armada</h4>
									</div>
									<div class="modal-body">
										<div class="dataTable_wrapper">
											<input type="hidden" id="id_armada_mutasi" name="id_armada_mutasi" class="form-control">
											<table class="table table-striped table-bordered table-hover" id="mutasiarmadaTable">
												<thead>
													<tr>
														<th>No</th>
														<th>Cabang Asal</th>
														<th>KD Armada</th>
														<th>Cabang Penerima</th>
														<th>No. SK Mutasi</th>
														<th>Tgl Mutasi</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- IJIN TRAYEK -->
				<div class="row" >
					<div class="col-lg-12">
						<div class="modal fade" id="addModalijintrayek" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="form-add" id="addModalLabel">Form List Ijin Trayek</h4>


									</div>
									<div class="modal-body">
										<div class="row">
											<form id="submit_ijintrayek">
												<input type="hidden" id="id_armada_ijintrayek" name="id_armada_ijintrayek" class="form-control">
												<input type="hidden" id="id_armadaijintrayek" name="id_armadaijintrayek" class="form-control">

												<div class="form-group col-lg-5">
													<label>Trayek</label>
													<select class="form-control select2" style="width: 100%;" id="id_trayek" name="id_trayek">
													</select>
												</div>

												<div class="form-group col-lg-2">
													<label>Berlaku Sampai Tgl</label>
													<input type="text" id="tgl_exp_ijintrayek" name="tgl_exp_ijintrayek" class="form-control" placeholder="yyyy-mm-dd">
												</div>
												<div class="form-group col-lg-3">
													<label>Nama Attachment</label>
													<input type="file" name="file" id="file_attachment_ijintrayek" accept=".jpg, .JPG, .jpeg, .pdf, .png" class="form-control">
													<p class="help-block">Upload Foto Ijin Trayek. Format File: Jpg.pdf  Size Max: 500KB </p>
												</div>
												<div class="form-group col-lg-2">
													<label>_</label>
													<button type="submit" class="form-control btn btn-success" id=''>Save</button>
												</div>
											</form>

										</div>

										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="ijintrayekTable">
												<thead>
													<tr>
														<th>Aksi</th>
														<th>#</th>
														<th>Nm Trayek</th>
														<th>Tanggal Berlaku</th>
														<th>Foto</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>







			</section>


		</div>
	</div>


	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		function status_armada_kso() {
			var status_armada = $('#status_armada').val();

			if(status_armada=="DAMRI"){
				$('#div_no_kontrak').hide();
				$('#div_masa_berlaku_kontrak').hide();

				$('#no_kontrak').val("");
				$('#masa_berlaku_kontrak').val("");
			}else{
				$('#div_no_kontrak').show();
				$('#div_masa_berlaku_kontrak').show();
			}
		}
		$( "#masa_berlaku_kontrak").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#masa_berlaku_kontrak" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		var armadaTable = $('#armadaTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],

					ajax: 
					{
						url: "<?= base_url()?>armada/ax_data_armada/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 

								"id_bu": $("#id_bu_filter").val()

							});
						}
					},
					columns: 
					[
					{
						data: "id_armada", render: function(data, type, full, meta){

							

							var id1 = "'"+data+"','"+full['kd_armada']+"','"+full['id_bu']+"'";
							var str = '';
							str += '<div class="btn-group">';
							str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							str += '<ul class="dropdown-menu">';
							if(session_level != 13){
								str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
							}
							var kd = "Foto('"+data+"')";
							str += '<li><a onclick="'+ kd +'"><i class="fa fa-truck"></i> Foto Armada</a></li>';
							str += '<li><a onclick="Stnk(' + data + ')"><i class="fa fa-truck"></i> Foto STNK</a></li>';
							str += '<li><a onclick="Keur(' + data + ')"><i class="fa fa-truck"></i> Foto Keur</a></li>';
							str += '<li><a onclick="Ijin(' + id1 + ')"><i class="fa fa-truck"></i> Foto Ijin Trayek</a></li>';

							if(session_level==1){
								str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							}

							str += '<li><a onclick="historymutasi(' + data + ')"><i class="fa fa-list"></i> History Mutasi</a></li>';

							if(session_level==1 || session_level==7 || session_level==3){
								str += '<li><a onClick="PrintArmada(' + id1 + ')"><i class="fa fa-print"></i> Print Armada</a></li>';
							}
							str += '</ul>';
							str += '</div>';

							// if(session_level != 13){
								return str;
							// }else{
							// 	return '';
							// }

						}
					},
					
					{ class:'intro', data: "id_armada" },
					{ class:'intro', data: "nm_bu" },
					{ class:'intro', data: "kd_armada" },
					{ class:'intro', data: "rangka_armada" },
					{ class:'intro', data: "mesin_armada" },
					{ class:'intro', data: "plat_armada" },
					{ class:'intro', data: "tahun_armada" },
					{ class:'intro', data: "tahun_perolehan" },
					{ class:'intro', data: "nm_merek" },
					{ class:'intro', data: "tipe_armada" },
					{ class:'intro', data: "nm_warna" },
					{ class:'intro', data: "silinder_armada" },
					{ class:'intro', data: "nm_ukuran" },
					{ class:'intro', data: "seat_armada" },
					{ class:'intro', data: "nm_layout" },
					{ class:'intro', data: "nm_segment" },
					{ class:'intro', data: "nm_layanan" },
					{ class:'intro', data: "no_polis" },
					{ class:'intro', data: "no_certificate" },
					{ class:'intro', data: "status_armada" },
					{ class:'intro', data: "cuser" },
					{ class:'intro', data: "cdate" },
					
					]
				});



		$('#btnSave').on('click', function () {
			if($('#kd_armada').val() == '')
			{
				alertify.alert("Warning", "ID Armada Belum di Isi.");
			}
			else
			{
				var url = '<?=base_url()?>armada/ax_set_data';
				var data = {
					id_armada: $('#id_armada').val(),
					kd_armada: $('#kd_armada').val(),
					rangka_armada: $('#rangka_armada').val(),
					plat_armada: $('#plat_armada').val(),
					mesin_armada: $('#mesin_armada').val(),
					tahun_armada: $('#tahun_armada').val(),
					tahun_perolehan: $('#tahun_perolehan').val(),
					tipe_armada: $('#tipe_armada').val(),
					id_merek: $('#id_merek').val(),
					tipe_armada: $('#tipe_armada').val(),
					silinder_armada: $('#silinder_armada').val(),
					id_ukuran: $('#id_ukuran').val(),
					id_segment: $('#id_segment').val(),
					id_layanan: $('#id_layanan').val(),
					seat_armada: $('#seat_armada').val(),
					id_layout: $('#id_layout').val(),
					id_warna: $('#id_warna').val(),
					status_armada: $('#status_armada').val(),
					id_bu: $('#id_bu').val(),
					active: $('#active').val(),
					no_kontrak 			: $('#no_kontrak').val(),
					masa_berlaku_kontrak: $('#masa_berlaku_kontrak').val(),
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Armada Disimpan.");
						$('#addModal').modal('hide');
						armadaTable.ajax.reload();
					}
				});
			}
		});




		function ViewData(id_armada)
		{
			if(id_armada == 0)
			{
				

				$('#addModalLabel').html('Form Add Armada');
				$('#select2-id_bu-container').html('--Cabang--');
				$('#select2-id_segment-container').html('--Segment--');
				$('#select2-id_layanan-container').html('--Layanan--');
				$('#select2-id_ukuran-container').html('--Ukuran--');
				$('#select2-id_warna-container').html('--Warna--');
				$('#select2-id_merek-container').html('--Merek--');
				$('#select2-id_layout-container').html('--Layout--');
				$('#id_bu').val(0);
				$('#id_segment').val(0);
				$('#id_layanan').val(0);
				$('#id_ukuran').val(0);
				$('#id_warna').val(0);
				$('#id_merek').val(0);
				$('#id_layout').val(0);
				$('#id_armada').val(0);
				$('#kd_armada').val('');
				$('#rangka_armada').val('');
				$('#plat_armada').val('');
				$('#mesin_armada').val('');
				$('#tahun_armada').val('');
				$('#tahun_perolehan').val('');
				$('#merek_armada').val('');
				$('#tipe_armada').val('');
				$('#silinder_armada').val('');
				$('#ukuran_armada').val('');
				$('#seat_armada').val('');
				$('#segment_armada').val('');

				$('#status_armada').val('DAMRI');
				status_armada_kso();

				$('#no_kontrak').val('');
				$('#masa_berlaku_kontrak').val('');
				$('#active').val(0);


				var id_level = ["1", "7"];
				if( id_level.includes(session_level) ){
					document.getElementById("kd_armada").readOnly = false;
				}else{
					document.getElementById("kd_armada").readOnly = true;
				}

				$('#addModal').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>armada/ax_get_data_by_id';
				var data = {
					id_armada: id_armada
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Form Edit Armada');
					$('#select2-id_bu-container').html(data['nm_bu']);
					$('#select2-id_segment-container').html(data['nm_segment']);
					$('#select2-id_layanan-container').html(data['nm_layanan']);
					$('#select2-id_ukuran-container').html(data['nm_ukuran']);
					$('#select2-id_warna-container').html(data['nm_warna']);
					$('#select2-id_merek-container').html(data['nm_merek']);
					$('#select2-id_layout-container').html(data['nm_layout']);
					$('#id_bu').val(data['id_bu']);
					$('#id_segment').val(data['id_segment']);
					$('#id_ukuran').val(data['id_ukuran']);
					$('#id_layanan').val(data['id_layanan']);
					$('#id_warna').val(data['id_warna']);
					$('#id_merek').val(data['id_merek']);
					$('#id_layout').val(data['id_layout']);
					$('#id_armada').val(data['id_armada']);
					$('#kd_armada').val(data['kd_armada']);
					$('#rangka_armada').val(data['rangka_armada']);
					$('#plat_armada').val(data['plat_armada']);
					$('#mesin_armada').val(data['mesin_armada']);
					$('#tahun_armada').val(data['tahun_armada']);
					$('#tahun_perolehan').val(data['tahun_perolehan']);
					$('#merek_armada').val(data['merek_armada']);
					$('#tipe_armada').val(data['tipe_armada']);
					$('#silinder_armada').val(data['silinder_armada']);
					$('#ukuran_armada').val(data['ukuran_armada']);
					$('#seat_armada').val(data['seat_armada']);
					$('#segment_armada').val(data['segment_armada']);
					
					$('#status_armada').val(data['status_armada']);
					status_armada_kso();

					$('#no_kontrak').val(data['no_kontrak']);
					$('#masa_berlaku_kontrak').val(data['masa_berlaku_kontrak']);
					$('#active').val(data['active']);


					var id_level = ["1", "7"];
					if( id_level.includes(session_level) ){
						document.getElementById("kd_armada").readOnly = false;
					}else{
						document.getElementById("kd_armada").readOnly = true;
					}
					
					$('#addModal').modal('show');
				});
			}
		}



		function DeleteData(id_armada)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>armada/ax_unset_data';
					var data = {
						id_armada: id_armada
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						armadaTable.ajax.reload();
						alertify.error('Data Armada Dihapus.');
					});
				},
				function(){ }
				);
		}


		var attachmentTable = $('#attachmentTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_foto/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 

						"id_armada": $("#id_armada").val(),

					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_foto", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
							// str += '<li><a onclick="'+kd+'"><i class="fa fa-pencil"></i> Edit</a></li>';
							str += '<li><a onClick="DeleteFoto(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							str += '</ul>';
							str += '</div>';
							if(session_level != 13){
								return str;
							 }else{
								return '';
							}
						}
					},
					
					{ class:'intro', data: "id_armada_foto" },
					{ data: "nm_armada_foto" },
					// { data: "attachment" },
					{ data: "attachment", render: function(data, type, full, meta)
					{
						var url = "<?= base_url()?>"+"uploads/armada/foto/"+data;
						var op =  "window.open('"+url+"', '_blank');";
						return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';

					}

				},




				],
			});



			/// attachment
			$('#submit').submit(function(e){
				e.preventDefault(); 

				if($('#nm_attachment').val() == '')
				{
					alertify.alert("Warning", "Please fill Name.");
				}
				else if($('#file_attachment').val() == ''){
					alertify.alert("Warning", "Please choose Attachment.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_foto',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
                   //    success: function(data){
                   //        alert("Attachment Uploaded.");
                   // }
                 // });
             }).done(function(data, textStatus, jqXHR) {
             	var data = JSON.parse(data);
             	attachmentTable.ajax.reload();
							// $('#addModalattach').modal('hide');
							alertify.success('Attachment Uploaded.');
						});
         }
     });

			function DeleteFoto(id_armada_foto)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_foto';
						var data = {
							id_armada_foto: id_armada_foto
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								attachmentTable.ajax.reload();
								alertify.error('Foto Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}

			var stnkTable = $('#stnkTable').DataTable({
				"ordering" : false,
				"scrollX": true,
				"processing": true,
				"serverSide": true,
				ajax: 
				{
					url: "<?= base_url()?>armada/ax_data_stnk/",
					type: 'POST',
					data: function ( d ) {
						return $.extend({}, d, { 

							"id_armada": $("#id_armadastnk").val(),

						});
					}
				},
				columns: 
				[
				{
					data: "id_armada_stnk", render: function(data, type, full, meta){
						var str = '';
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						var kd = "Foto('"+full['kd_armada']+"')";
						str += '<li><a onclick="EditStnk(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
						str += '<li><a onClick="DeleteStnk(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						str += '</ul>';
						str += '</div>';
						if(session_level != 13){
								return str;
							 }else{
								return '';
							}
					}
				},

				{ class:'intro', data: "id_armada_stnk" },
				{ data: "tgl_exp_stnk" },
				{ data: "masa" },
				{ data: "attachment", render: function(data, type, full, meta)
				{
					var url = "<?= base_url()?>"+"uploads/armada/stnk/"+data;
					var op =  "window.open('"+url+"', '_blank');";
					return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';

				}

			},




			],
		});



			/// attachment
			$('#submit_stnk').submit(function(e){
				e.preventDefault(); 

				if($('#tgl_exp_stnk').val() == '')
				{
					alertify.alert("Warning", "Please fill Expired Date.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_stnk',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						if(data['status'] == "success")
						{
							stnkTable.ajax.reload();
							alertify.success('Attachment Uploaded.');
							$('#id_armada_stnk').val('');
							$('#tgl_exp_stnk').val('');
							$('#masa').val('1');
						}else{
							alertify.alert("Peringatan", "Silahkan pilih/upload file terlebih dahulu.");
						}
					});
				}
			});

			function EditStnk(id_armada_stnk)
			{
				var url = '<?=base_url()?>armada/ax_get_data_by_id_stnk';
				var data = {
					id_armada_stnk: id_armada_stnk
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);

					$('#id_armada_stnk').val(id_armada_stnk);
					$('#tgl_exp_stnk').val(data['tgl_exp_stnk']);
					$('#masa').val(data['masa']);
				});
			}

			function DeleteStnk(id_armada_stnk)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_stnk';
						var data = {
							id_armada_stnk: id_armada_stnk
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								stnkTable.ajax.reload();
								alertify.error('STNK Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}








			var keurTable = $('#keurTable').DataTable({
				"ordering" : false,
				"scrollX": true,
				"processing": true,
				"serverSide": true,
				ajax: 
				{
					url: "<?= base_url()?>armada/ax_data_keur/",
					type: 'POST',
					data: function ( d ) {
						return $.extend({}, d, { 

							"id_armada": $("#id_armadakeur").val(),

						});
					}
				},
				columns: 
				[
				{
					data: "id_armada_keur", render: function(data, type, full, meta){
						var str = '';
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						var kd = "Foto('"+full['kd_armada']+"')";
						str += '<li><a onclick="EditKeur(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
						str += '<li><a onClick="DeleteKeur(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						str += '</ul>';
						str += '</div>';
						if(session_level != 13){
								return str;
							 }else{
								return '';
							}
					}
				},

				{ class:'intro', data: "id_armada_keur" },
				{ data: "tgl_exp" },
				{ data: "attachment", render: function(data, type, full, meta)
				{
					var url = "<?= base_url()?>"+"uploads/armada/keur/"+data;
					var op =  "window.open('"+url+"', '_blank');";
						// return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';
						return '<a href="'+url+'" target="_blank">Lihat attachment</a>';
					}

				},




				],
			});



			/// attachment
			$('#submit_keur').submit(function(e){
				e.preventDefault(); 

				if($('#tgl_exp_keur').val() == '')
				{
					alertify.alert("Warning", "Please fill Expired Date.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_keur',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						if(data['status'] == "success")
						{
							keurTable.ajax.reload();
							alertify.success('Attachment Uploaded.');
							$('#id_armada_keur').val('');
							$('#tgl_exp_keur').val('');
						}else{
							alertify.alert("Peringatan", "Silahkan pilih/upload file terlebih dahulu.");
						}

					});
				}
			});

			function EditKeur(id_armada_keur)
			{
				var url = '<?=base_url()?>armada/ax_get_data_by_id_keur';
				var data = {
					id_armada_keur: id_armada_keur
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#id_armada_keur').val(id_armada_keur);
					$('#tgl_exp_keur').val(data['tgl_exp']);
				});
			}

			function DeleteKeur(id_armada_keur)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_keur';
						var data = {
							id_armada_keur: id_armada_keur
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								keurTable.ajax.reload();
								alertify.error('KEUR Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}







			var ijintrayekTable = $('#ijintrayekTable').DataTable({
				"ordering" : false,
				"scrollX": true,
				"processing": true,
				"serverSide": true,
				ajax: 
				{
					url: "<?= base_url()?>armada/ax_data_ijintrayek/",
					type: 'POST',
					data: function ( d ) {
						return $.extend({}, d, { 

							"id_armada": $("#id_armadaijintrayek").val(),

						});
					}
				},
				columns: 
				[
				{
					data: "id_armada_ijintrayek", render: function(data, type, full, meta){
						var str = '';
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						var kd = "Foto('"+full['kd_armada']+"')";
						str += '<li><a onClick="Deleteijintrayek(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						str += '</ul>';
						str += '</div>';
						if(session_level != 13){
								return str;
							 }else{
								return '';
							}
					}
				},

				{ class:'intro', data: "id_armada_ijintrayek" },
				{ data: "nm_trayek" },
				{ data: "tgl_exp" },
				{ data: "attachment", render: function(data, type, full, meta)
				{
					var url = "<?= base_url()?>"+"uploads/armada/ijintrayek/"+data;
					var op =  "window.open('"+url+"', '_blank');";
						// return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';
						return '<a href="'+url+'" target="_blank">Lihat attachment</a>';
					}

				},




				],
			});



			/// attachment
			$('#submit_ijintrayek').submit(function(e){
				e.preventDefault(); 

				if($('#id_trayek').val() == '0'){
					alertify.alert("Warning", "Pilih Trayek.");
				}else if($('#tgl_exp_ijintrayek').val() == ''){
					alertify.alert("Warning", "Tgl Berlaku tidak boleh kosong.");
				}else if($('#file_attachment_ijintrayek').val() == ''){
					alertify.alert("Warning", "Attachment tidak boleh kosong.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_ijintrayek',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						ijintrayekTable.ajax.reload();
						alertify.success('Attachment Uploaded.');
					});
				}
			});

			function Deleteijintrayek(id_armada_ijintrayek)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_ijintrayek';
						var data = {
							id_armada_ijintrayek: id_armada_ijintrayek
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								ijintrayekTable.ajax.reload();
								alertify.error('KEUR Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}




// HISTORY MUTASI ARMADA
var mutasiarmadaTable = $('#mutasiarmadaTable').DataTable({
	"ordering" : false,
	"scrollX": true,
	"processing": true,
	"serverSide": true,
	ajax: 
	{
		url: "<?= base_url()?>armada/ax_data_mutasi_armada/",
		type: 'POST',
		data: function ( d ) {
			return $.extend({}, d, {
				"id_armada": $("#id_armada_mutasi").val(),
			});
		}
	},
	columns: 
	[
	{
		data: "id",
		render: function(data, type, row, meta) {
			return meta.row + meta.settings._iDisplayStart + 1;
		}},
		{ data: "nm_bu_sebelum" },
		{ class:'intro', data: "kd_armada" },
		{ data: "nm_bu_sesudah" },
		{ data: "no_surat" },
		{ data: "tgl_mutasi" },
		],
	});




$(document).ready(function() {
	$( "#tgl_pp_pegawai" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm-dd",
		yearRange: '1946:2019'
	});
	$("#tgl_pp_pegawai").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});

	$("#tahun_armada, #seat_armada").keydown(function (e) {

		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

			(e.keyCode == 65 && e.ctrlKey === true) ||

			(e.keyCode == 67 && e.ctrlKey === true) ||

			(e.keyCode == 88 && e.ctrlKey === true) ||

			(e.keyCode >= 35 && e.keyCode <= 39)) {

			return;
	}

	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});


});

$('#id_bu_filter').select2({
	'allowClear': true
}).on("change", function (e) {
	armadaTable.ajax.reload();
});


function Foto(id_armada)
{
	$('#addModalfotoarmada').modal('show');
	$('#id_armada').val(id_armada);
	attachmentTable.ajax.reload();

	setTimeout(function(){ attachmentTable.columns.adjust().draw(); }, 1000);
}

function Stnk(id_armada)
{
	$('#addModalfotostnk').modal('show');
	$('#id_armada_stnk').val('');
	$('#tgl_exp_stnk').val('');
	$('#masa').val('1');

	$('#id_armadastnk').val(id_armada);
	stnkTable.ajax.reload();

	setTimeout(function(){ stnkTable.columns.adjust().draw(); }, 1000);
}

function Keur(id_armada)
{
	$('#addModalfotokeur').modal('show');
	$('#id_armada_keur').val('');
	$('#tgl_exp_keur').val('');

	$('#id_armadakeur').val(id_armada);
	keurTable.ajax.reload();

	setTimeout(function(){ keurTable.columns.adjust().draw(); }, 1000);
}

function Ijin(id_armada,kd_armada,id_bu)
{
	$('#addModalijintrayek').modal('show');
	$('#id_armadaijintrayek').val(id_armada);
	combo_trayek(id_bu);
	ijintrayekTable.ajax.reload();

	setTimeout(function(){ ijintrayekTable.columns.adjust().draw(); }, 1000);
}

function historymutasi(id_armada)
{
	$('#addModalmutasiarmada').modal('show');
	$('#id_armada_mutasi').val(id_armada);
	mutasiarmadaTable.ajax.reload();

	setTimeout(function(){ mutasiarmadaTable.columns.adjust().draw(); }, 1000);
}

$( "#tgl_exp_stnk").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: "yy-mm-dd"
});
$( "#tgl_exp_keur").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: "yy-mm-dd"
});

$( "#tgl_exp_ijintrayek").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: "yy-mm-dd"
});

$( "#tgl_exp_stnk" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

$( "#tgl_exp_keur" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

$( "#tgl_exp_ijintrayek" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

function combo_trayek(id_bu){
	$.ajax({
		type: "POST", 
		url: "<?= base_url() ?>armada/ax_get_trayek", 
		data: {
			id_bu : id_bu,
		},
		dataType: "json",
		beforeSend: function(e) {
			if(e && e.overrideMimeType) {
				e.overrideMimeType("application/json;charset=UTF-8");
			}
		},
		success: function(response){ 
			$("#id_trayek").html(response.data_trayek).show();
			$('#select2-id_trayek-container').html("--Pilih Trayek--");
			// $('#id_trayek').val(kd_trayek);
		},
		error: function (xhr, ajaxOptions, thrownError) { 
			alert(thrownError); 
		}
	});
}


function printCetak() {
	var url 		= "<?=site_url("reports/print_fms")?>";

	var id_bu 		= $('#id_bu_filter').val();
	var format 		= $('#jenis_print').val();

	var REQ = "?id_bu="+id_bu+"&format="+format+"&uk=F4-L"+"&name=armada";
	open(url+REQ);
}

function PrintArmada(id_armada,kd_armada,id_bu) {
	var url 		= "<?=site_url("reports/print_fms")?>";
	var format 		= "html";

	var REQ = "?id_armada="+id_armada+"&kd_armada="+kd_armada+"&id_bu="+id_bu+"&format="+format+"&uk=F4-L"+"&name=armada_foto";
	open(url+REQ);
}

</script>
</body>
</html>
