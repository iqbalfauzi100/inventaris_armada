<!DOCTYPE html>
<html>

<head>
  <?= $this->load->view('head'); ?>

</head>
<style>
  #container {
    min-width: 310px;
    max-width: 800px;
    height: 400px;
    margin: 0 auto
  }
</style>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">

  <div class="wrapper">

    <?= $this->load->view('nav'); ?>
    <?= $this->load->view('menu_groups'); ?>
    <?php $session = $this->session->userdata('login'); ?>

    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
      </section>

      <section class="invoice">


        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-bus"></i></span>
              <div class="info-box-content">
                <span class="info-box-text" >TOTAL ARMADA</span>
                <span class="info-box-number" id="total_armada">0</span>
                <small style="font-size: 11px"><b>USUL AFKIR : <b id="armada_ua"></b></b></small><br>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-bus"></i></span>
              <div class="info-box-content">
                <span class="info-box-text" >ARMADA DAMRI</span>
                <span class="info-box-number" id="armada_damri">0</span>
                <small style="font-size: 11px"><b>USUL AFKIR : <b id="armada_damri_ua"></b></b></small><br>
              </div>
            </div>
          </div>

          <div class="clearfix visible-sm-block"></div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-bus"></i></span>
              <div class="info-box-content">
                <span class="info-box-text" >ARMADA KSO</span>
                <span class="info-box-number" id="armada_kso">0</span>
                <small style="font-size: 11px"><b>USUL AFKIR : <b id="armada_kso_ua"></b></b></small><br>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-default"><i class="fa fa-bus"></i></span>
              <div class="info-box-content">
                <span class="info-box-text" >JUMLAH CABANG</span>
                <span class="info-box-number" id="jumlah_cabang">4</span>
              </div>
            </div>
          </div>

          
        </div>

        <div class="row">
          <center>

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-bus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ARMADA SEWA</span>
                  <!-- <span class="info-box-number"  id="jumlah_cabang">58</span> -->
                  <span class="info-box-number" id="armada_sewa">0</span>
                  <small style="font-size: 11px"><b>USUL AFKIR : <b id="armada_sewa_ua"></b></b></small><br>
                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-default"><i class="fa fa-bus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text" >JUMLAH CABANG DG ARMADA SEWA</span>
                  <span class="info-box-number" id="jumlah_cabang_sewa">0</span>
                </div>
              </div>
            </div>
          </center>
        </div>





        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary">
              <div id="tulis_surat">
                <div class="box-header with-border">
                  <center><b><h3 class="box-title">USIA KENDARAAN BERDASARKAN TAHUN PEMBUATAN</h3></b></center>
                </div>
                <div class="row">
                  <div class=" form-group col-lg-12 col-xs-12">
                    <div class="input-group col-md-12">
                      <select class="form-control select2" style="width: 100%;" id="divisi_usia" name="divisi_usia">
                        <!-- <option value="0">-- Pilih Divre --</option>   -->
                        <option value="1">Divre 1</option>  
                        <!-- <option value="2">Divre 2</option>  
                        <option value="3">Divre 3</option>  
                        <option value="4">Divre 4</option>  
                        <option value="5">Semua Divre</option> -->  
                      </select>
                      <span class="input-group-btn">
                        <a href="<?=base_url('dashboard/usia');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
                      </span>
                    </div>
                  </div>
                </div> 
                <div class="row">
                  <div class="box-body">
                   <center><div id="grafik_usia" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
                 </div>
               </div>
             </div>
           </div>
         </div>
         <div class="col-md-6">
          <div class="box box-primary">
            <div id="tulis_surat">
              <div class="box-header with-border">
                <center><b><h3 class="box-title">USIA KENDARAAN BERDASARKAN TAHUN PEROLEHAN</h3></b></center>
              </div>
              <div class="row">
                <div class=" form-group col-lg-12 col-xs-12">

                 <div class="input-group col-md-12">
                  <select class="form-control select2" style="width: 100%;" id="divisi_tahun_perolehan" name="divisi_tahun_perolehan">
                    <!-- <option value="0">-- Pilih Divre --</option>   -->
                    <option value="1">Divre 1</option>  
                    <!-- <option value="2">Divre 2</option>  
                    <option value="3">Divre 3</option>  
                    <option value="4">Divre 4</option>  
                    <option value="5">Semua Divre</option>  --> 
                  </select>
                  <span class="input-group-btn">
                   <a href="<?=base_url('dashboard/tahun_perolehan');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
                 </span>
               </div>

             </div>
           </div> 
           <div class="row">
            <div class="box-body">
             <center>
               <div id="grafik_tahun_perolehan" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto">
                 <canvas id="graphCanvas"></canvas>
               </div>
             </center> 
               <!-- <center><div id="chart-container" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto">
                <canvas id="graphCanvas"></canvas>
              </div></center> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div id="tulis_surat">
          <div class="box-header with-border">
            <center><b><h3 class="box-title">MEREK KENDARAAN</h3></b></center>
          </div>
          <div class="row">
            <div class=" form-group col-lg-12 col-xs-12">

              <div class="input-group col-md-12">
                <select class="form-control select2" style="width: 100%;" id="divisi_merek" name="divisi_merek">
                  <!-- <option value="0">-- Pilih Divre --</option>   -->
                  <option value="1">Divre 1</option>  
                  <!-- <option value="2">Divre 2</option>  
                  <option value="3">Divre 3</option>  
                  <option value="4">Divre 4</option>  
                  <option value="5">Semua Divre</option>  --> 
                </select>
                <span class="input-group-btn">
                  <a href="<?=base_url('dashboard/merek');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
                </span>
              </div>

            </div>
          </div> 
          <div class="row">
            <div class="box-body">
              <center><div id="grafik_merek" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div id="tulis_surat">
          <div class="box-header with-border">
            <center><b><h3 class="box-title">JENIS ARMADA</h3></b></center>
          </div>
          <div class="row">
            <div class=" form-group col-lg-12 col-xs-12">

              <div class="input-group col-md-12">
                <select class="form-control select2" style="width: 100%;" id="divisi_jenis_armada" name="divisi_jenis_armada">
                  <!-- <option value="0">-- Pilih Divre --</option>   -->
                  <option value="1">Divre 1</option>  
                  <!-- <option value="2">Divre 2</option>  
                  <option value="3">Divre 3</option>  
                  <option value="4">Divre 4</option>  
                  <option value="5">Semua Divre</option> -->  
                </select>
                <span class="input-group-btn">
                  <a href="<?=base_url('dashboard/jenis_armada');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
                </span>
              </div>

            </div>
          </div> 
          <div class="row">
            <div class="box-body">
              <center><div id="grafik_jenis_armada" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div id="tulis_surat">
          <div class="box-header with-border">
            <center><b><h3 class="box-title">SEGMENTASI ARMADA</h3></b></center>
          </div>
          <div class="row">
            <div class=" form-group col-lg-12 col-xs-12">

             <div class="input-group col-md-12">
               <select class="form-control select2" style="width: 100%;" id="divisi_segment" name="divisi_segment">
                <!-- <option value="0">-- Pilih Divre --</option>   -->
                <option value="1">Divre 1</option>  
                <!-- <option value="2">Divre 2</option>  
                <option value="3">Divre 3</option>  
                <option value="4">Divre 4</option>  
                <option value="5">Semua Divre</option> -->  
              </select>
              <span class="input-group-btn">
                <a href="<?=base_url('dashboard/segment');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
              </span>
            </div>

          </div>
        </div> 
        <div class="row">
          <div class="box-body">
           <center><div id="grafik_segment" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">MASA BERLAKU STNK</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

         <div class="input-group col-md-12">
           <select class="form-control select2" style="width: 100%;" id="divisi_stnk" name="divisi_stnk">
            <!-- <option value="0">-- Pilih Divre --</option>   -->
            <option value="1">Divre 1</option>  
            <!-- <option value="2">Divre 2</option>  
            <option value="3">Divre 3</option>  
            <option value="4">Divre 4</option>  
            <option value="5">Semua Divre</option> -->
          </select>
          <span class="input-group-btn">
            <a href="<?=base_url('dashboard/stnk');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
          </span>
        </div>

      </div>
    </div> 
    <div class="row">
      <div class="box-body">
       <center><div id="grafik_stnk" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
     </div>
   </div>
 </div>
</div>
</div>
</div>



<div class="row">
 <div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">MASA BERLAKU KEUR</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

         <div class="input-group col-md-12">
           <select class="form-control select2" style="width: 100%;" id="divisi_keur" name="divisi_keur">
            <!-- <option value="0">-- Pilih Divre --</option>   -->
            <option value="1">Divre 1</option>  
            <!-- <option value="2">Divre 2</option>  
            <option value="3">Divre 3</option>  
            <option value="4">Divre 4</option>  
            <option value="5">Semua Divre</option> -->
          </select>
          <span class="input-group-btn">
            <a href="<?=base_url('dashboard/keur');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
          </span>
        </div>

      </div>
    </div> 
    <div class="row">
      <div class="box-body">
       <center><div id="grafik_keur" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
     </div>
   </div>
 </div>
</div>
</div>
<div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">MASA BERLAKU IJIN TRAYEK</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

         <div class="input-group col-md-12">
           <select class="form-control select2" style="width: 100%;" id="divisi_ijintrayek" name="divisi_ijintrayek">
            <!-- <option value="0">-- Pilih Divre --</option>   -->
            <option value="1">Divre 1</option>  
            <!-- <option value="2">Divre 2</option>  
            <option value="3">Divre 3</option>  
            <option value="4">Divre 4</option>  
            <option value="5">Semua Divre</option> -->
          </select>
          <span class="input-group-btn">
            <a href="<?=base_url('dashboard/ijintrayek');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
          </span>
        </div>

      </div>
    </div> 
    <div class="row">
      <div class="box-body">
       <center><div id="grafik_ijintrayek" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
     </div>
   </div>
 </div>
</div>
</div>
</div>



<!-- <div class="row">
 <div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">PEMBAYARAN ASURANSI</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

         <div class="col-md-6">
          <select class="form-control select2 " style="width: 100%;" id="tahun_outstanding" name="tahun_outstanding">
            <?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
            <option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
            <?php } ?>
          </select>

        </div>

        <div class="col-md-6">
          <div class="input-group">
           <select class="form-control select2" style="width: 100%;" id="divisi_outstanding" name="divisi_outstanding">
            <option value="0">-- Pilih Divre --</option>  
            <option value="1">Divre 1</option>  
            <option value="2">Divre 2</option>  
            <option value="3">Divre 3</option>  
            <option value="4">Divre 4</option>  
            <option value="5">Semua Divre</option>
          </select>
          <span class="input-group-btn">
           <a href="<?=base_url('dashboard/outstanding');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
         </span>
       </div>
     </div>

   </div>
 </div> 
 <div class="row">
  <div class="box-body">
   <center><div id="grafik_outstanding" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto">
     <canvas id="graphOutstanding"></canvas>
   </div></center>
 </div>
</div>
</div>
</div>
</div> -->
<!-- <div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">COVERAGE ASURANSI</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

          <div class="col-md-6">
            <select class="form-control select2 " style="width: 100%;" id="tahun_asuransi" name="tahun_asuransi">
              <?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
              <option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
              <?php } ?>
            </select>

          </div>

          <div class="col-md-6">
            <div class="input-group">
             <select class="form-control select2" style="width: 100%;" id="divisi_asuransi" name="divisi_asuransi">
              <option value="0">-- Pilih Divre --</option>  
              <option value="1">Divre 1</option>  
              <option value="2">Divre 2</option>  
              <option value="3">Divre 3</option>  
              <option value="4">Divre 4</option>  
              <option value="5">Semua Divre</option>
            </select>
            <span class="input-group-btn">
             <a href="<?=base_url('dashboard/asuransi');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
           </span>
         </div>
       </div>

     </div>
   </div> 
   <div class="row">
    <div class="box-body">
     <center><div id="grafik_asuransi" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
   </div>
 </div>
</div>
</div>
</div>
</div> -->


<!-- <div class="row">
 <div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">KLAIM ASURANSI</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

         <div class="col-md-6">
          <select class="form-control select2 " style="width: 100%;" id="tahun_klaim" name="tahun_klaim">
            <?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
            <option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
            <?php } ?>
          </select>

        </div>

        <div class="col-md-6">
          <div class="input-group">
           <select class="form-control select2" style="width: 100%;" id="divisi_klaim" name="divisi_klaim">
            <option value="0">-- Pilih Divre --</option>  
            <option value="1">Divre 1</option>  
            <option value="2">Divre 2</option>  
            <option value="3">Divre 3</option>  
            <option value="4">Divre 4</option>  
            <option value="5">Semua Divre</option>
          </select>
          <span class="input-group-btn">
           <a href="<?=base_url('dashboard/klaim');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
         </span>
       </div>
     </div>

   </div>
 </div> 
 <div class="row">
  <div class="box-body">
   <center><div id="grafik_klaim" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto">
     <canvas id="graphOutstanding"></canvas>
   </div></center>
 </div>
</div>
</div>
</div>
</div> -->
<div class="col-md-6">
  <div class="box box-primary">
    <div id="tulis_surat">
      <div class="box-header with-border">
        <center><b><h3 class="box-title">FOTO ARMADA</h3></b></center>
      </div>
      <div class="row">
        <div class=" form-group col-lg-12 col-xs-12">

          <div class="col-md-12">
            <div class="input-group">
             <select class="form-control select2" style="width: 100%;" id="divisi_foto_armada" name="divisi_foto_armada">
              <!-- <option value="0">-- Pilih Divre --</option>   -->
              <option value="1">Divre 1</option>  
              <!-- <option value="2">Divre 2</option>  
              <option value="3">Divre 3</option>  
              <option value="4">Divre 4</option>  
              <option value="5">Semua Divre</option> -->
            </select>
            <span class="input-group-btn">
             <a href="<?=base_url('dashboard/foto_armada');?>" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-list"></i> </a>
           </span>
         </div>
       </div>

     </div>
   </div> 
   <div class="row">
    <div class="box-body">
     <center><div id="grafik_foto_armada" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
   </div>
 </div>
</div>
</div>
</div>
</div>




<!-- <br><br>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div id="tulis_surat">
        <div class="box-header with-border">
          <center><b><h3 class="box-title">USIA KENDARAAN ALL CABANG</h3></b></center>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="dataTable_wrapper">
             <table class="table table-striped table-bordered table-hover" id="usiaTableXXX">
              <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Cabang</th>
                  <th class="text-center">Divre</th>
                  <th class="text-center">Kurang 5 Th</th>
                  <th class="text-center">6 sd 10 Th</th>
                  <th class="text-center">11 sd 15 Th</th>
                  <th class="text-center">16 sd 20 Th</th>
                  <th class="text-center">Diatas 20 Th</th>
                  <th class="text-center">Total</th>
                </tr>
              </thead>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</div> -->


<div class="row">
  <div class="col-lg-12 col-xs-12">
    <center><div id="container" style="min-width: auto; height: auto; max-width: auto; margin: 0 auto"></div></center>
  </div>
</div>

</section>

</div>
<?= $this->load->view('basic_js'); ?>

<script type='text/javascript'>

  $(function() {
    jQuery.fn.exists = function(){return this.length>0;}

    ViewDataUsia();
    ViewDataTahunPerolehan();
    ViewDataMerek();
    ViewDataJenisArmada();
    ViewDataSegment();
    ViewDataStnk();
    ViewDataKeur();
    ViewDataIjintrayek();
    ViewDataOutstanding();
    ViewDataAsuransi();

    ViewDataKlaim();
    ViewDataFotoArmada();

    viewJumlahArmada();

  });

  $('#divisi_usia').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataUsia();
  });

  $('#divisi_tahun_perolehan').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataTahunPerolehan();
  });


  $('#divisi_merek').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataMerek();
  });

  $('#divisi_jenis_armada').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataJenisArmada();
  });


  $('#divisi_segment').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataSegment();
  });

  $('#divisi_stnk').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataStnk();
  });

  $('#divisi_keur').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataKeur();
  });

  $('#divisi_ijintrayek').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataIjintrayek();
  });

  $('#divisi_outstanding').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataOutstanding();
  });

  $('#divisi_asuransi').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataAsuransi();
  });

  $('#divisi_klaim').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataKlaim();
  });
  $('#divisi_foto_armada').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataFotoArmada();
  });

  $('#tahun_outstanding').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataOutstanding();
  });

  $('#tahun_asuransi').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataAsuransi();
  });

  $('#tahun_klaim').select2({
    'allowClear': true
  }).on("change", function (e) {
    ViewDataKlaim();
  });





  function viewJumlahArmada() {
    var url = '<?=base_url()?>home/get_data_jumlah_armada';
    $.ajax({
      url: url,
      method: 'POST'
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);
      $('#total_armada').html(data['total_armada']);
      $('#armada_ua').html(data['armada_ua']);
      $('#armada_damri').html(data['armada_damri']);
      $('#armada_damri_ua').html(data['armada_damri_ua']);

      $('#armada_sewa').html(data['armada_sewa']);
      $('#armada_sewa_ua').html(data['armada_sewa_ua']);

      $('#armada_kso').html(data['armada_kso']);
      $('#armada_kso_ua').html(data['armada_kso_ua']);
      $('#jumlah_cabang_sewa').html(data['jumlah_cabang_sewa']);
    });
  }


  function ViewDataUsia(){
    var url = '<?=base_url()?>home/get_data_dashboard_usia';
    var data = {
      id_divisi   : $('#divisi_usia').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);
            //USIA
            var dibawah5      = data['dibawah5'][0];
            var antara6dan10  = data['antara6dan10'][0];
            var antara11dan15 = data['antara11dan15'][0];
            var antara16dan20 = data['antara16dan20'][0];
            var diatas20      = data['diatas20'][0];

            //USIA
            var hchart = new Highcharts.chart({
              chart: {
                renderTo: 'grafik_usia',
                type: 'column',
                margin: 75,
              },
              title: {
                text: ''
              },
              accessibility: {
                announceNewData: {
                  enabled: true
                }
              },
              xAxis: {
                type: 'category',
                title: {
                  text: 'Usia Kendaraan(Tahun)'
                }

              },
              yAxis: {
                title: {
                  text: 'Jumlah'
                }

              },
              legend: {
                enabled: false
              },
              plotOptions: {
                series: {
                  borderWidth: 0,
                  dataLabels: {
                    enabled: true,
                    format: '{y}'
                  }
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name} Tahun</span>: <b>{point.y}</b><br/>'
              },

              series: [
              {
                name: "Armada",
                colorByPoint: true,
                data: [
                {
                  name: "0-5",
                  y: dibawah5,
                  drilldown: "0-5"
                },
                {
                  name: "6-10",
                  y: antara6dan10,
                  drilldown: "5-10"
                },
                {
                  name: "11-15",
                  y: antara11dan15,
                  drilldown: "11-15"
                },
                {
                  name: "16-20",
                  y: antara16dan20,
                  drilldown: "16-20"
                },
                {
                  name: ">20",
                  y: diatas20,
                  drilldown: ">20"
                }
                ]
              }
              ]
            });

          });
  }

  function ViewDataTahunPerolehan(){
    var url = '<?=base_url()?>home/get_data_dashboard_usia_tahun_perolehan';
    var data = {
      id_divisi   : $('#divisi_tahun_perolehan').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);
            //USIA
            var dibawah5      = data['dibawah5'][0];
            var antara6dan10  = data['antara6dan10'][0];
            var antara11dan15 = data['antara11dan15'][0];
            var antara16dan20 = data['antara16dan20'][0];
            var diatas20      = data['diatas20'][0];

            //USIA
            var hchart = new Highcharts.chart({
              chart: {
                renderTo: 'grafik_tahun_perolehan',
                type: 'column',
                margin: 75,
              },
              title: {
                text: ''
              },
              accessibility: {
                announceNewData: {
                  enabled: true
                }
              },
              xAxis: {
                type: 'category',
                title: {
                  text: 'Usia Kendaraan(Tahun)'
                }

              },
              yAxis: {
                title: {
                  text: 'Jumlah'
                }

              },
              legend: {
                enabled: false
              },
              plotOptions: {
                series: {
                  borderWidth: 0,
                  dataLabels: {
                    enabled: true,
                    format: '{y}'
                  }
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name} Tahun</span>: <b>{point.y}</b><br/>'
              },

              series: [
              {
                name: "Armada",
                colorByPoint: true,
                data: [
                {
                  name: "0-5",
                  y: dibawah5,
                  drilldown: "0-5"
                },
                {
                  name: "6-10",
                  y: antara6dan10,
                  drilldown: "5-10"
                },
                {
                  name: "11-15",
                  y: antara11dan15,
                  drilldown: "11-15"
                },
                {
                  name: "16-20",
                  y: antara16dan20,
                  drilldown: "16-20"
                },
                {
                  name: ">20",
                  y: diatas20,
                  drilldown: ">20"
                }
                ]
              }
              ]
            });

          });
  }

  function ViewDataMerek(){
    var url = '<?=base_url()?>home/get_data_dashboard_merek';
    var data = {
      id_divisi   : $('#divisi_merek').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_merek',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Merek Kendaraan'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} Merek</span>: <b>{point.y}</b><br/>'
        },

        series: [
        {
          name: "Merek",
          colorByPoint: true,
          data: [
          // {
          //   name: "Beijing",
          //   y: data['beijing'][0],
          //   drilldown: "Beijing"
          // },
          {
            name: "Daihatsu",
            y: data['daihatsu'][0],
            drilldown: "Daihatsu"
          },
          // {
          //   name: "Golden Dragon",
          //   y: data['gdragon'][0],
          //   drilldown: "Golden Drago"
          // },
          {
            name: "Hino",
            y: data['hino'][0],
            drilldown: "Hino"
          },
          {
            name: "Hyundai",
            y: data['hyundai'][0],
            drilldown: "Hyundai"
          },
          // {
          //   name: "Inobus",
          //   y: data['inobus'][0],
          //   drilldown: "Inobus"
          // },
          {
            name: "Isuzu",
            y: data['isuzu'][0],
            drilldown: "Isuzu"
          },
          // {
          //   name: "Kinglong",
          //   y: data['kinglong'][0],
          //   drilldown: "Kinglong"
          // },
          {
            name: "Mercedes Benz",
            y: data['mercy'][0],
            drilldown: "Mercedes Benz"
          },
          {
            name: "Mitsubishi",
            y: data['mitsubishi'][0],
            drilldown: "Mitsubishi"
          },
          {
            name: "Nissan",
            y: data['nissan'][0],
            drilldown: "Nissan"
          },
          {
            name: "Toyota",
            y: data['toyota'][0],
            drilldown: "Toyota"
          },
          // {
          //   name: "Yutoong",
          //   y: data['yutoong'][0],
          //   drilldown: "Yutoong"
          // },
          {
            name: "ZhongTong",
            y: data['zhongtong'][0],
            drilldown: "ZhongTong"
          },
          {
            name: "AAI Bus",
            y: data['aaibus'][0],
            drilldown: "AAI Bus"
          },
          ]
        }
        ]
      });

    });
  }

  function ViewDataJenisArmada(){
    var url = '<?=base_url()?>home/get_data_dashboard_jenis_armada';
    var data = {
      id_divisi   : $('#divisi_jenis_armada').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_jenis_armada',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Jenis Armada'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y}</b><br/>'
        },
        series: [
        {
          name: "Jenis",
          colorByPoint: true,
          data: [
          {
            name: "Bus Besar",
            y: data['busbesar'][0],
            drilldown: "Bus Besar"
          },
          {
            name: "Bus Gandeng",
            y: data['busgandeng'][0],
            drilldown: "Bus Gandeng"
          },
          {
            name: "Bus Medium",
            y: data['busmedium'][0],
            drilldown: "Bus Medium"
          },
          {
            name: "Microbus",
            y: data['microbus'][0],
            drilldown: "Microbus"
          },
          {
            name: "Box Mini",
            y: data['boxmini'][0],
            drilldown: "Box Mini"
          },
          {
            name: "Box Medium",
            y: data['boxmedium'][0],
            drilldown: "Box Medium"
          },
          {
            name: "Box Besar",
            y: data['boxbesar'][0],
            drilldown: "Box Besar"
          },
          {
            name: "Medium Long",
            y: data['mediumlong'][0],
            drilldown: "Medium Long"
          }
          ]
        }
        ]
      });

    });
  }


  function ViewDataSegment(){
    var url = '<?=base_url()?>home/get_data_dashboard_segmentasi';
    var data = {
      id_divisi   : $('#divisi_segment').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_segment',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Segmentasi'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y}</b><br/>'
        },
        series: [
        {
          name: "Segment",
          colorByPoint: true,
          data: [
          {
            name: "Antarkota",
            y: data['antarkota'][0],
            drilldown: "Antarkota"
          },
          {
            name: "Perintis",
            y: data['perintis'][0],
            drilldown: "Perintis"
          },
          {
            name: "Pemadumoda",
            y: data['pemadumoda'][0],
            drilldown: "Pemadumoda"
          },
          {
            name: "Antar Negara",
            y: data['aneg'][0],
            drilldown: "Antar Negara"
          },
          {
            name: "Bis Kota",
            y: data['biskota'][0],
            drilldown: "Bis Kota"
          },
          {
            name: "Paket",
            y: data['paket'][0],
            drilldown: "Paket"
          },
          {
            name: "Pariwisata",
            y: data['pariwisata'][0],
            drilldown: "Pariwisata"
          },
          {
            name: "Aglomerasi",
            y: data['aglomerasi'][0],
            drilldown: "Aglomerasi"
          }
          ]
        }
        ]
      });

    });
  }


  function ViewDataStnk(){
    var url = '<?=base_url()?>home/get_data_dashboard_stnk';
    var data = {
      id_divisi   : $('#divisi_stnk').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_stnk',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Masa Berlaku STNK'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} Tahun</span>: <b>{point.y}</b><br/>'
        },
        colors: ['#50B432','#ff9900', '#fa2a2a', '#2a31fa'],

        series: [
        {
          name: "Masa Berlaku STNK",
          colorByPoint: true,
          data: [
          {
            name: "Berlaku",
            y: data['lebih30'][0],
            drilldown: "Berlaku"
          },
          {
            name: "Habis 30 hari",
            y: data['kurang30'][0],
            drilldown: "Habis dalam 30 hari"
          },
          {
            name: "Habis Masa",
            y: data['habis_masa'][0],
            drilldown: "Habis Masa Berlaku"
          },
          {
            name: "Belum Upload",
            y: data['belum_upload'][0],
            drilldown: "Belum Upload STNK"
          }
          ]
        }
        ]
      });

    });
  }

  function ViewDataKeur(){
    var url = '<?=base_url()?>home/get_data_dashboard_keur';
    var data = {
      id_divisi   : $('#divisi_keur').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_keur',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Masa Berlaku Keur'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} Tahun</span>: <b>{point.y}</b><br/>'
        },
        colors: ['#50B432','#ff9900', '#fa2a2a', '#2a31fa'],

        series: [
        {
          name: "Masa Berlaku Keur",
          colorByPoint: true,
          data: [
          {
            name: "Berlaku",
            y: data['lebih30'][0],
            drilldown: "Berlaku"
          },
          {
            name: "Habis 30 hari",
            y: data['kurang30'][0],
            drilldown: "Habis dalam 30 hari"
          },
          {
            name: "Habis Masa",
            y: data['habis_masa'][0],
            drilldown: "Habis Masa Berlaku"
          },
          {
            name: "Belum Upload",
            y: data['belum_upload'][0],
            drilldown: "Belum Upload KEUR"
          }
          ]
        }
        ]
      });

    });
  }


  function ViewDataIjintrayek(){
    var url = '<?=base_url()?>home/get_data_dashboard_ijintrayek';
    var data = {
      id_divisi   : $('#divisi_ijintrayek').val()
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_ijintrayek',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Masa Berlaku Ijin Trayek'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} Tahun</span>: <b>{point.y}</b><br/>'
        },
        colors: ['#50B432','#ff9900', '#fa2a2a', '#2a31fa'],

        series: [
        {
          name: "Masa Berlaku Ijin Trayek",
          colorByPoint: true,
          data: [
          {
            name: "Berlaku",
            y: data['lebih30'][0],
            drilldown: "Berlaku"
          },
          {
            name: "Habis 30 hari",
            y: data['kurang30'][0],
            drilldown: "Habis dalam 30 hari"
          },
          {
            name: "Habis Masa",
            y: data['habis_masa'][0],
            drilldown: "Habis Masa Berlaku"
          },
          {
            name: "Belum Upload",
            y: data['belum_upload'][0],
            drilldown: "Belum Upload"
          }
          ]
        }
        ]
      });

    });
  }


  function ViewDataOutstanding(){
    var url = '<?=base_url()?>home/get_data_dashboard_outstanding'; 
    var data = {
      tahun       : $('#tahun_outstanding').val(),
      id_divisi   : $('#divisi_outstanding').val(),
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);



      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_outstanding',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Outstanding Asuransi'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y:,.1f}</b><br/>'
        },
        // colors: ['#50B432','#ff9900', '#fa2a2a'],

        series: [
        {
          name: "Outstanding Asuransi",
          colorByPoint: true,
          data: [
          {
            name: "Nilai Premi",
            y: data['nilai_premi'][0],
            drilldown: "Nilai Premi"
          },
          {
            name: "Nilai Bayar",
            y: data['nilai_bayar'][0],
            drilldown: "Nilai Bayar"
          },
          {
            name: "Nilai outstanding",
            y: data['nilai_outstanding'][0],
            drilldown: "Nilai outstanding"
          },
          ]
        }
        ]
      });

    });
  }


  function ViewDataAsuransi(){
    var url = '<?=base_url()?>home/get_data_dashboard_asuransi';
    var data = {
      id_divisi   : $('#divisi_asuransi').val(),
      tahun   : $('#tahun_asuransi').val(),
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_asuransi',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Coverage Asuransi'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y}</b><br/>'
        },
        colors: ['#50B432','#ff9900', '#fa2a2a'],

        series: [
        {
          name: "Coverage Asuransi",
          colorByPoint: true,
          data: [
          {
            name: "Pengajuan Cabang",
            y: data['pengajuan_cabang'][0],
            drilldown: "Pengajuan Cabang"
          },
          {
            name: "Jaminan Bank",
            y: data['jaminan_bank'][0],
            drilldown: "Jaminan Bank"
          },
          {
            name: "Belum Terasuransi",
            y: data['belum_terasuransi'][0],
            drilldown: "Belum Terasuransi"
          },
          ]
        }
        ]
      });

    });
  }


  function ViewDataKlaim(){
    var url = '<?=base_url()?>home/get_data_dashboard_klaim';
    var data = {
      id_divisi   : $('#divisi_klaim').val(),
      tahun       : $('#tahun_klaim').val(),
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_klaim',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Klaim Asuransi'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y}</b><br/>'
        },
        colors: ['#50B432','#ff9900', '#fa2a2a'],

        series: [
        {
          name: "Klaim Asuransi",
          colorByPoint: true,
          data: [
          {
            name: "Progress",
            y: data['progress'][0],
            drilldown: "Progress"
          },
          {
            name: "Selesai",
            y: data['selesai'][0],
            drilldown: "Selesai"
          },
          ]
        }
        ]
      });

    });
  }

  function ViewDataFotoArmada(){
    var url = '<?=base_url()?>home/get_data_dashboard_foto_armada';
    var data = {
      id_divisi   : $('#divisi_foto_armada').val(),
    };

    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(function(data, textStatus, jqXHR) {
      var data = JSON.parse(data);

      var hchart = new Highcharts.chart({
        chart: {
          renderTo: 'grafik_foto_armada',
          type: 'column',
          margin: 75,
        },
        title: {
          text: ''
        },
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        xAxis: {
          type: 'category',
          title: {
            text: 'Foto Armada'
          }

        },
        yAxis: {
          title: {
            text: 'Jumlah'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{y}'
            }
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name} </span>: <b>{point.y}</b><br/>'
        },
        // colors: ['#50B432','#ff9900', '#fa2a2a'],

        series: [
        {
          name: "Foto Armada",
          colorByPoint: true,
          data: [
          {
            name: "Depan Kanan",
            y: data['depan_kanan'][0],
            drilldown: "Depan Kanan"
          },
          {
            name: "Depan Kanan Belum Upload",
            y: data['depan_kanan_belum_upload'][0],
            drilldown: "Depan Kanan Belum Upload"
          },
          {
            name: "Depan Kiri",
            y: data['depan_kiri'][0],
            drilldown: "Depan Kiri"
          },
          {
            name: "Depan Kiri Belum Upload",
            y: data['depan_kiri_belum_upload'][0],
            drilldown: "Depan Kiri Belum Upload"
          },
          {
            name: "Belakang",
            y: data['belakang'][0],
            drilldown: "Belakang"
          },
          {
            name: "Belakang Belum Upload",
            y: data['belakang_belum_upload'][0],
            drilldown: "Belakang Belum Upload"
          },
          {
            name: "Dalam",
            y: data['dalam'][0],
            drilldown: "Dalam"
          },
          {
            name: "Dalam Belum Upload",
            y: data['dalam_belum_upload'][0],
            drilldown: "Dalam Belum Upload"
          },
          {
            name: "Gesek Rangka",
            y: data['gesek_rangka'][0],
            drilldown: "Gesek Rangka"
          },
          {
            name: "Gesek Rangka Belum Upload",
            y: data['gesek_rangka_belum_upload'][0],
            drilldown: "Gesek Rangka Belum Upload"
          },
          {
            name: "Gesek Mesin",
            y: data['gesek_mesin'][0],
            drilldown: "Gesek Mesin"
          },
          {
            name: "Gesek Mesin Belum Upload",
            y: data['gesek_mesin_belum_upload'][0],
            drilldown: "Gesek Mesin Belum Upload"
          },
          {
            name: "Foto Mesin",
            y: data['foto_mesin'][0],
            drilldown: "Foto Mesin"
          },
          {
            name: "Foto Mesin Belum Upload",
            y: data['foto_mesin_belum_upload'][0],
            drilldown: "Foto Mesin Belum Upload"
          },
          ]
        }
        ]
      });

    });
  }


  function ViewDataTahunPerolehanXXX() {

    var url = '<?=base_url()?>home/get_data_tahun_perolehan';
    var id_divisi = $('#divisi_tahun_perolehan').val();

    $.ajax({
      url : url+"/"+id_divisi,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {

        var tahun_perolehan = [];
        var jumlah = [];

        for (var i = 0; i < data.length; i++) {
         tahun_perolehan.push(data[i].tahun_perolehan);
         jumlah.push(data[i].jumlah);
       }

       var chartdata = {
        labels: tahun_perolehan,
        datasets: [
        {
          label: 'Tahun Perolehan',
          backgroundColor: '#1fff3d',
          borderColor: '#eb46f1',
          hoverBackgroundColor: '#CCCCCC',
          hoverBorderColor: '#666666',
          data: jumlah 
        }
        ]
      };

      var graphTarget = $("#graphCanvas");
      var barGraph = new Chart(graphTarget, {
        type: 'bar',
        data: chartdata
      });


    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error get data!');
    }
  });
  }


  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

</script>
</body>
</html>
