<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>USIA KENDARAAN BERDASARKAN TAHUN PEMBUATAN</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>
							<div class="panel-body">
								<div class="row">
									<div class=" form-group col-lg-12 col-xs-12">
										<div class="form-group col-md-12">
											<center>
												<select class="form-control select2" style="width: 100%;" id="divisi" name="divisi">
													<!-- <option value="0">-- Pilih Divre --</option>	 -->
													<option value="1">Divre 1</option>	
<!-- 													<option value="2">Divre 2</option>	
													<option value="3">Divre 3</option>	
													<option value="4">Divre 4</option>	
													<option value="5">Semua Divre</option>	 -->
												</select>
											</center>
										</div>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="usiaTable">
										<thead>
											<tr>
												<th class="text-center">NO</th>
												<th class="text-center">CABANG</th>
												<th class="text-center">0sd5TH</th>
												<th class="text-center">6sd10TH</th>
												<th class="text-center">11sd15TH</th>
												<th class="text-center">16sd20TH</th>
												<th class="text-center">>20TH</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		$('#divisi').select2({
			'allowClear': true
		}).on("change", function (e) {
			usiaTable.ajax.reload();
		});


		var usiaTable = $('#usiaTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>report/ax_data_grafik_usia/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"divisi" : $("#divisi").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "cabang" },
			{ data: "satu" },
			{ data: "dua" },
			{ data: "tiga" },
			{ data: "empat" },
			{ data: "lima" },
			{ data: "satu", render: function(data, type, full, meta){
				var total = parseInt(data)+parseInt(full['dua'])+parseInt(full['tiga'])+parseInt(full['empat'])+parseInt(full['lima']);
				return $.fn.dataTable.render.number(',', '.', 0).display(parseInt(total));
			}},
			]
		});


	</script>
</body>
</html>
