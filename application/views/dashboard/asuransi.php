<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>COVERAGE ASURANSI</h1>
			</section>

			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label>Tahun</label>
										<select class="form-control select2 " style="width: 100%;" id="tahun_filter" name="tahun_filter">
											<!-- <option value="0" >--All--</option> -->
											<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
											<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-4">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-4">
										<label>Kategori</label>
										<select class="form-control select2" style="width: 100%;" id="kategori_filter" name="kategori_print">
											<option value="0" >--All--</option>
											<option value="1" >1. Pengajuan Cabang</option>
											<option value="2" >2. Jaminan Bank</option>
											<option value="3" >3. Belum Terasuransi</option>
										</select>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="asuransi_tableTable">
										<thead>
											<tr>
												<!-- <th class="text-center">Action</th> -->
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">KD Armada</th>
												<th class="text-center">Plat Armada</th>
												<th class="text-center">No Certificate</th>
												<th class="text-center">Segment</th>
												<th class="text-center">Status</th>
												<th class="text-center">Cdate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>
							<div class="panel-body"> 
								<div class="row">
									<div class=" form-group col-lg-12 col-xs-12">
										<div class="form-group col-md-6">
											<label>Tahun</label>
											<select class="form-control select2 " style="width: 100%;" id="tahun" name="tahun">
												<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
												<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label>Divre</label>
											<center>
												<select class="form-control select2" style="width: 100%;" id="divisi" name="divisi">
													<option value="0">-- Pilih Divre --</option>	
													<option value="1">Divre 1</option>	
													<option value="2">Divre 2</option>	
													<option value="3">Divre 3</option>	
													<option value="4">Divre 4</option>	
													<option value="5">Semua Divre</option>	
												</select>
											</center>
										</div>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="asuransiTable">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">Pengajuan Cabang</th>
												<th class="text-center">Jaminan Bank</th>
												<th class="text-center">Belum Asuransi</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		$('#tahun').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransiTable.ajax.reload();
		});

		$('#divisi').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransiTable.ajax.reload();
		});



		$('#tahun_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransi_tableTable.ajax.reload();
		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransi_tableTable.ajax.reload();
		});

		$('#kategori_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransi_tableTable.ajax.reload();
		});

		var asuransiTable = $('#asuransiTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_asuransi/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"tahun" : $("#tahun").val(),
						"divisi" : $("#divisi").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "cabang" },
			{ data: "id_divre" },
			{ data: "pengajuan_cabang" },
			{ data: "jaminan_bank" },
			{ data: "pengajuan_cabang", render: function(data, type, full, meta){
				var total = parseInt(full['total_armada'])-(parseInt(data)+parseInt(full['jaminan_bank']));
				return $.fn.dataTable.render.number(',', '.', 0).display(parseInt(total));
			}},
			{ data: "total_armada" },
			]
		});

		var asuransi_tableTable = $('#asuransi_tableTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_asuransi_detail_armada/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"tahun" 	: $("#tahun_filter").val(),
						"id_bu" 	: $("#id_bu_filter").val(),
						"kategori" : $("#kategori_filter").val(),
					});
				}
			},
			columns: 
			[
			// { data: "id_armada", render: function(data, type, full, meta){
			// 	var str = '';
			// 	var id2 = "'"+data+"','"+full['kd_armada']+"','"+full['id_bu']+"'";

			// 	str += '<a type="button" class="btn btn-sm btn-primary" title="View Data" onclick="Ijin(' + id2 + ')"><i class="fa fa-list"></i> </a>';

			// 	if(session_level != 13){
			// 		return str;
			// 	}else{
			// 		return '';
			// 	}

			// }},
			{ data: "id_armada_asuransi", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "nm_bu" },
			{ data: "id_divre" },
			{ data: "kd_armada" },
			{ data: "plat_armada" },
			{ data: "no_certificate" },
			{ data: "kd_segment" },
			{ data: "status_armada" },
			{ data: "cdate" },

			]
		});


	</script>


</body>
</html>
