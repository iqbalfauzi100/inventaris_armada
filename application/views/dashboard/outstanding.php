<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>PEMBAYARAN ASURANSI</h1>
			</section>

		<!-- 	<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label>Tahun</label>
										<select class="form-control select2 " style="width: 100%;" id="tahun_filter" name="tahun_filter">
											<option value="0" >--All--</option>
											<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
											<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-4">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-4">
										<label>Kategori</label>
										<select class="form-control select2" style="width: 100%;" id="kategori_filter" name="kategori_print">
											<option value="0" >--All--</option>
											<option value="1" >1. Pengajuan Cabang</option>
											<option value="2" >2. Jaminan Bank</option>
											<option value="3" >3. Belum Terasuransi</option>
										</select>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="asuransi_tableTable">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">KD Armada</th>
												<th class="text-center">Plat Armada</th>
												<th class="text-center">No Certificate</th>
												<th class="text-center">Segment</th>
												<th class="text-center">Status</th>
												<th class="text-center">Cdate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		-->
		<section class="invoice">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">

						</div>
						<div class="panel-body"> 
							<div class="row">
								<div class=" form-group col-lg-12 col-xs-12">
									<div class="form-group col-md-6">
										<label>Tahun</label>
										<select class="form-control select2 " style="width: 100%;" id="tahun" name="tahun">
											<?php foreach ($combobox_tahun->result() as $rowmenu) { ?>
											<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label>Cabang</label>
										<center>
											<!-- <select class="form-control select2" style="width: 100%;" id="divisi" name="divisi">
												<option value="0">-- Pilih Divre --</option>	
												<option value="1">Divre 1</option>	
												<option value="2">Divre 2</option>	
												<option value="3">Divre 3</option>	
												<option value="4">Divre 4</option>	
												<option value="5">Semua Divre</option>	
											</select> -->

											<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
												<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
													<option value="0">--All Cabang--</option>
													<?php } ?>
												<?php foreach ($combobox_bu->result() as $rowmenu) { ?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php } ?>
											</select>
										</center>
									</div>
								</div>
							</div>
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="outstandingTable">
									<thead>
										<tr>
											<th class="text-center">No</th>
											<th class="text-center">Divre</th>
											<th class="text-center">Cabang</th>
											<th class="text-center">No.Polis</th>
											<th class="text-center">Nilai Premi</th>
											<th class="text-center">Nilai Bayar</th>
											<th class="text-center">Nilai Outstanding</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th width="100px" style="background-color: #81f781" d="tfood_nilai_premi"></th>
											<th width="100px" style="background-color: #81f781" d="tfood_nilai_bayar"></th>
											<th width="100px" style="background-color: #81f781" d="tfood_nilai_outstanding"></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>
</div>

<?= $this->load->view('basic_js'); ?>
<script type='text/javascript'>

	var session_level = "<?=$session_level;?>";

	$('#tahun').select2({
		'allowClear': true
	}).on("change", function (e) {
		outstandingTable.ajax.reload();
	});
	$('#id_bu').select2({
		'allowClear': true
	}).on("change", function (e) {
		outstandingTable.ajax.reload();
	});


	$('#id_bu_filter').select2({
		'allowClear': true
	}).on("change", function (e) {
		asuransi_tableTable.ajax.reload();
	});

	$('#kategori_filter').select2({
		'allowClear': true
	}).on("change", function (e) {
		asuransi_tableTable.ajax.reload();
	});

	var outstandingTable = $('#outstandingTable').DataTable({
		"ordering" : false,
		"scrollX": true,
		"processing": true,
		"serverSide": true,

		"createdRow": function( row, data, dataIndex ) {
			if ( data[2] == "1" ) {        
				$(row).addClass('red');

			}
		},

		dom: 'Bfrtip',
		lengthMenu: [
		[ 10, 25, 50, 100, 10000 ],
		[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
		],
		buttons: [
		'pageLength', 'copy', 'csv', 'excel', 
		],

		ajax: 
		{
			url: "<?= base_url()?>home/ax_data_datatable_outstanding/",
			type: 'POST',
			data: function ( d ) {
				return $.extend({}, d, { 
					"tahun" : $("#tahun").val(),
					"id_bu" : $("#id_bu").val(),
				});
			}
		},
		columns: 
		[
		{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
		{ data: "id_divre" },
		{ data: "cabang" },
		{ data: "no_polis" },
		{ data: "nilai_premi", render: $.fn.dataTable.render.number( ',', '.',2 ) },
		{ data: "nilai_bayar", render: $.fn.dataTable.render.number( ',', '.',2 ) },
		{ data: "nilai_outstanding", render: $.fn.dataTable.render.number( ',', '.',2 ) },
		],

		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};

			premi = api
			.column( 4 )
			.data()
			.reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );

			bayar = api
			.column( 5 )
			.data()
			.reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );

			outstanding = api
			.column( 6 )
			.data()
			.reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );

			$( api.column( 4 ).footer() ).html( formatNumber(parseInt(premi).toFixed(2)));
			$( api.column( 5 ).footer() ).html( formatNumber(parseInt(bayar).toFixed(2)));
			$( api.column( 6 ).footer() ).html( formatNumber(parseInt(outstanding).toFixed(2)));
		}
	});

	var asuransi_tableTableX = $('#asuransi_tableTableX').DataTable({
		"ordering" : false,
		"scrollX": true,
		"processing": true,
		"serverSide": true,

		"createdRow": function( row, data, dataIndex ) {
			if ( data[2] == "1" ) {        
				$(row).addClass('red');

			}
		},

		dom: 'Bfrtip',
		lengthMenu: [
		[ 10, 25, 50, 100, 10000 ],
		[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
		],
		buttons: [
		'pageLength', 'copy', 'csv', 'excel', 
		],

		ajax: 
		{
			url: "<?= base_url()?>home/ax_data_datatable_asuransi_detail_armada/",
			type: 'POST',
			data: function ( d ) {
				return $.extend({}, d, { 
					"tahun" 	: $("#tahun_filter").val(),
					"id_bu" 	: $("#id_bu_filter").val(),
					"kategori" : $("#kategori_filter").val(),
				});
			}
		},
		columns: 
		[
		{ data: "id_armada_asuransi", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
		{ data: "nm_bu" },
		{ data: "id_divre" },
		{ data: "kd_armada" },
		{ data: "plat_armada" },
		{ data: "no_certificate" },
		{ data: "kd_segment" },
		{ data: "status_armada" },
		{ data: "cdate" },

		]
	});

	function formatNumber(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
		}


</script>


</body>
</html>
