<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>ARMADA</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>
							<div class="panel-body">
								<div class="row">
									<div class=" form-group col-lg-12 col-xs-12">
										<div class="form-group col-md-12">
											<label>Cabang</label>
											<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
												<?php if($session_level==1 or $session_level==7 or $session_level==13) {?>
													<option value="0">--All Cabang--</option>
													<?php } ?>

												<?php
												foreach ($combobox_bu->result() as $rowmenu) {
													?>
													<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
													<?php
												}
												?>
											</select>
										</div>

									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="armadaTable">
										<thead>
											<tr>
												<th class="text-center">AKSI</th>
												<th class="text-center">NO</th>
												<th class="text-center">DIVRE</th>
												<th class="text-center">CABANG</th>
												<th class="text-center">KD ARMADA</th>
												<th class="text-center">PLAT</th>
												<th class="text-center">TAHUN</th>
												<th class="text-center">DEPAN KANAN</th>
												<th class="text-center">DEPAN KIRI</th>
												<th class="text-center">BELAKANG</th>
												<th class="text-center">DALAM</th>
												<th class="text-center">GESEK RANGKA</th>
												<th class="text-center">GESEK MESIN</th>
												<th width="50px" class="text-center">FOTO MESIN</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<!-- FOTO ARMADA -->
	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="addModalfotoarmada" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="form-add" id="addModalLabel">Form List Foto Armada</h4>


						</div>
						<div class="modal-body">
							<div class="row">
								<form id="submit">
									<input type="hidden" id="id_armada_foto" name="id_armada_foto" class="form-control">
									<input type="hidden" id="id_armada" name="id_armada" class="form-control">
									<div class="form-group col-lg-3">

										<label>Sisi</label>
										<select class="form-control" id="nm_armada_foto" name="nm_armada_foto">
											<option value="DEPAN KANAN" >DEPAN KANAN</option>
											<option value="DEPAN KIRI" >DEPAN KIRI</option>
											<option value="BELAKANG" >BELAKANG</option>
											<option value="DALAM" >DALAM</option>
											<option value="GESEK RANGKA">GESEK RANGKA</option>
											<option value="GESEK MESIN">GESEK MESIN</option>
											<option value="FOTO MESIN">FOTO MESIN</option> 
										</select>
									</div>
									<div class="form-group col-lg-6">
										<label>Nama Attachment</label>
										<input type="file" name="file" id="file_attachment" accept=".jpg, .JPG, .jpeg" class="form-control">
										<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
									</div>
									<div class="form-group col-lg-3">
										<label>_</label>
										<button type="submit" class="form-control btn btn-success" id=''>Save</button>
									</div>
								</form>

							</div>

							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="attachmentTable">
									<thead>
										<tr>
											<th>Aksi</th>
											<th>#</th>
											<th>Sisi</th>
											<th>Foto Armada</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">

							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			armadaTable.ajax.reload();
		});


		var armadaTable = $('#armadaTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_foto_armada/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_bu" : $("#id_bu_filter").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id_armada", render: function(data, type, full, meta){
				var str = '';
				str += '<a type="button" class="btn btn-sm btn-primary" title="View Data" onclick="Foto(' + data + ')"><i class="fa fa-truck"></i> </a>';

				if(session_level != 13){
					return str;
				}else{
					return '';
				}

			}},

			{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "id_divre" },
			{ data: "nm_bu" },
			{ data: "kd_armada" },
			{ data: "plat_armada" },
			{ data: "tahun_armada" },
			{ data: "depan_kanan" },
			{ data: "depan_kiri" },
			{ data: "belakang" },
			{ data: "dalam" },

			{ data: "gesek_rangka" },
			{ data: "gesek_mesin" },
			{ data: "foto_mesin" },
			]
		});

	</script>
	<script type="text/javascript">
		function Foto(id_armada)
		{
			$('#addModalfotoarmada').modal('show');
			$('#id_armada').val(id_armada);
			attachmentTable.ajax.reload();

			setTimeout(function(){ attachmentTable.columns.adjust().draw(); }, 1000);
		}

		var attachmentTable = $('#attachmentTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_foto/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 

						"id_armada": $("#id_armada").val(),

					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_foto", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
							// str += '<li><a onclick="'+kd+'"><i class="fa fa-pencil"></i> Edit</a></li>';
							str += '<li><a onClick="DeleteFoto(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							str += '</ul>';
							str += '</div>';
							if(session_level != 13){
								return str;
							}else{
								return '';
							}
						}
					},
					
					{ class:'intro', data: "id_armada_foto" },
					{ data: "nm_armada_foto" },
					// { data: "attachment" },
					{ data: "attachment", render: function(data, type, full, meta)
					{
						var url = "<?= base_url()?>"+"uploads/armada/foto/"+data;
						var op =  "window.open('"+url+"', '_blank');";
						return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';

					}

				},




				],
			});



			/// attachment
			$('#submit').submit(function(e){
				e.preventDefault(); 

				if($('#nm_attachment').val() == '')
				{
					alertify.alert("Warning", "Please fill Name.");
				}
				else if($('#file_attachment').val() == ''){
					alertify.alert("Warning", "Please choose Attachment.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_foto',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
                   //    success: function(data){
                   //        alert("Attachment Uploaded.");
                   // }
                 // });
             }).done(function(data, textStatus, jqXHR) {
             	var data = JSON.parse(data);
             	attachmentTable.ajax.reload();
							// $('#addModalattach').modal('hide');
							alertify.success('Attachment Uploaded.');
						});
         }
     });

			function DeleteFoto(id_armada_foto)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_foto';
						var data = {
							id_armada_foto: id_armada_foto
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								attachmentTable.ajax.reload();
								alertify.error('Foto Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}


		</script>
	</body>
	</html>
