<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>MASA BERLAKU STNK</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-6">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label>Kategori</label>
										<select class="form-control select2" style="width: 100%;" id="kategori_filter" name="kategori_print">
											<option value="0" >-- All Kategori --</option>
											<option value="1" >1. Masih Berlaku</option>
											<option value="2" >2. Habis Dalam 30 Hari</option>
											<option value="3" >3. Habis Masa Berlaku</option>
											<option value="4" >4. Belum Upload STNK</option>
										</select>
									</div>
									<!-- <div class="form-group col-md-2">
										<label>Print</label>
										<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
											<option value="html" >1. HTML</option>
											<option value="pdf" >2. PDF</option>
											<option value="excell" >3. Excell</option>
										</select>
									</div>
									<div class="form-group col-md-2">
										<p style="height: 15px"></p>
										<button class="btn btn-info" title="Print" onclick='printCetak()'>
											<i class='fa fa-copy'></i> Print
										</button>
									</div> -->
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="stnk_tableTable">
										<thead>
											<tr>
												<th class="text-center">Action</th>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">KD Armada</th>
												<th class="text-center">Plat Armada</th>
												<th class="text-center">Tgl Exp</th>
												<th class="text-center">Segment</th>
												<th class="text-center">Status</th>
												<th class="text-center">Cdate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>
							<div class="panel-body">
								<div class="row">
									<div class=" form-group col-lg-12 col-xs-12">
										<div class="form-group col-md-12">
											<center>
												<select class="form-control select2" style="width: 100%;" id="divisi" name="divisi">
													<!-- <option value="0">-- Pilih Divre --</option>	 -->
													<option value="1">Divre 1</option>	
													<!-- <option value="2">Divre 2</option>	
													<option value="3">Divre 3</option>	
													<option value="4">Divre 4</option>	
													<option value="5">Semua Divre</option>	 -->
												</select>
											</center>
										</div>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="stnkTable">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">Berlaku</th>
												<th class="text-center">Habis Dalam 30 Hari</th>
												<th class="text-center">Habis Masa Berlaku</th>
												<th class="text-center">Belum Upload</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			


		</div>
	</div>





	<!-- FOTO STNK -->
	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="addModalfotostnk" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="form-add" id="addModalLabel">Form List Foto STNK</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<form id="submit_stnk">
									<input type="hidden" id="id_armada_stnk" name="id_armada_stnk" class="form-control">
									<input type="hidden" id="id_armadastnk" name="id_armadastnk" class="form-control">
									<div class="form-group col-lg-3">

										<label>Tanggal EXP</label>
										<input type="text" id="tgl_exp_stnk" name="tgl_exp_stnk" class="form-control" placeholder="yyyy-mm-dd">
									</div>
									<div class="form-group col-lg-2">
										<label>Masa</label>
										<select class="form-control " style="width: 100%;" id="masa" name="masa">
											<option value="1">1</option>
											<option value="5">5</option>
										</select>
									</div>
									<div class="form-group col-lg-4">
										<label>Nama Attachment</label>
										<input type="file" name="file" id="file_attachment_stnk" accept=".jpeg,.jpeg" class="form-control">
										<p class="help-block">Upload Foto Armada. Format File: Jpg.  Size Max: 500KB   </p>
									</div>
									<div class="form-group col-lg-3">
										<label>_</label>
										<button type="submit" class="form-control btn btn-success" id=''>Save</button>
									</div>
								</form>

							</div>

							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="stnkModalTable">
									<thead>
										<tr>
											<th>Aksi</th>
											<th>#</th>
											<th>Tanggal Kadaluarsa</th>
											<th>Masa</th>
											<th>Foto STNK</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">

							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		$('#divisi').select2({
			'allowClear': true
		}).on("change", function (e) {
			stnkTable.ajax.reload();
		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			stnk_tableTable.ajax.reload();
		});

		$('#kategori_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			stnk_tableTable.ajax.reload();
		});

		var stnkTable = $('#stnkTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_stnk/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"divisi" : $("#divisi").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "cabang" },
			{ data: "id_divre" },
			{ data: "lebih30" },
			{ data: "kurang30" },
			{ data: "habis_masa" },
			{ data: "lebih30", render: function(data, type, full, meta){
				var total = parseInt(full['total_armada'])-(parseInt(data)+parseInt(full['kurang30'])+parseInt(full['habis_masa']));
				return $.fn.dataTable.render.number(',', '.', 0).display(parseInt(total));
			}},
			{ data: "total_armada" },
			]
		});

		var stnk_tableTable = $('#stnk_tableTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_stnk_detail_armada/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_bu" 	: $("#id_bu_filter").val(),
						"kategori" : $("#kategori_filter").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id_armada", render: function(data, type, full, meta){
				var str = '';
				str += '<a type="button" class="btn btn-sm btn-primary" title="View Data" onclick="Stnk(' + data + ')"><i class="fa fa-list"></i> </a>';
				
				if(session_level != 13){
					return str;
				}else{
					return '';
				}
				
			}},
			{ data: "id_armada_stnk", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "nm_bu" },
			{ data: "id_divre" },
			{ data: "kd_armada" },
			{ data: "plat_armada" },
			{ data: "tgl_exp_stnk" },
			{ data: "kd_segment" },
			{ data: "status_armada" },
			{ data: "cdate" },

			]
		});


	</script>
	<script type="text/javascript">

		$( "#tgl_exp_stnk").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});
		$( "#tgl_exp_stnk" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		function Stnk(id_armada)
		{
			$('#addModalfotostnk').modal('show');
			$('#id_armada_stnk').val('');
			$('#tgl_exp_stnk').val('');
			$('#masa').val('1');

			$('#id_armadastnk').val(id_armada);
			stnkModalTable.ajax.reload();

			setTimeout(function(){ stnkModalTable.columns.adjust().draw(); }, 1000);
		}

		var stnkModalTable = $('#stnkModalTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_stnk/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 

						"id_armada": $("#id_armadastnk").val(),

					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_stnk", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onclick="EditStnk(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
					str += '<li><a onClick="DeleteStnk(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					return str;
				}
			},

			{ class:'intro', data: "id_armada_stnk" },
			{ data: "tgl_exp_stnk" },
			{ data: "masa" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/stnk/"+data;
				var op =  "window.open('"+url+"', '_blank');";
				return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';

			}

		},




		],
	});



			/// attachment
			$('#submit_stnk').submit(function(e){
				e.preventDefault(); 

				if($('#tgl_exp_stnk').val() == '')
				{
					alertify.alert("Warning", "Please fill Expired Date.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_stnk',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						if(data['status'] == "success")
						{
							stnkModalTable.ajax.reload();
							alertify.success('Attachment Uploaded.');
							$('#id_armada_stnk').val('');
							$('#tgl_exp_stnk').val('');
							$('#masa').val('1');
						}else{
							alertify.alert("Peringatan", "Silahkan pilih/upload file terlebih dahulu.");
						}
					});
				}
			});

			function EditStnk(id_armada_stnk)
			{
				var url = '<?=base_url()?>armada/ax_get_data_by_id_stnk';
				var data = {
					id_armada_stnk: id_armada_stnk
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);

					$('#id_armada_stnk').val(id_armada_stnk);
					$('#tgl_exp_stnk').val(data['tgl_exp_stnk']);
					$('#masa').val(data['masa']);
				});
			}

			function DeleteStnk(id_armada_stnk)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_stnk';
						var data = {
							id_armada_stnk: id_armada_stnk
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								stnkModalTable.ajax.reload();
								alertify.error('STNK Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}
		</script>
	</body>
	</html>
