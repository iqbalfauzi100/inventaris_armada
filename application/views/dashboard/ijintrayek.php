<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>
<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>MASA BERLAKU IJIN TRAYEK</h1>
			</section>

			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-6">
										<label>Cabang</label>
										<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
											<?php if($session_level==1 or $session_level==7) {?>
											<option value="0">--All Cabang--</option>
											<?php } ?>

											<?php
											foreach ($combobox_bu->result() as $rowmenu) {
												?>
												<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label>Kategori</label>
										<select class="form-control select2" style="width: 100%;" id="kategori_filter" name="kategori_print">
											<option value="0" >-- All Kategori --</option>
											<option value="1" >1. Masih Berlaku</option>
											<option value="2" >2. Habis Dalam 30 Hari</option>
											<option value="3" >3. Habis Masa Berlaku</option>
											<option value="4" >4. Belum Upload</option>
										</select>
									</div>
									<!-- <div class="form-group col-md-2">
										<label>Print</label>
										<select class="form-control " style="width: 100%;" id="jenis_print" name="jenis_print">
											<option value="html" >1. HTML</option>
											<option value="pdf" >2. PDF</option>
											<option value="excell" >3. Excell</option>
										</select>
									</div>
									<div class="form-group col-md-2">
										<p style="height: 15px"></p>
										<button class="btn btn-info" title="Print" onclick='printCetak()'>
											<i class='fa fa-copy'></i> Print
										</button>
									</div> -->
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="ijintrayek_tableTable">
										<thead>
											<tr>
												<th class="text-center">Action</th>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">KD Armada</th>
												<th class="text-center">Plat Armada</th>
												<th class="text-center">Tgl Exp</th>
												<th class="text-center">Segment</th>
												<th class="text-center">Status</th>
												<th class="text-center">Cdate</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">

							</div>
							<div class="panel-body">
								<div class="row">
									<div class=" form-group col-lg-12 col-xs-12">
										<div class="form-group col-md-12">
											<center>
												<select class="form-control select2" style="width: 100%;" id="divisi" name="divisi">
													<!-- <option value="0">-- Pilih Divre --</option>	 -->
													<option value="1">Divre 1</option>	
													<!-- <option value="2">Divre 2</option>	
													<option value="3">Divre 3</option>	
													<option value="4">Divre 4</option>	
													<option value="5">Semua Divre</option>	 -->
												</select>
											</center>
										</div>
									</div>
								</div>
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="ijintrayekTable">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Cabang</th>
												<th class="text-center">Divre</th>
												<th class="text-center">Berlaku</th>
												<th class="text-center">Habis Dalam 30 Hari</th>
												<th class="text-center">Habis Masa Berlaku</th>
												<th class="text-center">Belum Upload</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</div>



	<!-- IJIN TRAYEK -->
	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="addModalijintrayek" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="form-add" id="addModalLabel">Form List Ijin Trayek</h4>


						</div>
						<div class="modal-body">
							<div class="row">
								<form id="submit_ijintrayek">
									<input type="hidden" id="id_armada_ijintrayek" name="id_armada_ijintrayek" class="form-control">
									<input type="hidden" id="id_armadaijintrayek" name="id_armadaijintrayek" class="form-control">

									<div class="form-group col-lg-5">
										<label>Trayek</label>
										<select class="form-control select2" style="width: 100%;" id="id_trayek" name="id_trayek">
										</select>
									</div>

									<div class="form-group col-lg-2">
										<label>Berlaku Sampai Tgl</label>
										<input type="text" id="tgl_exp_ijintrayek" name="tgl_exp_ijintrayek" class="form-control" placeholder="yyyy-mm-dd">
									</div>
									<div class="form-group col-lg-3">
										<label>Nama Attachment</label>
										<input type="file" name="file" id="file_attachment_ijintrayek" accept=".jpg, .JPG, .jpeg, .pdf, .png" class="form-control">
										<p class="help-block">Upload Foto Ijin Trayek. Format File: Jpg.pdf  Size Max: 500KB </p>
									</div>
									<div class="form-group col-lg-2">
										<label>_</label>
										<button type="submit" class="form-control btn btn-success" id=''>Save</button>
									</div>
								</form>

							</div>

							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="ijintrayekModalTable">
									<thead>
										<tr>
											<th>Aksi</th>
											<th>#</th>
											<th>Nm Trayek</th>
											<th>Tanggal Berlaku</th>
											<th>Foto</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">

							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>

		var session_level = "<?=$session_level;?>";

		$('#divisi').select2({
			'allowClear': true
		}).on("change", function (e) {
			ijintrayekTable.ajax.reload();
		});

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			ijintrayek_tableTable.ajax.reload();
		});

		$('#kategori_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			ijintrayek_tableTable.ajax.reload();
		});

		var ijintrayekTable = $('#ijintrayekTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_ijintrayek/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"divisi" : $("#divisi").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "cabang" },
			{ data: "id_divre" },
			{ data: "lebih30" },
			{ data: "kurang30" },
			{ data: "habis_masa" },
			{ data: "lebih30", render: function(data, type, full, meta){
				var total = parseInt(full['total_armada'])-(parseInt(data)+parseInt(full['kurang30'])+parseInt(full['habis_masa']));
				return $.fn.dataTable.render.number(',', '.', 0).display(parseInt(total));
			}},
			{ data: "total_armada" },
			]
		});

		var ijintrayek_tableTable = $('#ijintrayek_tableTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			"createdRow": function( row, data, dataIndex ) {
				if ( data[2] == "1" ) {        
					$(row).addClass('red');

				}
			},

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
			'pageLength', 'copy', 'csv', 'excel', 
			],

			ajax: 
			{
				url: "<?= base_url()?>home/ax_data_datatable_ijintrayek_detail_armada/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_bu" 	: $("#id_bu_filter").val(),
						"kategori" : $("#kategori_filter").val(),
					});
				}
			},
			columns: 
			[
			{ data: "id_armada", render: function(data, type, full, meta){
				var str = '';
				var id2 = "'"+data+"','"+full['kd_armada']+"','"+full['id_bu']+"'";

				str += '<a type="button" class="btn btn-sm btn-primary" title="View Data" onclick="Ijin(' + id2 + ')"><i class="fa fa-list"></i> </a>';
				
				if(session_level != 13){
					return str;
				}else{
					return '';
				}
				
			}},
			{ data: "id_armada_ijintrayek", render: function (data, type, row, meta) { return meta.row + meta.settings._iDisplayStart + 1; }},
			{ data: "nm_bu" },
			{ data: "id_divre" },
			{ data: "kd_armada" },
			{ data: "plat_armada" },
			{ data: "tgl_exp" },
			{ data: "kd_segment" },
			{ data: "status_armada" },
			{ data: "cdate" },

			]
		});


	</script>

	<script type="text/javascript">
		$( "#tgl_exp_ijintrayek").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});
		$( "#tgl_exp_ijintrayek" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		function Ijin(id_armada,kd_armada,id_bu)
		{
			$('#addModalijintrayek').modal('show');
			$('#id_armadaijintrayek').val(id_armada);
			combo_trayek(id_bu);
			ijintrayekModalTable.ajax.reload();

			setTimeout(function(){ ijintrayekModalTable.columns.adjust().draw(); }, 1000);
		}


		var ijintrayekModalTable = $('#ijintrayekModalTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>armada/ax_data_ijintrayek/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 

						"id_armada": $("#id_armadaijintrayek").val(),

					});
				}
			},
			columns: 
			[
			{
				data: "id_armada_ijintrayek", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					var kd = "Foto('"+full['kd_armada']+"')";
					str += '<li><a onClick="Deleteijintrayek(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					return str;
				}
			},

			{ class:'intro', data: "id_armada_ijintrayek" },
			{ data: "nm_trayek" },
			{ data: "tgl_exp" },
			{ data: "attachment", render: function(data, type, full, meta)
			{
				var url = "<?= base_url()?>"+"uploads/armada/ijintrayek/"+data;
				var op =  "window.open('"+url+"', '_blank');";
						// return '<img width="200" height="100" class="attachment-img" onclick="'+op+'" src="'+url+'" alt="No Image">';
						return '<a href="'+url+'" target="_blank">Lihat attachment</a>';
					}

				},




				],
			});



			/// attachment
			$('#submit_ijintrayek').submit(function(e){
				e.preventDefault(); 

				if($('#id_trayek').val() == '0'){
					alertify.alert("Warning", "Pilih Trayek.");
				}else if($('#tgl_exp_ijintrayek').val() == ''){
					alertify.alert("Warning", "Tgl Berlaku tidak boleh kosong.");
				}else if($('#file_attachment_ijintrayek').val() == ''){
					alertify.alert("Warning", "Attachment tidak boleh kosong.");
				}
				else
				{
					$.ajax({
						url:'<?php echo base_url();?>armada/ax_upload_data_ijintrayek',
						type:"post",
						data:new FormData(this),
						processData:false,
						contentType:false,
						cache:false,
						async:false,
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						ijintrayekModalTable.ajax.reload();
						alertify.success('Attachment Uploaded.');
					});
				}
			});

			function Deleteijintrayek(id_armada_ijintrayek)
			{
				alertify.confirm(
					'Confirmation', 
					'Are you sure you want to delete this data?', 
					function(){
						var url = '<?=base_url()?>armada/ax_unset_ijintrayek';
						var data = {
							id_armada_ijintrayek: id_armada_ijintrayek
						};

						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							if(data['status'] == "success")
							{
								ijintrayekModalTable.ajax.reload();
								alertify.error('KEUR Armada Dihapus.');
							}
						});
					},
					function(){ }
					);
			}

			function combo_trayek(id_bu){
				$.ajax({
					type: "POST", 
					url: "<?= base_url() ?>armada/ax_get_trayek", 
					data: {
						id_bu : id_bu,
					},
					dataType: "json",
					beforeSend: function(e) {
						if(e && e.overrideMimeType) {
							e.overrideMimeType("application/json;charset=UTF-8");
						}
					},
					success: function(response){ 
						$("#id_trayek").html(response.data_trayek).show();
						$('#select2-id_trayek-container').html("--Pilih Trayek--");
			// $('#id_trayek').val(kd_trayek);
		},
		error: function (xhr, ajaxOptions, thrownError) { 
			alert(thrownError); 
		}
	});
			}


		</script>
	</body>
	</html>
