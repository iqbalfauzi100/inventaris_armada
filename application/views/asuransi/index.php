<!DOCTYPE html>
<html>
<head>
	<?= $this->load->view('head'); ?>
</head>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color')?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Asuransi</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								
								<div class="modal fade" id="addModal" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabel">Form Add Asuransi</h4>
											</div>
											<div class="modal-body">
												<input type="hidden" id="id_asuransi" name="id_asuransi" value='' />

												<div class="col-lg-6">
													<div class="form-group">
														<label>Tahun Asuransi</label>
														<select class="form-control select2" style="width: 100%;" id="tahun" name="tahun">
															<?php
															foreach ($combobox_tahun->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
																<?php
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Cabang</label>
														<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
															<option value="0">--All Cabang--</option>
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group">
														<label>Jenis Asuransi</label>
														<select class="form-control select2 " style="width: 100%;" id="id_jenis_asuransi" name="id_jenis_asuransi">
															<?php
															foreach ($combobox_jenis_asuransi->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_jenis_asuransi?>"  ><?= $rowmenu->nm_jenis_asuransi?></option>
																<?php
															}
															?>
														</select>
													</div>

													<div class="form-group">
														<label>Nama Polis/Insured</label>
														<input type="text" id="insured" name="insured" class="form-control" placeholder="Nama Polis/Insured" autocomplete="off">
													</div>
													<div class="form-group">
														<label>Nomor Polis</label>
														<input type="text" id="no_polis" name="no_polis" class="form-control" placeholder="Nomor Polis" autocomplete="off">
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group col-md-6">
														<label>Tanggal Awal</label>
														<input type="text" id="tgl_awal" name="tgl_awal" class="form-control" placeholder="Tanggal Awal" value="<?php echo date('Y-m-d');?>">
													</div>
													<div class="form-group  col-md-6">
														<label>Tanggal Akhir</label>
														<input type="text" id="tgl_akhir" name="tgl_akhir" class="form-control" placeholder="Tanggal Akhir" value="<?php echo date('Y-m-d');?>">
													</div>

													<div class="form-group">
														<label>Nilai Premi</label>
														<input type="number" id="nilai_premi" name="nilai_premi" class="form-control" placeholder="Nilai Premi" autocomplete="off">
													</div>
													<div class="form-group">
														<label>Active</label>
														<select class="form-control" id="active" name="active">
															<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
															<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
														</select>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSave'>Save</button>
											</div>
										</div>
									</div>
								</div>

								<div class="modal fade" id="addModalDetail" tabindex="" role="dialog" aria-labelledby="addModalLabelDetail" aria-hidden="true">
									<div class="modal-dialog  modal-lg">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="Form-add-bu" id="addModalLabelDetail">Form Add Asuransi</h4>
											</div>
											<div class="modal-body">
												<input type="hidden" id="id_asuransi_header" name="id_asuransi_header" value='' />
												<input type="hidden" id="id_asuransi_detail" name="id_asuransi_detail" value='' />
												<div class="col-lg-6">
													<div class="form-group">
														<label>Nomor Certificate</label>
														<input type="text" id="no_certificate" name="no_certificate" class="form-control" placeholder="Nomor Certificate" autocomplete="off">
													</div>
													<div class="form-group">
														<label>Armada</label>
														<select class="form-control select2" style="width: 100%;" id="id_armada" name="id_armada">
															<option value="0">--Armada--</option>
															<?php
															foreach ($combo_armada->result() as $rowmenu) {
																?>
																<option value="<?= $rowmenu->id_armada?>"  ><?= $rowmenu->kd_armada?></option>
																<?php
															}
															?>
														</select>
													</div>
													<div class="form-group">
														<label>Waktu Kejadian</label>
														<input type="text" id="waktu_kejadian" name="waktu_kejadian" class="form-control" placeholder="yyyy-mm-dd">
													</div>

													<div class="form-group">
														<label>Estimasi Perbaikan</label>
														<input type="number" id="estimasi_perbaikan" name="estimasi_perbaikan" class="form-control" placeholder="Estimasi Perbaikan">
													</div>
													<div class="form-group">
														<label>Klaim Disetujui</label>
														<input type="number" id="klaim_disetujui" name="klaim_disetujui" class="form-control" placeholder="Klaim Disetujui">
													</div>
												</div>

												<div class="col-lg-6">
													<div class="form-group">
														<label>THJ III</label>
														<input type="number" id="tjh_3" name="tjh_3" class="form-control" placeholder="THJ III">
													</div>
													<div class="form-group">
														<label>Own Risk</label>
														<input type="number" id="own_risk" name="own_risk" class="form-control" placeholder="Own Risk">
													</div>

													<div class="form-group">
														<label>Status</label>
														<select class="form-control" style="width: 100%;" id="status" name="status">
															<option value="PROGRESS">PROGRESS</option>
															<option value="SELESAI">SELESAI</option>
														</select>
													</div>

													<div class="form-group">
														<label>Active</label>
														<select class="form-control" id="active_detail" name="active_detail">
															<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
															<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
														</select>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary" id='btnSaveDetail'>Save</button>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="panel-body">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active disabled"><a href="#tab_1" class="disabled" data-toggle="tab" aria-expanded="true">List Asuransi</a></li>
										<li class=" disabled"><a href="#tab_2" class="disabled" aria-expanded="false">Asuransi Detail</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1">

											<div class="dataTable_wrapper">

												<div class="row">
													<div class="col-md-12">

														<div class="form-group col-lg-4">
															<select class="form-control select2" style="width: 100%;" id="tahun_filter" name="tahun_filter">
																<?php
																foreach ($combobox_tahun->result() as $rowmenu) {
																	?>
																	<option value="<?= $rowmenu->tahun?>"  ><?= $rowmenu->tahun?></option>
																	<?php
																}
																?>
															</select>
														</div>

														<div class="form-group col-lg-4">
															<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
																<option value="0">--All Cabang--</option>
																<?php foreach ($combobox_bu->result() as $rowmenu) { ?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php } ?>
															</select>
														</div>

														<div class="form-group col-lg-4">
															<?php if($session_level == 1 || $session_level == 7){ ?>
															<button class="btn btn-primary pull-right" onclick='ViewData(0)'>
																<i class='fa fa-plus'></i> Add Asuransi
															</button>
															<?php } ?>

															<div class="btn-group">
																<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-print"></i> Print <span class="caret"></span></button>
																<ul class="dropdown-menu">
																	<li><a onclick="print_laporan(1)"><i class="fa fa-print"></i> PDF</a></li>
																	<li><a onclick="print_laporan(2)"><i class="fa fa-print"></i> Excell</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>


												<!-- <div class="row">
													<div class="col-lg-12">
														<div class="form-group col-lg-4">
															<label>Cabang</label>
															<select class="form-control select2 " style="width: 100%;" id="id_bu_filter" name="id_bu_filter">
																<option value="0">--All Cabang--</option>
																<?php foreach ($combobox_bu->result() as $rowmenu) { ?>
																<option value="<?= $rowmenu->id_bu?>"  ><?= $rowmenu->nm_bu?></option>
																<?php } ?>
															</select>
														</div>
													</div>
												</div> -->

												<table class="table table-striped table-bordered table-hover" id="asuransiTable">
													<thead>
														<tr>
															<th>Options</th>
															<th>#</th>
															<th width="170px">Cabang</th>
															<th width="100px">Jenis Asuransi</th>
															<th width="250px">Insured</th>
															<th>No.Polis</th>
															<th>Tahun</th>
															<th>Tgl Awal</th>
															<th>Tgl Akhir</th>
															<th>Status Exp</th>
															<th width="100px">Nilai Premi</th>
															<th width="100px">Nilai Bayar</th>
															<th width="100px">Nilai Outstanding</th>
														</tr>
													</thead>
													<tfoot>
														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>

															<th>JUMLAH</th>
															<th width="100px" style="background-color: #81f781" d="tfood_nilai_premi"></th>
															<th width="100px" style="background-color: #81f781" d="tfood_nilai_bayar"></th>
															<th width="100px" style="background-color: #81f781" d="tfood_nilai_outstanding"></th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<div class="tab-pane" id="tab_2">
											<div class="row">
												<div class="col-lg-12">
													<?php if($session_level == 1 || $session_level == 7) { ?>
														<button class="btn btn-primary  pull-right" onclick='ViewDataDetail(0)'>
															<i class='fa fa-plus'></i> Add Asuransi Detail
														</button>
													<?php } else {

													}?>
													<button type="button" class="btn bg-purple btn-default" onClick='closeTab()'><i class="fa  fa-arrow-circle-left"></i> Kembali</button>	
												</div>
											</div>
											<br>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="asuransiDetail">

													<thead>
														<tr>
															<th>Options</th>
															<th>#</th>
															<th>No. Certificate</th>
															<th>Nomor Polis</th>
															<th>Kd Armada</th>
															<th>Plat Polisi</th>
															<th>Waktu Kejadian</th>
															<th>Estimasi Perbaikan</th>
															<th>Klaim Disetujui</th>
															<th>TJH III</th>
															<th>Own Risk</th>
															<th><b><font color="blue">Net Klaim</font></b></th>
															<th>Status</th>
															<th>Active</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
		</div>
	</div>


	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="modalDocuments" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<div class="form-inline">
								<h4 id="addModalLabel">List Documents</h4>
								<button class="btn btn-primary btn-sm" onclick="addDocuments()"><i class='fa fa-plus'></i> Tambah Documents</button>
							</div>
							<input type="hidden" id="id_asuransi_header_documents" name="id_asuransi_header_documents" class="form-control">
						</div>
						<div class="modal-body">
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataDocumentsTable">
									<thead>
										<tr>
											<th>Action</th>
											<th>#</th>
											<th>Documents</th>
											<th>File</th>
											<th>Active</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="addDocumentsModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="Form-add-bu" id="addDocumentsModalLabel">Form Add</h4>
				</div>
				<div class="modal-body">
					<form id="formAddDocuments">
						<input type="hidden" id="id_asuransi_header_documents_add" name="id_asuransi_header_documents_add" value='' />
						<input type="hidden" id="id_asuransi_documents" name="id_asuransi_documents" value='' />
						<div class="form-group">
							<label>Nama Documents</label>
							<input type="text" id="nm_documents" name="nm_documents" class="form-control" placeholder="Nama Documents">
						</div>
						<div class="form-group">
							<label>File</label>
							<input type="file" id="file" name="file" class="form-control" placeholder="Pilih File">
						</div>
						<div class="form-group">
							<label>Active</label>
							<select class="form-control" id="active_documents" name="active_documents">
								<option value="1" <?php echo set_select('myselect', '1', TRUE); ?> >Active</option>
								<option value="0" <?php echo set_select('myselect', '0'); ?> >Not Active</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id='btnSaveAddDocuments'>Save</button>
				</div>
			</div>
		</div>
	</div>

	<!-- DETAIL NILAI PEMBAYARAN -->
	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="modalBayar" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="Form-add-bu" id="addBayarModalLabel">Detail Pembayaran Premi</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<form id="submit_bayar">
									<input type="hidden" id="id_asuransi_header_bayar" name="id_asuransi_header_bayar" class="form-control">
									<div class="form-group col-lg-4">
										<label>Tanggal Bayar</label>
										<input type="text" id="tgl_bayar" name="tgl_bayar" class="form-control" placeholder="yyyy-mm-dd" autocomplete="off">
									</div>
									<div class="form-group col-lg-4">
										<label>Total Bayar</label>
										<input type="number" id="total_bayar" name="total_bayar" class="form-control" placeholder="Total Bayar" autocomplete="off">
									</div>
									<div class="form-group col-lg-4">
										<label>_</label>
										<button type="submit" class="form-control btn btn-success" id=''>Save</button>
									</div>
								</form>
							</div>
							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataBayar">
									<thead>
										<tr>
											<th>Action</th>
											<th>#</th>
											<th>Tgl Bayar</th>
											<th>Total Bayar</th>
											<th>Cdate</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- DETAIL HISTORY UPDATE PREMI -->
	<div class="row" >
		<div class="col-lg-12">
			<div class="modal fade" id="modalHistoryPremi" tabindex="" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="Form-add-bu" id="addBayarModalLabel">Detail History Update Nilai Premi</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<form id="submit_premi">
									<input type="hidden" id="id_asuransi_header_history_premi" name="id_asuransi_header_history_premi" class="form-control">
									<div class="form-group col-lg-3">
										<label>Nilai Premi (Penambahan)</label>
										<input type="number" id="nilai_premi_tambah" name="nilai_premi_tambah" class="form-control" placeholder="Nilai Premi (Penambahan)" autocomplete="off">
									</div>
									<div class="form-group col-lg-3">
										<label>Nilai Premi (Pengurangan)</label>
										<input type="number" id="nilai_premi_kurang" name="nilai_premi_kurang" class="form-control" placeholder="Nilai Premi (Pengurangan)" autocomplete="off">
									</div>
									<div class="form-group col-lg-4">
										<label>Keterangan</label>
										<input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" autocomplete="off">
									</div>
									<div class="form-group col-lg-2">
										<label>_</label>
										<button type="submit" class="form-control btn btn-success" id=''>Save</button>
									</div>
								</form>
							</div>

							<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataHistoryPremi">
									<thead>
										<tr>
											<th>Action</th>
											<th>#</th>
											<th>Nilai Premi(Penambahan)</th>
											<th>Nilai Premi(Pengurangan)</th>
											<th>Keterangan</th>
											<th>Cdate</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>
		var save_method;
		var base_url = '<?php echo base_url();?>';

		var session_level = '<?=$session_level;?>';



		var asuransiTable = $('#asuransiTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,

			dom: 'Bfrtip',
			lengthMenu: [
			[ 10, 25, 50, 100, 10000 ],
			[ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
			],
			buttons: [
					'pageLength', 'copy', 'csv', 'excel', //'pdf', 'print'
					],
					ajax: 
					{
						url: "<?= base_url()?>asuransi/ax_data_asuransi/",
						type: 'POST',
						data: function ( d ) {
							return $.extend({}, d, { 
								"id_bu": $("#id_bu_filter").val(),
								"tahun": $("#tahun_filter").val(),
							});
						}
					},
					columns:
					[
					{
						data: "id_asuransi", render: function(data, type, full, meta){
							// var str = '';
							// str += '<div class="btn-group">';
							// str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
							// str += '<ul class="dropdown-menu">';
							// str += '<li><a onclick="DetailData(' + data + ')"><i class="fa fa-list"></i> Detail</a></li>';
							// str += '<li><a onclick="DetailBayar(' + data + ')"><i class="fa fa-usd"></i> Detail Pembayaran</a></li>';
							// str += '<li><a onclick="DetailHistoryPremi(' + data + ')"><i class="fa fa-list"></i> History Update Premi</a></li>';
							// str += '<li><a onclick="Documents(' + data + ')"><i class="fa fa-file"></i> Documents</a></li>';
							// str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
							// str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
							// str += '</ul>';
							// str += '</div>';
							// if(session_level == 1 || session_level == 7){
							// 	return str;
							// }else{
							// 	return '';
							// }
							var str = '';
							if(session_level == 1 || session_level == 7){
								str += '<div class="btn-group">';
								str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
								str += '<ul class="dropdown-menu">';
								str += '<li><a onclick="DetailData(' + data + ')"><i class="fa fa-list"></i> Detail</a></li>';
								str += '<li><a onclick="DetailBayar(' + data + ')"><i class="fa fa-usd"></i> Detail Pembayaran</a></li>';
								str += '<li><a onclick="DetailHistoryPremi(' + data + ')"><i class="fa fa-list"></i> History Update Premi</a></li>';
								str += '<li><a onclick="Documents(' + data + ')"><i class="fa fa-file"></i> Documents</a></li>';
								str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
								str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
								str += '</ul>';
								str += '</div>';
							} else if(session_level == 3 ){
								str += '<div class="btn-group">';
								str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
								str += '<ul class="dropdown-menu">';
								str += '<li><a onclick="DetailData(' + data + ')"><i class="fa fa-list"></i> Detail</a></li>';
								str += '</ul>';
								str += '</div>';
							} else if(session_level == 13){
								str += '<div class="btn-group">';
								str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
								str += '<ul class="dropdown-menu">';
								str += '<li><a onclick="DetailData(' + data + ')"><i class="fa fa-list"></i> Detail</a></li>';
								str += '</ul>';
								str += '</div>';
							} else {
								str += '';
							}
							return str;
						}
					},

					{ data: "id_asuransi" },
					{ data: "nm_bu" },
					{ data: "nm_jenis_asuransi" },
					{ data: "insured" },
					{ data: "no_polis" },
					{ data: "tahun" },
					{ data: "tgl_awal" },
					{ data: "tgl_akhir" },
					{ class: "intro",data: "status_expired", render: function(data, type, full, meta){
						if(data == 1)
							return '<span class="label label-success">Active</span>';
						else return '<span class="label label-warning">Expired</span>';
					}},

					{ data: "nilai_premi", render: $.fn.dataTable.render.number( ',', '.',0 ) },
					{ data: "nilai_bayar", render: $.fn.dataTable.render.number( ',', '.',0 ) },
					{ data: "nilai_outstanding", render: $.fn.dataTable.render.number( ',', '.',0 ) },
					],

					"footerCallback": function ( row, data, start, end, display ) {
						var api = this.api(), data;

						var intVal = function ( i ) {
							return typeof i === 'string' ?
							i.replace(/[\$,]/g, '')*1 :
							typeof i === 'number' ?
							i : 0;
						};

						premi = api
						.column( 10 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );

						bayar = api
						.column( 11 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );

						outstanding = api
						.column( 12 )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );

						$( api.column( 10 ).footer() ).html( formatNumber(parseInt(premi).toFixed(2)));
						$( api.column( 11 ).footer() ).html( formatNumber(parseInt(bayar).toFixed(2)));
						$( api.column( 12 ).footer() ).html( formatNumber(parseInt(outstanding).toFixed(2)));
					}
				});

		var asuransiDetail = $('#asuransiDetail').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>asuransi/ax_data_asuransi_detail/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_asuransi": $("#id_asuransi_header").val()
					});
				}
			},
			columns: 
			[
			{
				data: "id_asuransi_detail", render: function(data, type, full, meta){
					var str = '';
					if(session_level == 1 || session_level == 7){
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						str += '<li><a onclick="ViewDataDetail(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
						str += '<li><a onClick="DeleteDataDetail(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						str += '</ul>';
						str += '</div>';	
					} else {
						str += '';
					}
					return str;
				}
			},

			{ data: "id_asuransi_detail" },
			{ data: "no_certificate" },
			{ data: "no_polis" },
			{ data: "kd_armada" },
			{ data: "plat_armada" },

			{ data: "waktu_kejadian" },

			{ data: "estimasi_perbaikan", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "klaim_disetujui", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "tjh_3", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "own_risk", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "nett_klaim", render: $.fn.dataTable.render.number( ',', '.',0 ) },

			{ data: "status" },

			{ data: "active", render: function(data, type, full, meta){
				if(data == 1)
					return "Active";
				else return "Not Active";
			}
		}
		]
	});

		var dataDocumentsTable = $('#dataDocumentsTable').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>asuransi/ax_data_asuransi_documents/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_asuransi": $("#id_asuransi_header_documents").val()
					});
				}
			},
			columns: 
			[
			{
				data: "id_asuransi_documents", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
					str += '<ul class="dropdown-menu">';
					str += '<li><a onclick="EditDataDocuments(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
					str += '<li><a onClick="DeleteDataDocuments(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
					str += '</ul>';
					str += '</div>';
					return str;
				}
			},

			{ data: "id_asuransi_documents" },
			{ data: "nm_documents" },
			{
				data: "file", render: function(data, type, full, meta){
					if(data != null){
						var str = '<a href="'+base_url+'uploads/asuransi/'+data+'" target="_blank">'+data+'</a>';
					}else{
						var str ='';
					}
					return str;
				}
			},

			{ data: "active", render: function(data, type, full, meta){
				if(data == 1)
					return "Active";
				else return "Not Active";
			}
		}
		]
	});

		var dataBayar = $('#dataBayar').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>asuransi/ax_data_asuransi_bayar/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_asuransi": $("#id_asuransi_header_bayar").val()
					});
				}
			},
			columns: 
			[
			{
				data: "id_asuransi_bayar", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<a type="button" class="btn btn-sm btn-danger" onclick="DeleteDataBayar(' + data + ')"><i class="fa fa-trash"></i> </a>';
					str += '</div>';
					return str;
				}
			},

			{ data: "id_asuransi_bayar" },
			{ data: "tgl_bayar" },
			{ data: "total_bayar", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "cdate" },
			]
		});


		var dataHistoryPremi = $('#dataHistoryPremi').DataTable({
			"ordering" : false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: 
			{
				url: "<?= base_url()?>asuransi/ax_data_asuransi_history_premi/",
				type: 'POST',
				data: function ( d ) {
					return $.extend({}, d, { 
						"id_asuransi": $("#id_asuransi_header_history_premi").val()
					});
				}
			},
			columns: 
			[
			{
				data: "id_asuransi_history_premi", render: function(data, type, full, meta){
					var str = '';
					str += '<div class="btn-group">';
					str += '<a type="button" class="btn btn-sm btn-danger" onclick="DeleteDataHistoryPremi(' + data + ')"><i class="fa fa-trash"></i> </a>';
					str += '</div>';
					return str;
				}
			},

			{ data: "id_asuransi_history_premi" },
			{ data: "nilai_premi_tambah", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "nilai_premi_kurang", render: $.fn.dataTable.render.number( ',', '.',0 ) },
			{ data: "keterangan" },
			{ data: "cdate" },
			]
		});

		$('#btnSave').on('click', function () {
			if($('#id_bu').val() == '0')
			{
				alertify.alert("Warning", "Pilih Cabang.");
			}else if($('#insured').val() == '')
			{
				alertify.alert("Warning", "Nama polis/Insured tidak boleh kosong.");
			}else if($('#no_polis').val() == '')
			{
				alertify.alert("Warning", "Nomor polis tidak boleh kosong.");
			}else if($('#tgl_awal').val() == '')
			{
				alertify.alert("Warning", "Isi Tanggal Awal");
			}else if($('#tgl_akhir').val() == '')
			{
				alertify.alert("Warning", "Isi Tanggal Akhir");
			}else if($('#tgl_akhir').val() < $('#tgl_awal').val())
			{
				alertify.alert("Warning", "Tanggal Akhir tidak boleh lebih dari tanggal awal");
			}
			else
			{
				var url = '<?=base_url()?>asuransi/ax_set_data';
				var data = {
					id_asuransi 	: $('#id_asuransi').val(),
					tahun 			: $('#tahun').val(),
					id_bu 			: $('#id_bu').val(),
					id_jenis_asuransi: $('#id_jenis_asuransi').val(),
					insured: $('#insured').val(),
					no_polis: $('#no_polis').val(),
					tgl_awal: $('#tgl_awal').val(),
					tgl_akhir: $('#tgl_akhir').val(),
					nilai_premi: $('#nilai_premi').val(),
					active: $('#active').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Saved.");
						$('#addModal').modal('hide');
						asuransiTable.ajax.reload();
					}
				});
			}
		});

		$('#btnSaveDetail').on('click', function () {
			if($('#no_certificate').val() == '')
			{
				alertify.alert("Warning", "Nomor Certificate belum diisi");
			}else if($('#id_armada').val() == '0')
			{
				alertify.alert("Warning", "Pilih Armada");
			}else
			{
				var url = '<?=base_url()?>asuransi/ax_set_data_detail';
				var data = {
					id_asuransi_detail: $('#id_asuransi_detail').val(),
					id_asuransi: $('#id_asuransi_header').val(),
					no_certificate: $('#no_certificate').val(),
					id_armada: $('#id_armada').val(),

					waktu_kejadian: $('#waktu_kejadian').val(),
					estimasi_perbaikan: $('#estimasi_perbaikan').val(),
					klaim_disetujui: $('#klaim_disetujui').val(),
					tjh_3: $('#tjh_3').val(),
					own_risk: $('#own_risk').val(),
					status: $('#status').val(),

					active: $('#active_detail').val()
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning","Data Sudah ada di Database");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						alertify.success("Data Saved.");
						$('#addModalDetail').modal('hide');
						asuransiDetail.ajax.reload();
					}
				});
			}
		});


		/// Premi
		$('#submit_premi').submit(function(e){
			e.preventDefault(); 

			if($('#nilai_premi_tambah').val() == '' && $('#nilai_premi_kurang').val() == '')
			{
				alertify.alert("Warning", "Nilai Premi (Penambahan) atau Nilai Premi (Pengurangan) tidak boleh kosong.");
			}
			else
			{
				$.ajax({
					url:'<?php echo base_url();?>asuransi/ax_set_data_premi',
					type:"post",
					data:new FormData(this),
					processData:false,
					contentType:false,
					cache:false,
					async:false,
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						dataHistoryPremi.ajax.reload();
						asuransiTable.ajax.reload();
						alertify.success('Data berhasil disimpan.');
						$('#nilai_premi_tambah').val('');
						$('#nilai_premi_kurang').val('');
						$('#keterangan').val('');
					}

				});
			}
		});


		/// Bayar
		$('#submit_bayar').submit(function(e){
			e.preventDefault(); 

			if($('#tgl_bayar').val() == '')
			{
				alertify.alert("Warning", "Please fill Tanggal Bayar.");
			}
			else
			{
				$.ajax({
					url:'<?php echo base_url();?>asuransi/ax_set_data_bayar',
					type:"post",
					data:new FormData(this),
					processData:false,
					contentType:false,
					cache:false,
					async:false,
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if(data['status'] == "success")
					{
						dataBayar.ajax.reload();
						asuransiTable.ajax.reload();
						alertify.success('Data berhasil disimpan.');
						$('#tgl_bayar').val('');
						$('#total_bayar').val('');
					}else{
						alertify.error('Data Gagal disimpan. Nilai Maksimal Inputan : '+formatNumber(data['data']));
					}

				});
			}
		});

		function ViewData(id_asuransi)
		{
			if(id_asuransi == 0)
			{
				$('#addModalLabel').html('Add Asuransi');
				$('#id_asuransi').val('');
				$('#id_bu').val('0').trigger('change');
				$('#id_jenis_asuransi').val('1').trigger('change');
				$('#insured').val('');
				$('#no_polis').val('');
				$('#nilai_premi').val('');

				$('#active').val('1');
				

				$('#addModal').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>asuransi/ax_get_data_by_id';
				var data = {
					id_asuransi: id_asuransi
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabel').html('Edit Asuransi');
					$('#id_asuransi').val(data['id_asuransi']);
					$('#id_bu').val(data['id_bu']).trigger('change');
					$('#tahun').val(data['tahun']).trigger('change');
					$('#id_jenis_asuransi').val(data['id_jenis_asuransi']).trigger('change');
					$('#insured').val(data['insured']);
					$('#no_polis').val(data['no_polis']);
					$('#tgl_awal').val(data['tgl_awal']);
					$('#tgl_akhir').val(data['tgl_akhir']);
					$('#nilai_premi').val(data['nilai_premi']);

					$('#active').val(data['active']);
					$('#addModal').modal('show');
				});
			}
		}

		function ViewDataDetail(id_asuransi_detail)
		{
			if(id_asuransi_detail == 0)
			{
				$('#addModalLabelDetail').html('Add Asuransi Detail');
				$('#id_asuransi_detail').val('');
				$('#no_certificate').val('');
				$('#id_armada').val('0').trigger('change');

				$('#waktu_kejadian').val('');
				$('#estimasi_perbaikan').val('');
				$('#klaim_disetujui').val('');
				$('#tjh_3').val('');
				$('#own_risk').val('');
				$('#status').val('PROGRESS');

				$('#active_detail').val('1');
				$('#addModalDetail').modal('show');
			}
			else
			{
				var url = '<?=base_url()?>asuransi/ax_get_data_by_id_detail';
				var data = {
					id_asuransi_detail: id_asuransi_detail
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					$('#addModalLabelDetail').html('Edit Asuransi');
					$('#id_asuransi_detail').val(data['id_asuransi_detail']);
					$('#id_asuransi_header').val(data['id_asuransi']);
					$('#no_certificate').val(data['no_certificate']);
					$('#id_armada').val(data['id_armada']).trigger('change');

					$('#waktu_kejadian').val(data['waktu_kejadian']);
					$('#estimasi_perbaikan').val(data['estimasi_perbaikan']);
					$('#klaim_disetujui').val(data['klaim_disetujui']);
					$('#tjh_3').val(data['tjh_3']);
					$('#own_risk').val(data['own_risk']);
					$('#status').val(data['status']);

					$('#active_detail').val(data['active']);
					$('#addModalDetail').modal('show');
				});
			}
		}

		function DeleteData(id_asuransi)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>asuransi/ax_unset_data';
					var data = {
						id_asuransi: id_asuransi
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						asuransiTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function DeleteDataDetail(id_asuransi_detail)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>asuransi/ax_unset_data_detail';
					var data = {
						id_asuransi_detail: id_asuransi_detail
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						asuransiDetail.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function DetailData(id_asuransi){
			$('#id_asuransi_header').val(id_asuransi);
			$('.nav-tabs a[href="#tab_2"]').tab('show');
			asuransiDetail.ajax.reload();
			asuransiDetail.columns.adjust().draw();
		}

		function DetailBayar(id_asuransi){
			$('#modalBayar').modal('show');
			$('#id_asuransi_header_bayar').val(id_asuransi);
			$('#tgl_bayar').val('');
			$('#total_bayar').val('');
			dataBayar.ajax.reload();
			setTimeout(function(){ dataBayar.columns.adjust().draw(); }, 1000);
		}

		function DetailHistoryPremi(id_asuransi){
			$('#modalHistoryPremi').modal('show');
			$('#id_asuransi_header_history_premi').val(id_asuransi);
			dataHistoryPremi.ajax.reload();
			setTimeout(function(){ dataHistoryPremi.columns.adjust().draw(); }, 1000);
		}

		$('#id_bu_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransiTable.ajax.reload();
		});

		$('#tahun_filter').select2({
			'allowClear': true
		}).on("change", function (e) {
			asuransiTable.ajax.reload();
		});

		$( "#tgl_awal").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});
		$( "#tgl_akhir").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#waktu_kejadian").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#tgl_bayar").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd"
		});

		$( "#tgl_awal" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});
		$( "#tgl_akhir" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});
		$( "#waktu_kejadian" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});
		$( "#tgl_bayar" ).inputmask("yyyy-mm-dd",{"placeholder": "yyyy-mm-dd"});

		function closeTab(){
			$('.nav-tabs a[href="#tab_1"]').tab('show');
			asuransiTable.columns.adjust().draw();
		}

		function Documents(id_asuransi){
			$('#modalDocuments').modal('show');
			$('#id_asuransi_header_documents').val(id_asuransi);
			dataDocumentsTable.ajax.reload();
			setTimeout(function(){ dataDocumentsTable.columns.adjust().draw(); }, 1000);
		}

		function addDocuments(){
			save_method = 'add';
			$('#formAddDocuments')[0].reset();
			$('#id_asuransi_header_documents_add').val($('#id_asuransi_header_documents').val());
			$('#addDocumentsModal').modal('show');
			$('#addDocumentsModalLabel').text('Tambah Data Documents');
		}

		function DeleteDataDocuments(id_asuransi_documents)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>asuransi_documents/ax_unset_data';
					var data = {
						id_asuransi_documents: id_asuransi_documents
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						dataDocumentsTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function DeleteDataBayar(id_asuransi_bayar)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>asuransi/ax_unset_data_bayar';
					var data = {
						id_asuransi_bayar: id_asuransi_bayar
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						dataBayar.ajax.reload();
						asuransiTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function DeleteDataHistoryPremi(id_asuransi_history_premi)
		{
			alertify.confirm(
				'Confirmation', 
				'Are you sure you want to delete this data?', 
				function(){
					var url = '<?=base_url()?>asuransi/ax_unset_data_history_premi';
					var data = {
						id_asuransi_history_premi: id_asuransi_history_premi
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						dataHistoryPremi.ajax.reload();
						asuransiTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function(){ }
				);
		}

		function EditDataDocuments(id_asuransi_documents)
		{
			save_method = 'update';
			$('#formAddDocuments')[0].reset();
			$.ajax({
				url : "<?php echo site_url('asuransi_documents/ax_get_data_by_id')?>",
				type: "POST",
				data :{id_asuransi_documents: id_asuransi_documents},
				dataType: "JSON",
				success: function(data)
				{
					$('#addDocumentsModalLabel').html('Edit Asuransi Documents');
					$('#id_asuransi_documents').val(id_asuransi_documents);
					$('#id_asuransi_header_documents_add').val(data['id_asuransi']);
					$('#nm_documents').val(data['nm_documents']);
				// $('#file').val(data['file']);
				$('#active_documents').val(data['active']);
				$('#addDocumentsModal').modal('show');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		});
		}

		$('#btnSaveAddDocuments').on('click', function ()
		{
			var url;
			if(save_method == 'add') {
				url = "<?php echo site_url('asuransi_documents/ax_set_data')?>";
			} else {
				url = "<?php echo site_url('asuransi_documents/ax_set_data_update')?>";
			}

			if($('#nm_documents').val() == '')
			{
				alertify.alert("Warning", "Isi Nama Document");
			}
			else
			{
				var formData = new FormData($('#formAddDocuments')[0]);
				$.ajax({
					url : url,
					type: "POST",
					data: formData,
					contentType: false,
					processData: false,
					dataType: "JSON",
					statusCode: {
						500: function() {
							alertify.alert("Warning","Data Duplicate");
						}
					},
					success: function(data)
					{

						if(data.status == true){
							alertify.success("Data Saved.");
							$('#addDocumentsModal').modal('hide');
							dataDocumentsTable.ajax.reload();
						}else{
							alertify.error(data.message);
							alertify.alert("Warning", data.message);
						}

					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alertify.alert("Warning", "Error Add data --> Pastikan File type [Gambar, PDF, PPT, Doc, Excell] dan Max.size 1 MB");

					}
				});
			}
		});

		$('#id_armada').select2({
			'placeholder': "--Armada--",
			'allowClear': true
		});

		function formatNumber(num) {
			return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
		}

		function print_laporan(format) {
			var url   = "<?= base_url() ?>asuransi_documents/print_laporan/";
			window.open(url+"?format="+format, '_blank');
			window.focus();
		}
	</script>
</body>
</html>
