<!DOCTYPE html>
<html>

<head>
	<?= $this->load->view('head'); ?>
</head>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color') ?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Absensi Armada (Teknik)</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">

							<div class="modal fade" id="absensiteknikModal" tabindex="-1" role="dialog" aria-labelledby="absensiteknikModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="Form-add-bu" id="addModalLabel">Absensi Armada (Teknik)</h4>
										</div>
										<div class="modal-body">
											<!-- <input type="hidden" name="kd_armada" id="kd_armada"> -->
											<!-- <input type="hidden" name="status" id="status"> -->
											<div class="form-group">
												<label>Keterangan</label>
												<textarea id="keterangan" name="keterangan" rows="4" cols="50" class="form-control"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary" id='btnSaveAbsensiteknik'>Absen</button>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-body">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active disabled"><a href="#tab_1" class="disabled" data-toggle="tab" aria-expanded="true">Data Jadwal</a></li>
										<li class=" disabled"><a href="#tab_2" class="disabled" aria-expanded="false">Tambah Jadwal</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1">

											<div class="row">
												<div class="form-group col-md-3">
													<label>Cabang</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-home"></i>
														</div>
														<select class="form-control select2" style="width: 100%;" id="id_cabang_filter" name="id_cabang_filter">
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
															?>
																<option value="<?= $rowmenu->id_bu ?>">
																	<?= $rowmenu->nm_bu ?>
																</option>
															<?php
															}
															?>
														</select>
													</div>

												</div>
												<div class="form-group col-md-3">
													<label>Segment</label>
													<select class="form-control select2" style="width: 100%;" id="id_segment_filter">
														<option value="0">-- All Segment --</option>
														<?php foreach ($combobox_segment->result() as $rowmenu) { ?>
															<option value="<?= $rowmenu->id_segment ?>"><?= $rowmenu->nm_segment ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Tanggal</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<input type="text" id="tanggal" name="tanggal" class="form-control datepicker" placeholder="Pilih tanggal" value="<?= date('Y-m-d'); ?>" autocomplete="off" />
													</div>
												</div>
												<div class="form-group col-md-1">
													<label>&nbsp;</label>
													<div class="input-group">
														<button class="btn btn-primary" onclick='ViewDetail()'>
															<i class='fa fa-plus'></i> Tambah Jadwal
														</button>
													</div>
												</div>
												<div class="form-group col-md-1">
													<label>&nbsp;</label>
													<div class="input-group">
														<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-print"></i> Cetak <span class="caret"></span></button>
														<ul class="dropdown-menu">
															<li><a onclick="print_pdf()" id="print_excell" value="2"><i class="fa fa-print"></i> Cetak SGO</a></li>
															<li><a onclick="print_pdf_km()" id="print_pdf" value="0"><i class="fa fa-print"></i> Cetak KM</a></li>
														</ul>
													</div>
												</div>
												<!-- <div class="form-group col-md-1">
													<label>&nbsp;</label>
													<div class="input-group">
														<button type="button" class="btn btn-success" onclick="prinf_pdf()"><i class="fa fa-print"></i> Cetak KM </button>
													</div>
												</div>
												<div class="form-group col-md-1">
													<label>&nbsp;</label>
													<div class="input-group">
														<button type="button" class="btn btn-success" onclick="prinf_pdf()"><i class="fa fa-print"></i> Cetak SGO </button>
													</div>
												</div> -->
											</div>
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="absenTable">
													<thead>
														<tr>
															<th width="150px">Options</th>
															<th>Tanggal</th>
															<th>Code</th>
															<th>Segment</th>
															<th>Odo Awal</th>
															<th>Odo Akhir</th>
															<th>KM Tempuh</th>
															<th>Status</th>
															<th>Keterangan</th>
															<th>Petugas</th>
															<th>Waktu</th>

														</tr>
													</thead>
												</table>
											</div>

										</div>

										<div class="tab-pane" id="tab_2">
											<div class="modal-body">
												<div class="row">
													<div class="col-lg-12">
														<div class="form-group pull-left">
															<p>
																<font color="blue"><b>Form Tambah Absensi Armada</b></font>
															</p>
														</div>
														<div class="form-group pull-right">
															<button type="button" class="btn bg-purple btn-default" onClick='closeTab()'><i class="fa  fa-arrow-circle-left"></i> Kembali</button>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-3">
														<label>Tanggal</label>
														<div class="input-group">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>
															<input type="text" id="tgl_absensi" name="tgl_absensi" class="form-control datepicker" placeholder="Pilih tanggal" value="<?= date('Y-m-d'); ?>" autocomplete="off" />
														</div>
													</div>
													<div class="form-group col-lg-3">
														<label>Cabang</label>
														<select class="form-control select2 " style="width: 100%;" id="id_bu" name="id_bu">
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
															?>
																<option value="<?= $rowmenu->id_bu ?>"><?= $rowmenu->nm_bu ?></option>
															<?php
															}
															?>
														</select>
													</div>
													<div class="form-group col-lg-3">
														<label>Armada</label>
														<select class="form-control select2" style="width: 100%;" id="kd_armada" onChange="odoauto()">
															<option value="0">-- Pilih Armada --</option>
														</select>
													</div>
													<div class="form-group col-lg-3">
														<label>Segment</label>
														<select class="form-control select2" style="width: 100%;" id="id_segment">
															<option value="0">-- Pilih Segment --</option>
															<?php foreach ($combobox_segment->result() as $rowmenu) { ?>
																<option value="<?= $rowmenu->id_segment ?>"><?= $rowmenu->nm_segment ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="form-group col-lg-2">
														<label>Status</label>
														<select class="form-control" style="width: 100%;" id="status" onChange="odoakhir()">
															<option value="0">-- Pilih Status --</option>
															<option value="1">RB</option>
															<option value="2">RR</option>
															<option value="3">S</option>
															<option value="4">HTP / HTM</option>
															<option value="7">IP</option>
															<option value="5">HJ</option>
															<option value="6">UA</option>
														</select>
													</div>
													<div class="form-group col-lg-2">
														<label>Odometer Awal</label>
														<input type="number" class="form-control" id="odometer_awal" placeholder="Awal">
													</div>
													<div class="form-group col-lg-2">
														<label>Odometer Akhir</label>
														<input type="number" class="form-control" id="odometer_akhir" placeholder="Akhir" onChange="kmtempuh()">
													</div>
													<div class="form-group col-lg-2">
														<label>KM Tempuh</label>
														<input type="number" class="form-control" id="km_tempuh" placeholder="KM Tempuh" disabled>
													</div>
													<div class="form-group col-lg-2">
														<label>&nbsp;</label>
														<div class="input-group">
															<button type="button" class="btn btn-primary" id='btnSaveJadwal'>
																<i class='fa fa-save'></i> Simpan
															</button>
														</div>
													</div>
													<div class="form-group col-lg-2">
														<label>&nbsp;</label>
														<div class="input-group">
															<button type="button" class="btn btn-info" onClick="uaauto()">
																<i class='fa fa-refresh'></i> UA Auto
															</button>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<small><b>Keterangan</b>
															<!-- <ul> -->
															<li>- RB : Rusak Berat - RR : Rusak Ringan - S : Service - SGO : Siap Guna Operasi - SO : Siap Operasi</li>
															<!-- </ul> -->
															<!-- <ul>
																<li>RB : Rusak Berat</li>
																<li>RR : Rusak Ringan</li>
																<li>S : Service</li>
																<li>SGO : Siap Guna Operasi</li>
															</ul> -->
														</small>
													</div>
													<!-- <div class="form-group col-md-4">
														<p style="height: 15px"></p>
														<button class="btn btn-info" title="Copy Absent" onclick='CopyData()'>
															<i class='fa fa-copy'></i> Copy Absent ke Tanggal
														</button>
														<a type="button" class="btn btn-danger" title="Hapus semua data by tanggal" onClick="deleteAllAbsent()" id="btnDeleteAll"><i class="fa fa-trash"></i> </a>
														<div class="btn-group">
															<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-print"></i> Print <span class="caret"></span></button>
															<ul class="dropdown-menu">
																<li><a onclick="print_pdf()" id="print_pdf" value="0"><i class="fa fa-print"></i> PDF</a></li>
																<li><a onclick="print_excell()" id="print_excell" value="2"><i class="fa fa-print"></i> Excell</a></li>
															</ul>
														</div>
													</div> -->
												</div>
											</div>

											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="jadwalTable">
													<thead>
														<tr>
															<th width="150px">Options</th>
															<th>Tanggal</th>
															<th>Code</th>
															<th>Segment</th>
															<th>Odo Awal</th>
															<th>Odo Akhir</th>
															<th>KM Tempuh</th>
															<th>Status</th>
															<th>Keterangan</th>
															<th>Petugas</th>
															<th>Waktu</th>

														</tr>
													</thead>
												</table>
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
			<div class="modal fade" id="copyDataModal" tabindex="-1" role="dialog" aria-labelledby="copyModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="Form-add-bu" id="barangModalLabel">Copy Absent ke Tanggal lain</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Dari Tanggal</label>
								<input type="text" id="tanggal_from" name="tanggal_from" class="form-control" placeholder="Dari Tanggal" readonly="readonly">
							</div>
							<div class="form-group">
								<label>Ke Tanggal</label>
								<input type="text" id="tanggal_to" name="tanggal_to" class="form-control" placeholder="Ke Tanggal" value="<?= date('Y-m-d') ?>">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id='btnCopy'>Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>
		// jadwalTable.ajax.reload();
		combo_armada($("#id_bu").val(), 0, '-- Armada --');

		var absenTable = $('#absenTable').DataTable({
			"ordering": false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: {
				url: "<?= base_url() ?>absensi_teknik/ax_data_absensi_teknik_new/",
				type: 'POST',
				data: function(d) {
					return $.extend({}, d, {

						"id_bu": $("#id_cabang_filter").val(),
						"tgl_absensi": $("#tanggal").val(),
						"id_segment": $("#id_segment_filter").val(),

					});
				}
			},
			columns: [{
					data: "id_absensi_armada",
					render: function(data, type, full, meta) {
						var id1 = "'" + data + "'";
						var str = '';
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
						str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 1 + ')"><i class="fa fa-check-square-o"></i> Rusak Berat (RB)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 2 + ')"><i class="fa fa-check-circle"></i> Rusak Ringan (RR)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 3 + ')"><i class="fa fa-check-square-o"></i> Service (S)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 4 + ')"><i class="fa fa-check-circle"></i> Siap Guna Operasi (SGO)</a></li>';
						str += '</ul>';
						str += '</div>';
						return str;
					}
				},

				{
					class: 'intro',
					data: "tgl_absensi"
				},
				{
					class: 'intro',
					data: "kd_armada"
				},
				{
					class: 'intro',
					data: "nm_segment"
				},
				{
					class: 'intro',
					data: "odometer_awal"
				},
				{
					class: 'intro',
					data: "odometer_akhir"
				},
				{
					class: 'intro',
					data: "km_tempuh"
				},
				{
					class: "intro",
					data: "status",
					render: function(data, type, full, meta) {
						if (data == 1) {
							return 'RB';
						} else if (data == 2) {
							return 'RR';
						} else if (data == 3) {
							return 'S';
						} else if (data == 4) {
							return 'HTP / HTM / IP';
						} else if (data == 5) {
							return 'HJ';
						} else if (data == 6) {
							return 'UA';
						} else {
							return '';
						}
					}
				},
				{
					class: 'intro',
					data: "keterangan"
				},
				{
					class: 'intro',
					data: "cnm_user"
				},
				{
					class: 'intro',
					data: "cdate"
				},
			]
		});

		var jadwalTable = $('#jadwalTable').DataTable({
			"ordering": false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			ajax: {
				url: "<?= base_url() ?>absensi_teknik/ax_data_absensi_teknik_new/",
				type: 'POST',
				data: function(d) {
					return $.extend({}, d, {

						"id_bu": $("#id_bu").val(),
						"tgl_absensi": $("#tgl_absensi").val(),
						"id_segment": $("#id_segment").val(),

					});
				}
			},
			columns: [{
					data: "id_absensi_armada",
					render: function(data, type, full, meta) {
						var id1 = "'" + data + "'";
						var str = '';
						str += '<div class="btn-group">';
						str += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>';
						str += '<ul class="dropdown-menu">';
						str += '<li><a onclick="ViewData(' + data + ')"><i class="fa fa-pencil"></i> Edit</a></li>';
						str += '<li><a onClick="DeleteData(' + data + ')"><i class="fa fa-trash"></i> Delete</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 1 + ')"><i class="fa fa-check-square-o"></i> Rusak Berat (RB)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 2 + ')"><i class="fa fa-check-circle"></i> Rusak Ringan (RR)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 3 + ')"><i class="fa fa-check-square-o"></i> Service (S)</a></li>';
						// str += '<li><a onClick="absen(' + id1 + ',' + 4 + ')"><i class="fa fa-check-circle"></i> Siap Guna Operasi (SGO)</a></li>';
						str += '</ul>';
						str += '</div>';
						return str;
					}
				},

				{
					class: 'intro',
					data: "tgl_absensi"
				},
				{
					class: 'intro',
					data: "kd_armada"
				},
				{
					class: 'intro',
					data: "nm_segment"
				},
				{
					class: 'intro',
					data: "odometer_awal"
				},
				{
					class: 'intro',
					data: "odometer_akhir"
				},
				{
					class: 'intro',
					data: "km_tempuh"
				},
				{
					class: "intro",
					data: "status",
					render: function(data, type, full, meta) {
						if (data == 1) {
							return 'RB';
						} else if (data == 2) {
							return 'RR';
						} else if (data == 3) {
							return 'S';
						} else if (data == 4) {
							return 'HTP / HTM / IP';
						} else if (data == 5) {
							return 'HJ';
						} else if (data == 6) {
							return 'UA';
						} else {
							return '';
						}
					}
				},
				{
					class: 'intro',
					data: "keterangan"
				},
				{
					class: 'intro',
					data: "cnm_user"
				},
				{
					class: 'intro',
					data: "cdate"
				},
			]
		});

		$('#btnSaveAbsensiteknik').on('click', function() {
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var keterangan = $("#keterangan").val();
			var kd_armada = $("#kd_armada").val();
			var status = $("#status").val();

			if ($('#id_cabang_filter').val() == '' || $('#id_cabang_filter').val() == '0') {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == 0) {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var url = '<?= base_url() ?>absensi_teknik/ax_set_data';
				var data = {
					id_cabang: id_cabang,
					tanggal: tanggal,
					keterangan: keterangan,
					kd_armada: kd_armada,
					status: status
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning", "Data Duplicate");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if (data['status'] == "success") {
						alertify.success("Data Disimpan.");
						absenTable.ajax.reload();
						$('#absensiteknikModal').modal('hide');
					} else {
						alertify.alert("Data Gagal Disimpan.");
					}
				});
			}
		});

		$('#btnSaveJadwal').on('click', function() {
			// alert($('#odometer_awal').val());
			var id_bu = $("#id_bu").val();
			var tgl_absensi = $("#tgl_absensi").val();
			var kd_armada = $("#kd_armada").val();
			var id_segment = $("#id_segment").val();
			var odometer_awal = $("#odometer_awal").val();
			var odometer_akhir = $("#odometer_akhir").val();
			var km_tempuh = $("#km_tempuh").val();
			var status = $("#status").val();
			// var keterangan = $("#keterangan").val();
			var selisih = $('#odometer_akhir').val() - $('#odometer_awal').val();
			alert(selisih);

			if ($('#id_bu').val() == '' || $('#id_bu').val() == '0') {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#kd_armada').val() == '' || $('#kd_armada').val() == '0') {
				alertify.alert("Perhatian", "Armada Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == 0) {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else if ($('#status').val() == 0) {
				alertify.alert("Perhatian", "Status Tidak Boleh Kosong");
			} else if ($('#odometer_awal').val() == '') {
				alertify.alert("Perhatian", "Odometer Awal Tidak Boleh Kosong");
			} else if ($('#odometer_akhir').val() == '') {
				alertify.alert("Perhatian", "Odometer Akhir Tidak Boleh Kosong");
			} else if ($('#odometer_awal').val() > $('#odometer_akhir').val() || $('#km_tempuh').val() < '0') {
				alertify.alert("Perhatian", "Odometer Akhir Tidak Boleh Lebih kecil dari Odometer Awal");
			} else if (selisih > 800) {
				alertify.alert("Perhatian", "Silahkan Cek Kembali Nilai Odometer yang Diinputkan Karena Ada Ketidaksesuaian Dengan Odometer di Hari Kemarin");
			} else {
				var url = '<?= base_url() ?>absensi_teknik/ax_set_data_new';
				var data = {
					id_bu: id_bu,
					tgl_absensi: tgl_absensi,
					kd_armada: kd_armada,
					id_segment: id_segment,
					odometer_awal: odometer_awal,
					odometer_akhir: odometer_akhir,
					km_tempuh: km_tempuh,
					status: status
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning", "Data Duplicate");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if (data['status'] == "success") {
						alertify.success("Data Disimpan.");
						jadwalTable.ajax.reload();
						absenTable.ajax.reload();
						// $('#absensiteknikModal').modal('hide');
					} else {
						alertify.alert("Notifikasi", "Data Gagal Disimpan. Tidak ada data absensi armada dengan code tersebut di tanggal sebelum nya.");
					}
				});
			}
		});

		function ViewDetail(unik, hr) {
			jadwalTable.ajax.reload();
			$('.nav-tabs a[href="#tab_2"]').tab('show');
		}

		function ViewData(id_absensi_armada) {
			$('.nav-tabs a[href="#tab_2"]').tab('show');
			var url = '<?= base_url() ?>absensi_teknik/ax_get_data_absensi_armada_teknik_by_id';
			var data = {
				id_absensi_armada: id_absensi_armada
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				// $('#nm_shift').val(data['nm_shift']);
				$('#id_bu').val(data['id_bu']);
				$('#select2-id_bu-container').html(data['nm_bu']);
				combo_armada(data['id_bu'], data['kd_armada'], data['kd_armada']);
				$('#id_segment').val(data['id_segment']);
				$('#select2-id_segment-container').html(data['nm_segment']);
				$('#tgl_absensi').val(data['tgl_absensi']);
				$('#odometer_awal').val(data['odometer_awal']);
				$('#odometer_akhir').val(data['odometer_akhir']);
				$('#status').val(data['status']);
				$('#km_tempuh').val(data['km_tempuh']);
			});
		}

		function absen(kd_armada, status) {
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();

			if ($('#id_cabang_filter').val() == '' || $('#id_cabang_filter').val() == '0') {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == 0) {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				$('#absensiteknikModal').modal('show');
				$("#kd_armada").val(kd_armada);
				$("#status").val(status);
			}
		}

		function closeTab() {
			$('.nav-tabs a[href="#tab_1"]').tab('show');
			tbljadwalnondefault.columns.adjust().draw();
		}

		function print_pdf() {
			var url = "<?= site_url("reports/prints") ?>";
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var format = 0;

			if (tanggal != '' && id_cabang != '') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				var REQ = "?bulan=" + bulan + "&tahun=" + tahun + "&tanggal=" + tanggal + "&id_cabang=" + id_cabang + "&format=html" + "&uk=F4-P" + "&name=absensi_armada_teknik_new";
				open(url + REQ);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function print_pdf_km() {
			var url = "<?= site_url("reports/prints") ?>";
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var format = 0;

			if (tanggal != '' && id_cabang != '') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				var REQ = "?bulan=" + bulan + "&tahun=" + tahun + "&tanggal=" + tanggal + "&id_cabang=" + id_cabang + "&format=html" + "&uk=F4-P" + "&name=absensi_armada_teknik_km_new";
				open(url + REQ);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function print_excell() {
			tanggal = $("#tanggal").val();
			var id_cabang = $("#id_cabang_filter").val();
			var format = 2;
			if (tanggal != '' && id_cabang != '0') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				window.open("<?= site_url() ?>absensi_teknik/laporan_absen_pengemudi/" + id_cabang + "/" + bulan + "/" + tahun + "/" + format);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function odoauto() {
			var kd_armada = $("#kd_armada").val();
			var tgl_absensi = $("#tgl_absensi").val();
			var url = '<?= base_url() ?>absensi_teknik/ax_get_odometer_auto';
			var data = {
				kd_armada: kd_armada,
				tgl_absensi: tgl_absensi
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				$('#odometer_awal').val(data['odometer_akhir']);
				$('#odometer_akhir').val('0.00');
				$('#km_tempuh').val('0.00');
				$('#status').val('0');
			});

			inputSegment();
		}

		function odoakhir() {
			var odometer = $("#odometer_awal").val();
			if ($("#status").val() == '5') {
				$('#odometer_akhir').val(0.00);
				$('#km_tempuh').val(0.00);
			} else {
				$('#km_tempuh').val(0);
				$('#odometer_akhir').val(odometer);
			}
			if ($('#status').val() == '6') {
				$('#odometer_awal').val(0.00);
				$('#odometer_akhir').val(0.00);
				$('#km_tempuh').val(0.00);
			}
		}

		function kmtempuh() {
			var odometer_awal = $("#odometer_awal").val();
			var odometer_akhir = $("#odometer_akhir").val();
			var kmtempuh = odometer_akhir - odometer_awal;
			$('#km_tempuh').val(kmtempuh);
		}

		function uaauto() {
			var url = '<?= base_url() ?>absensi_teknik/ax_get_ua_auto';
			var data = {
				id_bu: $("#id_bu").val(),
				tgl_absensi: $("#tgl_absensi").val(),
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				if (data['status'] == "success") {
					alertify.success("Armada UA Berhasil Diinput.");
					absenTable.ajax.reload();
					jadwalTable.ajax.reload();
				} else {
					alertify.error("Armada UA Gaga Diinput.");
					absenTable.ajax.reload();
					jadwalTable.ajax.reload();
				}
			});
		}

		function inputSegment() {
			var url = '<?= base_url() ?>absensi_teknik/ax_get_segment_auto';
			var data = {
				kd_armada: $("#kd_armada").val(),
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				$('#id_segment').val(data['id_segment']);
				$('#select2-id_segment-container').html(data['nm_segment']);
			});
		}

		$(document).ready(function() {
			$("#tanggal, #tgl_absensi").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd"
			});
			$("#tanggal_to").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd",
				yearRange: "-100:+20",
				minDate: '0'
			});

			$("#tanggal, #tgl_absensi").inputmask("yyyy-mm-dd", {
				"placeholder": "yyyy-mm-dd"
			});
			$("#tanggal_to").inputmask("yyyy-mm-dd", {
				"placeholder": "yyyy-mm-dd"
			});
			$("#tanggal, #tgl_absensi").on("change", function(e) {
				absenTable.ajax.reload();
				jadwalTable.ajax.reload();
				var id_bu = $("#id_bu").val();
				combo_armada(id_bu, 0, '-- Armada --');

				//show hide delete all button
				var date1 = new Date($('#tanggal').val());
				var date2 = new Date(Date.now());
				selisih = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
				if (selisih >= 1) {
					$('#btnDeleteAll').hide();
				} else {
					$('#btnDeleteAll').show();
				}
				//End show hide delete all button

			});

			$(".justnumber").keydown(function(e) {
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

					(e.keyCode == 65 && e.ctrlKey === true) ||

					(e.keyCode == 67 && e.ctrlKey === true) ||

					(e.keyCode == 88 && e.ctrlKey === true) ||

					(e.keyCode >= 35 && e.keyCode <= 39)) {

					return;
				}

				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			});

			$("#id_bu").change(function() {
				$("#kd_armada").hide();
				var id_bu = $("#id_bu").val();
				combo_armada(id_bu, 0, '-- Armada --');
			});
		});

		function combo_armada(id_bu, kd_armada, kd_armada) {
			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>absensi_teknik/ax_get_armada",
				data: {
					id_cabang: id_bu
				},
				dataType: "json",
				beforeSend: function(e) {
					if (e && e.overrideMimeType) {
						e.overrideMimeType("application/json;charset=UTF-8");
					}
				},
				success: function(response) {

					$("#kd_armada").html(response.data_armada).show();
					$('#select2-kd_armada-container').html(kd_armada);
					$('#kd_armada').val(kd_armada);

				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError);
				}
			});
		}

		$('#id_cabang_filter').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		$('#id_bu').select2({
			'allowClear': true
		}).on("change", function(e) {
			jadwalTable.ajax.reload();
		});

		$('#id_segment').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		$('#id_segment_filter').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		function DeleteData(id_absensi_armada) {
			alertify.confirm(
				'Confirmation',
				'Are you sure you want to delete this data?',
				function() {
					var url = '<?= base_url() ?>absensi_teknik/ax_unset_data_new';
					var data = {
						id_absensi_armada: id_absensi_armada
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						absenTable.ajax.reload();
						jadwalTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function() {}
			);
		}

		function deleteAllAbsent() {
			if ($('#id_cabang_filter').val() == 0) {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == '') {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var id_bu = $('#id_cabang_filter').val();
				var id_segment = $('#id_segment_filter').val();
				var tanggal = $('#tanggal').val();
				alertify.confirm(
					'Konfirmasi',
					'Apa anda yakin akan menghapus semua data pada tanggal ' + tanggal + ' ?',
					function() {
						var url = '<?= base_url() ?>absensi_teknik/ax_unset_data_all_absent';
						var data = {
							id_bu: id_bu,
							id_segment: id_segment,
							tanggal: tanggal
						};
						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {

							var data = JSON.parse(data);
							if (data['status'] == "1") {
								alertify.success("Data Berhasil Terhapus.");
								absenTable.ajax.reload();
							} else {
								alertify.error("Data Gagal Terhapus.");
								absenTable.ajax.reload();
							}
						});
					},
					function() {}
				);
			}
		}

		function CopyData() {
			if ($('#id_cabang').val() == 0) {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == '') {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var tanggal = $('#tanggal').val();
				$('#tanggal_from').val(tanggal);
				$('#copyDataModal').modal('show');
			}
		}

		$('#btnCopy').on('click', function() {
			if ($('#tanggal_to').val() == '') {
				alertify.alert("Perhatian", "Ke Tanggal Tidak Boleh Kosong");
				return;
			} else {
				var id_cabang = $('#id_cabang_filter').val();
				var id_segment = $('#id_segment_filter').val();
				var tanggal = $('#tanggal').val();
				var tanggal_to = $('#tanggal_to').val();

				alertify.confirm(
					'Confirmation',
					'Copy Absensi Armada (Teknik) dari tanggal ' + tanggal + ' ke tanggal ' + tanggal_to,
					function() {
						var url = '<?= base_url() ?>absensi_teknik/ax_copy_absensi_teknik';
						var data = {
							id_cabang: id_cabang,
							id_segment: id_segment,
							tanggal_from: tanggal,
							tanggal_to: tanggal_to,
						};
						$.ajax({
							url: url,
							method: 'POST',
							data: data,
							statusCode: {
								500: function() {
									alertify.alert("Perhatian", "Data Tidak Berhasil di Setting");
								}
							}
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							absenTable.ajax.reload();
							$('#copyDataModal').modal('hide');
							alertify.success('Absensi Armada (Teknik) Berhasil di Copy ke Tanggal ' + data.tanggal_to);
						});
					},
					function() {}
				);
			}
		});
	</script>
</body>

</html>