<!DOCTYPE html>
<html>

<head>
	<?= $this->load->view('head'); ?>
	<style>
		div.dataTables_wrapper div.dataTables_processing {
			top: 1%;
		}
	</style>
</head>

<body class="sidebar-mini wysihtml5-supported <?= $this->config->item('color') ?>">
	<div class="wrapper">
		<?= $this->load->view('nav'); ?>
		<?= $this->load->view('menu_groups'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>KM Service Armada</h1>
			</section>
			<section class="invoice">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">

							<div class="panel-body">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" class="disabled" data-toggle="tab" aria-expanded="true">Data SIMATEKNIK</a></li>
										<li class=""><a href="#tab_2" id="tab2" class="disabled" data-toggle="tab" aria-expanded="true">Data FMS</a></li>
										<li class=""><a href="#tab_3" class="disabled" data-toggle="tab" aria-expanded="true">Data Gabungan</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1">
											<div class="row">
												<div class="form-group col-md-12">
													<label>Cabang</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-home"></i>
														</div>
														<select class="form-control select2" style="width: 100%;" id="id_cabang_filter" name="id_cabang_filter">
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
															?>
																<option value="<?= $rowmenu->id_bu ?>">
																	<?= $rowmenu->nm_bu ?>
																</option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
												<!-- <div class="form-group col-md-1">
													<label>&nbsp;</label>
													<div class="input-group">
														<button type="button" class="btn btn-info" onclick="RecallData()"><i class="fa fa-refresh"></i> &nbsp; Recall KM </button>
													</div>
												</div> -->
											</div>
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="absenTable">
													<thead>
														<tr>
															<th>No</th>
															<th>Armada</th>
															<th>Item Service</th>
															<th>Tanggal Service</th>
															<th>KM Service</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>

										<div class="tab-pane" id="tab_2">
											<div class="row">
												<div class="form-group col-md-12">
													<label>Cabang</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-home"></i>
														</div>
														<select class="form-control select2" style="width: 100%;" id="id_cabang_filter_2" name="id_cabang_filter2">
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
															?>
																<option value="<?= $rowmenu->id_bu ?>">
																	<?= $rowmenu->nm_bu ?>
																</option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="absen2Table">
													<thead>
														<tr>
															<th>No</th>
															<th>Armada</th>
															<th>KM Tempuh</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>

										<div class="tab-pane" id="tab_3">
											<div class="row">
												<div class="form-group col-md-12">
													<label>Cabang</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-home"></i>
														</div>
														<select class="form-control select2" style="width: 100%;" id="id_cabang_filter_3" name="id_cabang_filter2">
															<?php
															foreach ($combobox_bu->result() as $rowmenu) {
															?>
																<option value="<?= $rowmenu->id_bu ?>">
																	<?= $rowmenu->nm_bu ?>
																</option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="dataTable_wrapper">
												<table class="table table-striped table-bordered table-hover" id="absen3Table">
													<thead>
														<tr>
															<th>No</th>
															<th>Armada</th>
															<th>KM FMS</th>
															<th>KM SIMA</th>
															<th>KM Total</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<?= $this->load->view('basic_js'); ?>
	<script type='text/javascript'>
		// var absenTable = $('#absenTable').DataTable({
		// 	"ordering": false,
		// 	"scrollX": true,
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"paging": false,
		// 	"ordering": false,
		// 	"info": false
		// 	ajax: {
		// 		url: "<?= base_url() ?>absensi_teknik/ax_data_km_service_armada/",
		// 		type: 'POST',
		// 		data: function(d) {
		// 			return $.extend({}, d, {
		// 				"id_bu": $("#id_cabang_filter").val(),
		// 			});
		// 		}
		// 	},
		// 	columns: [{
		// 			data: "id",
		// 			render: function(data, type, row, meta) {
		// 				return meta.row + meta.settings._iDisplayStart + 1;
		// 			}
		// 		},
		// 		{
		// 			class: 'intro',
		// 			data: "armada"
		// 		},
		// 		{
		// 			class: 'intro',
		// 			data: "nm_item"
		// 		},
		// 		{
		// 			class: 'intro',
		// 			data: "tgl_akhir_service"
		// 		},
		// 		{
		// 			class: 'intro',
		// 			data: "total"
		// 		},
		// 	]
		// });

		var absenTable = $('#absenTable').DataTable({
			"ordering": false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			"paging": false,
			"searching": false,
			"bPaginate": false,
			"info": false,
			"paging": false,
			ajax: {
				url: "<?= base_url() ?>absensi_teknik/ax_data_km_service_armada/",
				type: 'POST',
				data: function(d) {
					return $.extend({}, d, {
						"id_bu": $("#id_cabang_filter").val(),
					});
				}
			},
			columns: [

				// { class:'intro', data: "nik" },
				// { data: "email" },
				// { data: "nama" },
				// { class:'intro', data: "jam" },
				{
					data: "id",
					render: function(data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					class: 'intro',
					data: "armada"
				},
				{
					class: 'intro',
					data: "nm_item"
				},
				{
					class: 'intro',
					data: "tgl_akhir_service"
				},
				{
					class: 'intro',
					data: "total"
				},

			]
		});

		var absen2Table = $('#absen2Table').DataTable({
			"ordering": false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			"pageLength": 500,
			"bPaginate": false,
			ajax: {
				url: "<?= base_url() ?>absensi_teknik/ax_data_km_service_armada_2/",
				type: 'POST',
				data: function(d) {
					return $.extend({}, d, {
						"id_bu": $("#id_cabang_filter_2").val(),
					});
				}
			},
			columns: [{
					data: "id",
					render: function(data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					class: 'intro',
					data: "kd_armada"
				},
				{
					class: 'intro',
					data: "km_tempuh"
				},
			]
		});

		var absen3Table = $('#absen3Table').DataTable({
			"ordering": false,
			"scrollX": true,
			"processing": true,
			"serverSide": true,
			"pageLength": 500,
			"bPaginate": false,
			ajax: {
				url: "<?= base_url() ?>absensi_teknik/ax_data_km_service_armada_3/",
				type: 'POST',
				data: function(d) {
					return $.extend({}, d, {
						"id_bu": $("#id_cabang_filter_3").val(),
					});
				}
			},
			columns: [{
					data: "id",
					render: function(data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}
				},
				{
					class: 'intro',
					data: "kd_armada"
				},
				{
					class: 'intro',
					data: "km_tempuh"
				},
				{
					class: 'intro',
					data: "total"
				},
				{
					class: 'intro',
					data: "km_total"
				},
			]
		});

		$('#btnSaveAbsensiteknik').on('click', function() {
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var keterangan = $("#keterangan").val();
			var kd_armada = $("#kd_armada").val();
			var status = $("#status").val();

			if ($('#id_cabang_filter').val() == '' || $('#id_cabang_filter').val() == '0') {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == 0) {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var url = '<?= base_url() ?>absensi_teknik/ax_set_data';
				var data = {
					id_cabang: id_cabang,
					tanggal: tanggal,
					keterangan: keterangan,
					kd_armada: kd_armada,
					status: status
				};

				$.ajax({
					url: url,
					method: 'POST',
					data: data,
					statusCode: {
						500: function() {
							alertify.alert("Warning", "Data Duplicate");
						}
					}
				}).done(function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					if (data['status'] == "success") {
						alertify.success("Data Disimpan.");
						absenTable.ajax.reload();
						$('#absensiteknikModal').modal('hide');
					} else {
						alertify.alert("Data Gagal Disimpan.");
					}
				});
			}
		});

		function ViewData(id_absensi_armada) {
			$('.nav-tabs a[href="#tab_2"]').tab('show');
			var url = '<?= base_url() ?>absensi_teknik/ax_get_data_absensi_armada_teknik_by_id';
			var data = {
				id_absensi_armada: id_absensi_armada
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				// $('#nm_shift').val(data['nm_shift']);
				$('#id_bu').val(data['id_bu']);
				$('#select2-id_bu-container').html(data['nm_bu']);
				combo_armada(data['id_bu'], data['kd_armada'], data['kd_armada']);
				$('#id_segment').val(data['id_segment']);
				$('#select2-id_segment-container').html(data['nm_segment']);
				$('#tgl_absensi').val(data['tgl_absensi']);
				$('#odometer_awal').val(data['odometer_awal']);
				$('#odometer_akhir').val(data['odometer_akhir']);
				$('#status').val(data['status']);
				$('#km_tempuh').val(data['km_tempuh']);
			});
		}

		$('#tab2').on('click', function() {
			absenTable.ajax.reload();
			absenTable.columns.adjust().draw();
		})

		function tab2() {
			$('.nav-tabs a[href="#tab_2"]').tab('show');
			tbljadwalnondefault.columns.adjust().draw();
		}

		function tab3() {
			$('.nav-tabs a[href="#tab_3"]').tab('show');
			tbljadwalnondefault.columns.adjust().draw();
		}

		function RecallData() {
			alertify.confirm(
				'Konfirmasi',
				'Proses Recall KM akan menarik data KM Service terkahir armada dari Simadamri Teknik, Lanjutkan?',
				function() {
					var url = '<?= base_url() ?>absensi_teknik/ax_recall_data_km_service_simateknik';
					var data = {
						id_bu: $("#id_cabang_filter").val(),
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						absenTable.ajax.reload();
						alertify.success('Data berhasil di proses.');
					});
				},
				function() {}
			);
		}

		function closeTab() {
			$('.nav-tabs a[href="#tab_1"]').tab('show');
			tbljadwalnondefault.columns.adjust().draw();
		}

		function print_pdf() {
			var url = "<?= site_url("reports/prints") ?>";
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var format = 0;

			if (tanggal != '' && id_cabang != '') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				var REQ = "?bulan=" + bulan + "&tahun=" + tahun + "&tanggal=" + tanggal + "&id_cabang=" + id_cabang + "&format=html" + "&uk=F4-P" + "&name=absensi_armada_teknik_new";
				open(url + REQ);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function print_pdf_km() {
			var url = "<?= site_url("reports/prints") ?>";
			var id_cabang = $("#id_cabang_filter").val();
			var tanggal = $("#tanggal").val();
			var format = 0;

			if (tanggal != '' && id_cabang != '') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				var REQ = "?bulan=" + bulan + "&tahun=" + tahun + "&tanggal=" + tanggal + "&id_cabang=" + id_cabang + "&format=html" + "&uk=F4-P" + "&name=absensi_armada_teknik_km_new";
				open(url + REQ);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function print_excell() {
			tanggal = $("#tanggal").val();
			var id_cabang = $("#id_cabang_filter").val();
			var format = 2;
			if (tanggal != '' && id_cabang != '0') {
				var bulan = tanggal.substr(5, 2);
				var tahun = tanggal.substr(0, 4);
				window.open("<?= site_url() ?>absensi_teknik/laporan_absen_pengemudi/" + id_cabang + "/" + bulan + "/" + tahun + "/" + format);
			} else {
				alertify.alert("Warning", "Silahkan pilih cabang dan isi tanggal terlebih dahulu");
			}
		}

		function odoauto() {
			var kd_armada = $("#kd_armada").val();
			var tgl_absensi = $("#tgl_absensi").val();
			var url = '<?= base_url() ?>absensi_teknik/ax_get_odometer_auto';
			var data = {
				kd_armada: kd_armada,
				tgl_absensi: tgl_absensi
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				$('#odometer_awal').val(data['odometer_akhir']);
				$('#odometer_akhir').val('0.00');
				$('#km_tempuh').val('0.00');
				$('#status').val('0');
			});

			inputSegment();
		}

		function odoakhir() {
			var odometer = $("#odometer_awal").val();
			if ($("#status").val() == '5') {
				$('#odometer_akhir').val(0.00);
				$('#km_tempuh').val(0.00);
			} else {
				$('#km_tempuh').val(0);
				$('#odometer_akhir').val(odometer);
			}
			if ($('#status').val() == '6') {
				$('#odometer_awal').val(0.00);
				$('#odometer_akhir').val(0.00);
				$('#km_tempuh').val(0.00);
			}
		}

		function kmtempuh() {
			var odometer_awal = $("#odometer_awal").val();
			var odometer_akhir = $("#odometer_akhir").val();
			var kmtempuh = odometer_akhir - odometer_awal;
			$('#km_tempuh').val(kmtempuh);
		}

		function inputSegment() {
			var url = '<?= base_url() ?>absensi_teknik/ax_get_segment_auto';
			var data = {
				kd_armada: $("#kd_armada").val(),
			};

			$.ajax({
				url: url,
				method: 'POST',
				data: data
			}).done(function(data, textStatus, jqXHR) {
				var data = JSON.parse(data);
				$('#id_segment').val(data['id_segment']);
				$('#select2-id_segment-container').html(data['nm_segment']);
			});
		}

		$(document).ready(function() {
			$("#tanggal, #tgl_absensi").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd"
			});
			$("#tanggal_to").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd",
				yearRange: "-100:+20",
				minDate: '0'
			});

			$("#tanggal, #tgl_absensi").inputmask("yyyy-mm-dd", {
				"placeholder": "yyyy-mm-dd"
			});
			$("#tanggal_to").inputmask("yyyy-mm-dd", {
				"placeholder": "yyyy-mm-dd"
			});
			$("#tanggal, #tgl_absensi").on("change", function(e) {
				absenTable.ajax.reload();
				var id_bu = $("#id_bu").val();

				//show hide delete all button
				var date1 = new Date($('#tanggal').val());
				var date2 = new Date(Date.now());
				selisih = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
				if (selisih >= 1) {
					$('#btnDeleteAll').hide();
				} else {
					$('#btnDeleteAll').show();
				}
				//End show hide delete all button

			});

			$(".justnumber").keydown(function(e) {
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

					(e.keyCode == 65 && e.ctrlKey === true) ||

					(e.keyCode == 67 && e.ctrlKey === true) ||

					(e.keyCode == 88 && e.ctrlKey === true) ||

					(e.keyCode >= 35 && e.keyCode <= 39)) {

					return;
				}

				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			});

			$("#id_bu").change(function() {
				$("#kd_armada").hide();
				var id_bu = $("#id_bu").val();
				combo_armada(id_bu, 0, '-- Armada --');
			});
		});

		function combo_armada(id_bu, kd_armada, kd_armada) {
			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>absensi_teknik/ax_get_armada",
				data: {
					id_cabang: id_bu
				},
				dataType: "json",
				beforeSend: function(e) {
					if (e && e.overrideMimeType) {
						e.overrideMimeType("application/json;charset=UTF-8");
					}
				},
				success: function(response) {

					$("#kd_armada").html(response.data_armada).show();
					$('#select2-kd_armada-container').html(kd_armada);
					$('#kd_armada').val(kd_armada);

				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError);
				}
			});
		}

		$('#id_cabang_filter').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		$('#id_cabang_filter_2').select2({
			'allowClear': true
		}).on("change", function(e) {
			absen2Table.ajax.reload();
		});

		$('#id_cabang_filter_3').select2({
			'allowClear': true
		}).on("change", function(e) {
			absen3Table.ajax.reload();
		});

		$('#id_segment').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		$('#id_segment_filter').select2({
			'allowClear': true
		}).on("change", function(e) {
			absenTable.ajax.reload();
		});

		function DeleteData(id_absensi_armada) {
			alertify.confirm(
				'Confirmation',
				'Are you sure you want to delete this data?',
				function() {
					var url = '<?= base_url() ?>absensi_teknik/ax_unset_data_new';
					var data = {
						id_absensi_armada: id_absensi_armada
					};

					$.ajax({
						url: url,
						method: 'POST',
						data: data
					}).done(function(data, textStatus, jqXHR) {
						var data = JSON.parse(data);
						absenTable.ajax.reload();
						alertify.error('Data deleted.');
					});
				},
				function() {}
			);
		}

		function deleteAllAbsent() {
			if ($('#id_cabang_filter').val() == 0) {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == '') {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var id_bu = $('#id_cabang_filter').val();
				var id_segment = $('#id_segment_filter').val();
				var tanggal = $('#tanggal').val();
				alertify.confirm(
					'Konfirmasi',
					'Apa anda yakin akan menghapus semua data pada tanggal ' + tanggal + ' ?',
					function() {
						var url = '<?= base_url() ?>absensi_teknik/ax_unset_data_all_absent';
						var data = {
							id_bu: id_bu,
							id_segment: id_segment,
							tanggal: tanggal
						};
						$.ajax({
							url: url,
							method: 'POST',
							data: data
						}).done(function(data, textStatus, jqXHR) {

							var data = JSON.parse(data);
							if (data['status'] == "1") {
								alertify.success("Data Berhasil Terhapus.");
								absenTable.ajax.reload();
							} else {
								alertify.error("Data Gagal Terhapus.");
								absenTable.ajax.reload();
							}
						});
					},
					function() {}
				);
			}
		}

		function CopyData() {
			if ($('#id_cabang').val() == 0) {
				alertify.alert("Perhatian", "Cabang Tidak Boleh Kosong");
			} else if ($('#tanggal').val() == '') {
				alertify.alert("Perhatian", "Tanggal Tidak Boleh Kosong");
			} else {
				var tanggal = $('#tanggal').val();
				$('#tanggal_from').val(tanggal);
				$('#copyDataModal').modal('show');
			}
		}

		$('#btnCopy').on('click', function() {
			if ($('#tanggal_to').val() == '') {
				alertify.alert("Perhatian", "Ke Tanggal Tidak Boleh Kosong");
				return;
			} else {
				var id_cabang = $('#id_cabang_filter').val();
				var id_segment = $('#id_segment_filter').val();
				var tanggal = $('#tanggal').val();
				var tanggal_to = $('#tanggal_to').val();

				alertify.confirm(
					'Confirmation',
					'Copy Absensi Armada (Teknik) dari tanggal ' + tanggal + ' ke tanggal ' + tanggal_to,
					function() {
						var url = '<?= base_url() ?>absensi_teknik/ax_copy_absensi_teknik';
						var data = {
							id_cabang: id_cabang,
							id_segment: id_segment,
							tanggal_from: tanggal,
							tanggal_to: tanggal_to,
						};
						$.ajax({
							url: url,
							method: 'POST',
							data: data,
							statusCode: {
								500: function() {
									alertify.alert("Perhatian", "Data Tidak Berhasil di Setting");
								}
							}
						}).done(function(data, textStatus, jqXHR) {
							var data = JSON.parse(data);
							absenTable.ajax.reload();
							$('#copyDataModal').modal('hide');
							alertify.success('Absensi Armada (Teknik) Berhasil di Copy ke Tanggal ' + data.tanggal_to);
						});
					},
					function() {}
				);
			}
		});
	</script>
</body>

</html>